﻿
using log4net;

using Plus.Communication.Packets;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Moderation;
using Plus.HabboHotel.Support;
using Plus.HabboHotel.Catalog;
using Plus.HabboHotel.Items;
using Plus.HabboHotel.Items.Televisions;
using Plus.HabboHotel.Navigator;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Groups;
using Plus.HabboHotel.Quests;
using Plus.HabboHotel.Achievements;
using Plus.HabboHotel.LandingView;
using Plus.HabboHotel.Global;

using Plus.HabboHotel.Games;

using Plus.HabboHotel.Rooms.Chat;
using Plus.HabboHotel.Talents;
using Plus.HabboHotel.Bots;
using Plus.HabboHotel.Cache;
using Plus.HabboHotel.Rewards;
using Plus.HabboHotel.Badges;
using Plus.HabboHotel.Permissions;
using Plus.HabboHotel.Subscriptions;
using System.Threading;
using System.Threading.Tasks;
using Plus.HabboHotel.Settings;
using Plus.HabboHotel.Camera;
using Plus.Core;
using Plus.HabboHotel;
using Plus.HabboHotel.Users.Navigator.SavedSearches;
using Plus.HabboHotel.Groups.Forums;
using Plus.HabboHotel.ServerWeapons;

namespace Plus
{
    public static class HotelGameManager
    {
        public static ItemDataManager _itemDataManager;
        private static CatalogManager _catalogManager;
        private static TelevisionManager _televisionManager;
        private static NavigatorManager _navigatorManager;
        private static RoomManager _roomManager;
        private static GroupManager _groupManager;
        private static QuestManager _questManager;
        private static AchievementManager _achievementManager;
        private static TalentTrackManager _talentTrackManager;
        private static LandingViewManager _landingViewManager;//TODO: Rename class
        private static GameDataManager _gameDataManager;
        private static AntiMutant _antiMutant;
        private static BotManager _botManager;
        private static CacheManager _cacheManager;
        private static RewardManager _rewardManager;
        private static BadgeManager _badgeManager;
        private static PermissionManager _permissionManager;
        private static SubscriptionManager _subscriptionManager;
        private static GroupForumManager _forumManager;

        private static bool _cycleEnded;
        private static bool _cycleActive;
        private static Task _gameCycle;
        private static int _cycleSleepTime = 25;

        public static void Init()
        {
            PacketManager.Init();
            GameClientManager.Init();
            ModerationManager.Init();

            _itemDataManager = new ItemDataManager();
            _itemDataManager.Init();

            Manager.InitSettings();

            _catalogManager = new CatalogManager();
            _catalogManager.Init(_itemDataManager);

            _televisionManager = new TelevisionManager();

            _navigatorManager = new NavigatorManager();
            _roomManager = new RoomManager();
            ChatManager.Init();
            _groupManager = new GroupManager();
            _questManager = new QuestManager();
            _achievementManager = new AchievementManager();
            _talentTrackManager = new TalentTrackManager();
            _landingViewManager = new LandingViewManager();
            _gameDataManager = new GameDataManager();

            Manager.InitServer();
            _antiMutant = new AntiMutant();
            _botManager = new BotManager();
            _cacheManager = new CacheManager();
            _rewardManager = new RewardManager();

            _badgeManager = new BadgeManager();
            _badgeManager.Init();

            _permissionManager = new PermissionManager();
            _permissionManager.Init();
            _subscriptionManager = new SubscriptionManager();
            _subscriptionManager.Init();

            Manager.InitComponent();

            _forumManager = new GroupForumManager();

            ServerWeaponsManager.Init();
        }

        public static void StartGameLoop()
        {
            _gameCycle = new Task(GameCycle);
            _gameCycle.Start();

            _cycleActive = true;
        }

        private static void GameCycle()
        {
            while (_cycleActive)
            {
                _cycleEnded = false;

                GetRoomManager().OnCycle();
                GameClientManager.OnCycle();

                _cycleEnded = true;
                Thread.Sleep(_cycleSleepTime);
            }
        }

        public static void  StopGameLoop()
        {
            _cycleActive = false;

            while (!_cycleEnded)
            {
                Thread.Sleep(_cycleSleepTime);
            }
        }
        

        public static CatalogManager GetCatalog()
        {
            return _catalogManager;
        }

        public static NavigatorManager GetNavigator()
        {
            return _navigatorManager;
        }

        public static ItemDataManager GetItemManager()
        {
            return _itemDataManager;
        }

        public static RoomManager GetRoomManager()
        {
            return _roomManager;
        }

        public static AchievementManager GetAchievementManager()
        {
            return _achievementManager;
        }

        public static TalentTrackManager GetTalentTrackManager()
        {
            return _talentTrackManager;
        }
        public static PermissionManager GetPermissionManager()
        {
            return _permissionManager;
        }

        public static SubscriptionManager GetSubscriptionManager()
        {
            return _subscriptionManager;
        }

        public static QuestManager GetQuestManager()
        {
            return _questManager;
        }

        public static GroupManager GetGroupManager()
        {
            return _groupManager;
        }

        public static LandingViewManager GetLandingManager()
        {
            return _landingViewManager;
        }
        public static TelevisionManager GetTelevisionManager()
        {
            return _televisionManager;
        }

        public static GameDataManager GetGameDataManager()
        {
            return _gameDataManager;
        }

        public static AntiMutant GetAntiMutant()
        {
            return _antiMutant;
        }

        public static BotManager GetBotManager()
        {
            return _botManager;
        }

        public static CacheManager GetCacheManager()
        {
            return _cacheManager;
        }

        public static RewardManager GetRewardManager()
        {
            return _rewardManager;
        }

        public static BadgeManager GetBadgeManager()
        {
            return _badgeManager;
        }

        public static GroupForumManager GetGroupForumManager()
        {
            return _forumManager;
        }
    }
}