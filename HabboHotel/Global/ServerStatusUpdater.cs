﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Collections.Generic;

using log4net;
using Plus.Database.Interfaces;
using Plus.HabboHotel.GameClients;

namespace Plus.HabboHotel.Global
{
    public class ServerStatusUpdater : IDisposable
    {
        private static ILog log = LogManager.GetLogger("Mango.Global.ServerUpdater");

        private static int UPDATE_IN_SECS = 30;

        private static int _userPeak;
        

        private static Timer _timer;

        public static void Init()
        {
            _timer = new Timer(new TimerCallback(OnTick), null, TimeSpan.FromSeconds(UPDATE_IN_SECS), TimeSpan.FromSeconds(UPDATE_IN_SECS));

            Console.Title = "Plus Emulator - 0 online - 0 rooms loaded - 0 day(s) 0 hour(s) uptime";

            TextEmulator.WriteLine("Server Status Updater has been started.");
        }

        public static void OnTick(object Obj)
        {
            UpdateOnlineUsers();
        }

        private static void UpdateOnlineUsers()
        {
            TimeSpan Uptime = DateTime.Now - PlusEnvironment.ServerStarted;

            int UsersOnline = GameClientManager.Count;
            int RoomCount = HotelGameManager.GetRoomManager().Count;

            Console.Title = "Plus Emulator - " + UsersOnline + " users online - " + RoomCount + " rooms loaded - " + Uptime.Days + " day(s) " + Uptime.Hours + " hour(s) uptime";

            using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                if (UsersOnline > _userPeak)
                    _userPeak = UsersOnline;

                dbClient.SetQuery("UPDATE server_status SET users_online = @users, loaded_rooms = @loadedRooms, userpeak = @upeak LIMIT 1;");
                dbClient.AddParameter("users", UsersOnline);
                dbClient.AddParameter("loadedRooms", RoomCount);
                dbClient.AddParameter("upeak", _userPeak);
                dbClient.RunQuery();
            }
        }


        public void Dispose()
        {
            using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.RunQuery("UPDATE server_status SET users_online = '0', loaded_rooms = '0', status = '0'");
            }

            _timer.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}