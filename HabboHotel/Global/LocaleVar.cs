﻿using System.Collections.Generic;

namespace Plus
{
    public class LocaleVar
    {
        private string SetVar;
        private string OrigialVar;
        private bool Found;
        private List<string> Params;

        public List<string> Params1
        {
            get => Params; set => Params = value;
        }

        public LocaleVar(string str)
        {
            SetVar = str;
            OrigialVar = str;
            Found = true;
        }

        public LocaleVar(string str, bool found)
        {
            OrigialVar = str;
            SetVar = str;
            Found = found;
        }

        public LocaleVar Replace(string str, string newstr)
        {
            SetVar = SetVar.Replace(str, newstr);
            return this;
        }

        public LocaleVar Replace(params string[] Vars)
        {
            int num = Vars.Length / 2;
            int index = 0;
            while (index < num)
            {
                Replace(Vars[index], Vars[checked(index + 1)]);
                Params.Add(Vars[index]);
                Params.Add(Vars[checked(index + 1)]);
                checked { index += 2; }
            }
            return this;
        }

        public void AddParam(string val)
        {
            Params.Add(val);
        }

        public override string ToString()
        {
            return Found ? SetVar : OrigialVar + LanguageLocale.GetVarNames(Params.ToArray());
        }
    }
}
