﻿using Plus;
using log4net;
using Plus.Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;

namespace Plus
{
    public static class SettingsCamera
    {
        private static Dictionary<string, string> _values = new Dictionary<string, string>();

        public static void Init()
        {
            _values = new Dictionary<string, string>();
            if (_values.Count > 0)
                _values.Clear();
            using (IQueryAdapter queryReactor = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery("SELECT * FROM `settings_camera`");
                DataTable table = queryReactor.getTable();
                if (table != null)
                {
                    foreach (DataRow row in (InternalDataCollectionBase)table.Rows)
                        _values.Add(row["variable"].ToString(), row["value"].ToString());
                }
            }
        }

        public static string GetValue(string value)
        {
            return _values.ContainsKey(value) ? _values[value] : value;
        }

        public static string GetVarNames(object[] values)
        {
            if (values.Length == 0)
                return "";
            string str = "[ ";
            int num1 = values.Length / 2;
            int num2 = 1;
            while (num2 < values.Length)
            {
                str = str + values[checked(num2 - 1)] + (checked(values.Length - 1) > num2 ? ", " : "");
                checked { num2 += 2; }
            }
            return str + " ]";
        }

        public static string GetVarNames(string[] values)
        {
            object[] values1 = new object[values.Length];
            Array.Copy((Array)values, (Array)values1, values.Length);
            return GetVarNames(values1);
        }

        public static LocaleVar TryGetValue(string key, string defValue)
        {
            if (!_values.ContainsKey(key))
                return new LocaleVar(defValue);
            return new LocaleVar(_values[key]);
        }

        public static LocaleVar TryGetValue(string key)
        {
            if (!_values.ContainsKey(key))
                return new LocaleVar(key);
            return new LocaleVar(_values[key]);
        }

        public static string Value(string key, params object[] Values)
        {
            if (!_values.ContainsKey(key))
                return key + GetVarNames(Values);
            string str = _values[key];
            int num = Values.Length / 2;
            int index = 0;
            while (index < Values.Length)
            {
                str = str.Replace(Values[index].ToString(), Values[checked(index + 1)].ToString());
                checked { index += 2; }
            }
            return str;
        }

        public static string ValueInt(string key, int nums, params object[] Values)
        {
            if (!_values.ContainsKey(key))
                return key + GetVarNames(Values);
            string str = _values[key];
            int num = Values.Length / 2;
            int index = 0;
            while (index < Values.Length)
            {
                str = str.Replace(Values[index].ToString(), Values[checked(index + 1)].ToString());
                checked { index += 2; }
            }
            return str;
        }
    }
}
