﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;

using log4net;

using Plus.Core;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Groups;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Achievements;
using Plus.HabboHotel.Users.Badges;
using Plus.HabboHotel.Users.Inventory;
using Plus.HabboHotel.Users.Messenger;
using Plus.HabboHotel.Users.Relationships;
using Plus.HabboHotel.Users.UserDataManagement;

using Plus.HabboHotel.Users.Process;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;

using Plus.HabboHotel.Users.Navigator.SavedSearches;
using Plus.HabboHotel.Users.Effects;
using Plus.HabboHotel.Users.Messenger.FriendBar;
using Plus.HabboHotel.Users.Clothing;
using Plus.Communication.Packets.Outgoing.Navigator;
using Plus.Communication.Packets.Outgoing.Rooms.Engine;
using Plus.Communication.Packets.Outgoing.Rooms.Session;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Rooms.Chat.Commands;
using Plus.HabboHotel.Users.Permissions;
using Plus.HabboHotel.Subscriptions;
using Plus.HabboHotel.Users.Calendar;

namespace Plus.HabboHotel.Users
{
    public class HabboJob
    {
        public int Id;

        public int Rank;
        public string RankName;
        public string RankMotto;

        public string MaleLook;
        public string FemaleLook;

        public bool IsSupervisor;
        public bool IsManager;

        public int Payrate;

        public int RoomId;
        public bool CanRoam;

        public JobRights CommandType;

        public enum JobRights
        {
            Hospital,
            Police,
            Warehouse,
            Bank,
            Ammunation,
            DrugDealer,
            None
        }

        public HabboJob(int pJobId, int pJobRank, string pRankName, string pRankMotto, string pMaleLook, string pFemaleLook, bool pSupervisor, bool pManager, int pPayrate, bool pCanRoam, int pRoomId, string pCommandType)
        {
            this.Id = pJobId;
            this.Rank = pJobRank;
            this.RankName = pRankName;
            this.RankMotto = pRankMotto;
            this.MaleLook = pMaleLook;
            this.FemaleLook = pFemaleLook;
            this.IsSupervisor = pSupervisor;
            this.IsManager = pManager;
            this.Payrate = pPayrate;
            this.RoomId = pRoomId;
            this.CanRoam = pCanRoam;

            switch (pCommandType.ToLower())
            {
                case "police":
                    {
                        CommandType = JobRights.Police;
                        break;
                    }
                case "hospital":
                    {
                        CommandType = JobRights.Hospital;
                        break;
                    }
                case "warehouse":
                    {
                        CommandType = JobRights.Warehouse;
                        break;
                    }
                case "bank":
                    {
                        CommandType = JobRights.Bank;
                        break;
                    }
                case "ammunation":
                    {
                        CommandType = JobRights.Ammunation;
                        break;
                    }
                case "drugdealer":
                    {
                        CommandType = JobRights.DrugDealer;
                        break;
                    }
                default:
                    {
                        CommandType = JobRights.None;
                        break;
                    }                   
            }
        }
    }
}
