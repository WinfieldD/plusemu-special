﻿using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;

using log4net;

using Plus.Core;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Groups;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Achievements;
using Plus.HabboHotel.Users.Badges;
using Plus.HabboHotel.Users.Inventory;
using Plus.HabboHotel.Users.Messenger;
using Plus.HabboHotel.Users.Relationships;
using Plus.HabboHotel.Users.UserDataManagement;

using Plus.HabboHotel.Users.Process;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;

using Plus.HabboHotel.Users.Navigator.SavedSearches;
using Plus.HabboHotel.Users.Effects;
using Plus.HabboHotel.Users.Messenger.FriendBar;
using Plus.HabboHotel.Users.Clothing;
using Plus.Communication.Packets.Outgoing.Navigator;
using Plus.Communication.Packets.Outgoing.Rooms.Engine;
using Plus.Communication.Packets.Outgoing.Rooms.Session;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Rooms.Chat.Commands;
using Plus.HabboHotel.Users.Permissions;
using Plus.HabboHotel.Subscriptions;
using Plus.HabboHotel.Users.Calendar;

using Plus.HabboHotel.ServerWeapons;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;

using System.Timers;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Users
{
    public class Roleplay
    {
        private static readonly ILog log = LogManager.GetLogger("Plus.HabboHotel.Users.Roleplay");

        /// <summary>
        /// Stores the user id
        /// </summary>
        public int UserId;

        /// <summary>
        /// Stores hitpoints related data
        /// </summary>
        public int Hitpoints;
        public int MaxHitpoints;

        /// <summary>
        /// Stores the last x and y coordinates for position saving and loading
        /// </summary>
        public int LastX;
        public int LastY;

        /// <summary>
        /// Statistics
        /// </summary>
        public int StrengthLevel;
        public int StrengthExperience;

        /// <summary>
        /// Weapon storage and loading
        /// </summary>
        public Dictionary<int, HabboWeapon> OwnedWeapons;
        public HabboWeapon EquippedWeapon;

        /// <summary>
        /// Job related stuff
        /// </summary>
        public bool IsCurrentlyWorking;
        public HabboJob Company;
        public string LookBeforeWork;
        public int NextPayment;
        public string DefaultMotto;

        /// <summary>
        /// Hospital related data
        /// </summary>
        public bool TransportedToHospital;
        public bool IsCurrentlyDead;

        /// <summary>
        /// So we can teleport them to their stored xy coords
        /// </summary>
        public bool JustLoggedOn;

        /// <summary>
        /// Pay task and timer
        /// </summary>
        public Timer PayTimer;
        public Task PayTask;

        /// <summary>
        /// Stun task and timer
        /// </summary>
        public Timer StunTimer;
        public Task StunTask;

        /// <summary>
        /// Prison task and timer
        /// </summary>
        public Timer PrisonTimer;
        public Task PrisonTask;

        /// <summary>
        /// Taxi timer and task
        /// </summary>
        public Timer TaxiTimer;
        public Task TaxiTask;

        /// <summary>
        /// Catchin a taxi to id?
        /// </summary>
        public int TaxiTo;

        /// <summary>
        /// Is the player currently incarcerated?
        /// </summary>
        public bool IsIncarcerated;

        /// <summary>
        /// When was the player incarcerated?
        /// </summary>
        public DateTime WasIncarceratedAt;
        public int IncarceratedFor;

        /// <summary>
        /// Logged off in jail?
        /// </summary>
        public bool LoggedOffInPrison;

        /// <summary>
        /// Is the player handcuffed?
        /// </summary>
        public bool Handcuffed;

        /// <summary>
        /// Is the player wanted by the police?
        /// </summary>
        public bool Wanted;
        public string WantedReason;

        /// <summary>
        /// The DateTime when the user last ran a combat command (shoot, hit)
        /// </summary>
        public DateTime LastCombatCommand;

        /// <summary>
        /// Is the player AFK? Used to determine if they can be hit etc
        /// </summary>
        public bool Afk;

        /// <summary>
        /// Bank account storage
        /// </summary>
        public int BankAccount;

        /// <summary>
        /// Ammunation shit.
        /// </summary>
        public ServerWeapon WeaponOffer;
        public DateTime WeaponOfferAt;

        /// <summary>
        /// Drug selling shit
        /// </summary>
        public DrugType DrugOffer;
        public DateTime DrugOfferedAt;
        public bool HasDrugOffer;

        /// <summary>
        /// Has the user been sent the OnUserEnter message for properties?
        /// </summary>
        public bool NeedsPropertyWelcome;

        /// <summary>
        /// The timestamp where the player last won the slot machine jackpot.
        /// </summary>
        public int JackpotTimestamp;

        /// <summary>
        /// derp
        /// </summary>
        public DateTime LastGambledAt;

        /// <summary>
        /// Training timer shit
        /// </summary>
        public Task TrainingTask;
        public Timer TrainingTimer;

        /// <summary>
        /// other training shit
        /// </summary>
        public SkillType IsTrainingSkill;
        public bool IsTraining;
        public DateTime StartedTrainingAt;

        /// <summary>
        /// Cooling down for training level-up
        /// </summary>
        public int WorkoutTimestamp;

        /// <summary>
        /// Is the player VIP?
        /// </summary>
        public bool IsVip;

        /// <summary>
        /// Bounty related shit bro
        /// </summary>
        public bool HasBounty;
        public int BountyReward;

        /// <summary>
        /// Driving related shit
        /// </summary>
        public bool HasDriversLicense;
        public int DrivingLevel;
        public int DrivingExperience;
        public int DrivingFuel;

        /// <summary>
        /// Lets make sure all the users data is committed to db each minute.
        /// </summary>
        public Task SavingTask;
        public Timer SavingTimer;

        /// <summary>
        /// datetime when the player started working
        /// </summary>
        public DateTime StartedWorkingAt;

        /// <summary>
        /// Vault settings
        /// </summary>
        public DateTime StartedRobbingVaultAt;
        public Task VaultTask;
        public Timer VaultTimer;
        public bool IsRobbingVault;

        /// <summary>
        /// Defence shit.
        /// </summary>
        public int DefenceLevel;
        public int DefenceExperience;

        public Dictionary<DrugType, int> DrugInventory;

        public enum SkillType
        {
            Strength,
            Defence
        }

        public enum DrugType
        {
            Weed,
            Cocaine,
            Mdma,
            Heroin
        }

        /// <summary>
        /// Initializes the class
        /// </summary>
        /// <param name="pId">The users id</param>
        public Roleplay(int pId)
        {
            this.UserId = pId;
            LoadStatistics();
        }

        /// <summary>
        /// Loads the player from the DB.
        /// </summary>
        private void LoadStatistics()
        {
            if (this.UserId > 0)
            {
                int pHitpoints = 100;
                int pMaxHitpoints = 100;
                int pStrengthLevel = 1;
                int pStrengthExp = 0;
                int pLastX = 0;
                int pLastY = 0;
                int pJobId = 0;
                int pJobRank = 0;
                int pBank = 0;
                int pIncarcerated = 0;
                int pJackpot = 0;
                int pWorkout = 0;
                bool pVip = false;
                int pBountyAmount = 0;
                int pDrivingLevel = 0;
                int pDrivingExp = 0;
                int pDrivingFuel = 0;
                int pDefenceLevel = 0;
                int pDefenceExp = 0;

                HabboJob pCompany = null;

                using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                {
                    Adapter.SetQuery("SELECT * FROM user_rp_stats WHERE user_id = @userid");
                    Adapter.AddParameter("userid", this.UserId);

                    DataRow Row = Adapter.getRow();

                    if (Row != null)
                    {
                        pHitpoints = Convert.ToInt32(Row["hitpoints"]);
                        pLastX = Convert.ToInt32(Row["last_x"]);
                        pLastY = Convert.ToInt32(Row["last_y"]);
                        pStrengthLevel = Convert.ToInt32(Row["strength_level"]);
                        pStrengthExp = Convert.ToInt32(Row["strength_exp"]);
                        pJobId = Convert.ToInt32(Row["job_id"]);
                        pJobRank = Convert.ToInt32(Row["job_rank"]);
                        pBank = Convert.ToInt32(Row["bank_account"]);
                        pIncarcerated = Convert.ToInt32(Row["prison_time"]);
                        pJackpot = Convert.ToInt32(Row["jackpot_time"]);
                        pWorkout = Convert.ToInt32(Row["workout_time"]);
                        pVip = PlusEnvironment.EnumToBool(Row["is_vip"].ToString());
                        pBountyAmount = Convert.ToInt32(Row["bounty_amount"]);
                        pDrivingLevel = Convert.ToInt32(Row["driving_level"]);
                        pDrivingExp = Convert.ToInt32(Row["driving_exp"]);
                        pDrivingFuel = Convert.ToInt32(Row["driving_fuel"]);
                        pDefenceLevel = Convert.ToInt32(Row["defence_level"]);
                        pDefenceExp = Convert.ToInt32(Row["defence_exp"]);

                        if (pDefenceLevel >= 1)
                            pMaxHitpoints = pMaxHitpoints + (pDefenceLevel * 10);
                        else
                            pMaxHitpoints = 100;


                        if (pIncarcerated > 0)
                        {
                            RoomData TempData = HotelGameManager.GetRoomManager().GenerateRoomData(StaticSettings.PrisonRoomId);

                            if (TempData != null)
                            {
                                pLastX = TempData.Model.DoorX;
                                pLastY = TempData.Model.DoorY;
                            }

                            TempData = null;

                            SendToPrison(pIncarcerated, true);
                        }
                        else
                        {
                            this.WasIncarceratedAt = DateTime.Now;
                            this.IncarceratedFor = 0;
                        }

                        if (pJobId > 0 && pJobRank > 0)
                        {
                            Adapter.SetQuery("SELECT a.*,b.* FROM server_jobs AS a JOIN server_jobs_ranks AS b ON (a.id = b.job_id) WHERE b.job_id = @jobid AND b.rank_id = @rankid");
                            Adapter.AddParameter("jobid", pJobId);
                            Adapter.AddParameter("rankid", pJobRank);

                            DataRow JobRow = Adapter.getRow();

                            if (JobRow != null)
                            {
                                pCompany = LoadBusiness(JobRow);
                            }
                            else
                            {
                                Adapter.SetQuery("UPDATE user_rp_stats SET job_id = 0, job_rank = 0 WHERE user_id = @userid");
                                Adapter.AddParameter("userid", this.UserId);
                                Adapter.RunQuery();
                            }
                        }
                    }
                    else
                    {
                        Adapter.SetQuery("INSERT INTO user_rp_stats (user_id) VALUES (@userid)");
                        Adapter.AddParameter("userid", this.UserId);
                        Adapter.RunQuery();
                    }

                    this.OwnedWeapons = new Dictionary<int, HabboWeapon>();

                    Adapter.SetQuery("SELECT * FROM user_rp_weapons WHERE user_id = @uid");
                    Adapter.AddParameter("uid", this.UserId);

                    DataTable Weapons = Adapter.getTable();

                    if (Weapons != null)
                    {
                        foreach (DataRow WeaponRow in Weapons.Rows)
                        {
                            this.OwnedWeapons.Add(Convert.ToInt32(WeaponRow["weapon_id"]), LoadWeapon(WeaponRow));
                        }
                    }
                }

                this.LoadDrugInventory();

                this.Hitpoints = pHitpoints;
                this.MaxHitpoints = pMaxHitpoints;

                this.StrengthLevel = pStrengthLevel;
                this.StrengthExperience = pStrengthExp;

                this.IsCurrentlyWorking = false;

                this.TransportedToHospital = false;
                this.IsCurrentlyDead = false;

                this.EquippedWeapon = null;

                this.LastX = pLastX;
                this.LastY = pLastY;

                this.JustLoggedOn = true;

                this.Company = pCompany;
                this.LookBeforeWork = null;
                this.NextPayment = 0;

                this.BankAccount = pBank;

                this.DefaultMotto = "Citizen";

                this.Handcuffed = false;
                this.Wanted = false;
                this.WantedReason = String.Empty;

                this.IsIncarcerated = false;

                this.WeaponOffer = null;

                this.LastCombatCommand = DateTime.Now;
                this.WeaponOfferAt = DateTime.Now;

                this.TaxiTo = 0;
                this.NeedsPropertyWelcome = false;

                this.Afk = false;

                this.JackpotTimestamp = pJackpot;
                this.WorkoutTimestamp = pWorkout;

                this.LastGambledAt = DateTime.Now;

                this.IsTraining = false;

                this.IsVip = pVip;

                if (pBountyAmount > 0)
                    this.HasBounty = true;

                this.BountyReward = pBountyAmount;

                if (pDrivingLevel > 0)
                    this.HasDriversLicense = true;

                this.DrivingLevel = pDrivingLevel;
                this.DrivingExperience = pDrivingExp;
                this.DrivingFuel = pDrivingFuel;

                this.IsRobbingVault = false;

                this.DefenceExperience = pDefenceExp;
                this.DefenceLevel = pDefenceLevel;

                this.MaxHitpoints = pMaxHitpoints;

                this.HasDrugOffer = false;

                this.StartSaving();
                this.CheckStatistics();

                log.Info("User " + this.UserId.ToString() + " initialized.");
            }
        }

        public void SaveDrugInventory()
        {
            using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                Adapter.SetQuery("UPDATE user_rp_drugs SET weed = @weed, heroin = @heroin, mdma = @mdma, cocaine = @cocaine WHERE user_id = @userid");
                Adapter.AddParameter("weed", this.DrugCount(DrugType.Weed));
                Adapter.AddParameter("heroin", this.DrugCount(DrugType.Heroin));
                Adapter.AddParameter("mdma", this.DrugCount(DrugType.Mdma));
                Adapter.AddParameter("cocaine", this.DrugCount(DrugType.Cocaine));
                Adapter.AddParameter("userid", this.UserId);
                Adapter.RunQuery();
            }
        }

        public void LoadDrugInventory()
        {
            this.DrugInventory = new Dictionary<DrugType, int>();

            int pWeed = 0;
            int pCocaine = 0;
            int pHeroin = 0;
            int pMdma = 0;

            using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                Adapter.SetQuery("SELECT * FROM user_rp_drugs WHERE user_id = @userid");
                Adapter.AddParameter("userid", this.UserId);

                DataRow Row = Adapter.getRow();

                if (Row != null)
                {
                    pWeed = Convert.ToInt32(Row["weed"]);
                    pCocaine = Convert.ToInt32(Row["cocaine"]);
                    pHeroin = Convert.ToInt32(Row["heroin"]);
                    pMdma = Convert.ToInt32(Row["mdma"]);
                }
                else
                {
                    Adapter.SetQuery("INSERT INTO user_rp_drugs (user_id) VALUES (@userid)");
                    Adapter.AddParameter("userid", this.UserId);
                    Adapter.RunQuery();
                }
            }

            this.DrugInventory.Add(DrugType.Weed, pWeed);
            this.DrugInventory.Add(DrugType.Cocaine, pCocaine);
            this.DrugInventory.Add(DrugType.Heroin, pHeroin);
            this.DrugInventory.Add(DrugType.Mdma, pMdma);
        }

        public int DrugPrice(DrugType pType)
        {
            switch (pType)
            {
                case DrugType.Cocaine:
                    return 500;

                case DrugType.Heroin:
                    return 400;

                case DrugType.Mdma:
                    return 300;

                case DrugType.Weed:
                    return 150;
            }

            return 0;
        }

        public int DrugCount(DrugType pType)
        {
            return this.DrugInventory[pType];
        }

        public void RemoveDrug(DrugType pType, int Amount)
        {
            if (this.DrugInventory[pType] >= Amount)
                this.DrugInventory[pType] -= Amount;
        }

        public void AddDrug(DrugType pType, int Amount)
        {
            this.DrugInventory[pType] += Amount;
        }

        public void StartSaving()
        {
            this.SavingTimer = new Timer();
            this.SavingTimer.AutoReset = false;
            this.SavingTimer.Interval = 60000;
            this.SavingTimer.Elapsed += SavingTimer_Elapsed;

            this.SavingTask = new Task(this.SavingTimer.Start);
            this.SavingTask.Start();
        }

        void SavingTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (this.GetClient != null && this.GetClient.GetHabbo() != null)
            {
                this.SavePlayer(this.GetClient.GetHabbo(), true);
                this.StartSaving();
            }
            else
                this.GetClient.Disconnect();
        }

        public void CheckStatistics()
        {
            if (this.StrengthLevel > 15)
                this.StrengthLevel = 15;

            if (this.DrivingLevel > 15)
                this.DrivingLevel = 15;

            if (this.DefenceLevel > 15)
                this.DefenceLevel = 15;
        }

        public string GetVehicleName()
        {
            switch (this.DrivingLevel)
            {
                case 1:
                    {
                        return "shitty, rusty hatchback";
                    }

                case 2:
                    {
                        return "crappy old lowrider";
                    }

                case 3:
                    {
                        return "grandma car";
                    }

                case 4:
                    {
                        return "ex ice cream truck";
                    }

                case 5:
                    {
                        return "second-hand Honda";
                    }

                case 6:
                    {
                        return "two-toned Nissan";
                    }

                case 7:
                    {
                        return "soccer mom van";
                    }

                case 8:
                    {
                        return "basic sedan";
                    }

                case 9:
                    {
                        return "4WD jeep";
                    }

                case 10:
                    {
                        return "brand-new Prius";
                    }

                case 11:
                    {
                        return "Mercedes";
                    }

                case 12:
                    {
                        return "monster truck";
                    }

                case 13:
                    {
                        return "Ferrari";
                    }

                case 14:
                    {
                        return "Lamborghini";
                    }

                case 15:
                    {
                        return "hover car";
                    }
            }

            return "car";
        }

        public int AddRandomTime()
        {
            Random Rand = new Random();
            return Rand.Next(60000, 120000);
        }

        /// <summary>
        /// Loads the business data from the db and binds it to it's class
        /// </summary>
        /// <param name="JobRow"></param>
        /// <returns></returns>
        public HabboJob LoadBusiness(DataRow JobRow)
        {
            return new HabboJob(Convert.ToInt32(JobRow["job_id"]), Convert.ToInt32(JobRow["rank_id"]), JobRow["rank_name"].ToString(),
                                                    JobRow["rank_motto"].ToString(), JobRow["uniform_m"].ToString(), JobRow["uniform_f"].ToString(),
                                                    PlusEnvironment.EnumToBool(JobRow["supervisor"].ToString()), PlusEnvironment.EnumToBool(JobRow["manager"].ToString()),
                                                    Convert.ToInt32(JobRow["pay_rate"]), PlusEnvironment.EnumToBool(JobRow["can_roam"].ToString()),
                                                    Convert.ToInt32(JobRow["room_id"]), JobRow["commands"].ToString());
        }

        /// <summary>
        /// Loads the weapon data from the db and binds it to it's class
        /// </summary>
        /// <param name="WeaponRow"></param>
        /// <returns></returns>
        public HabboWeapon LoadWeapon(DataRow WeaponRow)
        {
            return new HabboWeapon(Convert.ToInt32(WeaponRow["weapon_id"]), Convert.ToInt32(WeaponRow["ammo_clip"]), Convert.ToInt32(WeaponRow["ammo_inv"]), Convert.ToInt32(WeaponRow["inventory_amount"]));
        }

        public bool TryGamble()
        {
            TimeSpan SinceLastSpin = DateTime.Now - this.LastGambledAt;

            if (SinceLastSpin.Seconds >= 2)
            {
                this.LastGambledAt = DateTime.Now;
                return true;
            }
            else
            {
                this.GetClient.SendWhisper("Cooling down! (" + SinceLastSpin.Seconds + "/2)");
                return false;
            }
        }

        public void HandleBounty(GameClient Killer)
        {
            if (!this.HasBounty || this.BountyReward < 1)
                return;

            this.HasBounty = false;
            this.BountyReward = 0;

            Killer.GetHabbo().Credits += this.BountyReward;
            Killer.GetHabbo().Roleplay.UpdateCreditsBalance();

            foreach (GameClient Client in GameClientManager.GetClients)
            {
                if (Client == null || Client.GetHabbo() == null || Client.GetHabbo().Roleplay == null || Client.GetHabbo().Roleplay.Afk)
                    continue;

                Client.SendWhisper("[BOUNTY ALERT] " + Killer.GetHabbo().Username + " has claimed the $" + this.BountyReward + " bounty on " + this.GetClient.GetHabbo().Username + "!");
            }
        }

        /// <summary>
        /// Saves the player in the database
        /// </summary>
        /// <param name="Player">Habbo instance to save to db</param>
        public void SavePlayer(Habbo Player, bool Autosave = false)
        {
            using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                int IncarcerationTime = 0;

                if (this.IsIncarcerated)
                {
                    TimeSpan IncarcerationTimeLeft = DateTime.Now - this.WasIncarceratedAt;
                    IncarcerationTime = IncarcerationTimeLeft.Minutes;
                }

                Adapter.SetQuery("UPDATE users SET home_room = @current_room WHERE id = @user_id");
                Adapter.AddParameter("current_room", (Player.CurrentRoomId > 0 ? Player.CurrentRoomId : 1));
                Adapter.AddParameter("user_id", Player.Id);
                Adapter.RunQuery();

                Adapter.SetQuery("UPDATE user_rp_stats SET hitpoints = @newhp, last_x = @lastx, last_y = @lasty, strength_level = @strlevel, strength_exp = @strexp, bank_account = @bank, prison_time = @jailtime, jackpot_time = @jptime, workout_time = @wktime, bounty_amount = @bounty, driving_level = @drivinglevel, driving_exp = @drivingexp, driving_fuel = @drivingfuel, defence_level = @deflevel, defence_exp = @defexp WHERE user_id = @userid");
                Adapter.AddParameter("newhp", this.Hitpoints);
                Adapter.AddParameter("lastx", this.LastX);
                Adapter.AddParameter("lasty", this.LastY);
                Adapter.AddParameter("strlevel", this.StrengthLevel);
                Adapter.AddParameter("strexp", this.StrengthExperience);
                Adapter.AddParameter("bank", this.BankAccount);
                Adapter.AddParameter("jailtime", IncarcerationTime);
                Adapter.AddParameter("jptime", this.JackpotTimestamp);
                Adapter.AddParameter("wktime", this.WorkoutTimestamp);
                Adapter.AddParameter("bounty", this.BountyReward);
                Adapter.AddParameter("drivinglevel", this.DrivingLevel);
                Adapter.AddParameter("drivingexp", this.DrivingExperience);
                Adapter.AddParameter("drivingfuel", this.DrivingFuel);
                Adapter.AddParameter("deflevel", this.DefenceLevel);
                Adapter.AddParameter("defexp", this.DefenceExperience);
                Adapter.AddParameter("userid", this.UserId);
                Adapter.RunQuery();
            }

            if (GameClientManager.HospitalStaffOnDuty.Contains(this.GetClient))
                GameClientManager.HospitalStaffOnDuty.Remove(this.GetClient);

            if (GameClientManager.AmmunationStaffOnDuty.Contains(this.GetClient))
                GameClientManager.AmmunationStaffOnDuty.Remove(this.GetClient);

            if (GameClientManager.PoliceStaffOnDuty.Contains(this.GetClient))
                GameClientManager.PoliceStaffOnDuty.Remove(this.GetClient);

            foreach (HabboWeapon Weapon in this.OwnedWeapons.Values)
            {
                if (this.EquippedWeapon != null)
                    this.SaveWeapon(this.EquippedWeapon);
                else
                    this.SaveWeapon(Weapon);
            }

            this.SaveCompanyStatus();
            this.SaveDrugInventory();

            if (!Autosave)
            {
                this.Dispose();
                log.Info("User " + this.UserId + " saved, objects disposed successfully.");
            }
        }

        public void Dispose()
        {
            if (this.PayTask != null)
                this.PayTask.Dispose();

            if (this.PayTimer != null)
                this.PayTimer.Dispose();

            if (this.PrisonTask != null)
                this.PrisonTask.Dispose();

            if (this.PrisonTimer != null)
                this.PrisonTimer.Dispose();

            if (this.TrainingTask != null)
                this.TrainingTask.Dispose();

            if (this.TrainingTimer != null)
                this.TrainingTimer.Dispose();

            if (this.VaultTask != null)
                this.VaultTask.Dispose();

            if (this.VaultTimer != null)
                this.VaultTimer.Dispose();

            if (this.TaxiTask != null)
                this.TaxiTask.Dispose();

            if (this.TaxiTimer != null)
                this.TaxiTimer.Dispose();

            if (this.SavingTask != null)
                this.SavingTask.Dispose();

            if (this.SavingTimer != null)
                this.SavingTimer.Dispose();

            if (this.StunTask != null)
                this.StunTask.Dispose();

            if (this.StunTimer != null)
                this.StunTimer.Dispose();

            this.PayTask = null;
            this.PayTimer = null;
            this.PrisonTask = null;
            this.PrisonTimer = null;
            this.TrainingTask = null;
            this.TrainingTimer = null;
            this.VaultTask = null;
            this.VaultTimer = null;
            this.TaxiTask = null;
            this.TaxiTimer = null;
            this.SavingTask = null;
            this.SavingTimer = null;
            this.StunTask = null;
            this.StunTimer = null;
        }

        /// <summary>
        /// Saves the players job status in the database
        /// </summary>
        public void SaveCompanyStatus()
        {
            using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                if (this.Company != null)
                {
                    Adapter.SetQuery("UPDATE user_rp_stats SET job_id = @jobid, job_rank = @rankid WHERE user_id = @userid");
                    Adapter.AddParameter("jobid", this.Company.Id);
                    Adapter.AddParameter("rankid", this.Company.Rank);
                    Adapter.AddParameter("userid", this.UserId);
                    Adapter.RunQuery();
                }
                else
                {
                    Adapter.SetQuery("UPDATE user_rp_stats SET job_id = 0, job_rank = 0 WHERE user_id = @userid");
                    Adapter.AddParameter("userid", this.UserId);
                    Adapter.RunQuery();
                }
            }
        }

        /// <summary>
        /// Saves the weapons data to the database
        /// </summary>
        /// <param name="Weapon">Weapon instance to save to db/update to db</param>
        public void SaveWeapon(HabboWeapon Weapon)
        {
            using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                Adapter.SetQuery("UPDATE user_rp_weapons SET ammo_clip = @ammo_clip, ammo_inv = @ammo_inv, inventory_amount = @inv_amount WHERE user_id = @user_id AND weapon_id = @wepid");
                Adapter.AddParameter("ammo_clip", Weapon.AmmoClip);
                Adapter.AddParameter("ammo_inv", Weapon.AmmoInventory);
                Adapter.AddParameter("inv_amount", Weapon.InventoryAmount);
                Adapter.AddParameter("user_id", this.UserId);
                Adapter.AddParameter("wepid", ServerWeaponsManager.TryGetWeapon(Weapon.WeaponName).Id);
                Adapter.RunQuery();
            }
        }

        public bool HasJobRights(HabboJob.JobRights RightsNeeded)
        {
            if (this.Company != null && this.Company.CommandType == RightsNeeded)
                return true;
            else
                return false;
        }

        public bool CanLevelUp(int Experience)
        {
            if (Experience >= 1000 || (Experience + 50) >= 1000)
                return true;

            return false;
        }

        public void StartVaultTimer()
        {
            this.ForceShout("*Starts robbing the Federal Treasury*");
            this.IsRobbingVault = true;
            this.StartedRobbingVaultAt = DateTime.Now;

            this.VaultTimer = new Timer();
            this.VaultTimer.Interval = 300000 + AddRandomTime();
            this.VaultTimer.AutoReset = false;
            this.VaultTimer.Elapsed += VaultTimer_Elapsed;

            this.VaultTask = new Task(this.VaultTimer.Start);
            this.VaultTimer.Start();
        }

        public void StopVaultTimer(bool Notify = true)
        {
            this.IsRobbingVault = false;

            if (Notify)
            {
                this.ForceShout("*Has stopped robbing the Federal Treasury*");
            }

            this.VaultTimer.Dispose();
            this.VaultTask.Dispose();
        }

        void VaultTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            int PrizeMoney = new Random().Next(250, 800);

            this.GetClient.GetHabbo().Credits += PrizeMoney;
            this.UpdateCreditsBalance();

            this.ForceShout("*Has stolen $" + PrizeMoney + " from the Federal Treasury*");
            this.IsRobbingVault = false;
        }

        public void StartTrainingTimer(SkillType Type)
        {
            switch (Type)
            {
                case SkillType.Strength:
                    {
                        this.ForceShout("*Prepares to workout, and begins doing Strength training*");
                        break;
                    }

                case SkillType.Defence:
                    {
                        this.ForceShout("*Prepares to workout, and begins doing Defence training*");
                        break;
                    }
            }

            this.IsTraining = true;
            this.IsTrainingSkill = Type;

            this.GetRoomUser.CanWalk = false;
            this.GetRoomUser.IsWalking = false;
            this.GetRoomUser.AllowOverride = false;

            this.StartedTrainingAt = DateTime.Now;

            TrainingTimer = new Timer();
            TrainingTimer.Interval = 300000 + AddRandomTime();
            TrainingTimer.AutoReset = false;
            TrainingTimer.Elapsed += TrainingTimer_Elapsed;

            TrainingTask = new Task(TrainingTimer.Start);
            TrainingTask.Start();
        }

        public void StopTrainingTimer(bool Notify = true)
        {
            this.IsTraining = false;

            TrainingTask.Dispose();
            TrainingTimer.Dispose();

            if (Notify)
            {
                this.ForceShout("*Has stopped working out*");
            }

            this.GetRoomUser.CanWalk = true;
        }

        void TrainingTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.IsTraining = false;
            bool NeedsCooldown = false;

            switch (this.IsTrainingSkill)
            {
                case SkillType.Strength:
                    {
                        if (this.CanLevelUp(this.StrengthExperience))
                        {
                            this.ForceShout("*Has finished training their Strength, levelling up in the process*");

                            this.StrengthLevel += 1;
                            this.StrengthExperience = 0;

                            NeedsCooldown = true;
                        }
                        else
                        {
                            this.ForceShout("*Has finished training their Strength*");
                            this.StrengthExperience += 50;
                        }

                        break;
                    }

                case SkillType.Defence:
                    {
                        if (this.CanLevelUp(this.DefenceExperience))
                        {
                            this.ForceShout("*Has finished training their Defence, levelling up in the process*");

                            this.DefenceLevel += 1;
                            this.DefenceExperience = 0;
                            this.MaxHitpoints = 100 + (this.DefenceLevel * 10);

                            NeedsCooldown = true;
                        }
                        else
                        {
                            this.ForceShout("*Has finished training their Defence*");
                            this.DefenceExperience += 50;
                        }

                        break;
                    }
            }

            if (NeedsCooldown)
            {
                if (this.IsVip)
                    this.WorkoutTimestamp = Convert.ToInt32(PlusEnvironment.GetUnixTimestamp() + 3600);
                else
                    this.WorkoutTimestamp = Convert.ToInt32(PlusEnvironment.GetUnixTimestamp() + 10800);
            }

            this.GetRoomUser.CanWalk = true;
        }

        public int RobPlayer(GameClient Player)
        {
            if (!Player.GetHabbo().Roleplay.MoneyTransferChecks())
            {
                return 0;
            }

            if (Player.GetHabbo().Credits > 2)
            {
                int Take = Convert.ToInt32(Math.Round(Convert.ToDouble(Player.GetHabbo().Credits / 2), 0, MidpointRounding.ToEven));

                if (Take > 1000)
                    Take = 1000;

                Player.GetHabbo().Credits -= Take;
                Player.GetHabbo().Roleplay.UpdateCreditsBalance();

                this.GetClient.GetHabbo().Credits += Take;
                this.GetClient.GetHabbo().Roleplay.UpdateCreditsBalance();

                return Take;
            }
            else
                return 0;
        }

        public bool CanReachItem(Room Room, int BaseId, int Range = 1)
        {
            RoomUser Me = GetClient.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(this.UserId);

            int MyX = Me.X;
            int MyY = Me.Y;

            foreach (Items.Item pItem in Room.GetRoomItemHandler().GetFloor)
            {
                if (pItem.GetBaseItem().Id != BaseId)
                    continue;

                int X = pItem.GetX;
                int Y = pItem.GetY;

                if (Range > 0)
                {
                    if (Gamemap.TileDistance(MyX, MyY, X, Y) <= Range || Gamemap.TilesTouching(MyX, MyY, X, Y))
                    {
                        return true;
                    }
                }
                else
                {
                    if (MyX == X && MyY == Y)
                        return true;
                }

            }

            return false;
        }

        /// <summary>
        /// Adds a weapon to players inventory.
        /// </summary>
        /// <param name="Weapon"></param>
        public void AddWeapon(ServerWeapon Weapon)
        {
            int WeaponId = Weapon.Id;
            int AmmoClip = Weapon.ClipSize;
            int AmmoInv = 0;
            int InvAmount = 1;

            if (!this.OwnedWeapons.ContainsKey(Weapon.Id))
            {
                using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                {
                    Adapter.SetQuery("INSERT INTO user_rp_weapons (user_id, weapon_id, ammo_clip, ammo_inv, inventory_amount) VALUES (@userid, @wepid, @clip, @amount, @invamount)");
                    Adapter.AddParameter("userid", this.UserId);
                    Adapter.AddParameter("wepid", WeaponId);
                    Adapter.AddParameter("clip", AmmoClip);
                    Adapter.AddParameter("amount", AmmoInv);
                    Adapter.AddParameter("invamount", InvAmount);
                    Adapter.RunQuery();
                }

                this.OwnedWeapons.Add(Weapon.Id, new HabboWeapon(WeaponId, AmmoClip, AmmoInv, InvAmount));
            }
            else
            {
                this.OwnedWeapons[Weapon.Id].InventoryAmount += 1;
            }

            this.SaveWeapon(this.OwnedWeapons[Weapon.Id]);
        }

        /// <summary>
        /// Checks if the player can reach the person they are performing an action against
        /// </summary>
        /// <param name="Me">RoomUser that requires checking</param>
        /// <param name="Victim">Victim that requires checking</param>
        /// <param name="MaxSquares">Maximum squares of range between 2 RoomUsers</param>
        /// <returns></returns>
        public bool CanReachUser(RoomUser Me, RoomUser Victim, int MaxSquares = 1)
        {
            if (Gamemap.TileDistance(Me.X, Me.Y, Victim.X, Victim.Y) <= MaxSquares || Gamemap.TilesTouching(Me.X, Me.Y, Victim.X, Victim.Y))
            {
                return true;
            }

            return false;
        }

        public void UpdateCreditsBalance()
        {
            this.GetClient.Send(new CreditBalanceComposer(this.GetClient.GetHabbo().Credits));
        }

        /// <summary>
        /// Forces the user to shout
        /// </summary>
        /// <param name="Message"></param>
        public void ForceShout(string Message)
        {
            //:))
            GameClient Player = GameClientManager.GetClientByUserID(this.UserId);
            GetClient.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(GetClient.GetHabbo().Username).OnChat(0, Message, true);
        }

        /// <summary>
        /// Process what happens when stun x is used on the roleplay players Habbo instance
        /// </summary>
        public void OnStun()
        {
            StunTimer = new Timer();
            StunTimer.Elapsed += StunTimer_Elapsed;
            StunTimer.AutoReset = false;
            StunTimer.Interval = 7000;

            GetRoomUser.CanWalk = false;
            GetRoomUser.SetStep = false;
            GetRoomUser.IsWalking = false;

            StunTask = new Task(StunTimer.Start);
            StunTask.Start();
        }

        /// <summary>
        /// Self explanatory, init
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void StunTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            StunTask.Dispose();
            StunTimer.Dispose();

            GetRoomUser.CanWalk = true;
        }

        public bool MoneyTransferChecks()
        {
            int DaysNeeded = 259200;
            double CreatedAt = this.GetClient.GetHabbo().AccountCreated;
            double CurrentTime = PlusEnvironment.GetUnixTimestamp();

            if ((CreatedAt + DaysNeeded) < CurrentTime)
                return true;

            return false;
        }

        public void CatchTaxi(int RoomId, bool Driving = false)
        {
            this.TaxiTo = RoomId;

            Rooms.RoomData pRoom = HotelGameManager.GetRoomManager().GenerateRoomData(RoomId);

            if (pRoom == null)
                return;

            if (!Driving)
            {
                this.ForceShout("*Calls for a Taxi to " + pRoom.Name + " (ID: " + pRoom.Id + ")*");

                TaxiTimer = new Timer();
                TaxiTimer.Elapsed += TaxiTimer_Elapsed;
                TaxiTimer.AutoReset = false;
                TaxiTimer.Interval = 4000;

                TaxiTask = new Task(TaxiTimer.Start);
                TaxiTask.Start();
            }
            else
            {
                if (this.IsCurrentlyWorking && this.HasJobRights(HabboJob.JobRights.Police))
                {
                    this.TaxiTo = 0;
                    this.ForceShout("*Gets in their Squad Car and drives to " + pRoom.Name + " (ID: " + pRoom.Id + ")*");
                    this.GetClient.GetHabbo().PrepareRoom(RoomId, "");
                }
                else
                {
                    this.TaxiTo = 0;
                    this.ForceShout("*Gets in their " + this.GetVehicleName() + ", and drives to " + pRoom.Name + " (ID: " + pRoom.Id + ")");
                    this.GetClient.GetHabbo().PrepareRoom(RoomId, "");

                    if (this.CanLevelUp(this.DrivingExperience) && this.DrivingLevel < 15)
                    {
                        this.DrivingExperience = 0;
                        this.DrivingLevel += 1;

                        this.GetClient.SendWhisper("You have used 5 of your fuel and levelled your driving to " + this.DrivingLevel);
                    }
                    else
                    {
                        if (this.DrivingLevel < 15)
                        {
                            if (this.DrivingLevel >= 10 && !this.IsVip)
                                this.GetClient.SendWhisper("You have used 5 fuel.");

                            this.DrivingExperience += 50;
                            this.GetClient.SendWhisper("You have used 5 of your fuel and gained 50 driving experience.");
                        }
                        else
                        {
                            this.GetClient.SendWhisper("You have used 5 fuel.");
                        }
                    }

                    this.DrivingFuel -= 5;
                }
            }
        }

        void TaxiTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            log.Info(GetClient.GetHabbo().Username + " has travelled to room ID " + this.TaxiTo);

            if (this.TaxiTo > 0)
            {
                this.GetClient.GetHabbo().Credits -= 5;
                this.UpdateCreditsBalance();

                this.GetClient.GetHabbo().PrepareRoom(this.TaxiTo, "");
                this.TaxiTo = 0;
            }

            TaxiTimer.Dispose();
            TaxiTask.Dispose();
        }

        /// <summary>
        /// Send the player to jail
        /// </summary>
        /// <param name="Time">Time in minutes</param>
        public void SendToPrison(int Time, bool JustLogged = false)
        {
            if (this.EquippedWeapon != null)
            {
                this.SaveWeapon(this.EquippedWeapon);
                this.EquippedWeapon = null;
            }

            if (this.IsCurrentlyWorking)
                this.StopPayTimer();

            if (this.IsTraining)
                this.StopTrainingTimer(false);

            this.IsIncarcerated = true;
            this.Handcuffed = false;

            this.Wanted = false;
            this.WantedReason = String.Empty;
            this.WasIncarceratedAt = DateTime.Now;

            this.IncarceratedFor = Time;

            PrisonTimer = new Timer();
            PrisonTimer.AutoReset = false;
            PrisonTimer.Elapsed += PrisonTimer_Elapsed;
            PrisonTimer.Interval = (Time * 60) * 1000;

            PrisonTask = new Task(PrisonTimer.Start);
            PrisonTask.Start();

            if (!JustLogged)
                this.GetClient.GetHabbo().PrepareRoom(StaticSettings.PrisonRoomId, "");
            else
                this.LoggedOffInPrison = true;
        }

        /// <summary>
        /// Self explanatory nerd zzzzzzzzzzzzzzzz
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void PrisonTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            PrisonTask.Dispose();
            PrisonTimer.Dispose();

            this.IsIncarcerated = false;
            this.GetClient.GetHabbo().PrepareRoom(StaticSettings.PrisonReleaseRoomId, "");
        }

        /// <summary>
        /// Starts a pay timer
        /// </summary>
        /// <param name="Pay"></param>
        public void StartPayTimer(int Pay)
        {
            if (PayTask != null)
            {
                PayTask.Dispose();
            }

            if (PayTimer != null)
            {
                PayTimer.Dispose();
            }

            NextPayment = Pay;

            this.StartedWorkingAt = DateTime.Now;

            PayTimer = new System.Timers.Timer();
            PayTimer.AutoReset = false;
            PayTimer.Elapsed += PayTimer_Elapsed;
            PayTimer.Interval = 600000 + AddRandomTime();

            PayTask = new System.Threading.Tasks.Task(PayTimer.Start);
            PayTask.Start();
        }

        /// <summary>
        /// Can the player use combat related commands? AKA have they passed the cooldown time.
        /// </summary>
        /// <returns>Boolean representing if they can use a combat command or not</returns>
        public bool CanUseCombat()
        {
            TimeSpan SinceLast = DateTime.Now - this.LastCombatCommand;

            if (SinceLast.Seconds >= 4)
            {
                this.LastCombatCommand = DateTime.Now;
                return true;
            }
            else
            {
                this.GetClient.SendWhisper("Cooling down! (" + SinceLast.Seconds + "/4)");
                return false;
            }
        }

        public void SendRoomNotification(string Title, string Message)
        {
            this.GetClient.Send(NotificationManager.NotificationCustom(Title, Message, "", ""));
        }

        /// <summary>
        /// Stops the pay timer prematurely
        /// </summary>
        /// <param name="Session"></param>
        public void StopPayTimer(bool Notify = false)
        {
            if (!GetClient.GetHabbo().Roleplay.IsCurrentlyWorking)
                return;

            GetClient.GetHabbo().Motto = GetClient.GetHabbo().Roleplay.DefaultMotto;

            if (this.Company != null && this.Company.CommandType == HabboJob.JobRights.Hospital)
                GameClientManager.HospitalStaffOnDuty.Remove(GetClient);
            else if (this.Company != null && this.Company.CommandType == HabboJob.JobRights.Ammunation)
                GameClientManager.AmmunationStaffOnDuty.Remove(GetClient);
            else if (this.Company != null && this.Company.CommandType == HabboJob.JobRights.Police)
                GameClientManager.PoliceStaffOnDuty.Remove(GetClient);

            RoomUser User = GetClient.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(GetClient.GetHabbo().Username);

            if (GetClient.GetHabbo().Roleplay.LookBeforeWork != null)
            {
                GetClient.GetHabbo().Look = GetClient.GetHabbo().Roleplay.LookBeforeWork;
                GetClient.GetHabbo().Roleplay.LookBeforeWork = null;
            }

            if (!Notify)
            {
                StringBuilder Message = new StringBuilder();

                if (GetClient.GetHabbo().Roleplay.Company != null)
                {
                    Message.Append("*Stops working as a " + GetClient.GetHabbo().Roleplay.Company.RankName + "*");
                }
                else
                {
                    Message.Append("*Stops recieving their unemployment check*");
                }

                GetClient.GetHabbo().Roleplay.ForceShout(Message.ToString());
            }

            GetClient.GetHabbo().Roleplay.IsCurrentlyWorking = false;
            GetClient.GetHabbo().Roleplay.PayTask.Dispose();
            GetClient.GetHabbo().Roleplay.PayTimer.Dispose();

            GetClient.Send(new UserChangeComposer(User, true));
            GetClient.GetHabbo().CurrentRoom.Send(new UserChangeComposer(User, false));
        }

        /// <summary>
        /// Self explanatory
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PayTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (GetClient == null)
            {
                PayTask.Dispose();
                PayTimer.Dispose();
                return;
            }

            GetClient.SendWhisper("You have just been paid $" + NextPayment + ". Your next paycheck will arrive in 10 minutes.");
            ForceShout("*Has been paid*");

            GetClient.GetHabbo().Credits += NextPayment;
            GetClient.Send(new CreditBalanceComposer(GetClient.GetHabbo().Credits));

            StartPayTimer(NextPayment);
        }

        /// <summary>
        /// Changes the look for the player and updates it roomwide
        /// </summary>
        /// <param name="Session"></param>
        /// <param name="Look"></param>
        public void ChangeClothingOnly(GameClients.GameClient Session, string Look)
        {
            string Face = null;
            string Hair = null;

            string[] SplitLook = Session.GetHabbo().Look.Split('.');

            foreach (string LookCode in SplitLook)
            {
                if (LookCode.StartsWith("hd"))
                {
                    Face = LookCode;
                }
                else if (LookCode.StartsWith("hr"))
                {
                    Hair = LookCode;
                }
            }

            RoomUser User = Session.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Username);
            Look += "." + Face + "." + Hair;

            Session.GetHabbo().Look = Look;

            Session.Send(new UserChangeComposer(User, true));
            Session.GetHabbo().CurrentRoom.Send(new UserChangeComposer(User, false));
        }

        /// <summary>
        /// Returns the gameclient instance for the roleplay user
        /// </summary>
        public GameClient GetClient
        {
            get { return GameClientManager.GetClientByUserID(this.UserId); }
        }

        /// <summary>
        /// Returns the roleplay users room user instance
        /// </summary>
        public RoomUser GetRoomUser
        {
            get { return GetClient.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(this.UserId); }
        }

        public ICollection<HabboWeapon> GetWeapons
        {
            get { return this.OwnedWeapons.Values; }
        }

    }
}
