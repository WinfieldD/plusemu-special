﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.Notifications;
using System.Text.RegularExpressions;
using Plus.Database.Interfaces;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;

namespace Plus.HabboHotel.Users
{
    public sealed class UserManager
    {
        private static List<UserNamed> _user;

        public static void Init()
        {
            _user = new List<UserNamed>();
            if (_user.Count > 0)
                _user.Clear();

            DataTable Data = null;
            using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `users`");
                Data = dbClient.getTable();

                if (Data != null)
                {
                    foreach (DataRow Row in Data.Rows)
                    {
                        _user.Add(new UserNamed(Convert.ToString(Row["username"])));
                    }
                }
            }
        }
        public static string CheckNotification(string Message, GameClient Session)
        {
            foreach (UserNamed Filter in _user.ToList())
            {
                if (Message.Contains(Filter.Username) && Filter.Username != Session.GetHabbo().Username)
                {
                    GameClient TargetClient = GameClientManager.GetClientByUsername(Filter.Username);

                    Message = Regex.Replace(Message, Filter.Username, Filter.Username, RegexOptions.IgnoreCase);
                    if (Session.GetHabbo().CurrentRoomId != TargetClient.GetHabbo().CurrentRoomId)
                    {
                        if (TargetClient.GetHabbo().Identifier == 0)
                            TargetClient.Send(NotificationManager.Bubble("notif", Session.GetHabbo().Username + " Ta identifier, clique ici pour le rejoindre !", "event:navigator/goto/" + Session.GetHabbo().CurrentRoom.RoomId + ""));
                    }
                    else
                    {
                        if (TargetClient.GetHabbo().Identifier == 0)
                            TargetClient.Send(NotificationManager.Bubble("notif", Session.GetHabbo().Username + " Ta identifier, dans l'appart où tu te situes !", ""));

                    }
                }
                else
                {
                    return Message;
                }
            }

            return Message;
        }

    }
}
