﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Collections.Generic;

using log4net;

using Plus.Communication.Packets.Outgoing.Inventory.Furni;
using Plus.Communication.Packets.Outgoing.Catalog;
using Plus.HabboHotel.Items;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Database.Interfaces;
using Plus.Communication.Packets.Outgoing.Groups;
using Plus.HabboHotel.Groups;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;

namespace Plus.HabboHotel.Users.Process
{
    sealed class ProcessComponent
    {
        private static readonly ILog log = LogManager.GetLogger("Plus.HabboHotel.Users.Process.ProcessComponent");
        private static Habbo _player = null;
        private static Timer _timer = null;    
        private static bool _timerRunning = false;
        private static bool _timerLagging = false;
        private static bool _disabled = false;
        private static AutoResetEvent _resetEvent = new AutoResetEvent(true);
        private static int _runtimeInSec = 60;
        public static bool Init(Habbo Player)
        {
            if (Player == null)
                return false;
            else if (_player != null)
                return false;

            _player = Player;
            _timer = new Timer(new TimerCallback(Run), null, _runtimeInSec * 1000, _runtimeInSec * 1000);
            return true;
        }

        public static void Run(object State)
        {
            try
            {
                if (_disabled)
                    return;

                if (_timerRunning)
                {
                    _timerLagging = true;
                    TextEmulator.Error("<Player " + _player.Id + "> Server can't keep up, Player timer is lagging behind.", "", ConsoleColor.Yellow);
                    return;
                }

                _resetEvent.Reset();

                // BEGIN CODE

                #region Muted Checks
                if (_player.TimeMuted > 0)
                    _player.TimeMuted -= 60;
                #endregion

                #region Console Checks
                if (_player.MessengerSpamTime > 0)
                    _player.MessengerSpamTime -= 60;
                if (_player.MessengerSpamTime <= 0)
                    _player.MessengerSpamCount = 0;
                #endregion

                _player.TimeAFK += 1;

                #region Respect checking
                if (_player.GetStats().RespectsTimestamp != DateTime.Today.ToString("MM/dd"))
                {
                    _player.GetStats().RespectsTimestamp = DateTime.Today.ToString("MM/dd");
                    using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                    {
                        dbClient.RunQuery("UPDATE `user_stats` SET `dailyRespectPoints` = '" + (_player.Rank == 1 && _player.VIPRank == 0 ? 10 : _player.VIPRank == 1 ? 15 : 20) + "', `dailyPetRespectPoints` = '" + (_player.Rank == 1 && _player.VIPRank == 0 ? 5 : _player.VIPRank == 1 ? 10 : 15) + "', `respectsTimestamp` = '" + DateTime.Today.ToString("MM/dd") + "' WHERE `id` = '" + _player.Id + "' LIMIT 1");
                    }

                    _player.GetStats().DailyRespectPoints = (_player.Rank == 1 && _player.VIPRank == 0 ? 10 : _player.VIPRank == 1 ? 10 : 15);
                    _player.GetStats().DailyPetRespectPoints = (_player.Rank == 1 && _player.VIPRank == 0 ? 10 : _player.VIPRank == 1 ? 10 : 15);

                    if (_player.GetClient() != null)
                    {
                        _player.GetClient().Send(new UserObjectComposer(_player));
                    }
                }
                #endregion

                #region Reset Scripting Warnings
                if (_player.GiftPurchasingWarnings < 15)
                    _player.GiftPurchasingWarnings = 0;

                if (_player.MottoUpdateWarnings < 15)
                    _player.MottoUpdateWarnings = 0;

                if (_player.ClothingUpdateWarnings < 15)
                    _player.ClothingUpdateWarnings = 0;
                #endregion


                if (_player.GetClient() != null)
                    HotelGameManager.GetAchievementManager().ProgressAchievement(_player.GetClient(), "ACH_AllTimeHotelPresence", 1);

                _player.CheckCreditsTimer();
                _player.Effects().CheckEffectExpiry(_player);

                using (IQueryAdapter adap = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                {
                    adap.SetQuery("UPDATE users SET credits = @c, activity_points = @d, vip_points = @v WHERE id = @id");
                    adap.AddParameter("c", _player.Credits);
                    adap.AddParameter("d", _player.Duckets);
                    adap.AddParameter("v", _player.Diamonds);
                    adap.AddParameter("id", _player.Id);
                    adap.RunQuery();
                }
                _player.GetClient().Send(new UnreadForumThreadPostsComposer(HotelGameManager.GetGroupForumManager().GetUnreadThreadForumsByUserId(_player.Id)));
                if (Convert.ToBoolean(HotelGameManager.GetGroupForumManager().GetUnreadThreadForumsByUserId(_player.Id)))
                {
                    foreach (GameClients.GameClient c in GameClients.GameClientManager.GetClients)
                    {
                        c.Send(NotificationManager.Custom("test"));
                    }
                }
                _timerRunning = false;
                _timerLagging = false;

                _resetEvent.Set();
            }
            catch { }
        }

        public static void Dispose()
        {
            // Wait until any processing is complete first.
            try
            {
                _resetEvent.WaitOne(TimeSpan.FromMinutes(5));
            }
            catch { } // give up

            // Set the timer to disabled
            _disabled = true;

            // Dispose the timer to disable it.
            try
            {
                if (_timer != null)
                    _timer.Dispose();
            }
            catch { }

            // Remove reference to the timer.
            _timer = null;

            // Null the player so we don't reference it here anymore
            _player = null;
            _timerLagging = Convert.ToBoolean(null);
        }
    }
}