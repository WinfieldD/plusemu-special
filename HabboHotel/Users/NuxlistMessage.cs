﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Users
{
    public enum NuxListMessage
    {
        NAVIGATOR,
        INVENTORY,
        CAMERA,
        CATALOGUE,
        HIDE,
        VIP,
        OPEN,
        PREDEFINED,
        PREDEFINEDLOBBY
    }
}
