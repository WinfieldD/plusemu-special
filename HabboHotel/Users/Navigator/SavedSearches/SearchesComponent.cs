﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections.Concurrent;
using Plus.Database.Interfaces;


namespace Plus.HabboHotel.Users.Navigator.SavedSearches
{
    public static class SearchesComponent
    {
        private static ConcurrentDictionary<int, SavedSearch> _savedSearches;

        public static void Init(Habbo Player)
        {
            _savedSearches = new ConcurrentDictionary<int, SavedSearch>();

            if (_savedSearches.Count > 0)
                _savedSearches.Clear();

            DataTable GetSearches = null;
            using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `id`,`filter`,`search_code` FROM `user_saved_searches` WHERE `user_id` = @UserId");
                dbClient.AddParameter("UserId", Player.Id);
                GetSearches = dbClient.getTable();

                if (GetSearches != null)
                {
                    foreach (DataRow Row in GetSearches.Rows)
                    {
                        _savedSearches.TryAdd(Convert.ToInt32(Row["id"]), new SavedSearch(false, Convert.ToInt32(Row["id"]), Convert.ToString(Row["filter"]), Convert.ToString(Row["search_code"])));
                    }
                }
            }
            return;
        }

        public static ICollection<SavedSearch> GetSearchLists()
        {
            return _savedSearches.Values; 
        }

        public static bool TryAdd(int Id, SavedSearch Search)
        {
            return _savedSearches.TryAdd(Id, Search);
        }

        public static bool TryRemove(int Id, out SavedSearch Removed)
        {
            return _savedSearches.TryRemove(Id, out Removed);
        }
    }
}
