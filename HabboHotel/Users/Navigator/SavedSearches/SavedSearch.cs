﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Plus.HabboHotel.Users.Navigator.SavedSearches
{
    public class SavedSearch
    {
        private bool _remove;
        private int _id;
        private string _filter;
        private string _search;

        public SavedSearch(bool Remove, int Id, string Filter, string Search)
        {
            this._remove = Remove;
            this._id = Id;
            this._filter = Filter;
            this._search = Search;
        }

        public bool Remove
        {
            get { return this._remove; }
            set { this._remove = value; }
        }
        public int Id
        {
            get { return this._id; }
            set { this._id = value; }
        }

        public string Filter
        {
            get { return this._filter; }
            set { this._filter = value; }
        }

        public string Search
        {
            get { return this._search; }
            set { this._search = value; }
        }
    }
}
