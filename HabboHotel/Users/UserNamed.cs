﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Users
{
    sealed class UserNamed
    {
        private string _username;

        public UserNamed(string Username)
        {
            this._username = Username;
        }

        public string Username
        {
            get { return this._username; }
        }
    }
}
