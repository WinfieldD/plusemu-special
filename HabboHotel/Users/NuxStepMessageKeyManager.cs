﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Communication.Packets.Outgoing.Rooms.Nux;
using Plus.HabboHotel.GameClients;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Subscriptions;

namespace Plus.HabboHotel.Users
{
    public static class NuxStepMessageKeyManager
    {
        public static string GetNuxStepKey(NuxListMessage key)
        {
            switch (key)
            {
                default:
                case NuxListMessage.CAMERA:
                    return "helpBubble/add/CAMERA_BUTTON/" + LanguageLocale.Value("new.user.camera");

                case NuxListMessage.INVENTORY:
                    return "helpBubble/add/BOTTOM_BAR_INVENTORY/" + LanguageLocale.Value("new.user.inventory");

                case NuxListMessage.NAVIGATOR:
                    return "helpBubble/add/BOTTOM_BAR_NAVIGATOR/" + LanguageLocale.Value("new.user.navigator");

                case NuxListMessage.CATALOGUE:
                    return "helpBubble/add/BOTTOM_BAR_CATALOGUE/" + LanguageLocale.Value("new.user.catalogue");

                case NuxListMessage.VIP:
                    return "helpBubble/add/HC_JOIN_BUTTON/" + LanguageLocale.Value("new.user.hc.button");

                case NuxListMessage.HIDE:
                    return "nux/lobbyoffer/hide";

                case NuxListMessage.OPEN:
                    return "nux/lobbyoffer/show";

                case NuxListMessage.PREDEFINED:
                    return "predefined_noob_lobby";

                case NuxListMessage.PREDEFINEDLOBBY:
                    return LanguageLocale.Value("room.nux.id");
            }
        }

        public static void GetNuxListRun(GameClient Session, int GetClicked)
        {
            switch (GetClicked)
            {
                case 1:
                    Session.Send(new SpecialNotificationComposer(GetNuxStepKey(NuxListMessage.CAMERA)));
                    break;
                case 2:
                    Session.Send(new SpecialNotificationComposer(GetNuxStepKey(NuxListMessage.INVENTORY)));
                    break;
                case 3:
                    Session.Send(new SpecialNotificationComposer(GetNuxStepKey(NuxListMessage.NAVIGATOR)));
                    Session.Send(new NuxUserStatusComposer(2));
                    Session.Send(new NuxItemListComposer());
                    Session.Send(new SpecialNotificationComposer(GetNuxStepKey(NuxListMessage.HIDE)));
                    Session.Send(new SpecialNotificationComposer(GetNuxStepKey(NuxListMessage.OPEN)));
                    break;
            }

            using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("UPDATE users SET nux_user = @nux WHERE id = @id;");
                dbClient.AddParameter("nux", "false");
                dbClient.AddParameter("id", Session.GetHabbo().Id);
                dbClient.RunQuery();
            }
        }
    }
}
