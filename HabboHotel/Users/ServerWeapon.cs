﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;

using log4net;

using Plus.Core;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Groups;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Achievements;
using Plus.HabboHotel.Users.Badges;
using Plus.HabboHotel.Users.Inventory;
using Plus.HabboHotel.Users.Messenger;
using Plus.HabboHotel.Users.Relationships;
using Plus.HabboHotel.Users.UserDataManagement;

using Plus.HabboHotel.Users.Process;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;

using Plus.HabboHotel.Users.Navigator.SavedSearches;
using Plus.HabboHotel.Users.Effects;
using Plus.HabboHotel.Users.Messenger.FriendBar;
using Plus.HabboHotel.Users.Clothing;
using Plus.Communication.Packets.Outgoing.Navigator;
using Plus.Communication.Packets.Outgoing.Rooms.Engine;
using Plus.Communication.Packets.Outgoing.Rooms.Session;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Rooms.Chat.Commands;
using Plus.HabboHotel.Users.Permissions;
using Plus.HabboHotel.Subscriptions;
using Plus.HabboHotel.Users.Calendar;

using Plus.HabboHotel.ServerWeapons;

namespace Plus.HabboHotel.Users
{
    public class HabboWeapon
    {
        private static ILog log = LogManager.GetLogger("Plus.HabboHotel.Users");

        public int WeaponId;
        public string WeaponName;

        public int MinDamage;
        public int MaxDamage;

        public bool IsRanged;
        public bool IsExplosive;

        public int RangeToHit;
        public int RadiusToHit;

        public int InventoryAmount;

        public int ClipSize;
        public int ReloadTime;

        public int AmmoClip;
        public int AmmoInventory;

        public HabboWeapon(int WeaponId, int pAmmoClip, int pAmmoInv, int pInvAmount)
        {
            ServerWeapon pWeapon = ServerWeaponsManager.TryGetWeapon(WeaponId);

            if (pWeapon == null)
            {
                log.Error("Weapon ID " + WeaponId + " failed to load.");
                return;
            }

            WeaponId = pWeapon.Id;
            WeaponName = pWeapon.Name;
            MinDamage = pWeapon.MinDamage;
            MaxDamage = pWeapon.MaxDamage;
            ClipSize = pWeapon.ClipSize;
            ReloadTime = pWeapon.ReloadTime;
            IsRanged = pWeapon.IsRanged;
            RangeToHit = pWeapon.HitRange;
            IsExplosive = pWeapon.IsExplosive;
            RadiusToHit = pWeapon.HitRadius;
            AmmoClip = pAmmoClip;
            AmmoInventory = pAmmoInv;
            InventoryAmount = pInvAmount;
        }
    }
}
