﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Plus.Database.Interfaces;

namespace Plus.HabboHotel.ServerWeapons
{
    public class ServerWeapon
    {
        public int Id;
        public string Name;
        public int MinDamage;
        public int MaxDamage;
        public int ClipSize;
        public int ReloadTime;
        public bool IsRanged;
        public int HitRange;
        public bool IsExplosive;
        public int HitRadius;
        public int CostCredits;
        public int ClipCostCredits;

        public ServerWeapon(int pId, string pName, int pMinDam, int pMaxDam, int pClipSize, int pReloadTime, bool pIsRanged, int pHitRange, bool pIsExplosive, int pHitRadius, int pCost, int pClipCost)
        {
            Id = pId;
            Name = pName;
            MinDamage = pMinDam;
            MaxDamage = pMaxDam;
            ClipSize = pClipSize;
            ReloadTime = pReloadTime;
            IsRanged = pIsRanged;
            HitRange = pHitRange;
            IsExplosive = pIsExplosive;
            HitRadius = pHitRadius;
            CostCredits = pCost;
            ClipCostCredits = pClipCost;
        }
    }
}
