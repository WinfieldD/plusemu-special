﻿using log4net;
using Plus.Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Plus.HabboHotel.ServerWeapons
{
    public static class ServerWeaponsManager
    {
        private static ILog log = LogManager.GetLogger("Plus.HabboHotel.ServerWeapons");

        public static Dictionary<int, ServerWeapon> Weapons;

        public static void Init()
        {
            Weapons = new Dictionary<int, ServerWeapon>();

            Weapons.Clear();

            using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                Adapter.SetQuery("SELECT * FROM server_weapons");

                DataTable WeaponTable = Adapter.getTable();

                if (WeaponTable != null)
                {
                    foreach (DataRow WeaponRow in WeaponTable.Rows)
                    {
                        Weapons.Add(Convert.ToInt32(WeaponRow["id"]), new ServerWeapon(Convert.ToInt32(WeaponRow["id"]), WeaponRow["weapon_name"].ToString(), Convert.ToInt32(WeaponRow["min_damage"]),
                                                             Convert.ToInt32(WeaponRow["max_damage"]), Convert.ToInt32(WeaponRow["clip_size"]), Convert.ToInt32(WeaponRow["reload_time"]), PlusEnvironment.EnumToBool(WeaponRow["ranged"].ToString()),
                                                             Convert.ToInt32(WeaponRow["shoot_range"]), PlusEnvironment.EnumToBool(WeaponRow["explosive"].ToString()), Convert.ToInt32(WeaponRow["radius"]), Convert.ToInt32(WeaponRow["cost_credits"]), Convert.ToInt32(WeaponRow["clip_cost"])));
                    }
                }
            }

            TextEmulator.Roleplay("Loaded " + Weapons.Count + " weapons.");
        }

        public static ServerWeapon TryGetWeapon(int WeaponId)
        {
            return (Weapons[WeaponId] != null ? Weapons[WeaponId] : null);
        }

        public static ServerWeapon TryGetWeapon(string WeaponName)
        {
            ServerWeapon Weapon = Weapons.First(x => x.Value.Name.ToLower() == WeaponName.ToLower()).Value;
            return Weapon != null ? Weapon : null;
        }
    }
}
