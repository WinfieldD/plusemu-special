﻿namespace Plus.HabboHotel.Settings
{
    public class NuxListItem
    {
        public int Id;
        public string Image;
        public string Title;

        public NuxListItem(int id, string image, string title)
        {
            this.Id = id;
            this.Image = image;
            this.Title = title;
        }
    }
}