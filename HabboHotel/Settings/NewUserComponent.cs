﻿using Plus.Communication.Packets.Outgoing.Notifications;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Communication.Packets.Outgoing.Rooms.Nux;
using Plus.Database.Interfaces;
using Plus.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Settings
{
    class NewUserComponent
    {
        static public void RunUserNewSystem(GameClient Session)
        {
            /*
                public static const FRIENDS_BAR_ALL_FRIENDS:String = "icon_all_friends";
                public static const FRIENDS_BAR_FIND_FRIENDS:String = "icon_find_friends";
                public static const BOTTOM_BAR_BUILDERS_CLUB:String = HabboToolbarIconEnum.BUILDER;//"HTIE_ICON_BUILDER"
                public static const BOTTOM_BAR_HOME:String = HabboToolbarIconEnum.HOME;//"HTIE_ICON_HOME"
                public static const BOTTOM_BAR_RECEPTION:String = HabboToolbarIconEnum.RECEPTION;//"HTIE_ICON_RECEPTION"
                public static const BOTTOM_BAR_NAVIGATOR:String = HabboToolbarIconEnum.NAVIGATOR;//"HTIE_ICON_NAVIGATOR"
                public static const BOTTOM_BAR_CATALOGUE:String = HabboToolbarIconEnum.CATALOGUE;//"HTIE_ICON_CATALOGUE"
                public static const BOTTOM_BAR_INVENTORY:String = HabboToolbarIconEnum.INVENTORY;//"HTIE_ICON_INVENTORY"
                public static const BOTTOM_BAR_STORIES:String = HabboToolbarIconEnum.STORIES;//"HTIE_ICON_STORIES"
                public static const BOTTOM_BAR_MEMENU:String = HabboToolbarIconEnum.MEMENU;//"HTIE_ICON_MEMENU"
                public static const BOTTOM_BAR_QUESTS:String = HabboToolbarIconEnum.QUESTS;//"HTIE_ICON_QUESTS"
                public static const MEMENU_ACHIEVEMENTS:String = "achievements";
                public static const MEMENU_CLOTHES:String = "clothes";
                public static const MEMENU_FORUMS:String = "forums";
                public static const MEMENU_TALENTS:String = "talents";
                public static const MEMENU_GUIDE:String = "guide";
                public static const MEMENU_MAIL:String = "mail";
                public static const MEMENU_PROFILE:String = "profile";
                public static const MEMENU_ROOMS:String = "rooms";
                public static const CHAT_INPUT:String = "chat_input";
                public static const HC_JOIN_BUTTON:String = "hc_join_button";
                public static const HELP_BUTTON:String = "help_button";
                public static const SETTINGS_BUTTON:String = "settings_button";
                public static const CREDITS_BUTTON:String = "credit_count_button";
                public static const DUCKETS_BUTTON:String = "ducket_count_button";
                public static const DIAMONDS_BUTTON:String = "diamond_count_button";
                public static const LOGOUT_BUTTON:String = "logout_button";
                public static const ROOM_HISTORY_BACK_BUTTON:String = "button_history_back";
                public static const ROOM_HISTORY_FORWARD_BUTTON:String = "button_history_back";
                public static const ROOM_HISTORY_BUTTON:String = "button_history";
                public static const CHAT_HISTORY_BUTTON:String = "button_chat_history";
                public static const LIKE_ROOM_BUTTON:String = "button_like";
                public static const CAMERA_BUTTON:String = "button_camera";
        */
        
            var habbo = Session.GetHabbo();

            if (habbo == null)
                return;

            if (habbo.CountClick == 0)
            {
                habbo.CountClick++;
                Session.Send(new SpecialNotificationComposer("helpBubble/add/BOTTOM_BAR_INVENTORY/" + LanguageLocale.Value("new.user.inventory")));
            }
            else if (habbo.CountClick == 1)
            {
                habbo.CountClick++;
                Session.Send(new SpecialNotificationComposer("helpBubble/add/MEMENU_PROFILE/" + LanguageLocale.Value("new.user.profile")));
            }
            else if (habbo.CountClick == 2)
            {
                habbo.CountClick++;
                Session.Send(new SpecialNotificationComposer("helpBubble/add/BOTTOM_BAR_NAVIGATOR/" + LanguageLocale.Value("new.user.navigator")));
            }
            else if (habbo.CountClick == 3)
            {
                habbo.CountClick++;
                Session.Send(new SpecialNotificationComposer("helpBubble/add/BOTTOM_BAR_MEMENU/" + LanguageLocale.Value("new.user.memenu")));
            }
            else if(habbo.CountClick == 4)
            {
                //CAMERA_BUTTON
                habbo.CountClick++;
                Session.Send(new SpecialNotificationComposer("helpBubble/add/CAMERA_BUTTON/" + LanguageLocale.Value("new.user.camera")));
            }
            else if (habbo.CountClick == 5)
            {
                habbo.CountClick++;
                Session.Send(new SpecialNotificationComposer("helpBubble/add/BOTTOM_BAR_CATALOGUE/" + LanguageLocale.Value("new.user.catalogue")));
                Session.Send(new NuxUserStatusComposer(2));
                Session.Send(new NuxItemListComposer());
                Session.Send(new SpecialNotificationComposer("nux/lobbyoffer/hide"));
                using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                {
                    dbClient.RunQuery("UPDATE users SET nux_user = 'false' WHERE id = " + habbo.Id + ";");
                }

                habbo.NewUser = false;
            }

        }
    }
}
