﻿namespace Plus.HabboHotel.Settings
{
    public class UserPerks
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public string Required { get; set; }
        public bool Enable { get; set; }

        public UserPerks(int id, string content, string required, bool enable)
        {
            this.Id = id;
            this.Content = content;
            this.Required = required;
            this.Enable = enable;
        }
    }
}