﻿namespace Plus.HabboHotel.Settings
{
    public class NavigatorSettings
    {
        public int Id { get; set; }
        public int Top { get; set; }
        public int Bottom { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public bool Unk { get; set; }

        public NavigatorSettings(int id, int top, int bottom, int width, int height, bool unk)
        {
            this.Id = id;
            this.Top = top;
            this.Bottom = bottom;
            this.Width = width;
            this.Height = height;
            this.Unk = unk;
        }
    }
}

