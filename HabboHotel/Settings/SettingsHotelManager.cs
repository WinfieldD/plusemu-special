﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using log4net;
using Plus.HabboHotel.Items;
using Plus.HabboHotel.Catalog.Pets;
using Plus.HabboHotel.Catalog.Vouchers;
using Plus.HabboHotel.Catalog.Marketplace;
using Plus.HabboHotel.Catalog.Clothing;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Settings;
using Plus.HabboHotel.Catalog;
using Plus.Communication.Packets.Outgoing;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Availability;
using Plus.Core;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Communication.Packets.Outgoing.Messenger;

namespace Plus.HabboHotel.Settings
{
    public static class SettingsHotelManager
    {
        private static Dictionary<int, NavigatorSettings> _navigator;
        private static Dictionary<int, CatalogPromotion> _promotions;
        private static Dictionary<int, UserPerks> _perks;
        private static Dictionary<int, NuxListItem> _gifts;

        public static int Maintenance;
        public static int Durations;
        public static void Init()
        {
            _navigator = new Dictionary<int, NavigatorSettings>();
            _promotions = new Dictionary<int, CatalogPromotion>();
            _perks = new Dictionary<int, UserPerks>();
            _gifts = new Dictionary<int, NuxListItem>();

            if (_navigator.Count > 0)
                _navigator.Clear();

            if (_promotions.Count > 0)
                _promotions.Clear();

            if (_perks.Count > 0)
                _perks.Clear();

            if (_gifts.Count > 0)
                _perks.Clear();

            using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {

                dbClient.SetQuery("SELECT * FROM `navigator_settings`");
                DataTable Navigator = dbClient.getTable();

                if (Navigator != null)
                {
                    foreach (DataRow Row in Navigator.Rows)
                    {
                        if (!_navigator.ContainsKey(Convert.ToInt32(Row["id"])))
                            _navigator.Add(Convert.ToInt32(Row["id"]), new NavigatorSettings(Convert.ToInt32(Row["id"]), Convert.ToInt32(Row["top"]), Convert.ToInt32(Row["bottom"]), Convert.ToInt32(Row["width"]), Convert.ToInt32(Row["height"]), Convert.ToBoolean(Row["unknown"])));
                    }
                }

                if (Navigator == null)
                {
                    TextEmulator.Error("Une erreur et survenue.. #1");
                }

                dbClient.SetQuery("SELECT * FROM `catalog_promotions`");
                DataTable GetPromotions = dbClient.getTable();

                if (GetPromotions != null)
                {
                    foreach (DataRow Row in GetPromotions.Rows)
                    {
                        if (!_promotions.ContainsKey(Convert.ToInt32(Row["id"])))
                            _promotions.Add(Convert.ToInt32(Row["id"]), new CatalogPromotion(Convert.ToInt32(Row["id"]), Convert.ToString(Row["title"]), Convert.ToString(Row["image"]), Convert.ToInt32(Row["unknown"]), Convert.ToString(Row["page_link"]), Convert.ToInt32(Row["parent_id"])));
                    }
                }

                if (GetPromotions == null)
                {
                    TextEmulator.Error("Une erreur et survenue.. #1");
                }

                dbClient.SetQuery("SELECT * FROM `user_perks`");
                DataTable Perks = dbClient.getTable();

                if (Perks != null)
                {
                    foreach (DataRow Row in Perks.Rows)
                    {
                        if (!_perks.ContainsKey(Convert.ToInt32(Row["id"])))
                            _perks.Add(Convert.ToInt32(Row["id"]), new UserPerks(Convert.ToInt32(Row["id"]), Convert.ToString(Row["content"]), Convert.ToString(Row["required"]), Convert.ToBoolean(Row["enable"])));
                    }
                }
                if (Perks == null)
                {
                    TextEmulator.Error("Une erreur et survenue.. #1");
                }

                dbClient.SetQuery("SELECT * FROM `nux_gifts`");
                DataTable Gifts = dbClient.getTable();

                if (Gifts != null)
                {
                    foreach (DataRow Row in Gifts.Rows)
                    {
                        if (!_gifts.ContainsKey(Convert.ToInt32(Row["id"])))
                            _gifts.Add(Convert.ToInt32(Row["id"]), new NuxListItem(Convert.ToInt32(Row["id"]), Convert.ToString(Row["image"]), Convert.ToString(Row["title"])));
                    }
                }

                if (Gifts == null)
                {
                    TextEmulator.Error("Une erreur et survenue.. #1");
                }
            }
        }

        public static void MaintenanceCount(string[] Params, GameClient Session)
        {
            if (Params.Length == 2 && Params[1].ToString() == ConfigurationData.data["key.maintenance"])
            {
                Maintenance = int.Parse(Params[2]);
                Durations = int.Parse(Params[3]);

                Session.Send(new MaintenanceStatusComposer(Maintenance, Durations));
                PlusEnvironment.PerformShutDown();
            }
            else
            {
                Session.Send(NotificationManager.Bubble("", "mehemeh", ""));
            }
        }

        public static ICollection<NavigatorSettings> GetSettingsNavigator()
        {
            return _navigator.Values;
        }

        public static ICollection<CatalogPromotion> GetSettingsCatalogPromotions()
        {
            return _promotions.Values;
        }

        public static ICollection<UserPerks> GetUserPerksComponent()
        {
            return _perks.Values;
        }

        public static ICollection<NuxListItem> GetListItemNux()
        {
            return _gifts.Values;
        }
    }
}