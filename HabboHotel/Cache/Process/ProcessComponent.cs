﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Collections.Generic;

using log4net;
using Plus.HabboHotel.Cache;
using Plus.HabboHotel.Users;
using Plus.Core;

namespace Plus.HabboHotel.Cache.Process
{
    sealed class ProcessComponents
    {
        private static Timer _timer = null;
        private static bool _timerRunning = false;
        private static bool _timerLagging = false;
        private static bool _disabled = false;
        private static AutoResetEvent _resetEvent = new AutoResetEvent(true);

        private static int _runtimeInSec = 1200;
        public static void  Init()
        {
            _timer = new Timer(new TimerCallback(Run), null, _runtimeInSec * 1000, _runtimeInSec * 1000);
        }

        /// <summary>
        /// Called for each time the timer ticks.
        /// </summary>
        /// <param name="State"></param>
        public static void  Run(object State)
        {
            try
            {
                if (_disabled)
                    return;

                if (_timerRunning)
                {
                    _timerLagging = true;
                    return;
                }

                _resetEvent.Reset();

                // BEGIN CODE
                List<UserCache> CacheList = HotelGameManager.GetCacheManager().GetUserCache().ToList();
                if (CacheList.Count > 0)
                {
                    foreach (UserCache Cache in CacheList)
                    {
                        try
                        {
                            if (Cache == null)
                                continue;

                            UserCache Temp = null;

                            if (Cache.isExpired())
                                HotelGameManager.GetCacheManager().TryRemoveUser(Cache.Id, out Temp);

                            Temp = null;
                        }
                        catch (Exception e)
                        {
                            Logging.LogCacheException(e.ToString());
                        }
                    }
                }

                CacheList = null;

                List<Habbo> CachedUsers = PlusEnvironment.GetUsersCached().ToList();
                if (CachedUsers.Count > 0)
                {
                    foreach (Habbo Data in CachedUsers)
                    {
                        try
                        {
                            if (Data == null)
                                continue;

                            Habbo Temp = null;

                            if (Data.CacheExpired())
                                PlusEnvironment.RemoveFromCache(Data.Id, out Temp);

                            if (Temp != null)
                                Temp.Dispose();

                            Temp = null;
                        }
                        catch (Exception e)
                        {
                            Logging.LogCacheException(e.ToString());
                        }
                    }
                }

                CachedUsers = null;
                // END CODE

                // Reset the values
                _timerRunning = false;
                _timerLagging = false;

                _resetEvent.Set();
            }
            catch (Exception e) { Logging.LogCacheException(e.ToString()); }
        }

        /// <summary>
        /// Stops the timer and disposes everything.
        /// </summary>
        public static void  Dispose()
        {
            // Wait until any processing is complete first.
            try
            {
                _resetEvent.WaitOne(TimeSpan.FromMinutes(5));
            }
            catch { } // give up

            // Set the timer to disabled
            _disabled = true;

            // Dispose the timer to disable it.
            try
            {
                if (_timer != null)
                    _timer.Dispose();
            }
            catch { }

            // Remove reference to the timer.
            _timer = null;
            _timerLagging = Convert.ToBoolean(null);
        }
    }
}