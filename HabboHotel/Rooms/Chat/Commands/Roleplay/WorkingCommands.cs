﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Utilities;
using Plus.HabboHotel.Users;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using Plus.Communication.Packets.Outgoing.Notifications;
using Plus.Communication.Packets.Outgoing.Rooms.Engine;

using Plus.Database.Interfaces;
using System.Data;
using Plus.HabboHotel.GameClients;

namespace Plus.HabboHotel.Rooms.Chat.Commands
{
    class StartWorkCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_startwork"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Starts working or recieving welfare."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Roleplay.IsCurrentlyWorking)
                return;

            if (Session.GetHabbo().Roleplay.Company != null)
            {
                if (Session.GetHabbo().Roleplay.Company.RoomId == Session.GetHabbo().CurrentRoomId)
                {
                    Session.GetHabbo().Roleplay.StartPayTimer(Session.GetHabbo().Roleplay.Company.Payrate);

                    Session.GetHabbo().Roleplay.IsCurrentlyWorking = true;
                    Session.GetHabbo().Roleplay.ForceShout("*Starts working as a " + Session.GetHabbo().Roleplay.Company.RankName + "*");
                    Session.GetHabbo().Roleplay.LookBeforeWork = Session.GetHabbo().Look;
                    Session.GetHabbo().Motto = "[WORKING] " + Session.GetHabbo().Roleplay.Company.RankMotto;

                    if (!Session.GetHabbo().Roleplay.Company.IsManager)
                        Session.GetHabbo().Roleplay.ChangeClothingOnly(Session, (Session.GetHabbo().Gender.ToLower() == "m" ? Session.GetHabbo().Roleplay.Company.MaleLook : Session.GetHabbo().Roleplay.Company.FemaleLook));             

                    Session.SendWhisper("Your current payrate is $" + Session.GetHabbo().Roleplay.Company.Payrate + " and you will recieve it in 10 minutes.");

                    if (Session.GetHabbo().Roleplay.Company.CommandType == HabboJob.JobRights.Hospital)
                        GameClientManager.HospitalStaffOnDuty.Add(Session);
                    else if(Session.GetHabbo().Roleplay.Company.CommandType == HabboJob.JobRights.Ammunation)
                        GameClientManager.AmmunationStaffOnDuty.Add(Session);
                    else if (Session.GetHabbo().Roleplay.Company.CommandType == HabboJob.JobRights.Police)
                        GameClientManager.PoliceStaffOnDuty.Add(Session);
                  
                }
                else
                {
                    Session.SendWhisper("You aren't in your Job Headquarters (ID: " + Session.GetHabbo().Roleplay.Company.RoomId + ")");
                }
            }
            else
            {
                RoomUser User = Session.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Username);

                Session.GetHabbo().Roleplay.StartPayTimer(15);

                Session.GetHabbo().Roleplay.IsCurrentlyWorking = true;
                Session.GetHabbo().Motto = "[WORKING] Unemployed";

                Session.GetHabbo().Roleplay.ForceShout("*Starts recieving their unemployment check*");
                Session.SendWhisper("Your current payrate is $15 and you will recieve it in 10 minutes.");

                Session.Send(new UserChangeComposer(User, true));
                Session.GetHabbo().CurrentRoom.Send(new UserChangeComposer(User, false));
            }
        }
    }

    class StopWorkCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_stopwork"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Stops working or recieving welfare."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (!Session.GetHabbo().Roleplay.IsCurrentlyWorking)
                return;

            Session.GetHabbo().Roleplay.StopPayTimer();
        }
    }

    class QuitJobCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_quitjob"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Quits your job if you have one."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Roleplay.Company != null)
            {
                Session.GetHabbo().Roleplay.StopPayTimer();
                Session.GetHabbo().Roleplay.ForceShout("*Has quit their job as a " + Session.GetHabbo().Roleplay.Company.RankName + "*");

                Session.GetHabbo().Roleplay.Company = null;
                Session.GetHabbo().Roleplay.SaveCompanyStatus();
            }
        }
    }

    class HireCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_hire"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Hires a player into a corporation."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Rank >= 8 || (Session.GetHabbo().Roleplay.Company != null && Session.GetHabbo().Roleplay.Company.IsManager))
            {
                string Username = (Params.Length >= 1 ? Params[1] : String.Empty);
                int JobId = (Session.GetHabbo().Rank >= 8 ? Convert.ToInt32(Params[2]) : Session.GetHabbo().Roleplay.Company.Id);
                int RankId = (Session.GetHabbo().Rank >= 8 ? Convert.ToInt32(Params[3]) : 1);

                if (String.IsNullOrEmpty(Username))
                    return;

                if (JobId < 1 || RankId < 1)
                    return;

                GameClients.GameClient Player = GameClientManager.GetClientByUsername(Username);

                if (Player == null)
                    return;

                if (Player.GetHabbo().CurrentRoomId != Session.GetHabbo().CurrentRoomId)
                    return;

                if (Player.GetHabbo().Roleplay.Company != null)
                {
                    Session.SendWhisper(Player.GetHabbo().Username + " has a job already!");
                    return;
                }

                using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                {
                    Adapter.SetQuery("SELECT a.*,b.* FROM server_jobs AS a JOIN server_jobs_ranks AS b ON (a.id = b.job_id) WHERE b.job_id = @jobid AND b.rank_id = @rankid");
                    Adapter.AddParameter("jobid", JobId);
                    Adapter.AddParameter("rankid", RankId);

                    DataRow Row = Adapter.getRow();

                    if (Row != null)
                    {
                        Player.GetHabbo().Roleplay.Company = Player.GetHabbo().Roleplay.LoadBusiness(Row);
                        Player.GetHabbo().Roleplay.SaveCompanyStatus();
                    }

                    Session.GetHabbo().Roleplay.ForceShout("*Has hired " + Player.GetHabbo().Username + " as a " + Row["rank_name"].ToString() + "*");
                }
            }
        }
    }

    class SendHomeCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_sendhome"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Sends a player home from their job."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Rank >= 8 || (Session.GetHabbo().Roleplay.Company != null && Session.GetHabbo().Roleplay.Company.IsSupervisor) || (Session.GetHabbo().Roleplay.Company != null && Session.GetHabbo().Roleplay.Company.IsManager))
            {
                string Username = (Params.Length >= 1 ? Params[1] : String.Empty);

                if (String.IsNullOrEmpty(Username))
                    return;

                GameClients.GameClient Player = GameClientManager.GetClientByUsername(Username);

                if (Player == null)
                    return;

                if (Player.GetHabbo().CurrentRoomId != Session.GetHabbo().CurrentRoomId)
                    return;

                if (Player.GetHabbo().Roleplay.Company != null && Player.GetHabbo().Roleplay.Company.Id == Session.GetHabbo().Roleplay.Company.Id)
                {
                    if (Player.GetHabbo().Roleplay.Company.IsManager || Player.GetHabbo().Roleplay.Company.IsSupervisor)
                        return;

                    Player.GetHabbo().Roleplay.StopPayTimer(true);

                    Session.GetHabbo().Roleplay.ForceShout("*Sends " + Player.GetHabbo().Username + " home*");
                }
            }
        }
    }

    class FireCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_fire"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Fires a player from their job."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Rank >= 8 || (Session.GetHabbo().Roleplay.Company != null && Session.GetHabbo().Roleplay.Company.IsManager))
            {
                string Username = (Params.Length >= 1 ? Params[1] : String.Empty);

                if (String.IsNullOrEmpty(Username))
                    return;

                GameClients.GameClient Player = GameClientManager.GetClientByUsername(Username);

                if (Player == null)
                    return;

                if (Player.GetHabbo().CurrentRoomId != Session.GetHabbo().CurrentRoomId)
                    return;

                if ((Player.GetHabbo().Roleplay.Company != null && Player.GetHabbo().Roleplay.Company.Id == Session.GetHabbo().Roleplay.Company.Id) || Session.GetHabbo().Rank >= 8)
                {
                    if (Player.GetHabbo().Roleplay.Company.IsManager && Session.GetHabbo().Rank < 8)
                        return;

                    Player.GetHabbo().Roleplay.Company = null;
                    Player.GetHabbo().Roleplay.SaveCompanyStatus();
                    Player.GetHabbo().Roleplay.StopPayTimer(true);

                    Session.GetHabbo().Roleplay.ForceShout("*Fires " + Player.GetHabbo().Username + " from their job*");
                }
            }
        }
    }
}
