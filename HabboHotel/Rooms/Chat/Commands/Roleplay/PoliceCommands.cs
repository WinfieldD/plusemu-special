﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Utilities;
using Plus.HabboHotel.Users;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Communication.Packets.Outgoing.Notifications;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.HabboHotel.GameClients;

namespace Plus.HabboHotel.Rooms.Chat.Commands
{
    class StunCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_stun"; }
        }

        public string Parameters
        {
            get { return "%username%"; }
        }

        public string Description
        {
            get { return "Stun a player."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Rank >= 8 || (Session.GetHabbo().Roleplay.Company.CommandType == HabboJob.JobRights.Police && Session.GetHabbo().Roleplay.IsCurrentlyWorking))
            {
                string Username = (Params.Length >= 1 ? Params[1] : "");

                if (String.IsNullOrEmpty(Username))
                    return;

                Habbo Player = GameClientManager.GetClientByUsername(Username).GetHabbo();

                if (Player != null)
                {
                    if (!Player.Roleplay.Wanted)
                        return;

                    if (Session.GetHabbo().Roleplay.CanReachUser(Session.GetHabbo().Roleplay.GetRoomUser, Player.Roleplay.GetRoomUser, 4))
                    {
                        Session.GetHabbo().Roleplay.ForceShout("*Shoots their Stun Gun at " + Player.Username + ", stopping them in their tracks*");
                        Player.Roleplay.OnStun();
                    }
                    else
                    {
                        Session.GetHabbo().Roleplay.ForceShout("*Shoots their Stun Gun at " + Player.Username + ", but it missed*");
                    }
                }
            }
        }
    }

    class CuffCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_cuff"; }
        }

        public string Parameters
        {
            get { return "%username%"; }
        }

        public string Description
        {
            get { return "Handcuff a player."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Rank >= 8 || (Session.GetHabbo().Roleplay.Company.CommandType == HabboJob.JobRights.Police && Session.GetHabbo().Roleplay.IsCurrentlyWorking))
            {
                string Username = (Params.Length >= 1 ? Params[1] : "");

                if (String.IsNullOrEmpty(Username))
                    return;

                Habbo Player = GameClientManager.GetClientByUsername(Username).GetHabbo();

                if (Player == null || Player == Session.GetHabbo())
                    return;

                if (Player.Roleplay.Handcuffed)
                    return;

                if (!Player.Roleplay.Wanted)
                {
                    Session.SendWhisper(Player.Username + " does not have an arrest warrant on them. (:w " + Player.Username + " reason)");
                    return;
                }

                if (Session.GetHabbo().Roleplay.CanReachUser(Session.GetHabbo().Roleplay.GetRoomUser, Player.Roleplay.GetRoomUser))
                {
                    Session.GetHabbo().Roleplay.ForceShout("*Removes their handcuffs from their utility belt and attaches them to " + Player.Username + "'s wrists*");
                    Session.GetHabbo().Roleplay.ForceShout("*Reads " + Player.Username + " their Miranda rights*");

                    if (Player.Roleplay.StunTask.Status == System.Threading.Tasks.TaskStatus.Running || Player.Roleplay.StunTimer.Enabled)
                    {
                        Player.Roleplay.StunTask.Dispose();
                        Player.Roleplay.StunTimer.Dispose();
                    }

                    Player.Roleplay.Handcuffed = true;
                    Player.Roleplay.GetRoomUser.CanWalk = false;
                    Player.Roleplay.GetRoomUser.IsWalking = false;
                    Player.Roleplay.GetRoomUser.SetStep = false;
                }
            }
        }
    }

    class WarrantCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_warrant"; }
        }

        public string Parameters
        {
            get { return "%username% %reason%"; }
        }

        public string Description
        {
            get { return "Place an arrest warrant on a player."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Rank >= 8 || (Session.GetHabbo().Roleplay.Company.CommandType == HabboJob.JobRights.Police && Session.GetHabbo().Roleplay.IsCurrentlyWorking))
            {
                string Username = (Params.Length >= 1 ? Params[1] : "");
                string Reason = (Params.Length >= 2 ? CommandManager.MergeParams(Params, 2) : "");

                if (String.IsNullOrEmpty(Username) || String.IsNullOrEmpty(Reason))
                    return;

                Habbo Player = GameClientManager.GetClientByUsername(Username).GetHabbo();

                if (Player == null || Player.Roleplay.Wanted)
                    return;

                Player.Roleplay.Wanted = true;
                Player.Roleplay.WantedReason = Reason;
                Player.GetClient().SendWhisper("You have had an arrest warrant placed on you for: " + Reason);

                Session.GetHabbo().Roleplay.ForceShout("*Places an arrest warrant on " + Player.Username + " for " + Reason + "*");
            }
        }
    }

    class ViewWarrantsCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_viewwarrants"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "View the open warrants."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Rank >= 8 || (Session.GetHabbo().Roleplay.Company.CommandType == HabboJob.JobRights.Police && Session.GetHabbo().Roleplay.IsCurrentlyWorking))
            {
                StringBuilder Builder = new StringBuilder();
                int Num = 0;

                foreach (GameClients.GameClient Client in GameClientManager.GetClients.Where(x => x.GetHabbo().Roleplay.Wanted == true))
                {
                    Num++;

                    Builder.Append("<b>");
                    Builder.Append(Client.GetHabbo().Username);
                    Builder.Append("</b> for:");
                    Builder.Append(" " + Client.GetHabbo().Roleplay.WantedReason + "<br>");
                }

                if (Num < 1)
                    Builder.Append("<b>There are no warrants!</b>");

                Session.Send(NotificationManager.NotificationCustom("Wanted List", Builder.ToString(), "", ""));
            }
        }
    }

    class RemoveWarrantCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_removewarrant"; }
        }

        public string Parameters
        {
            get { return "%username%"; }
        }

        public string Description
        {
            get { return "Remove a players warrant."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Rank >= 8 || (Session.GetHabbo().Roleplay.Company.CommandType == HabboJob.JobRights.Police && Session.GetHabbo().Roleplay.IsCurrentlyWorking))
            {
                string Username = (Params.Length >= 1 ? Params[1] : "");

                GameClients.GameClient Player = GameClientManager.GetClientByUsername(Username);

                if (Player == null)
                    return;

                if (!Player.GetHabbo().Roleplay.Wanted)
                    return;

                Player.GetHabbo().Roleplay.Wanted = false;
                Player.GetHabbo().Roleplay.WantedReason = String.Empty;

                Session.GetHabbo().Roleplay.ForceShout("*Has removed " + Player.GetHabbo().Username + " from the wanted list*");
            }
        }
    }

    class ArrestCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_arrest"; }
        }

        public string Parameters
        {
            get { return "%username% %minutes%"; }
        }

        public string Description
        {
            get { return "Arrest a player."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Rank >= 8 || (Session.GetHabbo().Roleplay.Company.CommandType == HabboJob.JobRights.Police && Session.GetHabbo().Roleplay.IsCurrentlyWorking))
            {
                string Username = (Params.Length >= 1 ? Params[1] : "");
                int Time = 0;

                if (String.IsNullOrEmpty(Username))
                    return;

                if (Int32.TryParse(Params[2], out Time))
                {
                    if (Session.GetHabbo().Rank < 8 && Time > 15)
                        return;

                    Habbo Player = GameClientManager.GetClientByUsername(Username).GetHabbo();

                    if (Player == null || Player == Session.GetHabbo())
                        return;

                    if (!Player.Roleplay.Wanted)
                    {
                        Session.SendWhisper(Player.Username + " does not have an arrest warrant on them. (:w " + Player.Username + " reason)");
                        return;
                    }

                    if (!Player.Roleplay.Handcuffed)
                    {
                        Session.SendWhisper(Player.Username + " has not been handcuffed.");
                    }

                    if (Session.GetHabbo().Roleplay.CanReachUser(Session.GetHabbo().Roleplay.GetRoomUser, Player.Roleplay.GetRoomUser))
                    {
                        Player.Roleplay.SendToPrison(Time);
                        Session.GetHabbo().Roleplay.ForceShout("*Sends " + Player.Username + " to Amp Penetentiary for " + Time + " years*");
                    }
                }
            }
        }
    }
}
