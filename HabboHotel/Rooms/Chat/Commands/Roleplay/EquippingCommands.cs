﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Utilities;
using Plus.HabboHotel.Users;
using Plus.Communication.Packets.Outgoing.Handshake;

namespace Plus.HabboHotel.Rooms.Chat.Commands
{
    class EquipCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_equip"; }
        }

        public string Parameters
        {
            get { return "%weapon%"; }
        }

        public string Description
        {
            get { return "Equips a weapon."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Roleplay.EquippedWeapon != null)
            {
                Session.SendWhisper("You must unequip your current weapon before equipping another (:unequip)");
                return;
            }

            if (Session.GetHabbo().Roleplay.IsIncarcerated)
                return;

            string WeaponName = (Params.Length >= 1 ? CommandManager.MergeParams(Params, 1) : "");

            if (String.IsNullOrEmpty(WeaponName))
                return;

            foreach (HabboWeapon Wep in Session.GetHabbo().Roleplay.GetWeapons)
            {
                if (Wep.WeaponName.ToLower() == WeaponName.ToLower())
                {
                    Session.GetHabbo().Roleplay.ForceShout("*Opens their backpack, and equips their " + Wep.WeaponName + "*");

                    if (Wep.IsRanged)
                    {
                        Session.SendWhisper("You have " + Wep.AmmoClip + " ammo in your clip, and " + Wep.AmmoInventory + " ammo in your inventory.");
                    }

                    Session.GetHabbo().Roleplay.EquippedWeapon = Wep;

                    if(!Wep.IsRanged)
                        Session.GetHabbo().Effects().ApplyEffect(162);
                    else
                        Session.GetHabbo().Effects().ApplyEffect(164);

                    break;
                }
            }
        }
    }

    class UnequipCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_unequip"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Unequips a weapon."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Roleplay.EquippedWeapon == null)
            {
                return;
            }

            Session.GetHabbo().Roleplay.OwnedWeapons[Session.GetHabbo().Roleplay.EquippedWeapon.WeaponId] = Session.GetHabbo().Roleplay.EquippedWeapon;

            Session.GetHabbo().Roleplay.ForceShout("*Unequips their " + Session.GetHabbo().Roleplay.EquippedWeapon.WeaponName + " and places it in their bag*");
            Session.GetHabbo().Roleplay.EquippedWeapon = null;
            Session.GetHabbo().Effects().ApplyEffect(0);
        }
    }
}
