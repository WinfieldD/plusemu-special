﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Utilities;
using Plus.HabboHotel.Users;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;

using Plus.Communication.Packets.Outgoing.Notifications;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User
{
    class TrainCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_train"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Trains a specified skill."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Roleplay.CanReachItem(Room, StaticSettings.RedDoorMatFurniId, 0))
            {
                int Stamp = Convert.ToInt32(PlusEnvironment.GetUnixTimestamp());

                if (Session.GetHabbo().Roleplay.WorkoutTimestamp > 0)
                {
                    if (Session.GetHabbo().Roleplay.WorkoutTimestamp > Stamp)
                    {
                        int SecondsLeft = Session.GetHabbo().Roleplay.WorkoutTimestamp - Stamp;
                        int MinutesLeft = Convert.ToInt32(SecondsLeft / 60);
                        int HoursLeft = Convert.ToInt32(MinutesLeft / 60);

                        StringBuilder Output = new StringBuilder();

                        Output.Append("You must wait ");

                        if (HoursLeft >= 1)
                            Output.Append(HoursLeft + " hour" + (HoursLeft > 1 ? "s" : "") + ", " + MinutesLeft + " minutes and " + SecondsLeft + " seconds");
                        else if (MinutesLeft >= 1 && HoursLeft < 1)
                            Output.Append(MinutesLeft + " minute" + (MinutesLeft > 1 ? "s" : "") + " and " + SecondsLeft + " seconds");
                        else
                            Output.Append(SecondsLeft + " second(s)");

                        Output.Append(" until you can workout again.");

                        if (!Session.GetHabbo().Roleplay.IsVip)
                        {
                            Output.Append(" Consider purchasing VIP to lower your cooldown to 1 hour.");
                        }

                        Session.SendWhisper(Output.ToString());
                        return;
                    }
                }

                if (Session.GetHabbo().Roleplay.IsTraining)
                {
                    Session.GetHabbo().Roleplay.StopTrainingTimer();
                    return;
                }

                string Type = (Params.Length >= 1 ? CommandManager.MergeParams(Params, 1) : "");

                if (String.IsNullOrEmpty(Type))
                    return;

                switch (Type.ToLower())
                {
                    case "str":
                    case "strength":
                        {
                            if (Session.GetHabbo().Roleplay.StrengthLevel >= 10 && !Session.GetHabbo().Roleplay.IsVip)
                            {
                                Session.SendWhisper("You have reached the max Strength level for non-VIP members. Consider purchasing VIP to level up further.");
                                break;
                            }
                            else if (Session.GetHabbo().Roleplay.StrengthLevel >= 15)
                            {
                                Session.SendWhisper("You have reached the max Strength level for VIP members, congrats!");
                                break;
                            }
                            else
                            {
                                Session.GetHabbo().Roleplay.StartTrainingTimer(Roleplay.SkillType.Strength);
                                break;
                            }
                        }

                    case "def":
                    case "defence":
                        {
                            if (Session.GetHabbo().Roleplay.DefenceLevel >= 10 && !Session.GetHabbo().Roleplay.IsVip)
                            {
                                Session.SendWhisper("You have reached the max Defence level for non-VIP members. Consider purchasing VIP to level up further.");
                                break;
                            }
                            else if (Session.GetHabbo().Roleplay.DefenceLevel >= 15)
                            {
                                Session.SendWhisper("You have reached the max Defence level for VIP members, congrats!");
                                break;
                            }
                            else
                            {
                                Session.GetHabbo().Roleplay.StartTrainingTimer(Roleplay.SkillType.Defence);
                                break;
                            }
                        }
                }
            }
        }
    }
}