﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Utilities;
using Plus.HabboHotel.Users;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;

using Plus.Communication.Packets.Outgoing.Notifications;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.HabboHotel.ServerWeapons;
using Plus.HabboHotel.GameClients;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User
{
    class ListWeaponsCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_listweapons"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Lists the weapons on the server."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            StringBuilder Builder = new StringBuilder();

            foreach (ServerWeapons.ServerWeapon Weapon in ServerWeaponsManager.Weapons.Values.OrderByDescending(x => x.IsRanged == true))
            {
                Builder.Append("<b>" + Weapon.Name + "</b>");
                Builder.Append("- Type: " + (Weapon.IsRanged ? "Gun" : "Melee"));
                Builder.Append(", Damage: " + Weapon.MinDamage + "-" + Weapon.MaxDamage);
                Builder.Append(", Cost: $" + Weapon.CostCredits);

                if (Weapon.IsRanged || Weapon.IsExplosive)
                {
                    Builder.Append(", Clip Size: " + Weapon.ClipSize);
                    Builder.Append(", Range: " + Weapon.HitRange);
                    Builder.Append(", Clip Cost: $" + Weapon.ClipCostCredits);
                }

                Builder.Append("<br><br>");
            }

            Session.Send(NotificationManager.NotificationCustom("Weapons List", Builder.ToString(), "", ""));
        }
    }

    class SellWeaponCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_sellweapon"; }
        }

        public string Parameters
        {
            get { return "%username% %weapon%"; }
        }

        public string Description
        {
            get { return "Sells a weapon to a player."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Rank >= 8 || (Session.GetHabbo().Roleplay.HasJobRights(HabboJob.JobRights.Ammunation) && Session.GetHabbo().Roleplay.IsCurrentlyWorking))
            {
                string Username = (Params.Length >= 1 ? Params[1] : "");
                string WeaponName = (Params.Length >= 2 ? CommandManager.MergeParams(Params, 2) : "");

                if (String.IsNullOrEmpty(Username) || String.IsNullOrEmpty(WeaponName))
                    return;

                GameClients.GameClient Player = GameClientManager.GetClientByUsername(Username);

                if (Player == null)
                    return;

                ServerWeapons.ServerWeapon Weapon = ServerWeaponsManager.TryGetWeapon(WeaponName);

                if (Weapon == null)
                    return;

                if (Player.GetHabbo().Roleplay.EquippedWeapon != null)
                {
                    Session.SendWhisper(Player.GetHabbo().Username + " has to un-equip their weapon before purchasing.");
                    return;
                }

                if (Player.GetHabbo().Roleplay.WeaponOffer != null)
                {
                    TimeSpan SinceOffer = DateTime.Now - Player.GetHabbo().Roleplay.WeaponOfferAt;

                    if (SinceOffer.Minutes >= 2)
                        Player.GetHabbo().Roleplay.WeaponOfferAt = DateTime.Now;
                    else
                    {
                        Session.SendWhisper("This player has a pending offer.");
                        return;
                    }
                }

                Player.GetHabbo().Roleplay.WeaponOffer = Weapon;
                Session.GetHabbo().Roleplay.ForceShout("*Has offered to sell " + Player.GetHabbo().Username + " a " + Weapon.Name + " for $" + Weapon.CostCredits + "*");
                Player.SendWhisper("You have been offered a " + Weapon.Name + " for $" + Weapon.CostCredits + ", you can use :buyweapon " + Weapon.Name + " to buy it, or ignore this message.");
            }
        }
    }

    class BuyWeaponCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_buyweapon"; }
        }

        public string Parameters
        {
            get { return "%weapon%"; }
        }

        public string Description
        {
            get { return "Buys a weapon from Ammunation."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().CurrentRoomId == StaticSettings.AmmunationRoomId)
            {
                string WeaponName = (Params.Length >= 1 ? CommandManager.MergeParams(Params, 1) : "");

                if (String.IsNullOrEmpty(WeaponName))
                    return;

                if (Session.GetHabbo().Roleplay.EquippedWeapon != null)
                {
                    Session.SendWhisper("Please un-equip your weapon before purchasing another.");
                    return;
                }

                if (GameClientManager.AmmunationStaffOnDuty.Count >= 1)
                {
                    if (Session.GetHabbo().Roleplay.WeaponOffer != null && (WeaponName.ToLower() == Session.GetHabbo().Roleplay.WeaponOffer.Name.ToLower()))
                    {
                        TimeSpan SinceOffer = DateTime.Now - Session.GetHabbo().Roleplay.WeaponOfferAt;

                        if (SinceOffer.Minutes >= 2)
                        {
                            Session.SendWhisper("Weapon offers only last for two minutes, so be quick about it!");
                            return;
                        }

                        if (Session.GetHabbo().Credits >= Session.GetHabbo().Roleplay.WeaponOffer.CostCredits)
                        {
                            Session.GetHabbo().Roleplay.AddWeapon(Session.GetHabbo().Roleplay.WeaponOffer);
                            Session.GetHabbo().Credits -= Session.GetHabbo().Roleplay.WeaponOffer.CostCredits;
                            Session.GetHabbo().Roleplay.UpdateCreditsBalance();

                            Session.GetHabbo().Roleplay.ForceShout("*Has purchased a " + Session.GetHabbo().Roleplay.WeaponOffer.Name + "*");

                            if (Session.GetHabbo().Roleplay.WeaponOffer.IsRanged || Session.GetHabbo().Roleplay.WeaponOffer.IsExplosive)
                            {
                                Session.SendWhisper("Your weapon comes with a complementary, full clip of " + Session.GetHabbo().Roleplay.WeaponOffer.ClipSize + " bullets.");
                            }

                            Session.GetHabbo().Roleplay.WeaponOffer = null;
                        }
                        else
                        {
                            Session.SendWhisper("You can't afford to buy that!");
                            Session.GetHabbo().Roleplay.WeaponOffer = null;
                            return;
                        }
                    }
                    else
                    {
                        Session.SendWhisper("You need to get an Ammunation worker to sell this weapon to you.");
                        return;
                    }
                }
                else
                {
                    ServerWeapons.ServerWeapon Weapon = ServerWeaponsManager.TryGetWeapon(WeaponName);

                    if (Weapon == null)
                        return;

                    if (Session.GetHabbo().Credits >= Weapon.CostCredits)
                    {
                        Session.GetHabbo().Roleplay.AddWeapon(Weapon);
                        Session.GetHabbo().Credits -= Weapon.CostCredits;
                        Session.GetHabbo().Roleplay.UpdateCreditsBalance();

                        Session.GetHabbo().Roleplay.ForceShout("*Has purchased a " + Weapon.Name + "*");

                        if (Weapon.IsRanged || Weapon.IsExplosive)
                        {
                            Session.SendWhisper("Your weapon comes with a complementary, full clip of " + Weapon.ClipSize + " bullets.");
                        }
                    }
                    else
                    {
                        Session.SendWhisper("You can't afford to buy that!");
                        return;
                    }
                }
            }
        }
    }

    class BuyAmmoCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_buyammo"; }
        }

        public string Parameters
        {
            get { return "%weapon%"; }
        }

        public string Description
        {
            get { return "Buys ammo from Ammunation."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Roleplay.EquippedWeapon != null)
            {
                Session.SendWhisper("Please unequip your weapon before you purchase ammunition.");
                return;
            }

            string WeaponName = (Params.Length >= 1 ? CommandManager.MergeParams(Params, 1) : "");

            ServerWeapons.ServerWeapon Weapon = ServerWeaponsManager.TryGetWeapon(WeaponName);

            if (Weapon == null)
                return;

            if (!Weapon.IsRanged)
                return;

            if (Session.GetHabbo().Roleplay.OwnedWeapons.ContainsKey(Weapon.Id))
            {
                if (Session.GetHabbo().Credits >= Weapon.ClipCostCredits)
                {
                    Session.GetHabbo().Roleplay.OwnedWeapons[Weapon.Id].AmmoInventory += Weapon.ClipSize;
                    Session.GetHabbo().Credits -= Weapon.ClipCostCredits;
                    Session.GetHabbo().Roleplay.UpdateCreditsBalance();
                    Session.GetHabbo().Roleplay.SaveWeapon(Session.GetHabbo().Roleplay.OwnedWeapons[Weapon.Id]);

                    Session.GetHabbo().Roleplay.ForceShout("Purchases a clip of " + Weapon.ClipSize + " for their " + Weapon.Name + " for $" + Weapon.ClipCostCredits + "!");
                }
                else
                {
                    Session.SendWhisper("You can't afford a clip of ammo for a " + Weapon.Name + "! ($" + Weapon.ClipCostCredits + ")");
                    return;
                }
            }
            else
            {
                Session.SendWhisper("You don't own a " + Weapon.Name + "!");
                return;
            }
        }
    }
}
