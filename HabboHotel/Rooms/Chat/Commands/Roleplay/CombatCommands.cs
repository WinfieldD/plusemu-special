﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Utilities;
using Plus.HabboHotel.Users;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.HabboHotel.GameClients;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User
{
    class HitCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_hit"; }
        }

        public string Parameters
        {
            get { return "%username%"; }
        }

        public string Description
        {
            get { return "Attacks another player with fists or melee weapon."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Room.Description.ToLower().Contains("hit_disabled"))
            {
                Session.SendWhisper("Hitting has been disabled in this room.");
                return;
            }

            if (Session.GetHabbo().Roleplay.IsIncarcerated)
                return;

            GameClients.GameClient Victim = GameClientManager.GetClientByUsername(Params[1]);

            if (Victim == null || Victim.GetHabbo().CurrentRoomId != Session.GetHabbo().CurrentRoomId)
                return;

            RoomUser Me = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            RoomUser Them = Room.GetRoomUserManager().GetRoomUserByHabbo(Victim.GetHabbo().Id);

            if (Me == null || Them == null || Me == Them)
                return;

            if (!Session.GetHabbo().Roleplay.CanReachUser(Me, Them))
                return;

            if (!Session.GetHabbo().Roleplay.CanUseCombat())
                return;

            if (Victim.GetHabbo().Roleplay.Afk)
                return;

            Random HitRandomizer = new Random();
            int Damage = 2;

            HabboWeapon EquippedWeap = Session.GetHabbo().Roleplay.EquippedWeapon;

            if (EquippedWeap == null)
            {
                Damage = HitRandomizer.Next(1, (Session.GetHabbo().Roleplay.StrengthLevel * 10) + 1);
            }
            else
            {
                if (Room.Description.ToLower().Contains("weapons_disabled"))
                {
                    Session.SendWhisper("The use of weapons has been disabled in this room.");
                    return;
                }

                Damage = HitRandomizer.Next(EquippedWeap.MinDamage, EquippedWeap.MaxDamage + 1);
            }

            int Take = 0;

            StringBuilder Message = new StringBuilder();

            Message.Append("*Swings their " + (EquippedWeap != null ? EquippedWeap.WeaponName : "fists") + " at " + Victim.GetHabbo().Username);

            if ((Victim.GetHabbo().Roleplay.Hitpoints - Damage) < 1)
            {
                if (Victim.GetHabbo().Roleplay.EquippedWeapon != null)
                {
                    Victim.GetHabbo().Roleplay.SaveWeapon(Victim.GetHabbo().Roleplay.EquippedWeapon);
                    Victim.GetHabbo().Roleplay.EquippedWeapon = null;
                    Victim.GetHabbo().Effects().ApplyEffect(0);
                }

                if (Victim.GetHabbo().Roleplay.IsCurrentlyWorking)
                {
                    Victim.GetHabbo().Roleplay.StopPayTimer(true);
                }

                if (Victim.GetHabbo().Roleplay.IsTraining)
                {
                    Victim.GetHabbo().Roleplay.StopTrainingTimer(true);
                }

                if (Victim.GetHabbo().Roleplay.HasBounty)
                    Victim.GetHabbo().Roleplay.HandleBounty(Session);

                Take = Session.GetHabbo().Roleplay.RobPlayer(Victim);

                Victim.GetHabbo().Roleplay.Hitpoints = 0;
                Victim.GetHabbo().Roleplay.IsCurrentlyDead = true;
                Victim.GetHabbo().Roleplay.TransportedToHospital = true;
                Victim.GetHabbo().PrepareRoom(StaticSettings.HospitalRoomId, "");
                Message.Append(", killing them*");
            }
            else
            {
                Victim.GetHabbo().Roleplay.Hitpoints -= Damage;
                Message.Append(", hitting them for " + Damage.ToString() + " damage (" + Victim.GetHabbo().Roleplay.Hitpoints + "/" + Victim.GetHabbo().Roleplay.MaxHitpoints + ")*");
            }

            Me.OnChat(0, Message.ToString(), true);

            if (Take > 0)
                Session.GetHabbo().Roleplay.ForceShout("*Picks up $" + Take + " from their dead body*");
        }
    }

    class ShootCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_shoot"; }
        }

        public string Parameters
        {
            get { return "%username%"; }
        }

        public string Description
        {
            get { return "Attacks another player with a ranged weapon."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            HabboWeapon Weapon = Session.GetHabbo().Roleplay.EquippedWeapon;

            if (Weapon == null)
                return;

            if (Session.GetHabbo().Roleplay.IsIncarcerated)
                return;

            if (Room.Description.ToLower().Contains("weapons_disabled"))
            {
                Session.SendWhisper("The use of weapons has been disabled in this room.");
                return;
            }

            GameClients.GameClient Victim = GameClientManager.GetClientByUsername(Params[1]);

            if (Victim == null || Victim.GetHabbo().CurrentRoomId != Session.GetHabbo().CurrentRoomId)
                return;

            RoomUser Me = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            RoomUser Them = Room.GetRoomUserManager().GetRoomUserByHabbo(Victim.GetHabbo().Id);

            if (Me == null || Them == null || Me == Them)
                return;

            if (!Session.GetHabbo().Roleplay.CanReachUser(Me, Them, Weapon.RangeToHit))
                return;

            if (!Session.GetHabbo().Roleplay.CanUseCombat())
                return;

            if (Victim.GetHabbo().Roleplay.Afk)
                return;

            if (Session.GetHabbo().Roleplay.EquippedWeapon.AmmoClip < 1)
            {
                Session.SendWhisper("Your weapon has no ammo loaded.");
                return;
            }

            Random HitRandomizer = new Random();
            int Damage = HitRandomizer.Next(Weapon.MinDamage, Weapon.MaxDamage + 1);

            int Take = 0;

            StringBuilder Message = new StringBuilder();

            Message.Append("*Shoots their " + Weapon.WeaponName + " at " + Victim.GetHabbo().Username);

            if ((Victim.GetHabbo().Roleplay.Hitpoints - Damage) < 1)
            {
                if (Victim.GetHabbo().Roleplay.EquippedWeapon != null)
                {
                    Victim.GetHabbo().Roleplay.SaveWeapon(Victim.GetHabbo().Roleplay.EquippedWeapon);
                    Victim.GetHabbo().Roleplay.EquippedWeapon = null;
                    Victim.GetHabbo().Effects().ApplyEffect(0);
                }

                if (Victim.GetHabbo().Roleplay.IsCurrentlyWorking)
                {
                    Victim.GetHabbo().Roleplay.StopPayTimer(true);
                }

                if (Victim.GetHabbo().Roleplay.IsTraining)
                {
                    Victim.GetHabbo().Roleplay.StopTrainingTimer(true);
                }

                if (Victim.GetHabbo().Roleplay.HasBounty)
                    Victim.GetHabbo().Roleplay.HandleBounty(Session);

                Take = Session.GetHabbo().Roleplay.RobPlayer(Victim);

                Victim.GetHabbo().Roleplay.Hitpoints = 0;
                Victim.GetHabbo().Roleplay.IsCurrentlyDead = true;
                Victim.GetHabbo().Roleplay.TransportedToHospital = true;
                Victim.GetHabbo().PrepareRoom(StaticSettings.HospitalRoomId, "");
                
                Message.Append(", killing them*");
            }
            else
            {               
                Victim.GetHabbo().Roleplay.Hitpoints -= Damage;
                Message.Append(", hitting them for " + Damage.ToString() + " damage (" + Victim.GetHabbo().Roleplay.Hitpoints + "/" + Victim.GetHabbo().Roleplay.MaxHitpoints + ")*");
            }

            Session.GetHabbo().Roleplay.EquippedWeapon.AmmoClip -= 1;

            Me.OnChat(0, Message.ToString(), true);

            if (Take > 0)
                Session.GetHabbo().Roleplay.ForceShout("*Picks up $" + Take + " from their dead body*");
        }
    }

    class ReloadCommand : IChatCommand
    {
        System.Timers.Timer ReloadTimer;
        System.Threading.Tasks.Task ReloadTask;

        int CurrentAmmo;
        int MaxClipSize;
        int AmmoOwned;
        int NeedToReload;
        HabboWeapon Weapon;
        GameClients.GameClient Player;

        public string PermissionRequired
        {
            get { return "command_reload"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Reloads your current equipped weapon."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Roleplay.EquippedWeapon != null && Session.GetHabbo().Roleplay.EquippedWeapon.IsRanged)
            {
                CurrentAmmo = Session.GetHabbo().Roleplay.EquippedWeapon.AmmoClip;
                MaxClipSize = Session.GetHabbo().Roleplay.EquippedWeapon.ClipSize;
                AmmoOwned = Session.GetHabbo().Roleplay.EquippedWeapon.AmmoInventory;

                Weapon = Session.GetHabbo().Roleplay.EquippedWeapon;
                Player = Session;

                NeedToReload = 0;
                
                if (CurrentAmmo <= MaxClipSize && AmmoOwned > 0)
                {
                    NeedToReload = MaxClipSize - CurrentAmmo;

                    ReloadTimer = new System.Timers.Timer();
                    ReloadTimer.AutoReset = false;
                    ReloadTimer.Interval = Session.GetHabbo().Roleplay.EquippedWeapon.ReloadTime * 1000;
                    ReloadTimer.Elapsed += ReloadTimer_Elapsed;

                    ReloadTask = new System.Threading.Tasks.Task(ReloadTimer.Start);
                    ReloadTask.Start();

                    Session.GetHabbo().Roleplay.ForceShout("*Starts reloading their " + Weapon.WeaponName + "*");
                }
            }
        }

        void ReloadTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (Weapon != null && Weapon.IsRanged)
            {
                if (Weapon.AmmoInventory >= NeedToReload)
                {
                    Weapon.AmmoClip = Weapon.ClipSize;
                    Weapon.AmmoInventory -= NeedToReload;
                }
                else
                {
                    Weapon.AmmoClip += Weapon.AmmoInventory;
                    Weapon.AmmoInventory = 0;                
                }

                Player.GetHabbo().Roleplay.ForceShout("*Has reloaded their " + Weapon.WeaponName + "*");
                Player.SendWhisper("You have " + Weapon.AmmoInventory + " bullets left in your inventory, and " + Weapon.AmmoClip + " bullets in your clip.");
            }
        }
    }
}
