﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Utilities;
using Plus.HabboHotel.Users;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using Plus.HabboHotel.GameClients;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User
{
    class WithdrawCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_withdraw"; }
        }

        public string Parameters
        {
            get { return "%username% %amount%"; }
        }

        public string Description
        {
            get { return "Withdraws money from a users bank account."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Roleplay.CanReachItem(Room, StaticSettings.AtmMachineFurniId))
            {
                int Amount = (Params.Length >= 1 ? Convert.ToInt32(Params[1]) : 0);

                if (Amount < 1)
                    return;

                if (Amount > 5000)
                {
                    Session.SendWhisper("You can only withdraw a maximum of $5,000 at a time from an ATM! See a banker to withdraw more.");
                    return;
                }

                if (Session.GetHabbo().Roleplay.BankAccount >= Amount)
                {
                    Session.GetHabbo().Credits += Amount;
                    Session.GetHabbo().Roleplay.BankAccount -= Amount;
                    Session.GetHabbo().Roleplay.UpdateCreditsBalance();

                    Session.GetHabbo().Roleplay.ForceShout("*Places their bank card in the ATM and punches in their PIN code*");
                    Session.GetHabbo().Roleplay.ForceShout("*Makes a transaction and withdraws $" + Amount + " from their bank account*");
                }
                else
                    Session.SendWhisper("You don't have $" + Amount + " in your bank account.");
            }
            else
            {
                if (Session.GetHabbo().Rank >= 8 || (Session.GetHabbo().Roleplay.Company.CommandType == HabboJob.JobRights.Bank && Session.GetHabbo().Roleplay.IsCurrentlyWorking))
                {
                    string Username = (Params.Length >= 1 ? Params[1] : String.Empty);
                    int Amount = (Params.Length >= 2 ? Convert.ToInt32(Params[2]) : 0);

                    if (String.IsNullOrEmpty(Username) || Amount < 1)
                        return;

                    GameClients.GameClient Player = GameClientManager.GetClientByUsername(Username);

                    if (Player == null)
                        return;

                    if (Player.GetHabbo().CurrentRoomId != Session.GetHabbo().CurrentRoomId)
                        return;

                    if (Player.GetHabbo().Roleplay.BankAccount >= Amount)
                    {
                        Player.GetHabbo().Roleplay.BankAccount -= Amount;
                        Player.GetHabbo().Credits += Amount;
                        Player.Send(new CreditBalanceComposer(Player.GetHabbo().Credits));

                        Session.GetHabbo().Roleplay.ForceShout("*Withdraws $" + Amount + " from " + Player.GetHabbo().Username + "'s bank account*");
                    }
                }
            }
        }
    }
    class BalanceCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_balance"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Deposits money into a users bank account."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Roleplay.CanReachItem(Room, StaticSettings.AtmMachineFurniId))
            {
                Session.GetHabbo().Roleplay.ForceShout("*Places their bank card in the ATM and punches in their PIN code*");
                Session.GetHabbo().Roleplay.ForceShout("*Checks their balance and discovers they have $" + Session.GetHabbo().Roleplay.BankAccount + "*");
            }
        }
    }


    class DepositCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_deposit"; }
        }

        public string Parameters
        {
            get { return "%username% %amount%"; }
        }

        public string Description
        {
            get { return "Deposits money into a users bank account."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Roleplay.CanReachItem(Room, StaticSettings.AtmMachineFurniId))
            {
                int Amount = (Params.Length >= 1 ? Convert.ToInt32(Params[1]) : 0);

                if (Amount < 1)
                    return;

                if (Amount > 5000)
                {
                    Session.SendWhisper("You can only deposit a maximum of $5,000 at a time from an ATM! See a banker to deposit more.");
                    return;
                }

                if (Session.GetHabbo().Credits >= Amount)
                {
                    Session.GetHabbo().Credits -= Amount;
                    Session.GetHabbo().Roleplay.BankAccount += Amount;
                    Session.GetHabbo().Roleplay.UpdateCreditsBalance();

                    Session.GetHabbo().Roleplay.ForceShout("*Places their bank card in the ATM and punches in their PIN code*");
                    Session.GetHabbo().Roleplay.ForceShout("*Makes a transaction and deposits $" + Amount + " to their bank account*");
                }
                else
                    Session.SendWhisper("You don't have $" + Amount + " in your wallet.");
            }
            else
            {
                if (Session.GetHabbo().Rank >= 8 || (Session.GetHabbo().Roleplay.Company.CommandType == HabboJob.JobRights.Bank && Session.GetHabbo().Roleplay.IsCurrentlyWorking))
                {
                    string Username = (Params.Length >= 1 ? Params[1] : String.Empty);
                    int Amount = (Params.Length >= 2 ? Convert.ToInt32(Params[2]) : 0);

                    if (String.IsNullOrEmpty(Username) || Amount < 1)
                        return;

                    GameClients.GameClient Player = GameClientManager.GetClientByUsername(Username);

                    if (Player == null)
                        return;

                    if (Player.GetHabbo().CurrentRoomId != Session.GetHabbo().CurrentRoomId)
                        return;

                    if (Player.GetHabbo().Credits >= Amount)
                    {
                        Player.GetHabbo().Roleplay.BankAccount += Amount;
                        Player.GetHabbo().Credits -= Amount;
                        Player.Send(new CreditBalanceComposer(Player.GetHabbo().Credits));

                        Session.GetHabbo().Roleplay.ForceShout("*Deposits $" + Amount + " into " + Player.GetHabbo().Username + "'s bank account*");
                    }
                }
            }
        }
    }
}
