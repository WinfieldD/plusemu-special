﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Utilities;
using Plus.HabboHotel.Users;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Communication.Packets.Outgoing.Notifications;

namespace Plus.HabboHotel.Rooms.Chat.Commands
{
    class HealCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_heal"; }
        }

        public string Parameters
        {
            get { return "%username%"; }
        }

        public string Description
        {
            get { return "Heals a player to full health."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Rank >= 8 || (Session.GetHabbo().Roleplay.HasJobRights(HabboJob.JobRights.Hospital) && Session.GetHabbo().Roleplay.IsCurrentlyWorking))
            {
                string Player = (Params.Length >= 1 ? Params[1] : "");

                if (String.IsNullOrEmpty(Player))
                    return;

                RoomUser Me = Session.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Username);
                RoomUser Them = Session.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(Player);

                if (Me == null || Them == null)
                    return;

                if (Session.GetHabbo().Roleplay.CanReachUser(Me, Them, 1))
                {
                    Habbo Patient = Them.GetClient().GetHabbo();
                    StringBuilder Message = new StringBuilder();

                    Message.Append("*Prepares a syringe and injects it into " + Patient.Username + ", ");

                    if (Patient.Roleplay.Hitpoints < Patient.Roleplay.MaxHitpoints && !Patient.Roleplay.IsCurrentlyDead)
                    {
                        Message.Append("healing them to full health*");
                    }
                    else if (Patient.Roleplay.IsCurrentlyDead)
                    {
                        Message.Append("reviving them*");

                        Patient.Roleplay.IsCurrentlyDead = false;
                        Them.UnlockWalking();
                    }
                    else
                    {
                        Message.Append("but they were already at perfect health*");
                    }

                    Patient.Roleplay.Hitpoints = Patient.Roleplay.MaxHitpoints;
                    Session.GetHabbo().Roleplay.ForceShout(Message.ToString());
                }
            }
        }
    }
}
