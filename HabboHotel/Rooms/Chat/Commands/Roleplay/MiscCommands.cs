﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data;

using Plus.Database.Interfaces;

using Plus.Utilities;
using Plus.HabboHotel.Users;
using Plus.Communication.Packets.Outgoing.Handshake;

using Plus.Communication.Packets.Outgoing.Notifications;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.HabboHotel.GameClients;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User
{
    class RoomIdCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_roomid"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Displays the current room ID."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            Session.SendWhisper("The current room ID is " + Session.GetHabbo().CurrentRoomId.ToString());
        }
    }

    class AcceptCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_accept"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Accepts an offer."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Roleplay.HasDrugOffer)
            {
                TimeSpan Span = DateTime.Now - Session.GetHabbo().Roleplay.DrugOfferedAt;
                if (Span.Minutes > 2)
                {
                    Session.SendWhisper("This offer has expired.");
                    return;
                }

                int Price = Session.GetHabbo().Roleplay.DrugPrice(Session.GetHabbo().Roleplay.DrugOffer);

                if (Session.GetHabbo().Credits >= Price)
                {
                    Session.GetHabbo().Credits -= Price;
                    Session.GetHabbo().Roleplay.UpdateCreditsBalance();
                }
                else
                {
                    Session.SendWhisper("You can't afford to buy that! ($" + Price + ")");
                }
            }
            else
            {
                Session.SendWhisper("You have no offers to accept!");
            }
        }
    }

    class SellCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_sell"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Sells a drug to another player."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Rank >= 8 || (Session.GetHabbo().Roleplay.Company.CommandType == HabboJob.JobRights.DrugDealer && Session.GetHabbo().Roleplay.IsCurrentlyWorking))
            {
                string Username = (Params.Length >= 1 ? Params[1] : "");
                string Type = (Params.Length >= 2 ? Params[2] : "");

                if (String.IsNullOrEmpty(Username) || String.IsNullOrEmpty(Type))
                    return;

                GameClients.GameClient Player = GameClientManager.GetClientByUsername(Username);

                if (Player == null)
                    return;

                if (Player.GetHabbo().Roleplay.HasDrugOffer)
                {
                    TimeSpan TimeLeft = DateTime.Now - Player.GetHabbo().Roleplay.DrugOfferedAt;

                    if (TimeLeft.Minutes < 2)
                    {
                        Session.SendWhisper("This player has a drug offer active already!");
                        return;
                    }
                }

                StringBuilder Builder = new StringBuilder();
                Builder.Append("*Pulls out a bag of ");

                switch (Type.ToLower())
                {
                    case "weed":
                    case "marijuana":
                        {
                            Player.GetHabbo().Roleplay.DrugOffer = Roleplay.DrugType.Weed;
                            Player.GetHabbo().Roleplay.HasDrugOffer = true;
                            Player.GetHabbo().Roleplay.DrugOfferedAt = DateTime.Now;
                            Builder.Append("weed buds ");
                            break;
                        }

                    case "cocaine":
                    case "coke":
                        {
                            Player.GetHabbo().Roleplay.DrugOffer = Roleplay.DrugType.Cocaine;
                            Player.GetHabbo().Roleplay.HasDrugOffer = true;
                            Player.GetHabbo().Roleplay.DrugOfferedAt = DateTime.Now;
                            Builder.Append("cocaine powder ");
                            break;
                        }

                    case "mdma":
                    case "extacy":
                        {
                            Player.GetHabbo().Roleplay.DrugOffer = Roleplay.DrugType.Mdma;
                            Player.GetHabbo().Roleplay.HasDrugOffer = true;
                            Player.GetHabbo().Roleplay.DrugOfferedAt = DateTime.Now;
                            Builder.Append("mdma crystals ");
                            break;
                        }

                    case "heroin":
                        {

                            Player.GetHabbo().Roleplay.DrugOffer = Roleplay.DrugType.Heroin;
                            Player.GetHabbo().Roleplay.HasDrugOffer = true;
                            Player.GetHabbo().Roleplay.DrugOfferedAt = DateTime.Now;
                            Builder.Append("sticky heroin ");
                            break;
                        }

                    default:
                        {
                            Session.SendWhisper("Usage: :sell x weed/heroin/mdma/cocaine");
                            return;
                        }
                }

                int Cost = Player.GetHabbo().Roleplay.DrugPrice(Player.GetHabbo().Roleplay.DrugOffer);

                Builder.Append("and offers it to " + Player.GetHabbo().Username + " for $" + Cost + "*");
                Session.GetHabbo().Roleplay.ForceShout(Builder.ToString());
                Player.SendWhisper("You have been offered drugs for $" + Cost + ", type :accept to buy them.");
            }
        }
    }

    class BuyRoomCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_buyroom"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Buys a property that is for sale."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Room.RoomData.IsProperty && Room.RoomData.PropertyForSale)
            {
                if (Session.GetHabbo().Credits >= Room.RoomData.PropertyPrice)
                {
                    Session.GetHabbo().Credits -= Room.RoomData.PropertyPrice;
                    Session.GetHabbo().Roleplay.UpdateCreditsBalance();

                    GameClients.GameClient Player = GameClientManager.GetClientByUsername(Room.RoomData.PropertyOwner);

                    if (Player != null)
                    {
                        Player.GetHabbo().UsersRooms.Remove(Room.RoomData);
                        Player.GetHabbo().Credits += Room.RoomData.PropertyPrice;
                    }
                    else
                    {
                        if (Room.RoomData.PropertyOwner != "Government")
                        {
                            using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                            {
                                Adapter.SetQuery("UPDATE users SET credits = credits + @amount WHERE username = @name");
                                Adapter.AddParameter("amount", Room.RoomData.PropertyPrice);
                                Adapter.AddParameter("name", Room.RoomData.PropertyOwner);
                                Adapter.RunQuery();
                            }
                        }
                    }

                    Session.GetHabbo().UsersRooms.Add(Room.RoomData);

                    Room.UsersWithRights.Clear();
                    Room.RoomData.PropertyPrice = 0;
                    Room.RoomData.PropertyForSale = false;
                    Room.RoomData.PropertyOwner = Session.GetHabbo().Username;
                    
                    using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                    {
                        Adapter.SetQuery("UPDATE rooms SET property_forsale = '0', property_price='0', property_owner = @newowner WHERE id = @roomid");
                        Adapter.AddParameter("newowner", Session.GetHabbo().Username);
                        Adapter.AddParameter("roomid", Room.Id);
                        Adapter.RunQuery();

                        Adapter.SetQuery("DELETE FROM room_rights WHERE room_id = @roomid");
                        Adapter.AddParameter("roomid", Room.Id);
                        Adapter.RunQuery();

                        if (Room.Group != null)
                        {
                            Adapter.SetQuery("SELECT id FROM groups WHERE room_id = @roomid");
                            Adapter.AddParameter("roomid", Room.Id);

                            int GroupId = Adapter.getInteger();

                            if (GroupId > 0)
                            {
                                Room.Group.ClearRequests();

                                foreach (int MemberId in Room.Group.GetAllMembers)
                                {
                                    Room.Group.DeleteMember(MemberId);

                                    GameClients.GameClient MemberClient = GameClientManager.GetClientByUserID(MemberId);

                                    if (MemberClient == null)
                                        continue;

                                    if (MemberClient.GetHabbo().GetStats().FavouriteGroupId == GroupId)
                                    {
                                        MemberClient.GetHabbo().GetStats().FavouriteGroupId = 0;
                                    }                                  
                                }

                                Adapter.RunQuery("DELETE FROM `groups` WHERE `id` = '" + Room.Group.Id + "'");
                                Adapter.RunQuery("DELETE FROM `group_memberships` WHERE `group_id` = '" + Room.Group.Id + "'");
                                Adapter.RunQuery("DELETE FROM `group_requests` WHERE `group_id` = '" + Room.Group.Id + "'");
                                Adapter.RunQuery("UPDATE `rooms` SET `group_id` = '0' WHERE `group_id` = '" + Room.Group.Id + "' LIMIT 1");
                                Adapter.RunQuery("UPDATE `user_stats` SET `groupid` = '0' WHERE `groupid` = '" + Room.Group.Id + "' LIMIT 1");
                                Adapter.RunQuery("DELETE FROM `items_groups` WHERE `group_id` = '" + Room.Group.Id + "'");
                            }

                            HotelGameManager.GetGroupManager().DeleteGroup(Room.Group.Id);
                            Room.RoomData.Group = null;
                            Room.Group = null;
                        }
                    }

                    foreach (RoomUser User in Room.GetRoomUserManager().GetRoomUsers())
                    {
                        if (User.GetClient() == Session)
                            User.GetClient().GetHabbo().PrepareRoom(Room.Id, Room.Password);
                        else
                            User.GetClient().GetHabbo().PrepareRoom(1, "");
                    }
                }
            }
        }
    }

   

    class TimeLeftCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_timeleft"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Checks up on timers."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            Roleplay R = Session.GetHabbo().Roleplay;
            StringBuilder Builder = new StringBuilder();

            Builder.Append("Current Active Timers<br><br>");

            if (R.IsCurrentlyWorking)
            {
                int PayMinutes = 10 - (DateTime.Now - R.StartedWorkingAt).Minutes;
                Builder.Append(PayMinutes + " minutes until you are paid.<br>");
            }

            if (R.IsTraining)
            {
                int TrainMinutes = 5 - (DateTime.Now - R.StartedTrainingAt).Minutes;
                Builder.Append(TrainMinutes + " minutes until you finish your workout.<br>");
            }

            if (R.IsIncarcerated)
            {
                int PrisonMinutes = R.IncarceratedFor - (DateTime.Now - R.WasIncarceratedAt).Minutes;
                Builder.Append(PrisonMinutes + " minutes until you finish your prison sentence.<br>");
            }

            if (R.IsRobbingVault)
            {
                int VaultMinutes = 5 - (DateTime.Now - R.StartedRobbingVaultAt).Minutes;
                Builder.Append(VaultMinutes + " minutes until you finish robbing the treasury.<br>");
            }

            Builder.Append("<br><b>All timers have a random interval added to them,</b> which means we cannot 100% accurately calculate when they end!");
            R.SendRoomNotification("Active Timers", Builder.ToString());
        }
    }

    class GambleCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_gamble"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Gambles on a slot machine."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Roleplay.CanReachItem(Room, StaticSettings.SlotMachineFurniId))
            {
                if (!Session.GetHabbo().Roleplay.TryGamble())
                    return;

                bool HasWonJackpotToday = false;

                if (Session.GetHabbo().Roleplay.JackpotTimestamp > 0)
                {
                    if (PlusEnvironment.GetUnixTimestamp() <= Session.GetHabbo().Roleplay.JackpotTimestamp)
                    {
                        HasWonJackpotToday = true;
                    }
                }

                int AmountToGamble = (Params.Length >= 1 ? Convert.ToInt32(Params[1]) : 0);

                if (!Int32.TryParse(Params[1], out AmountToGamble) || AmountToGamble < 25 || AmountToGamble > 1000)
                {
                    Session.SendWhisper("You must gamble at least 25 credits, and less than 1001.");
                    return;
                }

                if (Session.GetHabbo().Credits < AmountToGamble)
                    return;

                Session.GetHabbo().Credits -= AmountToGamble;
                Session.GetHabbo().Roleplay.UpdateCreditsBalance();

                Random Randomizer = new Random();

                int MyNumber = Randomizer.Next(1, 9);
                int JackpotNumber = Randomizer.Next(1, 9);
                int SmallNumber = Randomizer.Next(1, 9);
                int MyJackpotNumber = Randomizer.Next(1, 400);
                int MajorJackpot = Randomizer.Next(1, 400);

                Session.GetHabbo().Roleplay.ForceShout("*Places $" + AmountToGamble + " in the machine, finalizes the bet and spins the slots*");

                int JackpotAmount = Convert.ToInt32(Math.Round(AmountToGamble * 2.6, 0, MidpointRounding.AwayFromZero));
                int SmallAmount = Convert.ToInt32(Math.Round(AmountToGamble * 1.6, 0, MidpointRounding.AwayFromZero));

                if (MyNumber == JackpotNumber)
                {
                    Session.GetHabbo().Credits += JackpotAmount;
                    Session.GetHabbo().Roleplay.UpdateCreditsBalance();
                    Session.GetHabbo().Roleplay.ForceShout("*Has won the minature jackpot of $" + JackpotAmount + " from the machine*");
                }
                else if (MyNumber == SmallNumber)
                {
                    Session.GetHabbo().Credits += SmallNumber;
                    Session.GetHabbo().Roleplay.UpdateCreditsBalance();

                    Session.GetHabbo().Roleplay.ForceShout("*Has won a minor prize of $" + SmallAmount + " from the machine*");
                }
                else if (MyJackpotNumber == MajorJackpot)
                {
                    if (AmountToGamble < 100)
                    {
                        Session.SendWhisper("You would have won the Jackpot of $" + PlusEnvironment.CurrentJackpot + ", but you didn't gamble over $100.");
                        return;
                    }

                    if (!HasWonJackpotToday)
                    {
                        Session.GetHabbo().Roleplay.JackpotTimestamp = Convert.ToInt32(PlusEnvironment.GetUnixTimestamp() + 86400);
                        Session.GetHabbo().Credits += PlusEnvironment.CurrentJackpot;
                        Session.GetHabbo().Roleplay.UpdateCreditsBalance();

                        Session.GetHabbo().Roleplay.ForceShout("*Has won the all-time Jackpot of $" + PlusEnvironment.CurrentJackpot + "*");

                        PlusEnvironment.CurrentJackpot = 0;
                    }
                    else
                    {
                        Session.GetHabbo().Credits += JackpotAmount;
                        Session.GetHabbo().Roleplay.UpdateCreditsBalance();

                        Session.GetHabbo().Roleplay.ForceShout("*Has won the minature jackpot of $" + JackpotAmount + " from the machine*");
                    }
                }
                else
                {
                    Session.GetHabbo().Roleplay.ForceShout("*Has won nothing from the machine*");

                    if (PlusEnvironment.CurrentJackpot < 75000)
                        PlusEnvironment.CurrentJackpot += AmountToGamble;
                }
            }
        }
    }

    class InventoryCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_inventory"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Displays your character inventory."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            StringBuilder Inv = new StringBuilder();

            Inv.Append("User Inventory for " + Session.GetHabbo().Username + "\r");
            Inv.Append("----------------------------\r");
            Inv.Append("Wallet: $" + Session.GetHabbo().Credits + "\r");
            Inv.Append("Bank Account: $" + Session.GetHabbo().Roleplay.BankAccount + "\r");
            Inv.Append("\rWeapons\r----------------------------\r");

            if (Session.GetHabbo().Roleplay.OwnedWeapons.Count >= 1)
            {
                foreach (HabboWeapon Weapon in Session.GetHabbo().Roleplay.OwnedWeapons.Values)
                {
                    Inv.Append(Weapon.InventoryAmount + " " + Weapon.WeaponName + "" + (Weapon.InventoryAmount > 1 ? "s" : "") + " " + (Weapon.IsRanged ? "with " + (Weapon.AmmoClip + Weapon.AmmoInventory) + " Total Ammo" : "") + "\r");
                }
            }
            else
            {
                Inv.Append("You have no owned weapons.");
            }

            Session.Send(new MOTDNotificationComposer(Inv.ToString()));
        }
    }

    class WireCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_wire"; }
        }

        public string Parameters
        {
            get { return "%username% %amount%"; }
        }

        public string Description
        {
            get { return "Pays another user via bank account."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            string Username = (Params.Length >= 1 ? Params[1] : String.Empty);
            int Amount = (Params.Length >= 2 ? Convert.ToInt32(Params[2]) : 0);

            if (String.IsNullOrEmpty(Username) || Amount < 1)
                return;

            GameClients.GameClient Player = GameClientManager.GetClientByUsername(Username);

            if (Player == null || Player == Session)
                return;

            if (!Session.GetHabbo().Roleplay.MoneyTransferChecks())
            {
                Session.SendWhisper("You have to wait 3 days after creating your account to use this feature.");
                return;
            }

            if (Session.GetHabbo().Roleplay.BankAccount < Amount)
                return;

            Player.GetHabbo().Roleplay.BankAccount += Amount;
            Session.GetHabbo().Roleplay.BankAccount -= Amount;

            Session.GetHabbo().Roleplay.ForceShout("*Pulls out their phone and transfers $" + Amount + " to " + Player.GetHabbo().Username + "'s bank account*");
            Player.GetHabbo().Roleplay.ForceShout("*Has received a bank transfer of $" + Amount + " from " + Session.GetHabbo().Username + "*");
        }
    }

    class PayCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_pay"; }
        }

        public string Parameters
        {
            get { return "%username% %amount%"; }
        }

        public string Description
        {
            get { return "Pays another user."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            string Username = (Params.Length >= 1 ? Params[1] : String.Empty);
            int Amount = (Params.Length >= 2 ? Convert.ToInt32(Params[2]) : 0);

            if (String.IsNullOrEmpty(Username) || Amount < 1)
                return;

            GameClients.GameClient Player = GameClientManager.GetClientByUsername(Username);

            if (Player == null || Player == Session)
                return;

            if (Session.GetHabbo().Credits < Amount)
                return;

            if (!Session.GetHabbo().Roleplay.MoneyTransferChecks())
            {
                Session.SendWhisper("You have to wait 3 days after creating your account to use this feature.");
                return;
            }

            RoomUser Me = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Username);
            RoomUser Them = Room.GetRoomUserManager().GetRoomUserByHabbo(Player.GetHabbo().Username);

            if (Me == null || Them == null)
                return;

            if (Session.GetHabbo().Roleplay.CanReachUser(Me, Them))
            {
                Player.GetHabbo().Credits += Amount;
                Session.GetHabbo().Credits -= Amount;

                Session.GetHabbo().Roleplay.UpdateCreditsBalance();
                Player.GetHabbo().Roleplay.UpdateCreditsBalance();

                Session.GetHabbo().Roleplay.ForceShout("*Takes out their wallet, counts out $" + Amount + " in cash and gives it to " + Player.GetHabbo().Username + "*");
                Player.GetHabbo().Roleplay.ForceShout("*Takes the $" + Amount + " and places it in their wallet*");
            }
        }
    }

    class BountiesCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_bounties"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "View the currently active bounties."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            StringBuilder Builder = new StringBuilder();
            int Amount = 0;

            foreach (GameClients.GameClient Client in GameClientManager.GetClients.Where(x => x.GetHabbo().Roleplay.HasBounty == true))
            {
                Builder.Append("<b>" + Client.GetHabbo().Username + "</b>: $" + Client.GetHabbo().Roleplay.BountyReward + "<br>");
                Amount++;
            }

            if (Amount < 1)
                Builder.Append("There are currently no active bounties.");

            Session.Send(NotificationManager.NotificationCustom("Active Bounties", Builder.ToString(), "", ""));
        }
    }

    class SetBountyCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_setbounty"; }
        }

        public string Parameters
        {
            get { return "%player% %amount%"; }
        }

        public string Description
        {
            get { return "Set a bounty on a player."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (!Session.GetHabbo().Roleplay.MoneyTransferChecks())
            {
                Session.SendWhisper("Your account must be 3 days old in order to use this function.");
                return;
            }

            string Username = (Params.Length >= 1 ? Params[1].ToLower() : "");
            int Amount = 0;

            if (Params.Length >= 1 && Int32.TryParse(Params[2], out Amount))
            {
                if (Amount < 1)
                    return;

                if (Amount < 150)
                {
                    Session.SendWhisper("You must set a bounty of over $150!");
                    return;
                }

                GameClients.GameClient Player = GameClientManager.GetClientByUsername(Username);

                if (Player == null)
                    return;

                if (Player.GetHabbo().Roleplay.HasBounty)
                {
                    Session.SendWhisper(Player.GetHabbo().Username + " already has a bounty on them! ($" + Player.GetHabbo().Roleplay.BountyReward + ")");
                    return;
                }

                if (Session.GetHabbo().Credits >= Amount)
                {
                    Session.GetHabbo().Credits -= Amount;
                    Session.GetHabbo().Roleplay.UpdateCreditsBalance();

                    Session.GetHabbo().Roleplay.ForceShout("*Has placed a bounty on " + Player.GetHabbo().Username + " for $" + Amount + "*");
                    Player.SendWhisper("[BOUNTY ALERT] You have had a bounty placed on you!");

                    foreach (GameClients.GameClient Client in GameClientManager.GetClients)
                    {
                        if (Client == null || Client.GetHabbo() == null || Client.GetHabbo().Roleplay == null || Client == Player || Client.GetHabbo().Roleplay.Afk)
                            continue;

                        Client.SendWhisper("[BOUNTY ALERT] " + Player.GetHabbo().Username + " has a bounty on their head for $" + Amount + "!");
                    }

                    Player.GetHabbo().Roleplay.BountyReward = Amount;
                    Player.GetHabbo().Roleplay.HasBounty = true;
                }
            }
        }
    }

    class KissesCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_kisses"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Kisses another player."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            StringBuilder Message = new StringBuilder();
            string Username = (Params.Length >= 1 ? Params[1] : "");

            if (String.IsNullOrEmpty(Username))
                return;

            GameClients.GameClient Player = GameClientManager.GetClientByUsername(Username);

            if (Player == null)
                return;

            if (Session.GetHabbo().Roleplay.CanReachUser(Session.GetHabbo().Roleplay.GetRoomUser, Player.GetHabbo().Roleplay.GetRoomUser))
            {
                Message.Append("*Kisses " + Player.GetHabbo().Username + "*");
                Session.GetHabbo().Roleplay.ForceShout(Message.ToString());
                Session.GetHabbo().Effects().ApplyEffect(9);
            }
        }
    }

    class SlapCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_slap"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Slaps another player."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            StringBuilder Message = new StringBuilder();
            string Username = (Params.Length >= 1 ? Params[1] : "");

            if (String.IsNullOrEmpty(Username))
                return;

            GameClients.GameClient Player = GameClientManager.GetClientByUsername(Username);

            if (Player == null)
                return;

            if (Session.GetHabbo().Roleplay.CanReachUser(Session.GetHabbo().Roleplay.GetRoomUser, Player.GetHabbo().Roleplay.GetRoomUser))
            {
                Message.Append("*Slaps " + Player.GetHabbo().Username + "*");
                Session.GetHabbo().Roleplay.ForceShout(Message.ToString());
            }
        }
    }

    class SpitCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_spit"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Spits at or on a user."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            StringBuilder Message = new StringBuilder();
            string Username = (Params.Length >= 1 ? Params[1] : "");

            if (String.IsNullOrEmpty(Username))
                return;

            GameClients.GameClient Player = GameClientManager.GetClientByUsername(Username);

            if (Player == null)
                return;

            if (Session.GetHabbo().Roleplay.CanReachUser(Session.GetHabbo().Roleplay.GetRoomUser, Player.GetHabbo().Roleplay.GetRoomUser))
            {
                Message.Append("*Spits on " + Player.GetHabbo().Username + "*");
            }
            else
            {
                Message.Append("*Spits towards " + Player.GetHabbo().Username + "*");
            }

            Session.GetHabbo().Roleplay.ForceShout(Message.ToString());
        }
    }

    class RobVaultCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_robvault"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Checks the current populated rooms."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Room.Id == StaticSettings.VaultRoomId)
            {
                if (GameClientManager.PoliceStaffOnDuty.Count < 1)
                {
                    Session.SendWhisper("You can only rob the vault when there are police on duty.");
                    return;
                }

                foreach (GameClients.GameClient Client in GameClientManager.GetClients)
                {
                    if (Client == null || Client == Session)
                        continue;

                    if (Client.GetHabbo().Roleplay.IsRobbingVault)
                    {
                        Session.SendWhisper(Client.GetHabbo().Username + " is already robbing the vault!");
                        return;
                    }
                }

                if (Session.GetHabbo().Roleplay.IsRobbingVault)
                {
                    Session.GetHabbo().Roleplay.StopVaultTimer();
                    return;
                }

                Session.GetHabbo().Roleplay.StartVaultTimer();
            }
        }
    }

    class HotRoomsCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_hotrooms"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Checks the current populated rooms."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            StringBuilder Builder = new StringBuilder();

            foreach (Room PopulatedRoom in HotelGameManager.GetRoomManager().GetRooms().OrderByDescending(x => x.UsersNow))
            {
                Builder.Append("<b>" + PopulatedRoom.Name + " (ID: " + PopulatedRoom.Id + ")</b> has " + PopulatedRoom.RoomData.UsersNow + " citizen(s)<br>");
            }

            Session.GetHabbo().Roleplay.SendRoomNotification("Populated Rooms", Builder.ToString());
        }
    }

    class DriveCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_drive"; }
        }

        public string Parameters
        {
            get { return "%room id%"; }
        }

        public string Description
        {
            get { return "Drives to a different room."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Roleplay.Company != null && Session.GetHabbo().Roleplay.IsCurrentlyWorking && !Session.GetHabbo().Roleplay.Company.CanRoam)
                return;

            if (Session.GetHabbo().Roleplay.IsIncarcerated || Session.GetHabbo().Roleplay.Handcuffed || Session.GetHabbo().Roleplay.IsTraining)
                return;

            int RoomId = (Params.Length >= 1 ? Convert.ToInt32(Params[1]) : 0);

            if (RoomId < 1 || RoomId == Session.GetHabbo().CurrentRoomId)
                return;

            Rooms.RoomData pRoom = HotelGameManager.GetRoomManager().GenerateRoomData(RoomId);

            if (pRoom == null)
                return;

            if (pRoom.Description.ToLower().Contains("no_taxi"))
            {
                Session.SendWhisper("This room cannot be driven to. Looks like you gotta walk!");
                return;
            }

            if (Session.GetHabbo().Roleplay.IsCurrentlyWorking && Session.GetHabbo().Roleplay.HasJobRights(HabboJob.JobRights.Police))
            {
                Session.GetHabbo().Roleplay.CatchTaxi(RoomId, true);
            }
            else
            {
                if (Session.GetHabbo().Roleplay.DrivingLevel < 1)
                {
                    Session.SendWhisper("You don't have a drivers license!");
                    return;
                }

                if (Session.GetHabbo().Roleplay.DrivingFuel < 5)
                {
                    Session.SendWhisper("You don't have enough fuel to drive! Buy more fuel or catch a cab!");
                    return;
                }

                Session.GetHabbo().Roleplay.CatchTaxi(RoomId, true);
            }
        }
    }

    class TaxiCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_taxi"; }
        }

        public string Parameters
        {
            get { return "%room id%"; }
        }

        public string Description
        {
            get { return "Calls for a taxi to a different room."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Roleplay.TaxiTo > 0)
            {
                if (Session.GetHabbo().Roleplay.TaxiTask.Status == System.Threading.Tasks.TaskStatus.Running || Session.GetHabbo().Roleplay.TaxiTimer.Enabled)
                {
                    Session.GetHabbo().Roleplay.TaxiTimer.Stop();
                    Session.GetHabbo().Roleplay.TaxiTask.Dispose();
                    Session.GetHabbo().Roleplay.TaxiTimer.Dispose();
                    Session.GetHabbo().Roleplay.TaxiTo = 0;

                    Session.GetHabbo().Roleplay.ForceShout("*Has cancelled their Taxi*");
                    return;
                }
            }

            if (Session.GetHabbo().Roleplay.Company != null && Session.GetHabbo().Roleplay.IsCurrentlyWorking && !Session.GetHabbo().Roleplay.Company.CanRoam)
                return;

            if (Session.GetHabbo().Roleplay.IsIncarcerated || Session.GetHabbo().Roleplay.Handcuffed || Session.GetHabbo().Roleplay.IsTraining)
                return;

            int RoomId = (Params.Length >= 1 ? Convert.ToInt32(Params[1]) : 0);

            if (RoomId < 1 || RoomId == Session.GetHabbo().CurrentRoomId)
                return;

            if (Session.GetHabbo().Credits < 5)
            {
                Session.SendWhisper("You can't afford to catch a taxi! ($5)");
                return;
            }

            Rooms.RoomData pRoom = HotelGameManager.GetRoomManager().GenerateRoomData(RoomId);

            if (pRoom == null)
                return;

            if (pRoom.Description.ToLower().Contains("no_taxi"))
            {
                Session.SendWhisper("This room cannot be taxied to. Looks like you gotta walk!");
                return;
            }

            Session.GetHabbo().Roleplay.CatchTaxi(RoomId);
        }
    }

    class StatsCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_stats"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Displays your characters statistics."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            StringBuilder Stats = new StringBuilder();

            Stats.Append("User Statistics for " + Session.GetHabbo().Username + "\r");
            Stats.Append("----------------------------\r");
            Stats.Append("Current Health: " + Session.GetHabbo().Roleplay.Hitpoints + " (Max: " + Session.GetHabbo().Roleplay.MaxHitpoints + ")\r");
            Stats.Append("Strength Level: " + Session.GetHabbo().Roleplay.StrengthLevel + " (Exp: " + Session.GetHabbo().Roleplay.StrengthExperience + "/1000)\r");
            Stats.Append("Defence Level: " + Session.GetHabbo().Roleplay.DefenceLevel + " (Exp: " + Session.GetHabbo().Roleplay.DefenceExperience + "/1000)\r");
            Session.Send(new MOTDNotificationComposer(Stats.ToString()));
        }
    }
}
