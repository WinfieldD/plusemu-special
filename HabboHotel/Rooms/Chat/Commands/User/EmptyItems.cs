﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.Inventory.Furni;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User
{
    class EmptyItems : IChatCommand
    {
        public string PermissionRequired => "command_empty_items"; 
        public string Parameters => "%yes%"; 
        public string Description => LocaleCommands.Get("commands.message.empty.desc"); 

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.Send(NotificationManager.Custom(LocaleCommands.Get("commands.message.empty")));
                return;
            }
            else
            {
                if (Params.Length == 2 && Params[1].ToString() == LocaleCommands.Get("commands.message.confirm.empty"))
                {
                    Session.GetHabbo().GetInventoryComponent().ClearItems();
                    Session.Send(NotificationManager.Bubble("", LocaleCommands.Get("commands.message.empty.success"), ""));
                    return;
                }
                else if (Params.Length == 2 && Params[1].ToString() != LocaleCommands.Get("commands.message.confirm.empty"))
                {
                    Session.Send(NotificationManager.Bubble("", LocaleCommands.Get("commands.message.empty.error"), ""));
                    return;
                }
            }
        }
    }
}
