﻿using Plus.Communication.Packets.Outgoing.Help;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Database.Interfaces;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Moderation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User
{
    class SanctionCommand : IChatCommand
    {
        public string PermissionRequired => "command_sanction";
        public string Parameters => "%username% %mode% %Reason%";
        public string Description => LocaleCommands.Get("sanction.desc");


        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            String error = Params[1];
            if (error == LocaleCommands.Get("name.list"))
            {
                Session.SendNotification(LocaleCommands.Get("message.list.mods"));
            }
            else
            {
                GameClient TargetClient = GameClientManager.GetClientByUsername(Params[1]);
                if (TargetClient == null)
                {
                    Session.SendWhisper(LocaleCommands.Get("sanction.usertarget.message"));
                    return;
                }

                String mode = Params[2];
                String Reason = Params[3];
                if(mode == null)
                {
                    Session.SendWhisper(LocaleCommands.Get("sanction.error.mode.message"));
                    return;
                }

                if (Reason == null)
                {
                    Session.SendWhisper(LocaleCommands.Get("sanction.error.reason.message"));
                    return;
                }

                if (mode == "ALERT")
                {
                    // idk not have idea xd!
                }
                if (mode == "MUTE")
                {
                    double Time = 200;
                    using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                    {
                        dbClient.SetQuery("UPDATE `users` SET `time_muted` = @time WHERE `id` = @habbo LIMIT 1");
                        dbClient.AddParameter("time", Time);
                        dbClient.AddParameter("habbo", TargetClient.GetHabbo().Id);
                        dbClient.RunQuery();
                    }

                    if (TargetClient.GetHabbo().GetClient() != null)
                    {
                        TargetClient.GetHabbo().TimeMuted = Time;
                        TargetClient.GetHabbo().GetClient().SendNotification(LocaleCommands.Get("sanction.message.success", "%time%", Time));
                    }
                    Session.SendWhisper(LocaleCommands.Get("sanction.message.mute.success", "%username%", TargetClient.GetHabbo().Username, "%time%", Time));

                }
                if (mode == "BAN_PERMANENT")
                {
                    String IPAddress = String.Empty;
                    Double Expire = PlusEnvironment.GetUnixTimestamp() + 78892200;
                    using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                    {
                        dbClient.RunQuery("UPDATE `user_info` SET `bans` = `bans` + '1' WHERE `user_id` = '" + TargetClient.GetHabbo().Id + "' LIMIT 1");

                        dbClient.SetQuery("SELECT `ip_last` FROM `users` WHERE `id` = @id LIMIT 1");
                        dbClient.AddParameter("id", TargetClient.GetHabbo().Id);
                        IPAddress = dbClient.getString();
                    }

                    if (!string.IsNullOrEmpty(IPAddress))
                        ModerationManager.BanUser(Session.GetHabbo().Username, ModerationBanType.IP, IPAddress, Reason, Expire);
                    ModerationManager.BanUser(Session.GetHabbo().Username, ModerationBanType.USERNAME, TargetClient.GetHabbo().Username, Reason, Expire);

                    TargetClient.Disconnect();
                    Session.SendWhisper(LocaleCommands.Get("sanction.message.ban.success", "%username%", TargetClient.GetHabbo().Username, "%reason%", Reason));
                }

                TargetClient.Send(new SanctionStatusComposer(mode, Reason));
            }
        }
    }
}
