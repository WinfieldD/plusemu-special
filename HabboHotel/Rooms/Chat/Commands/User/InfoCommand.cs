﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;

using Plus.Communication.Packets.Outgoing.Users;
using Plus.Communication.Packets.Outgoing.Notifications;


using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Communication.Packets.Outgoing.Quests;
using Plus.HabboHotel.Items;
using Plus.Communication.Packets.Outgoing.Inventory.Furni;
using Plus.Communication.Packets.Outgoing.Catalog;
using Plus.HabboHotel.Quests;
using Plus.HabboHotel.Rooms;
using System.Threading;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Rooms.Avatar;
using Plus.Communication.Packets.Outgoing.Pets;
using Plus.Communication.Packets.Outgoing.Messenger;
using Plus.HabboHotel.Users.Messenger;
using Plus.Communication.Packets.Outgoing.Rooms.Polls;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Communication.Packets.Outgoing.Availability;
using Plus.Communication.Packets.Outgoing;
using Plus.Communication.Packets.Outgoing.Rooms.Polls.Questions;
using Plus.Core;
using Plus.HabboHotel.Groups;
using Plus.Communication.Packets.Outgoing.Rooms.Session;
using Plus.HabboHotel.Users;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User
{
    class InfoCommand : IChatCommand
    {
        public string PermissionRequired => "command_info";
        public string Parameters => ""; 
        public string Description => LocaleCommands.Get("about.desc");

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
       /* private function _SafeStr_11884(k:_SafeStr_4095):void
        {
                switch (k._SafeStr_5939()._SafeStr_6039)
                {
                    case _SafeStr_4615._SafeStr_7789:
                        this._SafeStr_11888();
                        return;
                    case _SafeStr_4615.OK:
                    case _SafeStr_4615._SafeStr_11859:
                    case _SafeStr_4615._SafeStr_11854:
                        this._SafeStr_11888();
                        this._SafeStr_11882 = (k._SafeStr_5939()._SafeStr_11896 + getTimer());
                        this._SafeStr_11895();
                        return;
                    case _SafeStr_4615.ERROR:
                    case _SafeStr_4615._SafeStr_11855:
                    case _SafeStr_4615._SafeStr_11856:
                    case _SafeStr_4615._SafeStr_11860:
                        if (((!(this._SafeStr_11879)) && (!(this._SafeStr_11878))))
                        {
                            this._SafeStr_11891();
                        }
                        else
                        {
                            if (this._SafeStr_11879)
                            {
                                this._SafeStr_11864(false);
                            };
                        };
                        this._SafeStr_6820.alert("${generic.alert.title}", (("${phone.number.collect.error." + k._SafeStr_5939()._SafeStr_6039) + "}"), 0, null);
                        this._SafeStr_11878._SafeStr_11868(k._SafeStr_5939()._SafeStr_6039);
                        return;
                };
            }
            */
            var getpacket = new ServerPacket(3879);
            getpacket.WriteBoolean(true);
            getpacket.WriteInteger(1);
            getpacket.WriteInteger(1);
            getpacket.WriteBoolean(true);
            getpacket.WriteInteger(1);
            getpacket.WriteBoolean(true);
            Session.Send(getpacket);
            
            Habbo habbo = PlusEnvironment.GetHabboById(Session.GetHabbo().Id);
            var OnlineUsers = GameClientManager.Count;
            var RoomCount = HotelGameManager.GetRoomManager().Count;
            var CountItems = HotelGameManager.GetItemManager()._items.Count;
            var CountRank = HotelGameManager.GetPermissionManager().GetPermissionsForPlayer(habbo).Count;
            var CountR = GameClientManager.GetClients.Count;
            var TypeNotification = Session.GetHabbo().NotificationSwitcher;

            switch (TypeNotification)
            {
                case "stop":
                    break;

                case "normal":
                    StringBuilder Build = new StringBuilder();
                    Build.Append("<b>Credits</b>:\n");
                    Build.Append("<i>");
                    Build.Append("Sledmore (Developer)\n");
                    Build.Append("Joopie (Encryption)\n");
                    Build.Append("Amine (Developer)\n");
                    if (Session.GetHabbo().GetPermissions().HasRight("mod_tickets"))
                    {
                        Build.Append("#ZozoGang\n");
                    }
                    Build.Append("Butterfly Emulator developers\n\n");
                    Build.Append("</i>");
                    if (Session.GetHabbo().GetPermissions().HasRight("mod_tickets"))
                    {
                        Build.Append("<b>Count features:</b>\n");
                        Build.Append("Count Items: " + CountItems + "\n");
                        Build.Append("Your using type notification: " + TypeNotification + "\n");
                        Build.Append("Count of rank by users: " + CountRank + "\n");
                        Build.Append("test: " + CountR + "\n");
                        Build.Append("\n");
                    }
                    Build.Append("<b>Current run time information</b>:\n");
                    Build.Append("<i>");
                    Build.Append("Online Users: " + OnlineUsers + "\n");
                    Build.Append("Rooms Loaded: " + RoomCount + "\n");
                    Build.Append("</i>");

                    Session.Send(NotificationManager.NotificationCustom("Powered by Plus Emulator",
                        Build.ToString(),
                         "plus", "Fermer", "event:close"));
                    break;

                case "new":
                    StringBuilder Builds = new StringBuilder();
                    Builds.Append("Credits:\n");
                    Builds.Append("Sledmore (Developer)\n");
                    Builds.Append("Joopie (Encryption)\n");
                    Builds.Append("Amine (Developer)\n");
                    if (Session.GetHabbo().GetPermissions().HasRight("mod_zozo"))
                    {
                        Builds.Append("#ZozoGang\n");
                    }
                    Builds.Append("Butterfly Emulator developers\n\n");
                    if (Session.GetHabbo().GetPermissions().HasRight("mod_tickets"))
                    {
                        Builds.Append("Count features:\n");
                        Builds.Append("Count Items: " + CountItems + "\n");
                        Builds.Append("Your using type notification: " + TypeNotification + "\n");
                        Builds.Append("\n");
                    }
                    Builds.Append("Information:\n");
                    Builds.Append("Online Users: " + OnlineUsers + "\n");
                    Builds.Append("Rooms Loaded: " + RoomCount + "\n");
                    Session.Send(NotificationManager.Custom(Builds.ToString()));
                    break;

                case "custom":
                    StringBuilder Buildd = new StringBuilder();
                    Buildd.Append("Credits:\n");
                    Buildd.Append("Sledmore (Developer)\n");
                    Buildd.Append("Joopie (Encryption)\n");
                    Buildd.Append("Amine (Developer)\n");
                    if (Session.GetHabbo().GetPermissions().HasRight("mod_zozo"))
                    {
                        Buildd.Append("#ZozoGang\n");
                    }
                    Buildd.Append("Butterfly Emulator developers\n\n");
                    if (Session.GetHabbo().GetPermissions().HasRight("mod_tickets"))
                    {
                        Buildd.Append("Count features:\n");
                        Buildd.Append("Count Items: " + CountItems + "\n");
                        Buildd.Append("Your using type notification: " + TypeNotification + "\n");
                    }
                    Session.Send(NotificationManager.Bubble("", Buildd.ToString(), ""));
                    break;
            }
        }
    }
}