﻿using Plus.Communication.Packets.Outgoing.Availability;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Core;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User
{
    class MaintenanceCommand : IChatCommand
    {
        public string PermissionRequired => "command_maint";
        public string Parameters => "%Minutes% %Durations%";
        public string Description => LocaleCommands.Get("maintenance.desc");

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            if (Session == null)
                return;

            if (Params.Length == 0)
            {
                Session.Send(NotificationManager.Bubble("", "meh1", ""));
                return;
            }
            else
            {
                if (Params[1].ToString() == ConfigurationData.data["key.maintenance"])
                {
                    double Maintenance = double.Parse(Params[2]);
                    double Durations = double.Parse(Params[3]);

                    Session.Send(new MaintenanceStatusComposer(Maintenance, Durations));

                    if (int.Parse(Params[3]) >= Durations)
                    {
                        PlusEnvironment.PerformShutDownWithTime(Maintenance, Durations, true);
                    }
                    else
                    {
                        PlusEnvironment.PerformShutDownWithTime(Maintenance, Durations, false);
                    }
                }
                else
                {
                    Session.Send(NotificationManager.Bubble("", "mehemeh", ""));
                }
            }
        }
    }
}