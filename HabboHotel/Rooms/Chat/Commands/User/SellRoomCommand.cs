﻿using Plus;
using Plus.Database.Interfaces;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Rooms.Chat.Commands;
using System;

class SellRoomCommand : IChatCommand
{
    public string PermissionRequired
    {
        get { return "command_sellroom"; }
    }

    public string Parameters
    {
        get { return ""; }
    }

    public string Description
    {
        get { return "Lists your property on the open market."; }
    }

    public void Execute(GameClient Session, Room Room, string[] Params)
    {
        if (!Room.CheckRights(Session, true) || !Room.RoomData.IsProperty)
        {
            return;
        }

        int Price = (Params.Length >= 1 ? Convert.ToInt32(Params[1]) : 0);

        if (Price > 0)
        {
            Room.RoomData.PropertyForSale = true;
            Room.RoomData.PropertyPrice = Price;

            using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                Adapter.SetQuery("UPDATE rooms SET property_forsale = '1', property_price = @sellprice WHERE id = @roomid");
                Adapter.AddParameter("sellprice", Price);
                Adapter.AddParameter("roomid", Room.Id);
                Adapter.RunQuery();
            }

            Session.SendWhisper("You have listed your property on the market for $" + Price + ", to remove it, use :sellroom 0");

            foreach (RoomUser Player in Room.GetRoomUserManager().GetRoomUsers())
            {
                if (Player.GetClient() == Session)
                    continue;

                Player.GetClient().SendWhisper("This property has just been listed on the market for $" + Price + ", use :buyroom to buy it!");
            }
        }
        else
        {
            if (Room.RoomData.PropertyForSale)
            {
                Room.RoomData.PropertyForSale = false;
                Room.RoomData.PropertyPrice = 0;

                using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                {
                    Adapter.SetQuery("UPDATE rooms SET property_forsale = '0', property_price = @sellprice WHERE id = @roomid");
                    Adapter.AddParameter("sellprice", 0);
                    Adapter.AddParameter("roomid", Room.Id);
                    Adapter.RunQuery();
                }

                foreach (RoomUser Player in Room.GetRoomUserManager().GetRoomUsers())
                {
                    Player.GetClient().SendWhisper("This property has just been taken off the market!");
                }
            }
        }
    }
}