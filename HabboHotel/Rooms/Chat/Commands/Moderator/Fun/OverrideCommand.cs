﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Rooms;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;

namespace Plus.HabboHotel.Rooms.Chat.Commands.Moderator.Fun
{
    class OverrideCommand : IChatCommand
    {
        public string PermissionRequired => "command_override"; 
        public string Parameters => ""; 
        public string Description => "Gives you the ability to walk over anything."; 
        
        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (User == null)
                return;

            User.AllowOverride = !User.AllowOverride;
            if (User.AllowOverride)
            {
                Session.Send(NotificationManager.Whisper(Session, "=message.error.active.override", 0));
            }
            else
            {
                Session.Send(NotificationManager.Whisper(Session, "=message.error.override", 0));
            }
        }
    }
}
