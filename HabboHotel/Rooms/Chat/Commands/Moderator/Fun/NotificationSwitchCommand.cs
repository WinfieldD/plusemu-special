﻿using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Database.Interfaces;
using Plus.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Rooms.Chat.Commands.Moderator.Fun
{
    class NotificationSwitchCommand : IChatCommand
    {
        public string PermissionRequired => "command_notification";
        public string Parameters => "";
        public string Description => LocaleCommands.Get("notification.switch.desc");

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            var i = Params[1];
            if (i == "liste")
            {
                GameClientManager.Send(NotificationManager.Bubble("", "test", ""));
            }
            else
            {
                using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                {
                    dbClient.SetQuery("UPDATE `users` SET `notification_switch` = @switch WHERE `id` = @id LIMIT 1");
                    dbClient.AddParameter("switch", i);
                    dbClient.AddParameter("id", Session.GetHabbo().Id);
                    dbClient.RunQuery();
                }

                Session.GetHabbo().NotificationSwitcher = i;
            }
        }
    }
}
