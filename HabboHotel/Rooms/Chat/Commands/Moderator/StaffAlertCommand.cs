﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Communication.Packets.Outgoing.Moderation;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;

namespace Plus.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class StaffAlertCommand : IChatCommand
    {
        public string PermissionRequired => "command_staff_alert"; 
        public string Parameters => "%message%";
        public string Description => LocaleCommands.Get("staffalert.message.desc"); 
        
        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.Send(NotificationManager.Whisper(Session, "=staffalert.error", 0));
                return;
            }
            string Message = CommandManager.MergeParams(Params, 1);
            string Url = CommandManager.MergeParams(Params, 2);

            GameClientManager.Alert(NotificationManager.Basic("=staffalert.sendalert.message\t%message% " + Message +",%user%, " + Session.GetHabbo().Username +"", "=staffalert.link\t%url%, " + Url +""));
            return;
        }
    }
}
