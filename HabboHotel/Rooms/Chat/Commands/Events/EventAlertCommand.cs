﻿using Plus.Communication.Packets.Outgoing.Moderation;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.HabboHotel.GameClients;
using System;
namespace Plus.HabboHotel.Rooms.Chat.Commands.Events
{
    public class EventAlertCommand : IChatCommand
    {
        public string PermissionRequired => "command_event_alert";
        public string Parameters => "";
        public string Description => LocaleCommands.Get("eha.desc");

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            if (Session == null)
                return;

            if (Room == null)
                return;

            if (Session != null)
            {
                if (Room != null)
                {
                    if (Params.Length == 1)
                    {
                        Session.SendWhisper(LocaleCommands.Get("eha.message.error"));
                        return;
                    }
                    else
                    {
                        string Message = CommandManager.MergeParams(Params, 1);
                        //todo: make configurable eha to the database :)
                        GameClientManager.Send(NotificationManager.NotificationCustom(
                            LocaleCommands.Get("eha.message.title"),
                            LocaleCommands.Get("eha.message.alert", 
                            "%userR%", Room.OwnerName, 
                            "%message%", Message),

                            "eha_command",
                            LocaleCommands.Get("eha.message.button", "%room%", Room.Name),
                            "event:navigator/goto/" + Session.GetHabbo().CurrentRoomId));
                    }
                }
            }
        }
    }
}