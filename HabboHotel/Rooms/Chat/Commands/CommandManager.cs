﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Utilities;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.GameClients;

using Plus.HabboHotel.Rooms.Chat.Commands.User;
using Plus.HabboHotel.Rooms.Chat.Commands.User.Fun;
using Plus.HabboHotel.Rooms.Chat.Commands.Moderator;
using Plus.HabboHotel.Rooms.Chat.Commands.Moderator.Fun;
using Plus.HabboHotel.Rooms.Chat.Commands.Administrator;

using Plus.Communication.Packets.Outgoing.Rooms.Chat;
using Plus.Communication.Packets.Outgoing.Notifications;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Rooms.Chat.Commands.Events;
using Plus.HabboHotel.Items.Wired;
using Plus.Communication.Packets.Outgoing;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Communication.Packets.Outgoing.Messenger;
using Plus.Core;
using Plus.HabboHotel.Users;
using Plus.Communication.Packets.Incoming;

namespace Plus.HabboHotel.Rooms.Chat.Commands
{
    public class CommandManager
    {
        private string _prefix = ":";
        private readonly Dictionary<string, IChatCommand> _commands;

        public CommandManager(string Prefix)
        {
            this._prefix = Prefix;
            this._commands = new Dictionary<string, IChatCommand>();

            this.RegisterAll();
        }

        public bool Parse(GameClient Session, string Message)
        {
            if (Session == null || Session.GetHabbo() == null || Session.GetHabbo().CurrentRoom == null)
                return false;

            if (!Message.StartsWith(_prefix))
                return false;

            if (Message == _prefix + "commands")
            {
                switch (Convert.ToBoolean(ConfigurationData.data["enable.new.cmd"]))
                {
                    case true:
                        var RankRequired = Session.GetHabbo().Rank;
                        Session.Send(new SpecialNotificationComposer(LocaleCommands.Get("", "%rank%", RankRequired)));
                        break;

                    case false:
                        StringBuilder List = new StringBuilder();
                        List.Append(LocaleCommands.Get("list.for.commands"));
                        foreach (var CmdList in _commands.ToList())
                        {
                            if (!string.IsNullOrEmpty(CmdList.Value.PermissionRequired))
                            {
                                if (!Session.GetHabbo().GetPermissions().HasCommand(CmdList.Value.PermissionRequired))
                                    continue;
                            }

                            List.Append(":" + CmdList.Key + " " + CmdList.Value.Parameters + " - " + CmdList.Value.Description + "\n");
                        }
                        Session.Send(new MOTDNotificationComposer(List.ToString()));
                        break;
                }
                return true;
            }

            Message = Message.Substring(1);
            string[] Split = Message.Split(' ');

            if (Split.Length == 0)
                return false;

            IChatCommand Cmd = null;
            if (_commands.TryGetValue(Split[0].ToLower(), out Cmd))
            {
                if (Session.GetHabbo().GetPermissions().HasRight("mod_tool"))
                    this.LogCommand(Session.GetHabbo().Id, Message, Session.GetHabbo().MachineId, Session.GetHabbo().Username);

                if (!string.IsNullOrEmpty(Cmd.PermissionRequired))
                {
                    if (!Session.GetHabbo().GetPermissions().HasCommand(Cmd.PermissionRequired))
                        return false;
                }


                Session.GetHabbo().IChatCommand = Cmd;
                Session.GetHabbo().CurrentRoom.GetWired().TriggerEvent(WiredBoxType.TriggerUserSaysCommand, Session.GetHabbo(), this);

                Cmd.Execute(Session, Session.GetHabbo().CurrentRoom, Split);
                return true;
            }
            return false;
        }
        private void RegisterAll()
        {
            this.Register(LocaleCommands.Get("abTextEmulator.name"), new InfoCommand());
            this.Register(LocaleCommands.Get("sanction.name"), new SanctionCommand());
            this.Register("pickall", new PickAllCommand());
            this.Register("ejectall", new EjectAllCommand());
            this.Register("lay", new LayCommand());
            this.Register("sit", new SitCommand());
            this.Register("stand", new StandCommand());
            this.Register("mutepets", new MutePetsCommand());
            this.Register("mutebots", new MuteBotsCommand());
            this.Register(LocaleCommands.Get("spull.name"), new SuperPullCommand());
            this.Register(LocaleCommands.Get("eha.name"), new EventAlertCommand());
            this.Register(LocaleCommands.Get("maintenance.name"), new MaintenanceCommand());
            this.Register("mimic", new MimicCommand());
            this.Register("dance", new DanceCommand());
            this.Register("push", new PushCommand());
            this.Register("pull", new PullCommand());
            this.Register("enable", new EnableCommand());
            this.Register("follow", new FollowCommand());
            this.Register("faceless", new FacelessCommand());
            this.Register("moonwalk", new MoonwalkCommand());

            this.Register("unload", new UnloadCommand());
            this.Register("regenmaps", new RegenMaps());
            this.Register("emptyitems", new EmptyItems());
            this.Register("setmax", new SetMaxCommand());
            this.Register("setspeed", new SetSpeedCommand());
            this.Register("disablediagonal", new DisableDiagonalCommand());
            this.Register("flagme", new FlagMeCommand());

            this.Register("stats", new StatsCommand());
            this.Register("kickpets", new KickPetsCommand());
            this.Register("kickbots", new KickBotsCommand());

            this.Register("room", new RoomCommand());
            this.Register("dnd", new DNDCommand());
            this.Register("disablegifts", new DisableGiftsCommand());
            this.Register("convertcredits", new ConvertCreditsCommand());
            this.Register("disablewhispers", new DisableWhispersCommand());
            this.Register("disablemimic", new DisableMimicCommand()); ;

            this.Register("pet", new PetCommand());
            this.Register("spush", new SuperPushCommand());
            this.Register("superpush", new SuperPushCommand());
            this.Register(LocaleCommands.Get("notification.command.name"), new NotificationSwitchCommand());
            
            this.Register("ban", new BanCommand());
            this.Register("mip", new MIPCommand());
            this.Register("ipban", new IPBanCommand());

            this.Register("ui", new UserInfoCommand());
            this.Register("userinfo", new UserInfoCommand());
            this.Register("sa", new AlertCommand());
            this.Register("roomunmute", new RoomUnmuteCommand());
            this.Register("roommute", new RoomMuteCommand());
            this.Register("roombadge", new RoomBadgeCommand());
            this.Register("roomalert", new RoomAlertCommand());
            this.Register("roomkick", new RoomKickCommand());
            this.Register("mute", new MuteCommand());
            this.Register("smute", new MuteCommand());
            this.Register("unmute", new UnmuteCommand());
            this.Register("massbadge", new MassBadgeCommand());
            this.Register("massgive", new MassGiveCommand());
            this.Register("globalgive", new GlobalGiveCommand());
            this.Register("kick", new KickCommand());
            this.Register("skick", new KickCommand());
            this.Register("ha", new HotelAlertCommand());
            this.Register("hotelalert", new HotelAlertCommand());
            this.Register("hal", new HALCommand());
            this.Register("give", new GiveCommand());
            this.Register("givebadge", new GiveBadgeCommand());
            this.Register("dc", new DisconnectCommand());
            this.Register("kill", new DisconnectCommand());
            this.Register("disconnect", new DisconnectCommand());
            this.Register("alert", new AlertCommand());
            this.Register("tradeban", new TradeBanCommand());

            this.Register("teleport", new TeleportCommand());
            this.Register("summon", new SummonCommand());
            this.Register("override", new OverrideCommand());
            this.Register("massenable", new MassEnableCommand());
            this.Register("massdance", new MassDanceCommand());
            this.Register("freeze", new FreezeCommand());
            this.Register("unfreeze", new UnFreezeCommand());
            this.Register("fastwalk", new FastwalkCommand());
            this.Register("superfastwalk", new SuperFastwalkCommand());
            this.Register("coords", new CoordsCommand());
            this.Register("alleyesonme", new AllEyesOnMeCommand());
            this.Register("allaroundme", new AllAroundMeCommand());
            this.Register("forcesit", new ForceSitCommand());

            this.Register("ignorewhispers", new IgnoreWhispersCommand());
            this.Register("forced_effects", new DisableForcedFXCommand());

            this.Register("makesay", new MakeSayCommand());
            this.Register("flaguser", new FlagUserCommand());

            this.Register("bubble", new BubbleCommand());
            this.Register("update", new UpdateCommand());
            this.Register("deletegroup", new DeleteGroupCommand());
            this.Register("carry", new CarryCommand());
            this.Register("goto", new GOTOCommand());

            // Roleplay commands
            this.Register("hit", new HitCommand());
            this.Register("shoot", new ShootCommand());
            this.Register("inv", new InventoryCommand());
            this.Register("eq", new EquipCommand());
            this.Register("equip", new EquipCommand());
            this.Register("uneq", new UnequipCommand());
            this.Register("unequip", new UnequipCommand());
            this.Register("reload", new ReloadCommand());
            this.Register("heal", new HealCommand());
            this.Register("startwork", new StartWorkCommand());
            this.Register("stopwork", new StopWorkCommand());
            this.Register("stun", new StunCommand());
            this.Register("cuff", new CuffCommand());
            this.Register("arrest", new ArrestCommand());
            this.Register("w", new WarrantCommand());
            this.Register("warrant", new WarrantCommand());
            this.Register("hire", new HireCommand());
            this.Register("fire", new FireCommand());
            this.Register("quitjob", new QuitJobCommand());
            this.Register("withdraw", new WithdrawCommand());
            this.Register("deposit", new DepositCommand());
            this.Register("taxi", new TaxiCommand());
            this.Register("sellweapon", new SellWeaponCommand());
            this.Register("buyweapon", new BuyWeaponCommand());
            this.Register("listweapons", new ListWeaponsCommand());
            this.Register("lw", new ListWeaponsCommand());
            this.Register("id", new RoomIdCommand());
            this.Register("buyammo", new BuyAmmoCommand());
            this.Register("sellroom", new SellRoomCommand());
            this.Register("removewarrant", new RemoveWarrantCommand());
            this.Register("rw", new RemoveWarrantCommand());
            this.Register("viewwarrants", new ViewWarrantsCommand());
            this.Register("vw", new ViewWarrantsCommand());
            this.Register("gamble", new GambleCommand());
            this.Register("sendhome", new SendHomeCommand());
            this.Register("buyroom", new BuyRoomCommand());
            this.Register("train", new TrainCommand());
            this.Register("setbounty", new SetBountyCommand());
            this.Register("bounties", new BountiesCommand());
            this.Register("drive", new DriveCommand());
            this.Register("pay", new PayCommand());
            this.Register("wire", new WireCommand());
            this.Register("rooms", new HotRoomsCommand());
            this.Register("hotrooms", new HotRoomsCommand());
            this.Register("timeleft", new TimeLeftCommand());
            this.Register("balance", new BalanceCommand());
            this.Register("robvault", new RobVaultCommand());
            this.Register("kisses", new KissesCommand());
            this.Register("kissu", new KissesCommand());
            this.Register("slap", new SlapCommand());
            this.Register("spit", new SpitCommand());
            this.Register("sell", new SellCommand());
            this.Register("accept", new AcceptCommand());
        }

        public void Register(string CommandText, IChatCommand Command)
        {
            this._commands.Add(CommandText, Command);
        }

        public static string MergeParams(string[] Params, int Start)
        {
            var Merged = new StringBuilder();
            for (int i = Start; i < Params.Length; i++)
            {
                if (i > Start)
                    Merged.Append(" ");
                Merged.Append(Params[i]);
            }

            return Merged.ToString();
        }

        public void LogCommand(int UserId, string Data, string MachineId, string Username)
        {
            using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("INSERT INTO `logs_client_staff` (`user_id`, `data_string`, `machine_id`, `timestamp`, `username`) VALUES (@UserId, @Data, @MachineId, @Timestamp, @Username)");
                dbClient.AddParameter("UserId", UserId);
                dbClient.AddParameter("Data", Data);
                dbClient.AddParameter("MachineId", MachineId);
                dbClient.AddParameter("Timestamp", PlusEnvironment.GetUnixTimestamp());
                dbClient.AddParameter("Username", Username);
                dbClient.RunQuery();
            }
                GameClientManager.Alert(new NewConsoleMessageComposer(0x7fffffff, LanguageLocale.Value("message.alert.commands", "%user%", Username, "%commands%", Data), 0, 0x7fffffff));
        }
        

        public bool TryGetCommand(string Command, out IChatCommand IChatCommand)
        {
            return this._commands.TryGetValue(Command, out IChatCommand);
        }
    }
}
