﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Rooms.Chat.Logs;
using Plus.HabboHotel.Rooms.Chat.Filter;
using Plus.HabboHotel.Rooms.Chat.Emotions;
using Plus.HabboHotel.Rooms.Chat.Commands;
using Plus.HabboHotel.Rooms.Chat.Pets.Commands;
using Plus.HabboHotel.Rooms.Chat.Pets.Locale;
using log4net;
using Plus.HabboHotel.Rooms.Chat.Styles;

namespace Plus.HabboHotel.Rooms.Chat
{
    public sealed class ChatManager
    {
        private static ChatEmotionsManager _emotions;
        private static ChatlogManager _logs;
        private static WordFilterManager _filter;
        private static CommandManager _commands;
        private static PetCommandManager _petCommands;
        private static PetLocale _petLocale;
        private static ChatStyleManager _chatStyles;


        public static void Init()
        {
            _emotions = new ChatEmotionsManager();
            _logs = new ChatlogManager();
         
            _filter = new WordFilterManager();
            _filter.Init();

            _commands = new CommandManager(":");
            _petCommands = new PetCommandManager();
            _petLocale = new PetLocale();
      
            _chatStyles = new ChatStyleManager();
            _chatStyles.Init();

            TextEmulator.WriteLine("Chat Manager -> LOADED");
        }

        public static ChatEmotionsManager GetEmotions()
        {
            return _emotions;
        }

        public static ChatlogManager GetLogs()
        {
            return _logs;
        }

        public static WordFilterManager GetFilter()
        {
            return _filter;
        }

        public static CommandManager GetCommands()
        {
            return _commands;
        }

        public static PetCommandManager GetPetCommands()
        {
            return _petCommands;
        }

        public static PetLocale GetPetLocale()
        {
            return _petLocale;
        }

        public static ChatStyleManager GetChatStyles()
        {
            return _chatStyles;
        }
    }
}
