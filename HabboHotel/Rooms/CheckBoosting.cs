﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Users;
using Plus.Communication.Packets.Outgoing.Rooms.Chat;
using Plus.Communication.Packets.Outgoing.Rooms;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Communication.Packets.Outgoing.Rooms.Session;

namespace Plus.HabboHotel.Rooms
{
    class CheckBoosting
    {

        public static List<int> XCoordinates = new List<int>();
        public static List<int> YCoordinates = new List<int>();
    //    private static int SpotDetection;
        private static GameClient mClient;
        static int HabboId;

        public static void BoostingMovement(Room room, GameClient RecieveClient, int IdOfHabbo, int x, int y)
        {
            HabboId = IdOfHabbo;
            mClient = RecieveClient;

            XCoordinates.Add(x);
            YCoordinates.Add(y);

            if (XCoordinates.Count == 200)
            {

                for (int i = XCoordinates.Count - 1; i > (XCoordinates.Count / 2); i--)
                {
                    if (XCoordinates[i] == XCoordinates[i - 1])
                    {
                        continue;
                    }
                    else
                    {
                        GetClient().GetHabbo().BoostingCheck = false;
                        return;

                    }
                }
                room.GetRoomUserManager().RemoveUserFromRoom(RecieveClient, true, true);
                GetClient().Send(new RoomReadyComposer(3, "You Have Been Kicked for Boosting ! Better Luck Next Time"));
            }
        }

        public static GameClient GetClient()
        {

            if (mClient == null)
                mClient = GameClientManager.GetClientByUserID(HabboId);
            return mClient;
        }

    }
}