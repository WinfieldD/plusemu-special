﻿using System;

using Plus.Net;
using Plus.Core;
using Plus.Communication.Packets.Incoming;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Users;
using Plus.Communication.Interfaces;
using Plus.HabboHotel.Users.UserDataManagement;

using ConnectionManager;

using Plus.Communication.Packets.Outgoing.Sound;
using Plus.Communication.Packets.Outgoing.Rooms.Chat;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Communication.Packets.Outgoing.Navigator;
using Plus.Communication.Packets.Outgoing.Moderation;
using Plus.Communication.Packets.Outgoing.Inventory.AvatarEffects;
using Plus.Communication.Packets.Outgoing.Inventory.Achievements;


using Plus.Communication.Encryption.Crypto.Prng;
using Plus.HabboHotel.Users.Messenger.FriendBar;
using Plus.Communication.Packets.Outgoing.BuildersClub;
using Plus.HabboHotel.Moderation;

using Plus.Database.Interfaces;
using Plus.Utilities;
using Plus.HabboHotel.Achievements;
using Plus.HabboHotel.Subscriptions;
using Plus.HabboHotel.Permissions;
using Plus.Communication.Packets.Outgoing.Notifications;
using Plus.Communication.Packets.Outgoing.Rooms.Session;
using Plus.Communication.Packets.Outgoing.Campaigns;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using Plus.Communication.Packets.Outgoing.AuthentificationSMS;
using System.Data;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using System.Threading;
using Plus.Communication.Packets.Outgoing.Talents;
using Plus.Communication.Packets.Outgoing;
using Plus.Communication.Packets.Outgoing.Purse;
using Plus.Communication.Packets.Outgoing.Messenger;
using Plus.HabboHotel.Users.Navigator.SavedSearches;
using Plus.HabboHotel.Users.Messenger;
using System.Collections.Generic;
using System.Linq;
using Plus.Communication.Packets;
using Plus.HabboHotel.Users.UserData;
using Plus.HabboHotel.Users.Process;
using Plus.HabboHotel.Cache.Process;
namespace Plus.HabboHotel.GameClients
{
    public class GameClient
    {
        private readonly int _id;
        private Habbo _habbo;
        public string MachineId;
        private bool _disconnected;
        public ARC4 RC4Client = null;
        private GamePacketParser _packetParser;
        private ConnectionInformation _connection;
        public int PingCount { get; set; }

        public GameClient(int ClientId, ConnectionInformation pConnection)
        {
            this._id = ClientId;
            this._connection = pConnection;
            this._packetParser = new GamePacketParser(this);

            this.PingCount = 0;
        }

        private void SwitchParserRequest()
        {
            _packetParser.SetConnection(_connection);
            _packetParser.onNewPacket += parser_onNewPacket;
            byte[] data = (_connection.parser as InitialPacketParser).currentData;
            _connection.parser.Dispose();
            _connection.parser = _packetParser;
            _connection.parser.handlePacketData(data);
        }

        private void parser_onNewPacket(ClientPacket Message)
        {
            try
            {
                PacketManager.TryExecutePacket(this, Message);
            }
            catch (Exception e)
            {
                Logging.LogPacketException(Message.ToString(), e.ToString());
            }
        }

        private void PolicyRequest()
        {
            _connection.SendData(PlusEnvironment.GetDefaultEncoding().GetBytes("<?xml version=\"1.0\"?>\r\n" +
                   "<!DOCTYPE cross-domain-policy SYSTEM \"/xml/dtds/cross-domain-policy.dtd\">\r\n" +
                   "<cross-domain-policy>\r\n" +
                   "<allow-access-from domain=\"*\" to-ports=\"1-31111\" />\r\n" +
                   "</cross-domain-policy>\x0"));
        }


        public void StartConnection()
        {
            if (_connection == null)
                return;

            this.PingCount = 0;

            (_connection.parser as InitialPacketParser).PolicyRequest += PolicyRequest;
            (_connection.parser as InitialPacketParser).SwitchParserRequest += SwitchParserRequest;
            _connection.startPacketProcessing();
        }

        public bool TryAuthenticate(string AuthTicket)
        {
            try
            {
                byte errorCode = 0;
                UserData userData = UserDataFactory.GetUserData(AuthTicket, out errorCode);
                if (errorCode == 1 || errorCode == 2)
                {
                    Disconnect();
                    return false;
                }

                #region Ban Checking
                //Let's have a quick search for a ban before we successfully authenticate..
                ModerationBan BanRecord = null;
                if (!string.IsNullOrEmpty(MachineId))
                {
                    if (ModerationManager.IsBanned(MachineId, out BanRecord))
                    {
                        if (ModerationManager.MachineBanCheck(MachineId))
                        {
                            Disconnect();
                            return false;
                        }
                    }
                }

                if (userData.user != null)
                {
                    //Now let us check for a username ban record..
                    BanRecord = null;
                    if (ModerationManager.IsBanned(userData.user.Username, out BanRecord))
                    {
                        if (ModerationManager.UsernameBanCheck(userData.user.Username))
                        {
                            Disconnect();
                            return false;
                        }
                    }
                }
                #endregion

                GameClientManager.RegisterClient(this, userData.userID, userData.user.Username);
                _habbo = userData.user;
                if (_habbo != null)
                {
                    userData.user.Init(this, userData);

                    Send(new AuthenticationOKComposer());
                    Send(new AvatarEffectsComposer(_habbo.Effects().GetAllEffects));
                    //FurniListNotification -> why?
                    Send(new NavigatorSettingsComposer(_habbo.HomeRoom));
                    Send(new FavouritesComposer(userData.user.FavoriteRooms));
                    Send(new FigureSetIdsComposer(_habbo.GetClothing().GetClothingAllParts));
                    //1984
                    //2102
                    Send(new UserRightsComposer(_habbo));
                    Send(new AvailabilityStatusComposer());
                    //1044
                    Send(new AchievementScoreComposer(_habbo.GetStats().AchievementPoints));
                    //3674
                    //3437
                    Send(new BuildersClubMembershipComposer());
                    Send(new CfhTopicsInitComposer(ModerationManager.UserActionPresets));

                    Send(new BadgeDefinitionsComposer(HotelGameManager.GetAchievementManager()._achievements));
                    Send(new SoundSettingsComposer(_habbo.ClientVolume, _habbo.ChatPreference, _habbo.AllowMessengerInvites, _habbo.FocusPreference, FriendBarStateUtility.GetInt(_habbo.FriendbarState)));
                    Send(new TalentTrackLevelComposer());
                    SearchesComponent.Init(_habbo);
                    ProcessComponent.Init(_habbo);

                    var habboGetExpired = HabboClubManager.HasSubscription("habbo_vip");
                    switch (habboGetExpired)
                    {
                        case true:
                            Send(new HCGiftsAlertComposer(1));
                            break;

                        case false:
                            Send(NotificationManager.Bubble("", "tu n'est plus HC?", ""));
                            break;
                    }


                    if (!string.IsNullOrEmpty(MachineId))
                    {
                        if (this._habbo.MachineId != MachineId)
                        {
                            using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                            {
                                dbClient.SetQuery("UPDATE `users` SET `machine_id` = @MachineId WHERE `id` = @id LIMIT 1");
                                dbClient.AddParameter("MachineId", MachineId);
                                dbClient.AddParameter("id", _habbo.Id);
                                dbClient.RunQuery();
                            }
                        }

                        _habbo.MachineId = MachineId;
                    }

                    PermissionGroup PermissionGroup = null;
                    if (HotelGameManager.GetPermissionManager().TryGetGroup(_habbo.Rank, out PermissionGroup))
                    {
                        if (!String.IsNullOrEmpty(PermissionGroup.Badge))
                            if (!_habbo.GetBadgeComponent().HasBadge(PermissionGroup.Badge))
                                _habbo.GetBadgeComponent().GiveBadge(PermissionGroup.Badge, true, this);
                    }

                    SubscriptionData SubData = null;
                    if (HotelGameManager.GetSubscriptionManager().TryGetSubscriptionData(this._habbo.VIPRank, out SubData))
                    {
                        if (!String.IsNullOrEmpty(SubData.Badge))
                        {
                            if (!_habbo.GetBadgeComponent().HasBadge(SubData.Badge))
                                _habbo.GetBadgeComponent().GiveBadge(SubData.Badge, true, this);
                        }
                    }

                    if (!HotelGameManager.GetCacheManager().ContainsUser(_habbo.Id))
                        HotelGameManager.GetCacheManager().GenerateUser(_habbo.Id);

                    ProcessComponents.Init();
                    Send(new CampaignCalendarDataComposer(_habbo.GetStats().openedGifts));
                   
                    if (GetHabbo().NewUser)
                    {
                        Send(new SpecialNotificationComposer(NuxStepMessageKeyManager.GetNuxStepKey(NuxListMessage.HIDE)));
                        HabboClubManager.AddOrExtendSubscription("habbo_vip", 31 * 24 * 3600, this);
                        this.GetHabbo().GetBadgeComponent().GiveBadge("HC1", true, this);
                    }

                    if (GetHabbo().BonusPoints >= StaticSettings.BonusRareTotalScore)
                    {
                        using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.SetQuery("UPDATE `users` SET `user_points` = @points WHERE `id` = @id LIMIT 1");
                            dbClient.AddParameter("points", GetHabbo().BonusPoints == 0);
                            dbClient.AddParameter("id", GetHabbo().Id);
                            dbClient.RunQuery();
                        }
                    }

                    Send(new SpecialNotificationComposer("habbopages/cmds.php?message=testtestbibi"));
                

                    switch (Convert.ToBoolean(ConfigurationData.data["enable.beta.access"]) && GetHabbo().GetPermissions().HasRight("mod_tool"))
                    {
                        case true:
                            Send(new VerifyMobilePhoneWindowComposer(1, 1));

                            var Key = ConfigurationData.data["key.access"];

                            try
                            {
                                DataRow UserData = null;
                                using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                                {
                                    dbClient.SetQuery("SELECT `id`,`username`,`mail` FROM users WHERE `username` = @Username LIMIT 1");
                                    dbClient.AddParameter("Username", userData.user.Username);
                                    UserData = dbClient.getRow();
                                }
                            }
                            catch
                            {
                                Send(NotificationManager.Custom(LanguageLocale.Value("message.value.key", "%user%", GetHabbo().Username, "%rank%", GetHabbo().Rank)));
                            }
                            PlusEnvironment.KeyPass = Key;
                            userData.user.PinStaff = true;
                            break;

                        case false:
                            GameClientManager.Alert(NotificationManager.Custom(";security.message.disabled"));
                            break;
                    }

                    ICollection<MessengerBuddy> Friends = new List<MessengerBuddy>();
                    foreach (MessengerBuddy Buddy in this.GetHabbo().GetMessenger().GetFriends().ToList())
                    {
                        if (Buddy == null)
                            continue;

                        GameClient Friend = GameClientManager.GetClientByUserID(Buddy.Id);
                        if (Friend == null)
                            continue;
                        string figure = this.GetHabbo().Look;


                        Friend.Send(NotificationManager.Bubble("", "", ""));
                    }
                    HotelGameManager.GetRewardManager().CheckRewards(this);
                    return true;
                }
            }
            catch (Exception e)
            {
                Logging.LogCriticalException("Bug during user login: " + e);
            }
            return false;
        }

        public void SendWhisper(string Message, int Colour = 33)
        {
            if (this == null || GetHabbo() == null || GetHabbo().CurrentRoom == null)
                return;

            RoomUser User = GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(GetHabbo().Username);
            if (User == null)
                return;

            Send(new WhisperComposer(User.VirtualId, Message, 0, (Colour == 0 ? User.LastBubble : Colour)));
        }

        public void SendNotification(string Message)
        {
            if (Message.StartsWith(";"))
            {
                string[] strArray1 = Message.Split('\t');
                string key = strArray1[0].Substring(1);
                string[] strArray2 = new string[0];
                if (strArray1.Length > 1)
                    strArray2 = strArray1[1].Split(',');
                Message = LanguageLocale.Value(key, strArray2);
            }

            Send(new BroadcastMessageAlertComposer(Message));
        }

        public void Send(IServerPacket Message)
        {
            byte[] bytes = Message.GetBytes();

            if (Message == null)
                return;

            if (GetConnection() == null)
                return;

            GetConnection().SendData(bytes);
        }

        public void SendStaffMessage(IServerPacket Message)
        {
            if (GetHabbo().GetPermissions().HasRight("mod_tickets"))
            {
                byte[] bytes = Message.GetBytes();

                if (Message == null)
                    return;

                if (GetConnection() == null)
                    return;

                GetConnection().SendData(bytes);
            }
        }

        public int ConnectionID
        {
            get { return _id; }
        }

        public ConnectionInformation GetConnection()
        {
            return _connection;
        }

        public Habbo GetHabbo()
        {
            return _habbo;
        }

        public void DisconnectWithMsg(string Message)
        {
            try
            {
                if (GetHabbo() != null)
                {
                    using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                    {
                        dbClient.RunQuery(GetHabbo().GetQueryString);
                    }

                    GetHabbo().OnDisconnect();
                }

                Send(NotificationManager.Custom(Message));
                Thread.Sleep(2000);
            }
            catch (Exception e)
            {
                Logging.LogException(e.ToString());
            }


            if (!_disconnected)
            {
                if (_connection != null)
                    _connection.Dispose();
                _disconnected = true;
            }
        }
        public void Disconnect()
        {
            try
            {
                if (GetHabbo() != null)
                {
                    using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                    {
                        dbClient.RunQuery(GetHabbo().GetQueryString);
                    }

                    GetHabbo().OnDisconnect();
                }
            }
            catch (Exception e)
            {
                Logging.LogException(e.ToString());
            }


            if (!_disconnected)
            {
                if (_connection != null)
                    _connection.Dispose();
                _disconnected = true;
            }
        }

        public void Dispose()
        {
            if (GetHabbo() != null)
                GetHabbo().OnDisconnect();

            this.MachineId = string.Empty;
            this._disconnected = true;
            this._habbo = null;
            this._connection = null;
            this.RC4Client = null;
            this._packetParser = null;
        }
    }
}