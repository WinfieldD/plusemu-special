﻿using Plus.Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.LandingView
{
    public static class HotelViewLooksManager
    {
        private static Dictionary<int, HotelViewLooks> _BestList;

        public static void Init()
        {
            _BestList = new Dictionary<int, HotelViewLooks>();

            if (_BestList.Count > 0)
                _BestList.Clear();
            using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `server_view`");
                DataTable BestUser = dbClient.getTable();

                if (BestUser != null)
                {
                    foreach (DataRow Row in BestUser.Rows)
                    {
                        if (!_BestList.ContainsKey(Convert.ToInt32(Row["id"])))
                            _BestList.Add(Convert.ToInt32(Row["id"]), new HotelViewLooks(Convert.ToInt32(Row["id"]), Convert.ToString(Row["username"]), Convert.ToString(Row["look"]), Convert.ToInt32(Row["place"])));
                    }
                }
            }
        }

        public static ICollection<HotelViewLooks> GetHotelLookUser()
        {
            return _BestList.Values;
        }
    }
}