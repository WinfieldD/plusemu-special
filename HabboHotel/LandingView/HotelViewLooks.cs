﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.LandingView
{
    public class HotelViewLooks
    {
        public int Id;
        public string Username;
        public string Look;
        public int Place;

        public HotelViewLooks(int id, string username, string look, int place)
        {
            this.Id = id;
            this.Username = username;
            this.Look = look;
            this.Place = place;
        }
    }
}
