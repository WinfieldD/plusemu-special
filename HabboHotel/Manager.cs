﻿using Plus.Core;
using Plus.HabboHotel.Camera;
using Plus.HabboHotel.Global;
using Plus.HabboHotel.LandingView;
using Plus.HabboHotel.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel
{
    class Manager
    {
        public static void InitSettings()
        {
            LanguageLocale.Init();
            SettingsManager.Init();
            LocaleCommands.Init();
            ServerConfiguration.Init();
            SettingsHotelManager.Init();
        }

        public static void InitServer()
        {
            ServerStatusUpdater.Init();
        }

        public static void InitComponent()
        {
            HotelViewLooksManager.Init();
            SettingsCamera.Init();
            CameraPhotoManager.LoadItem(HotelGameManager._itemDataManager);
            CameraPhotoManager.Init();
        }
    }
}
