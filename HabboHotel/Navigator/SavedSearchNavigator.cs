﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Plus.HabboHotel.Navigator
{
    public class SavedSearchNavigator
    {
        private int _id;
        private string _searchCode;
        private string _filter;

        public SavedSearchNavigator(int Id, string SearchCode, string Filter)
        {
            this._id = Id;
            this._searchCode = SearchCode;
            this._filter = Filter;
        }

        public int Id
        {
            get { return this._id; }
            set { this._id = value; }
        }

        public string SearchCode
        {
            get { return this._searchCode; }
            set { this._searchCode = value; }
        }

        public string Filter
        {
            get { return this._filter; }
            set { this._filter = value; }
        }
    }
}