﻿using Plus.Database.Interfaces;
using Plus.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Navigator
{
    class NavigatorSavedManager
    {
        public int Id;
        public string Filter;
        public string SearchCode;

        public NavigatorSavedManager(int id, string filter, string searchCode)
        {
            Id = id;
            Filter = filter;
            SearchCode = searchCode;
        }
    }
}
