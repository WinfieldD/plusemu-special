﻿using System.Linq;
using System.Collections.Generic;

using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Incoming;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using Plus.Communication.Packets.Outgoing.Quests;

using Plus.Database.Interfaces;
using log4net;
using Plus.HabboHotel.Items;
using Plus.Core;

namespace Plus.HabboHotel.Camera
{
    /**
     * Mainly loads data by receiving RCON data of the
     * Camera front-end
    **/
    public static class CameraPhotoManager
    {

        private static Dictionary<int, CameraPhotoPreview> _previews;

        private static string _previewPath = "preview/{1}-{0}.png";
        private static string _purchasedPath = "purchased/{1}-{0}.png";
        private static int _maxPreviewCacheCount = 1000;

        private static ItemData _photoPoster;

        public static ItemData PhotoPoster
        {
            get
            {
                return _photoPoster;
            }
        }

        public static void Init()
        {
            _previews = new Dictionary<int, CameraPhotoPreview>();
            ConfigurationData.data.TryGetValue("camera.path.preview", out _previewPath);
            ConfigurationData.data.TryGetValue("camera.path.purchased", out _purchasedPath);
            if (ConfigurationData.data.ContainsKey("camera.preview.maxcache"))
            {
                _maxPreviewCacheCount = int.Parse(ConfigurationData.data["camera.preview.maxcache"]);
            }

            TextEmulator.WriteLine("Gestionnaire de photos -> CHARGÉ");
        }

        public static void LoadItem(ItemDataManager itemData)
        {
            int ItemId = int.Parse(SettingsCamera.Value("camera.photo.purchase.item_id"));

            if (!itemData.GetItem(ItemId, out _photoPoster))
                return;
        }

        public static CameraPhotoPreview GetPreview(int PhotoId)
        {
            if (!_previews.ContainsKey(PhotoId))
            {
                return null;
            }

            return _previews[PhotoId];
        }
        public static void AddPreview(CameraPhotoPreview preview)
        {
            if (_previews.Count >= _maxPreviewCacheCount)
            {
                _previews.Remove(_previews.Keys.First());
            }

            _previews.Add(preview.Id, preview);
        }

        public static string GetPath(CameraPhotoType type, int PhotoId, int CreatorId)
        {
            string path = "{1}-{0}.png";

            switch (type)
            {
                case CameraPhotoType.PREVIEW:
                    path = _previewPath;
                    break;
                case CameraPhotoType.PURCHASED:
                    path = _purchasedPath;
                    break;
            }
            return string.Format(path, PhotoId, CreatorId);
        }
    }
}