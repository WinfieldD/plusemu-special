﻿using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;

using Plus.Communication.Packets.Incoming;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Users;
using Plus.Communication.Packets.Outgoing.Rooms.Chat;
using System.Globalization;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Rooms.Games.Teams;

namespace Plus.HabboHotel.Items.Wired.Boxes.Effects
{/*
    class AddGiveScoreBoxTeam : IWiredItem, IWiredCycle
    {
        public Room Instance { get; set; }
        public Item Item { get; set; }
        public WiredBoxType Type { get { return WiredBoxType.EffectGiveScoreTeam; } }
        public ConcurrentDictionary<int, Item> SetItems { get; set; }
        public string StringData { get; set; }
        public bool BoolData { get; set; }
        public double Delay { get { return this._delay; } set { this._delay = value; this.TickCount = value + 1; } }
        public double TickCount { get; set; }
        public string ItemsData { get; set; }

        private Queue _queue;
        private double _delay = 0;
        private TEAM _team;

        public AddGiveScoreBoxTeam(Room Instance, Item Item)
        {
            this.Instance = Instance;
            this.Item = Item;
            this.SetItems = new ConcurrentDictionary<int, Item>();

            this._queue = new Queue();
            this.TickCount = Delay;
        }

        public void HandleSave(ClientPacket Packet)
        {
            int Unknown = Packet.ReadInteger();
            int score = Packet.ReadInteger();
            int times = Packet.ReadInteger();
            int team = Packet.ReadInteger();
            int Unknown3 = Packet.ReadInteger();
            int Delay = Packet.ReadInteger();

            this.Delay = Delay;
            this.StringData = Convert.ToString(score + ";" + times + ";" + team);

            // this.Delay = Packet.ReadInteger();
        }

        public bool OnCycle()
        {
            if (_queue.Count == 0)
            {
                this._queue.Clear();
                this.TickCount = Delay;
                return true;
            }

            while (_queue.Count > 0)
            {
                Habbo Player = (Habbo)_queue.Dequeue();
                if (Player == null || Player.CurrentRoom != Instance)
                    continue;

                this.TeleportUser(Player);
            }

            this.TickCount = Delay;
            return true;
        }

        public bool Execute(params object[] Params)
        {
            if (Params == null || Params.Length == 0)
                return false;

            Habbo Player = (Habbo)Params[0];

            if (Player == null)
                return false;

            RoomUser User = Instance.GetRoomUserManager().GetRoomUserByHabbo(Player.Id);
            if (User == null)
                return false;

            this._team = User.Team;

            this._queue.Enqueue(Player);
            return true;
        }

        private void TeleportUser(Habbo Player)
        {
            RoomUser User = Player.CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(Player.Id);
            if (User == null)
                return;

            Room Instance = Player.CurrentRoom;
            TeamManager t = User.GetClient().GetHabbo().CurrentRoom.GetTeamManagerForFreeze();

            int mScore = int.Parse(StringData.Split(';')[0]) * int.Parse(StringData.Split(';')[1]);
            int mTeam = int.Parse(StringData.Split(';')[2]);
            string sTeam = "";
            bool bTeam = true;
            string TeamMembers = "";
            List<RoomUser> cTeam = new List<RoomUser>();

            switch (mTeam)
            {
                case 1:
                    sTeam = "Red";
                    bTeam = t.RedTeam.Any();
                    cTeam = t.RedTeam;
                    break;
                case 2:
                    sTeam = "Green";
                    bTeam = t.GreenTeam.Any();
                    cTeam = t.GreenTeam;
                    break;
                case 3:
                    sTeam = "Blue";
                    bTeam = t.BlueTeam.Any();
                    cTeam = t.BlueTeam;
                    break;
                case 4:
                    sTeam = "Yellow";
                    bTeam = t.YellowTeam.Any();
                    cTeam = t.YellowTeam;
                    break;
                default:
                    sTeam = "Ningun Equipo";
                    bTeam = true;
                    break;
            }

            if (Instance != null || User != null)
            {

                if (bTeam)
                {
                    foreach (RoomUser _cteam in cTeam)
                    {
                        TeamMembers += _cteam.GetUsername() + ",";
                    }

                    if (Instance.GetRoomItemHandler().wf_highscore_count != 0)
                    {
                        foreach (Item item in Instance.GetRoomItemHandler().GetFloor.ToList())
                        {
                            if (item.GetBaseItem().InteractionType == InteractionType.WIRED_HIGHSCORE)
                            {
                                string Type = item.GetBaseItem().ItemName.Split('_')[1].Split('*')[0];
                                int Sort = Convert.ToInt32(item.GetBaseItem().ItemName.Split('_')[1].Split('*')[1]);

                                Instance.GetRoomItemHandler().InsertScoreOnWired(TeamMembers, mScore, Type, Sort, item);
                            }
                        }
                        //Instance.GetRoomItemHandler().UpdateWiredScoreBoard();
                    }

                }
            }
            else
                return;
        }
    }
}
}
*/

}