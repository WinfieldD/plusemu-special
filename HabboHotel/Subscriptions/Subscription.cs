﻿using Plus;
using Plus.Core;

namespace Plus.HabboHotel.Subscriptions
{
    public class Subscription
    {
        private readonly string Caption;
        private int TimeExpire;
        private int ActivateTimes;

        internal string SubscriptionId
        {
            get
            {
                return this.Caption;
            }
        }

        internal int ExpireTime
        {
            get
            {
                return this.TimeExpire;
            }
        }


        internal int ActivateTime
        {
            get
            {
                return this.ActivateTimes;
            }
        }

        internal Subscription(string Caption, int TimeExpire, int ActivateTimes)
        {
            this.Caption = Caption;
            this.TimeExpire = TimeExpire;
            this.ActivateTimes = ActivateTimes;
        }

        internal bool IsValid()
        {
            return this.TimeExpire > PlusEnvironment.GetUnixTimestamp();
        }

        internal void SetEndTime(int time)
        {
            this.TimeExpire = time;
        }

        internal void ExtendSubscription(int Time)
        {
            try
            {
                this.TimeExpire = (this.TimeExpire + Time);
            }
            catch
            {
            }
        }
    }
}
