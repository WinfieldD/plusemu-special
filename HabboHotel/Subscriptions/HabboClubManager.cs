﻿using Plus.HabboHotel.Subscriptions;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Database.Interfaces;
using Plus.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using Plus.HabboHotel.Users.UserDataManagement;
using Plus.HabboHotel.Users.UserData;

namespace Plus.HabboHotel.Subscriptions
{
    public static class HabboClubManager
    {
        private static int UserId;
        private static Dictionary<string, Subscription> Subscriptions;

        public static void Init(int userID, UserData userData)
        {
            UserId = userID;
            Subscriptions = userData.subscriptions;
        }

        public static void Clear()
        {
            Subscriptions.Clear();
        }

        public static Subscription GetSubscription(string SubscriptionId)
        {
            if (Subscriptions.ContainsKey(SubscriptionId))
            {
                return Subscriptions[SubscriptionId];
            }
            else
            {
                return null;
            }
        }

        public static bool HasSubscription(string SubscriptionId)
        {
            if (!Subscriptions.ContainsKey(SubscriptionId))
            {
                return false;
            }

            Subscription subscription = Subscriptions[SubscriptionId];
            return subscription.IsValid();
        }

        public static void AddOrExtendSubscription(string SubscriptionId, int DurationSeconds, GameClient Session)
        {
            SubscriptionId = SubscriptionId.ToLower();

            var clientByUserId = GameClientManager.GetClientByUserID(UserId);
            if (Subscriptions.ContainsKey(SubscriptionId))
            {
                Subscription subscription = Subscriptions[SubscriptionId];

                if (subscription.IsValid())
                {
                    subscription.ExtendSubscription(DurationSeconds);
                }
                else
                {
                    subscription.SetEndTime((int)PlusEnvironment.GetUnixTimestamp() + DurationSeconds);
                }

                using (IQueryAdapter adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                {
                    adapter.SetQuery(string.Concat(new object[] { "UPDATE user_subscriptions SET timestamp_expire = ", subscription.ExpireTime, " WHERE user_id = ", UserId, " AND subscription_id = '", subscription.SubscriptionId, "'" }));
                    adapter.RunQuery();
                }
                HotelGameManager.GetAchievementManager().TryProgressHabboClubAchievements(clientByUserId);
            }
            else
            {
                int unixTimestamp = (int)PlusEnvironment.GetUnixTimestamp();
                int timeExpire = (int)PlusEnvironment.GetUnixTimestamp() + DurationSeconds;
                string SubscriptionType = SubscriptionId;
                Subscription subscription2 = new Subscription(SubscriptionId, timeExpire, unixTimestamp);

                using (IQueryAdapter adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                {
                    adapter.SetQuery(string.Concat(new object[] { "INSERT INTO user_subscriptions (user_id,subscription_id,timestamp_activated,timestamp_expire) VALUES (", UserId, ",'", SubscriptionType, "',", unixTimestamp, ",", timeExpire, ")" }));
                    adapter.RunQuery();
                }

                Subscriptions.Add(subscription2.SubscriptionId.ToLower(), subscription2);
                HotelGameManager.GetAchievementManager().TryProgressHabboClubAchievements(clientByUserId);
            }
        }


        public static void ReloadSubscription(GameClient Session)
        {
            Session.Send(new UserRightsComposer(Session.GetHabbo()));
        }
    }
}