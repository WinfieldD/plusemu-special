﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Plus
{
    class StaticSettings
    {
        // Update Timers.
        public static int UserCreditsUpdateTimer = Convert.ToInt32(ServerConfiguration.Value("user.credits.updatetimer", 15));
        public static int UserPointsUpdateTimer = Convert.ToInt32(ServerConfiguration.Value("user.points.updatetimer", 15));

        //Limit furni.
        public static int RoomFurnitureLimit = Convert.ToInt32(ServerConfiguration.Value("room.furniture.limit", 7500));
        public static int RoomPetPlacementLimit = Convert.ToInt32(ServerConfiguration.Value("room.petplacement.limit", 25));
        public static int UserPetPlacementRoomLimit = Convert.ToInt32(ServerConfiguration.Value("room.petplacementroom.limit", 25));
        public static int RoomPromotionLifeTime = Convert.ToInt32(ServerConfiguration.Value("room.promotion.lifetime", 120));
        public static int MessengerFriendLimit = Convert.ToInt32(ServerConfiguration.Value("messenger.limit.friends", 5000));
        public static int GroupMemberDeletionLimit = Convert.ToInt32(ServerConfiguration.Value("group.memberdeletation.limit", 500));

        //Amount purchase
        public static int GroupPurchaseAmount = Convert.ToInt32(ServerConfiguration.Value("group.purchase.amount", 150));
        public static int BannedPhrasesAmount = Convert.ToInt32(ServerConfiguration.Value("banned.phrases.amount", 6));
        public static int UserPixelsUpdateAmount = Convert.ToInt32(ServerConfiguration.Value("user.pixels.update.amount", 100));
        public static int UserCreditsUpdateAmount = Convert.ToInt32(ServerConfiguration.Value("user.credits.update.amount", 100));

        //BonusRare
        public static string BonusRareProduct = ServerConfiguration.ValueStr("bonusrare.productdata.name");
        public static string BonusRareRequestClear = ServerConfiguration.ValueStr("bonusrare.quest.clear");
        public static int BonusRareBaseId = Convert.ToInt32(ServerConfiguration.Value("bonusrare.item.baseid", 1546));
        public static int BonusRareTotalScore = Convert.ToInt32(ServerConfiguration.Value("bonusrare.total.score", 200));

        // RolePlay
        public static int HospitalRoomId = Convert.ToInt32(ServerConfiguration.Value("roleplay.hospital.roomid", 2));//2;
        public static int PrisonRoomId = Convert.ToInt32(ServerConfiguration.Value("roleplay.prison.roomid", 5));//5;
        public static int BankRoomId = Convert.ToInt32(ServerConfiguration.Value("roleplay.bank.roomid", 4));//4;
        public static int PrisonReleaseRoomId = Convert.ToInt32(ServerConfiguration.Value("roleplay.prisonrelease.roomid", 2));// 6;
        public static int AmmunationRoomId = Convert.ToInt32(ServerConfiguration.Value("roleplay.ammunation.roomid", 9));//9;
        public static int VaultRoomId = Convert.ToInt32(ServerConfiguration.Value("roleplay.vault.roomid", 7));//7;
        public static int SlotMachineFurniId = Convert.ToInt32(ServerConfiguration.Value("roleplay.slotmachine.furniid", 77711));//77711;
        public static int RedDoorMatFurniId = Convert.ToInt32(ServerConfiguration.Value("roleplay.doormat.furniid", 53));//53;
        public static int AtmMachineFurniId = Convert.ToInt32(ServerConfiguration.Value("roleplay.atmmachine.furniid", 1000000111));//1000000111;
    }
}