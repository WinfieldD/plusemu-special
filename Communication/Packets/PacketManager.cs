﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;

using log4net;

using Plus.Core;
using Plus.Communication.Packets.Incoming;
using Plus.HabboHotel.GameClients;

using Plus.Communication.Packets.Incoming.Catalog;
using Plus.Communication.Packets.Incoming.Handshake;
using Plus.Communication.Packets.Incoming.Navigator;
using Plus.Communication.Packets.Incoming.Quests;
using Plus.Communication.Packets.Incoming.Rooms.Avatar;
using Plus.Communication.Packets.Incoming.Rooms.Chat;
using Plus.Communication.Packets.Incoming.Rooms.Connection;
using Plus.Communication.Packets.Incoming.Rooms.Engine;
using Plus.Communication.Packets.Incoming.Rooms.Action;
using Plus.Communication.Packets.Incoming.Users;
using Plus.Communication.Packets.Incoming.Inventory.AvatarEffects;
using Plus.Communication.Packets.Incoming.Inventory.Purse;
using Plus.Communication.Packets.Incoming.Sound;
using Plus.Communication.Packets.Incoming.Misc;
using Plus.Communication.Packets.Incoming.Inventory.Badges;
using Plus.Communication.Packets.Incoming.Avatar;
using Plus.Communication.Packets.Incoming.Inventory.Achievements;
using Plus.Communication.Packets.Incoming.Inventory.Bots;
using Plus.Communication.Packets.Incoming.Inventory.Pets;
using Plus.Communication.Packets.Incoming.LandingView;
using Plus.Communication.Packets.Incoming.Messenger;
using Plus.Communication.Packets.Incoming.Groups;
using Plus.Communication.Packets.Incoming.Rooms.Settings;
using Plus.Communication.Packets.Incoming.Rooms.AI.Pets;
using Plus.Communication.Packets.Incoming.Rooms.AI.Bots;
using Plus.Communication.Packets.Incoming.Rooms.AI.Pets.Horse;
using Plus.Communication.Packets.Incoming.Rooms.Furni;
using Plus.Communication.Packets.Incoming.Rooms.Furni.RentableSpaces;
using Plus.Communication.Packets.Incoming.Rooms.Furni.YouTubeTelevisions;
using Plus.Communication.Packets.Incoming.Help;
using Plus.Communication.Packets.Incoming.Rooms.FloorPlan;
using Plus.Communication.Packets.Incoming.Rooms.Furni.Wired;
using Plus.Communication.Packets.Incoming.Moderation;
using Plus.Communication.Packets.Incoming.Inventory.Furni;
using Plus.Communication.Packets.Incoming.Rooms.Furni.Stickys;
using Plus.Communication.Packets.Incoming.Rooms.Furni.Moodlight;
using Plus.Communication.Packets.Incoming.Inventory.Trading;
using Plus.Communication.Packets.Incoming.GameCenter;
using Plus.Communication.Packets.Incoming.Marketplace;
using Plus.Communication.Packets.Incoming.Rooms.Furni.LoveLocks;
using Plus.Communication.Packets.Incoming.Talents;
using Plus.Communication.Packets.Incoming.AuthentificationSMS;
using Plus.Communication.Packets.Incoming.Rooms.Camera;
using Plus.Communication.Packets.Incoming.Navigator.New;
using Plus.Communication.Packets.Incoming.Rooms.Nux;
using Plus.HabboHotel.Rooms;

namespace Plus.Communication.Packets
{
    public sealed class PacketManager
    {
        private static bool IgnoreTasks = true;
        private static int MaximumRunTimeInSec = 10;
        private static bool ThrowUserErrors = false;
        private static TaskFactory _eventDispatcher;
        private static Dictionary<int, IPacketEvent> _incomingPackets;
        private static Dictionary<int, string> _packetNames;
        private static ConcurrentDictionary<int, Task> _runningTasks;

        public static void TaskRun()
        {
            _incomingPackets = new Dictionary<int, IPacketEvent>();

            _eventDispatcher = new TaskFactory(TaskCreationOptions.PreferFairness, TaskContinuationOptions.None);
            _runningTasks = new ConcurrentDictionary<int, Task>();
            _packetNames = new Dictionary<int, string>();
        }

        public static void Init()
        {
            TaskRun();

            RegisterHandshake();
            RegisterLandingView();
            RegisterCatalog();
            RegisterMarketplace();
            RegisterNavigator();
            RegisterNewNavigator();
            RegisterRoomAction();
            RegisterQuests();
            RegisterRoomConnection();
            RegisterRoomChat();
            RegisterRoomEngine();
            RegisterFurni();
            RegisterUsers();
            RegisterRoomCamera();
            RegisterSound();
            RegisterMisc();
            RegisterInventory();
            RegisterTalents();
            RegisterPurse();
            RegisterRoomAvatar();
            RegisterAvatar();
            RegisterMessenger();
            RegisterGroups();
            RegisterRoomSettings();
            RegisterPets();
            RegisterBots();
            RegisterHelp();
            FloorPlanEditor();
            RegisterModeration();
            RegisterGameCenter();
            RegisterNames();
        }

        public static void TryExecutePacket(GameClient Session, ClientPacket Packet)
        {
            IPacketEvent Pak = null;
            if (!_incomingPackets.TryGetValue(Packet.Id, out Pak))
            {
                if (System.Diagnostics.Debugger.IsAttached)
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.Write("INCOMING ");
                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.Write("NON MANIPULÉ ");
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.Write(Environment.NewLine + Packet.ToString());
                if (Packet.ToString().Length > 0)
                    Console.WriteLine();
                return;
            }

            if (System.Diagnostics.Debugger.IsAttached)
            {
                if (_packetNames.ContainsKey(Packet.Id))
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write("INCOMING ");
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.Write("MANIPULÉ ");
                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.Write(Environment.NewLine + Packet.ToString());
                if (Packet.ToString().Length > 0)
                    Console.WriteLine();
            }

            if (!IgnoreTasks)
                ExecutePacketAsync(Session, Packet, Pak);
            else
                Pak.Parse(Session, Packet);
        }

        private static void ExecutePacketAsync(GameClient Session, ClientPacket Packet, IPacketEvent Pak)
        {
            DateTime Start = DateTime.Now;
            var CancelSource = new CancellationTokenSource();
            CancellationToken Token = CancelSource.Token;

            Task t = _eventDispatcher.StartNew(() =>
            {
                Pak.Parse(Session, Packet);
                Token.ThrowIfCancellationRequested();
            }, Token);

            _runningTasks.TryAdd(t.Id, t);

            try
            {
                if (!t.Wait(MaximumRunTimeInSec * 1000, Token))
                {
                    CancelSource.Cancel();
                }
            }
            catch (AggregateException ex)
            {
                foreach (Exception e in ex.Flatten().InnerExceptions)
                {
                    if (ThrowUserErrors)
                    {
                        throw e;
                    }
                    else
                    {
                        Session.Disconnect();
                    }
                }
            }
        }

        public static void WaitForAllToComplete()
        {
            foreach (Task t in _runningTasks.Values.ToList())
            {
                t.Wait();
            }
        } 

        public static void UnregisterAll()
        {
            _incomingPackets.Clear();
        }

        public static void ExecutePacket(int key, IPacketEvent Value)
        {
            _incomingPackets.Add(key, Value);
        }

        private static void RegisterHandshake()
        {
            ExecutePacket(ClientPacketHeader.GetClientVersionMessageEvent, new GetClientVersionEvent());
            ExecutePacket(ClientPacketHeader.InitCryptoMessageEvent, new InitCryptoEvent());
            ExecutePacket(ClientPacketHeader.GenerateSecretKeyMessageEvent, new GenerateSecretKeyEvent());
            ExecutePacket(ClientPacketHeader.UniqueIDMessageEvent, new UniqueIDEvent());
            ExecutePacket(ClientPacketHeader.SSOTicketMessageEvent, new SSOTicketEvent());
            ExecutePacket(ClientPacketHeader.InfoRetrieveMessageEvent, new InfoRetrieveEvent());
            ExecutePacket(ClientPacketHeader.PingMessageEvent, new PingEvent());
        }

        private static void RegisterLandingView()
        {
            ExecutePacket(ClientPacketHeader.RefreshCampaignMessageEvent, new RefreshCampaignEvent());
            ExecutePacket(ClientPacketHeader.GetPromoArticlesMessageEvent, new GetPromoArticlesEvent());
            ExecutePacket(ClientPacketHeader.OpenCalendarBoxMessageEvent, new OpenCalendarBoxEvent());
            ExecutePacket(ClientPacketHeader.RequestBonusRareMessageEvent, new RequestBonusRareEvent());
        }

        private static void RegisterCatalog()
        {
            ExecutePacket(ClientPacketHeader.GetCatalogModeMessageEvent, new GetCatalogModeEvent());
            ExecutePacket(ClientPacketHeader.GetCatalogIndexMessageEvent, new GetCatalogIndexEvent());
            ExecutePacket(ClientPacketHeader.GetCatalogPageMessageEvent, new GetCatalogPageEvent());
            ExecutePacket(ClientPacketHeader.GetCatalogOfferMessageEvent, new GetCatalogOfferEvent());
            ExecutePacket(ClientPacketHeader.PurchaseFromCatalogMessageEvent, new PurchaseFromCatalogEvent());
            ExecutePacket(ClientPacketHeader.PurchaseFromCatalogAsGiftMessageEvent, new PurchaseFromCatalogAsGiftEvent());
            ExecutePacket(ClientPacketHeader.PurchaseRoomPromotionMessageEvent, new PurchaseRoomPromotionEvent());
            ExecutePacket(ClientPacketHeader.GetGiftWrappingConfigurationMessageEvent, new GetGiftWrappingConfigurationEvent());
            ExecutePacket(ClientPacketHeader.GetMarketplaceConfigurationMessageEvent, new GetMarketplaceConfigurationEvent());
            ExecutePacket(ClientPacketHeader.GetRecyclerRewardsMessageEvent, new GetRecyclerRewardsEvent());
            ExecutePacket(ClientPacketHeader.CheckPetNameMessageEvent, new CheckPetNameEvent());
            ExecutePacket(ClientPacketHeader.RedeemVoucherMessageEvent, new RedeemVoucherEvent());
            ExecutePacket(ClientPacketHeader.GetSellablePetBreedsMessageEvent, new GetSellablePetBreedsEvent());
            ExecutePacket(ClientPacketHeader.GetPromotableRoomsMessageEvent, new GetPromotableRoomsEvent());
            ExecutePacket(ClientPacketHeader.GetCatalogRoomPromotionMessageEvent, new GetCatalogRoomPromotionEvent());
            ExecutePacket(ClientPacketHeader.GetGroupFurniConfigMessageEvent, new GetGroupFurniConfigEvent());
            ExecutePacket(ClientPacketHeader.CheckGnomeNameMessageEvent, new CheckGnomeNameEvent());
            ExecutePacket(ClientPacketHeader.GetClubGiftsMessageEvent, new GetClubGiftsEvent());

        }

        private static void RegisterMarketplace()
        {
            ExecutePacket(ClientPacketHeader.GetOffersMessageEvent, new GetOffersEvent());
            ExecutePacket(ClientPacketHeader.GetOwnOffersMessageEvent, new GetOwnOffersEvent());
            ExecutePacket(ClientPacketHeader.GetMarketplaceCanMakeOfferMessageEvent, new GetMarketplaceCanMakeOfferEvent());
            ExecutePacket(ClientPacketHeader.GetMarketplaceItemStatsMessageEvent, new GetMarketplaceItemStatsEvent());
            ExecutePacket(ClientPacketHeader.MakeOfferMessageEvent, new MakeOfferEvent());
            ExecutePacket(ClientPacketHeader.CancelOfferMessageEvent, new CancelOfferEvent());
            ExecutePacket(ClientPacketHeader.BuyOfferMessageEvent, new BuyOfferEvent());
            ExecutePacket(ClientPacketHeader.RedeemOfferCreditsMessageEvent, new RedeemOfferCreditsEvent());
        }

        private static void RegisterNavigator()
        {
            ExecutePacket(ClientPacketHeader.AddFavouriteRoomMessageEvent, new AddFavouriteRoomEvent());
            ExecutePacket(ClientPacketHeader.GetUserFlatCatsMessageEvent, new GetUserFlatCatsEvent());
            ExecutePacket(ClientPacketHeader.DeleteFavouriteRoomMessageEvent, new RemoveFavouriteRoomEvent());
            ExecutePacket(ClientPacketHeader.GoToHotelViewMessageEvent, new GoToHotelViewEvent());
            ExecutePacket(ClientPacketHeader.UpdateNavigatorSettingsMessageEvent, new UpdateNavigatorSettingsEvent());
            ExecutePacket(ClientPacketHeader.CanCreateRoomMessageEvent, new CanCreateRoomEvent());
            ExecutePacket(ClientPacketHeader.CreateFlatMessageEvent, new CreateFlatEvent());
            ExecutePacket(ClientPacketHeader.GetGuestRoomMessageEvent, new GetGuestRoomEvent());
            ExecutePacket(ClientPacketHeader.EditRoomPromotionMessageEvent, new EditRoomEventEvent());
            ExecutePacket(ClientPacketHeader.GetEventCategoriesMessageEvent, new GetNavigatorFlatsEvent());
        }

        public static void RegisterNewNavigator()
        {
            ExecutePacket(ClientPacketHeader.InitializeNewNavigatorMessageEvent, new InitializeNewNavigatorEvent());
            ExecutePacket(ClientPacketHeader.NewNavigatorSearchMessageEvent, new NewNavigatorSearchEvent());
            ExecutePacket(ClientPacketHeader.FindRandomFriendingRoomMessageEvent, new FindRandomFriendingRoomEvent());
            ExecutePacket(ClientPacketHeader.NavigatorSavedSearchMessageEvent, new NavigatorSavedSearchEvent());
            ExecutePacket(ClientPacketHeader.DeleteNavigatorSavedSearchEvent, new DeleteNavigatorSavedSearchEvent());
        }

        private static void RegisterQuests()
        {
            ExecutePacket(ClientPacketHeader.GetQuestListMessageEvent, new GetQuestListEvent());
            ExecutePacket(ClientPacketHeader.StartQuestMessageEvent, new StartQuestEvent());
            ExecutePacket(ClientPacketHeader.CancelQuestMessageEvent, new CancelQuestEvent());
            ExecutePacket(ClientPacketHeader.GetCurrentQuestMessageEvent, new GetCurrentQuestEvent());
            ExecutePacket(ClientPacketHeader.GetDailyQuestMessageEvent, new GetDailyQuestEvent());
            //todo fix
            //ExecutePacket(ClientPacketHeader.GetCommunityGoalHallOfFameMessageEvent, new GetCommunityGoalHallOfFameEvent());
        }

        private static void RegisterHelp()
        {
            ExecutePacket(ClientPacketHeader.OnBullyClickMessageEvent, new OnBullyClickEvent());
            ExecutePacket(ClientPacketHeader.SendBullyReportMessageEvent, new SendBullyReportEvent());
            ExecutePacket(ClientPacketHeader.SubmitBullyReportMessageEvent, new SubmitBullyReportEvent());
            ExecutePacket(ClientPacketHeader.GetSanctionStatusMessageEvent, new GetSanctionStatusEvent());
        }

        private static void RegisterRoomAction()
        {
            ExecutePacket(ClientPacketHeader.LetUserInMessageEvent, new LetUserInEvent());
            ExecutePacket(ClientPacketHeader.BanUserMessageEvent, new BanUserEvent());
            ExecutePacket(ClientPacketHeader.KickUserMessageEvent, new KickUserEvent());
            ExecutePacket(ClientPacketHeader.AssignRightsMessageEvent, new AssignRightsEvent());
            ExecutePacket(ClientPacketHeader.RemoveRightsMessageEvent, new RemoveRightsEvent());
            ExecutePacket(ClientPacketHeader.RemoveAllRightsMessageEvent, new RemoveAllRightsEvent());
            ExecutePacket(ClientPacketHeader.MuteUserMessageEvent, new MuteUserEvent());
            ExecutePacket(ClientPacketHeader.GiveHandItemMessageEvent, new GiveHandItemEvent());
            ExecutePacket(ClientPacketHeader.RemoveMyRightsMessageEvent, new RemoveMyRightsEvent());
        }

        private static void RegisterAvatar()
        {
            ExecutePacket(ClientPacketHeader.GetWardrobeMessageEvent, new GetWardrobeEvent());
            ExecutePacket(ClientPacketHeader.SaveWardrobeOutfitMessageEvent, new SaveWardrobeOutfitEvent());
        }

        private static void RegisterRoomAvatar()
        {
            ExecutePacket(ClientPacketHeader.ActionMessageEvent, new ActionEvent());
            ExecutePacket(ClientPacketHeader.ApplySignMessageEvent, new ApplySignEvent());
            ExecutePacket(ClientPacketHeader.DanceMessageEvent, new DanceEvent());
            ExecutePacket(ClientPacketHeader.SitMessageEvent, new SitEvent());
            ExecutePacket(ClientPacketHeader.ChangeMottoMessageEvent, new ChangeMottoEvent());
            ExecutePacket(ClientPacketHeader.LookToMessageEvent, new LookToEvent());
            ExecutePacket(ClientPacketHeader.DropHandItemMessageEvent, new DropHandItemEvent());
            ExecutePacket(ClientPacketHeader.GiveRoomScoreMessageEvent, new GiveRoomScoreEvent());
            ExecutePacket(ClientPacketHeader.IgnoreUserMessageEvent, new IgnoreUserEvent());
            ExecutePacket(ClientPacketHeader.UnIgnoreUserMessageEvent, new UnIgnoreUserEvent());
        }

        private static void RegisterRoomConnection()
        {
            ExecutePacket(ClientPacketHeader.OpenFlatConnectionMessageEvent, new OpenFlatConnectionEvent());
            ExecutePacket(ClientPacketHeader.GoToFlatMessageEvent, new GoToFlatEvent());
        }

        private static void RegisterRoomChat()
        {
            ExecutePacket(ClientPacketHeader.ChatMessageEvent, new ChatEvent());
            ExecutePacket(ClientPacketHeader.ShoutMessageEvent, new ShoutEvent());
            ExecutePacket(ClientPacketHeader.WhisperMessageEvent, new WhisperEvent());
            ExecutePacket(ClientPacketHeader.StartTypingMessageEvent, new StartTypingEvent());
            ExecutePacket(ClientPacketHeader.CancelTypingMessageEvent, new CancelTypingEvent());
        }

        private static void RegisterRoomEngine()
        {
            ExecutePacket(ClientPacketHeader.GetRoomEntryDataMessageEvent, new GetRoomEntryDataEvent());
            ExecutePacket(ClientPacketHeader.GetFurnitureAliasesMessageEvent, new GetFurnitureAliasesEvent());
            ExecutePacket(ClientPacketHeader.MoveAvatarMessageEvent, new MoveAvatarEvent());
            ExecutePacket(ClientPacketHeader.MoveObjectMessageEvent, new MoveObjectEvent());
            ExecutePacket(ClientPacketHeader.PickupObjectMessageEvent, new PickupObjectEvent());
            ExecutePacket(ClientPacketHeader.MoveWallItemMessageEvent, new MoveWallItemEvent());
            ExecutePacket(ClientPacketHeader.ApplyDecorationMessageEvent, new ApplyDecorationEvent());
            ExecutePacket(ClientPacketHeader.PlaceObjectMessageEvent, new PlaceObjectEvent());
            ExecutePacket(ClientPacketHeader.UseFurnitureMessageEvent, new UseFurnitureEvent());
            ExecutePacket(ClientPacketHeader.UseWallItemMessageEvent, new UseWallItemEvent());
        }

        private static void RegisterInventory()
        {
            ExecutePacket(ClientPacketHeader.InitTradeMessageEvent, new InitTradeEvent());
            ExecutePacket(ClientPacketHeader.TradingOfferItemMessageEvent, new TradingOfferItemEvent());
            ExecutePacket(ClientPacketHeader.TradingOfferItemsMessageEvent, new TradingOfferItemsEvent());
            ExecutePacket(ClientPacketHeader.TradingRemoveItemMessageEvent, new TradingRemoveItemEvent());
            ExecutePacket(ClientPacketHeader.TradingAcceptMessageEvent, new TradingAcceptEvent());
            ExecutePacket(ClientPacketHeader.TradingCancelMessageEvent, new TradingCancelEvent());
            ExecutePacket(ClientPacketHeader.TradingConfirmMessageEvent, new TradingConfirmEvent());
            ExecutePacket(ClientPacketHeader.TradingModifyMessageEvent, new TradingModifyEvent());
            ExecutePacket(ClientPacketHeader.TradingCancelConfirmMessageEvent, new TradingCancelConfirmEvent());
            ExecutePacket(ClientPacketHeader.RequestFurniInventoryMessageEvent, new RequestFurniInventoryEvent());
            ExecutePacket(ClientPacketHeader.GetBadgesMessageEvent, new GetBadgesEvent());
            ExecutePacket(ClientPacketHeader.GetAchievementsMessageEvent, new GetAchievementsEvent());
            ExecutePacket(ClientPacketHeader.SetActivatedBadgesMessageEvent, new SetActivatedBadgesEvent());
            ExecutePacket(ClientPacketHeader.GetBotInventoryMessageEvent, new GetBotInventoryEvent());
            ExecutePacket(ClientPacketHeader.GetPetInventoryMessageEvent, new GetPetInventoryEvent());
            ExecutePacket(ClientPacketHeader.AvatarEffectActivatedMessageEvent, new AvatarEffectActivatedEvent());
            ExecutePacket(ClientPacketHeader.AvatarEffectSelectedMessageEvent, new AvatarEffectSelectedEvent());
        }

        private static void RegisterTalents()
        {
            ExecutePacket(ClientPacketHeader.GetTalentTrackMessageEvent, new GetTalentTrackEvent());
        }

        private static void RegisterPurse()
        {
            ExecutePacket(ClientPacketHeader.GetCreditsInfoMessageEvent, new GetCreditsInfoEvent());
            ExecutePacket(ClientPacketHeader.GetHabboClubWindowMessageEvent, new GetHabboClubWindowEvent());
            ExecutePacket(ClientPacketHeader.GetHabboClubCenterInfoMessageEvent, new GetHabboClubCenterInfoEvent());
        }
        private static void RegisterRoomCamera()
        {
            ExecutePacket(ClientPacketHeader.RenderRoomEvent, new CustomRenderRoomEvent());
            ExecutePacket(ClientPacketHeader.PurchasePhotoEvent, new PurchasePhotoEvent());
            ExecutePacket(ClientPacketHeader.GetCameraPriceEvent, new GetCameraPriceEvent());
        }

        private static void RegisterUsers()
        {
            ExecutePacket(ClientPacketHeader.ScrGetUserInfoMessageEvent, new ScrGetUserInfoEvent());
            ExecutePacket(ClientPacketHeader.SetChatPreferenceMessageEvent, new SetChatPreferenceEvent());
            ExecutePacket(ClientPacketHeader.SetUserFocusPreferenceEvent, new SetUserFocusPreferenceEvent());
            ExecutePacket(ClientPacketHeader.SetMessengerInviteStatusMessageEvent, new SetMessengerInviteStatusEvent());
            ExecutePacket(ClientPacketHeader.RespectUserMessageEvent, new RespectUserEvent());
            ExecutePacket(ClientPacketHeader.UpdateFigureDataMessageEvent, new UpdateFigureDataEvent());
            ExecutePacket(ClientPacketHeader.OpenPlayerProfileMessageEvent, new OpenPlayerProfileEvent());
            ExecutePacket(ClientPacketHeader.GetSelectedBadgesMessageEvent, new GetSelectedBadgesEvent());
            ExecutePacket(ClientPacketHeader.GetRelationshipsMessageEvent, new GetRelationshipsEvent());
            ExecutePacket(ClientPacketHeader.SetRelationshipMessageEvent, new SetRelationshipEvent());
            ExecutePacket(ClientPacketHeader.CheckValidNameMessageEvent, new CheckValidNameEvent());
            ExecutePacket(ClientPacketHeader.ChangeNameMessageEvent, new ChangeNameEvent());
            ExecutePacket(ClientPacketHeader.SetUsernameMessageEvent, new SetUsernameEvent());
            ExecutePacket(ClientPacketHeader.GetHabboGroupBadgesMessageEvent, new GetHabboGroupBadgesEvent());
            ExecutePacket(ClientPacketHeader.GetUserTagsMessageEvent, new GetUserTagsEvent());
            ExecutePacket(ClientPacketHeader.AuthentificationSMSMessageEvent, new AuthentificationSMSEvent());
            ExecutePacket(ClientPacketHeader.NuxAlertNotificationMessageEvent, new NuxAlertNotificationEvent());
            ExecutePacket(ClientPacketHeader.NuxAcceptGiftsMessageEvent, new NuxAcceptGiftsEvent());
            ExecutePacket(ClientPacketHeader.GetNuxPresentMessageEvent, new GetNuxPresentEvent());
        }

        private static void RegisterSound()
        {
            ExecutePacket(ClientPacketHeader.SetSoundSettingsMessageEvent, new SetSoundSettingsEvent());
            ExecutePacket(ClientPacketHeader.GetSongInfoMessageEvent, new GetSongInfoEvent());
            // todo jukebox?
        }

        private static void RegisterMisc()
        {
            ExecutePacket(ClientPacketHeader.EventTrackerMessageEvent, new EventTrackerEvent());
            ExecutePacket(ClientPacketHeader.ClientVariablesMessageEvent, new ClientVariablesEvent());
            ExecutePacket(ClientPacketHeader.DisconnectionMessageEvent, new DisconnectEvent());
            ExecutePacket(ClientPacketHeader.LatencyTestMessageEvent, new LatencyTestEvent());
            ExecutePacket(ClientPacketHeader.MemoryPerformanceMessageEvent, new MemoryPerformanceEvent());
            ExecutePacket(ClientPacketHeader.SetFriendBarStateMessageEvent, new SetFriendBarStateEvent());
        }


        private static void RegisterMessenger()
        {
            ExecutePacket(ClientPacketHeader.MessengerInitMessageEvent, new MessengerInitEvent());
            ExecutePacket(ClientPacketHeader.GetBuddyRequestsMessageEvent, new GetBuddyRequestsEvent());
            ExecutePacket(ClientPacketHeader.FollowFriendMessageEvent, new FollowFriendEvent());
            ExecutePacket(ClientPacketHeader.FindNewFriendsMessageEvent, new FindNewFriendsEvent());
            ExecutePacket(ClientPacketHeader.FriendListUpdateMessageEvent, new FriendListUpdateEvent());
            ExecutePacket(ClientPacketHeader.RemoveBuddyMessageEvent, new RemoveBuddyEvent());
            ExecutePacket(ClientPacketHeader.RequestBuddyMessageEvent, new RequestBuddyEvent());
            ExecutePacket(ClientPacketHeader.SendMsgMessageEvent, new SendMsgEvent());
            ExecutePacket(ClientPacketHeader.SendRoomInviteMessageEvent, new SendRoomInviteEvent());
            ExecutePacket(ClientPacketHeader.HabboSearchMessageEvent, new HabboSearchEvent());
            ExecutePacket(ClientPacketHeader.AcceptBuddyMessageEvent, new AcceptBuddyEvent());
            ExecutePacket(ClientPacketHeader.DeclineBuddyMessageEvent, new DeclineBuddyEvent());
        }

        private static void RegisterGroups()
        {
            ExecutePacket(ClientPacketHeader.GetForumsListDataMessageEvent, new GetForumsListDataEvent());
            ExecutePacket(ClientPacketHeader.GetForumStatsMessageEvent, new GetForumStatsEvent());
            ExecutePacket(ClientPacketHeader.GetThreadsListDataMessageEvent, new GetThreadsListDataEvent());
            ExecutePacket(ClientPacketHeader.GetThreadDataMessageEvent, new GetThreadDataEvent());
            ExecutePacket(ClientPacketHeader.PostGroupContentMessageEvent, new PostGroupContentEvent());
            ExecutePacket(ClientPacketHeader.DeleteGroupThreadMessageEvent, new DeleteGroupThreadEvent());
            ExecutePacket(ClientPacketHeader.UpdateForumReadMarkerMessageEvent, new UpdateForumReadMarkerEvent());
            ExecutePacket(ClientPacketHeader.UpdateForumSettingsMessageEvent, new UpdateForumSettingsEvent());
            ExecutePacket(ClientPacketHeader.UpdateForumThreadStatusMessageEvent, new UpdateForumThreadStatusEvent());
            ExecutePacket(ClientPacketHeader.JoinGroupMessageEvent, new JoinGroupEvent());
            ExecutePacket(ClientPacketHeader.RemoveGroupFavouriteMessageEvent, new RemoveGroupFavouriteEvent());
            ExecutePacket(ClientPacketHeader.SetGroupFavouriteMessageEvent, new SetGroupFavouriteEvent());
            ExecutePacket(ClientPacketHeader.GetGroupInfoMessageEvent, new GetGroupInfoEvent());
            ExecutePacket(ClientPacketHeader.GetGroupMembersMessageEvent, new GetGroupMembersEvent());
            ExecutePacket(ClientPacketHeader.GetGroupCreationWindowMessageEvent, new GetGroupCreationWindowEvent());
            ExecutePacket(ClientPacketHeader.GetBadgeEditorPartsMessageEvent, new GetBadgeEditorPartsEvent());
            ExecutePacket(ClientPacketHeader.PurchaseGroupMessageEvent, new PurchaseGroupEvent());
            ExecutePacket(ClientPacketHeader.UpdateGroupIdentityMessageEvent, new UpdateGroupIdentityEvent());
            ExecutePacket(ClientPacketHeader.UpdateGroupBadgeMessageEvent, new UpdateGroupBadgeEvent());
            ExecutePacket(ClientPacketHeader.UpdateGroupColoursMessageEvent, new UpdateGroupColoursEvent());
            ExecutePacket(ClientPacketHeader.UpdateGroupSettingsMessageEvent, new UpdateGroupSettingsEvent());
            ExecutePacket(ClientPacketHeader.ManageGroupMessageEvent, new ManageGroupEvent());
            ExecutePacket(ClientPacketHeader.GiveAdminRightsMessageEvent, new GiveAdminRightsEvent());
            ExecutePacket(ClientPacketHeader.TakeAdminRightsMessageEvent, new TakeAdminRightsEvent());
            ExecutePacket(ClientPacketHeader.RemoveGroupMemberMessageEvent, new RemoveGroupMemberEvent());
            ExecutePacket(ClientPacketHeader.AcceptGroupMembershipMessageEvent, new AcceptGroupMembershipEvent());
            ExecutePacket(ClientPacketHeader.DeclineGroupMembershipMessageEvent, new DeclineGroupMembershipEvent());
            ExecutePacket(ClientPacketHeader.DeleteGroupMessageEvent, new DeleteGroupEvent());
        }

        private static void RegisterRoomSettings()
        {
            ExecutePacket(ClientPacketHeader.GetRoomSettingsMessageEvent, new GetRoomSettingsEvent());
            ExecutePacket(ClientPacketHeader.SaveRoomSettingsMessageEvent, new SaveRoomSettingsEvent());
            ExecutePacket(ClientPacketHeader.DeleteRoomMessageEvent, new DeleteRoomEvent());
            ExecutePacket(ClientPacketHeader.ToggleMuteToolMessageEvent, new ToggleMuteToolEvent());
            ExecutePacket(ClientPacketHeader.GetRoomFilterListMessageEvent, new GetRoomFilterListEvent());
            ExecutePacket(ClientPacketHeader.ModifyRoomFilterListMessageEvent, new ModifyRoomFilterListEvent());
            ExecutePacket(ClientPacketHeader.GetRoomRightsMessageEvent, new GetRoomRightsEvent());
            ExecutePacket(ClientPacketHeader.GetRoomBannedUsersMessageEvent, new GetRoomBannedUsersEvent());
            ExecutePacket(ClientPacketHeader.UnbanUserFromRoomMessageEvent, new UnbanUserFromRoomEvent());
            ExecutePacket(ClientPacketHeader.SaveEnforcedCategorySettingsMessageEvent, new SaveEnforcedCategorySettingsEvent());
        }

        private static void RegisterPets()
        {
            ExecutePacket(ClientPacketHeader.RespectPetMessageEvent, new RespectPetEvent());
            ExecutePacket(ClientPacketHeader.GetPetInformationMessageEvent, new GetPetInformationEvent());
            ExecutePacket(ClientPacketHeader.PickUpPetMessageEvent, new PickUpPetEvent());
            ExecutePacket(ClientPacketHeader.PlacePetMessageEvent, new PlacePetEvent());
            ExecutePacket(ClientPacketHeader.RideHorseMessageEvent, new RideHorseEvent());
            ExecutePacket(ClientPacketHeader.ApplyHorseEffectMessageEvent, new ApplyHorseEffectEvent());
            ExecutePacket(ClientPacketHeader.RemoveSaddleFromHorseMessageEvent, new RemoveSaddleFromHorseEvent());
            ExecutePacket(ClientPacketHeader.ModifyWhoCanRideHorseMessageEvent, new ModifyWhoCanRideHorseEvent());
            ExecutePacket(ClientPacketHeader.GetPetTrainingPanelMessageEvent, new GetPetTrainingPanelEvent());
        }

        private static void RegisterBots()
        {
            ExecutePacket(ClientPacketHeader.PlaceBotMessageEvent, new PlaceBotEvent());
            ExecutePacket(ClientPacketHeader.PickUpBotMessageEvent, new PickUpBotEvent());
            ExecutePacket(ClientPacketHeader.OpenBotActionMessageEvent, new OpenBotActionEvent());
            ExecutePacket(ClientPacketHeader.SaveBotActionMessageEvent, new SaveBotActionEvent());
        }

        private static void RegisterFurni()
        {
            ExecutePacket(ClientPacketHeader.UpdateMagicTileMessageEvent, new UpdateMagicTileEvent());
            ExecutePacket(ClientPacketHeader.GetYouTubeTelevisionMessageEvent, new GetYouTubeTelevisionEvent());
            ExecutePacket(ClientPacketHeader.GetRentableSpaceMessageEvent, new GetRentableSpaceEvent());
            ExecutePacket(ClientPacketHeader.ToggleYouTubeVideoMessageEvent, new ToggleYouTubeVideoEvent());
            ExecutePacket(ClientPacketHeader.YouTubeVideoInformationMessageEvent, new YouTubeVideoInformationEvent());
            ExecutePacket(ClientPacketHeader.YouTubeGetNextVideo, new YouTubeGetNextVideo());
            ExecutePacket(ClientPacketHeader.SaveWiredTriggerConfigMessageEvent, new SaveWiredConfigEvent());
            ExecutePacket(ClientPacketHeader.SaveWiredEffectConfigMessageEvent, new SaveWiredConfigEvent());
            ExecutePacket(ClientPacketHeader.SaveWiredConditionConfigMessageEvent, new SaveWiredConfigEvent());
            ExecutePacket(ClientPacketHeader.SaveBrandingItemMessageEvent, new SaveBrandingItemEvent());
            ExecutePacket(ClientPacketHeader.SetTonerMessageEvent, new SetTonerEvent());
            ExecutePacket(ClientPacketHeader.DiceOffMessageEvent, new DiceOffEvent());
            ExecutePacket(ClientPacketHeader.ThrowDiceMessageEvent, new ThrowDiceEvent());
            ExecutePacket(ClientPacketHeader.SetMannequinNameMessageEvent, new SetMannequinNameEvent());
            ExecutePacket(ClientPacketHeader.SetMannequinFigureMessageEvent, new SetMannequinFigureEvent());
            ExecutePacket(ClientPacketHeader.CreditFurniRedeemMessageEvent, new CreditFurniRedeemEvent());
            ExecutePacket(ClientPacketHeader.GetStickyNoteMessageEvent, new GetStickyNoteEvent());
            ExecutePacket(ClientPacketHeader.AddStickyNoteMessageEvent, new AddStickyNoteEvent());
            ExecutePacket(ClientPacketHeader.UpdateStickyNoteMessageEvent, new UpdateStickyNoteEvent());
            ExecutePacket(ClientPacketHeader.DeleteStickyNoteMessageEvent, new DeleteStickyNoteEvent());
            ExecutePacket(ClientPacketHeader.GetMoodlightConfigMessageEvent, new GetMoodlightConfigEvent());
            ExecutePacket(ClientPacketHeader.MoodlightUpdateMessageEvent, new MoodlightUpdateEvent());
            ExecutePacket(ClientPacketHeader.ToggleMoodlightMessageEvent, new ToggleMoodlightEvent());
            ExecutePacket(ClientPacketHeader.UseOneWayGateMessageEvent, new UseFurnitureEvent());
            ExecutePacket(ClientPacketHeader.UseHabboWheelMessageEvent, new UseFurnitureEvent());
            ExecutePacket(ClientPacketHeader.OpenGiftMessageEvent, new OpenGiftEvent());
            ExecutePacket(ClientPacketHeader.GetGroupFurniSettingsMessageEvent, new GetGroupFurniSettingsEvent());
            ExecutePacket(ClientPacketHeader.UseSellableClothingMessageEvent, new UseSellableClothingEvent());
            ExecutePacket(ClientPacketHeader.ConfirmLoveLockMessageEvent, new ConfirmLoveLockEvent());
            
        }

        private static void FloorPlanEditor()
        {
            ExecutePacket(ClientPacketHeader.SaveFloorPlanModelMessageEvent, new SaveFloorPlanModelEvent());
            ExecutePacket(ClientPacketHeader.InitializeFloorPlanSessionMessageEvent, new InitializeFloorPlanSessionEvent());
            ExecutePacket(ClientPacketHeader.FloorPlanEditorRoomPropertiesMessageEvent, new FloorPlanEditorRoomPropertiesEvent());
        }

        private static void RegisterModeration()
        {
            ExecutePacket(ClientPacketHeader.OpenHelpToolMessageEvent, new OpenHelpToolEvent());
            ExecutePacket(ClientPacketHeader.GetModeratorRoomInfoMessageEvent, new GetModeratorRoomInfoEvent());
            ExecutePacket(ClientPacketHeader.GetModeratorUserInfoMessageEvent, new GetModeratorUserInfoEvent());
            ExecutePacket(ClientPacketHeader.GetModeratorUserRoomVisitsMessageEvent, new GetModeratorUserRoomVisitsEvent());
            ExecutePacket(ClientPacketHeader.ModerateRoomMessageEvent, new ModerateRoomEvent());
            ExecutePacket(ClientPacketHeader.ModeratorActionMessageEvent, new ModeratorActionEvent());
            ExecutePacket(ClientPacketHeader.SubmitNewTicketMessageEvent, new SubmitNewTicketEvent());
            ExecutePacket(ClientPacketHeader.GetModeratorRoomChatlogMessageEvent, new GetModeratorRoomChatlogEvent());
            ExecutePacket(ClientPacketHeader.GetModeratorUserChatlogMessageEvent, new GetModeratorUserChatlogEvent());
            ExecutePacket(ClientPacketHeader.GetModeratorTicketChatlogsMessageEvent, new GetModeratorTicketChatlogsEvent());
            ExecutePacket(ClientPacketHeader.PickTicketMessageEvent, new PickTicketEvent());
            ExecutePacket(ClientPacketHeader.ReleaseTicketMessageEvent, new ReleaseTicketEvent());
            ExecutePacket(ClientPacketHeader.CloseTicketMesageEvent, new CloseTicketEvent());
            ExecutePacket(ClientPacketHeader.ModerationMuteMessageEvent, new ModerationMuteEvent());
            ExecutePacket(ClientPacketHeader.ModerationKickMessageEvent, new ModerationKickEvent());
            ExecutePacket(ClientPacketHeader.ModerationBanMessageEvent, new ModerationBanEvent());
            ExecutePacket(ClientPacketHeader.ModerationMsgMessageEvent, new ModerationMsgEvent());
            ExecutePacket(ClientPacketHeader.ModerationCautionMessageEvent, new ModerationCautionEvent());
            ExecutePacket(ClientPacketHeader.ModerationTradeLockMessageEvent, new ModerationTradeLockEvent());
        }

        public static void RegisterGameCenter()
        {
            ExecutePacket(ClientPacketHeader.GetGameListingMessageEvent, new GetGameListingEvent());
            ExecutePacket(ClientPacketHeader.InitializeGameCenterMessageEvent, new InitializeGameCenterEvent());
            ExecutePacket(ClientPacketHeader.GetPlayableGamesMessageEvent, new GetPlayableGamesEvent());
            ExecutePacket(ClientPacketHeader.JoinPlayerQueueMessageEvent, new JoinPlayerQueueEvent());
            ExecutePacket(ClientPacketHeader.Game2GetWeeklyLeaderboardMessageEvent, new Game2GetWeeklyLeaderboardEvent());
        }

        public static void RegisterNames()
        {
            _packetNames.Add(ClientPacketHeader.GetClientVersionMessageEvent, "GetClientVersionEvent");
            _packetNames.Add(ClientPacketHeader.InitCryptoMessageEvent, "InitCryptoEvent");
            _packetNames.Add(ClientPacketHeader.GenerateSecretKeyMessageEvent, "GenerateSecretKeyEvent");
            _packetNames.Add(ClientPacketHeader.UniqueIDMessageEvent, "UniqueIDEvent");
            _packetNames.Add(ClientPacketHeader.SSOTicketMessageEvent, "SSOTicketEvent");
            _packetNames.Add(ClientPacketHeader.InfoRetrieveMessageEvent, "InfoRetrieveEvent");
            _packetNames.Add(ClientPacketHeader.PingMessageEvent, "PingEvent");
            _packetNames.Add(ClientPacketHeader.RefreshCampaignMessageEvent, "RefreshCampaignEvent");
            _packetNames.Add(ClientPacketHeader.GetPromoArticlesMessageEvent, "RefreshPromoEvent");
            _packetNames.Add(ClientPacketHeader.GetCatalogModeMessageEvent, "GetCatalogModeEvent");
            _packetNames.Add(ClientPacketHeader.GetCatalogIndexMessageEvent, "GetCatalogIndexEvent");
            _packetNames.Add(ClientPacketHeader.GetCatalogPageMessageEvent, "GetCatalogPageEvent");
            _packetNames.Add(ClientPacketHeader.GetCatalogOfferMessageEvent, "GetCatalogOfferEvent");
            _packetNames.Add(ClientPacketHeader.PurchaseFromCatalogMessageEvent, "PurchaseFromCatalogEvent");
            _packetNames.Add(ClientPacketHeader.PurchaseFromCatalogAsGiftMessageEvent, "PurchaseFromCatalogAsGiftEvent");
            _packetNames.Add(ClientPacketHeader.PurchaseRoomPromotionMessageEvent, "PurchaseRoomPromotionEvent");
            _packetNames.Add(ClientPacketHeader.GetGiftWrappingConfigurationMessageEvent, "GetGiftWrappingConfigurationEvent");
            _packetNames.Add(ClientPacketHeader.GetMarketplaceConfigurationMessageEvent, "GetMarketplaceConfigurationEvent");
            _packetNames.Add(ClientPacketHeader.GetRecyclerRewardsMessageEvent, "GetRecyclerRewardsEvent");
            _packetNames.Add(ClientPacketHeader.CheckPetNameMessageEvent, "CheckPetNameEvent");
            _packetNames.Add(ClientPacketHeader.RedeemVoucherMessageEvent, "RedeemVoucherEvent");
            _packetNames.Add(ClientPacketHeader.GetSellablePetBreedsMessageEvent, "GetSellablePetBreedsEvent");
            _packetNames.Add(ClientPacketHeader.GetPromotableRoomsMessageEvent, "GetPromotableRoomsEvent");
            _packetNames.Add(ClientPacketHeader.GetCatalogRoomPromotionMessageEvent, "GetCatalogRoomPromotionEvent");
            _packetNames.Add(ClientPacketHeader.GetGroupFurniConfigMessageEvent, "GetGroupFurniConfigEvent");
            _packetNames.Add(ClientPacketHeader.CheckGnomeNameMessageEvent, "CheckGnomeNameEvent");
            _packetNames.Add(ClientPacketHeader.GetOffersMessageEvent, "GetOffersEvent");
            _packetNames.Add(ClientPacketHeader.GetOwnOffersMessageEvent, "GetOwnOffersEvent");
            _packetNames.Add(ClientPacketHeader.GetMarketplaceCanMakeOfferMessageEvent, "GetMarketplaceCanMakeOfferEvent");
            _packetNames.Add(ClientPacketHeader.GetMarketplaceItemStatsMessageEvent, "GetMarketplaceItemStatsEvent");
            _packetNames.Add(ClientPacketHeader.MakeOfferMessageEvent, "MakeOfferEvent");
            _packetNames.Add(ClientPacketHeader.CancelOfferMessageEvent, "CancelOfferEvent");
            _packetNames.Add(ClientPacketHeader.BuyOfferMessageEvent, "BuyOfferEvent");
            _packetNames.Add(ClientPacketHeader.RedeemOfferCreditsMessageEvent, "RedeemOfferCreditsEvent");
            _packetNames.Add(ClientPacketHeader.AddFavouriteRoomMessageEvent, "AddFavouriteRoomEvent");
            _packetNames.Add(ClientPacketHeader.GetUserFlatCatsMessageEvent, "GetUserFlatCatsEvent");
            _packetNames.Add(ClientPacketHeader.DeleteFavouriteRoomMessageEvent, "RemoveFavouriteRoomEvent");
            _packetNames.Add(ClientPacketHeader.GoToHotelViewMessageEvent, "GoToHotelViewEvent");
            _packetNames.Add(ClientPacketHeader.UpdateNavigatorSettingsMessageEvent, "UpdateNavigatorSettingsEvent");
            _packetNames.Add(ClientPacketHeader.CanCreateRoomMessageEvent, "CanCreateRoomEvent");
            _packetNames.Add(ClientPacketHeader.CreateFlatMessageEvent, "CreateFlatEvent");
            _packetNames.Add(ClientPacketHeader.GetGuestRoomMessageEvent, "GetGuestRoomEvent");
            _packetNames.Add(ClientPacketHeader.EditRoomPromotionMessageEvent, "EditRoomEventEvent");
            _packetNames.Add(ClientPacketHeader.GetEventCategoriesMessageEvent, "GetNavigatorFlatsEvent");
            _packetNames.Add(ClientPacketHeader.InitializeNewNavigatorMessageEvent, "InitializeNewNavigatorEvent");
            _packetNames.Add(ClientPacketHeader.NewNavigatorSearchMessageEvent, "NewNavigatorSearchEvent");
            _packetNames.Add(ClientPacketHeader.FindRandomFriendingRoomMessageEvent, "FindRandomFriendingRoomEvent");
            _packetNames.Add(ClientPacketHeader.GetQuestListMessageEvent, "GetQuestListEvent");
            _packetNames.Add(ClientPacketHeader.StartQuestMessageEvent, "StartQuestEvent");
            _packetNames.Add(ClientPacketHeader.CancelQuestMessageEvent, "CancelQuestEvent");
            _packetNames.Add(ClientPacketHeader.GetCurrentQuestMessageEvent, "GetCurrentQuestEvent");
            _packetNames.Add(ClientPacketHeader.OnBullyClickMessageEvent, "OnBullyClickEvent");
            _packetNames.Add(ClientPacketHeader.SendBullyReportMessageEvent, "SendBullyReportEvent");
            _packetNames.Add(ClientPacketHeader.SubmitBullyReportMessageEvent, "SubmitBullyReportEvent");
            _packetNames.Add(ClientPacketHeader.LetUserInMessageEvent, "LetUserInEvent");
            _packetNames.Add(ClientPacketHeader.BanUserMessageEvent, "BanUserEvent");
            _packetNames.Add(ClientPacketHeader.KickUserMessageEvent, "KickUserEvent");
            _packetNames.Add(ClientPacketHeader.AssignRightsMessageEvent, "AssignRightsEvent");
            _packetNames.Add(ClientPacketHeader.RemoveRightsMessageEvent, "RemoveRightsEvent");
            _packetNames.Add(ClientPacketHeader.RemoveAllRightsMessageEvent, "RemoveAllRightsEvent");
            _packetNames.Add(ClientPacketHeader.MuteUserMessageEvent, "MuteUserEvent");
            _packetNames.Add(ClientPacketHeader.GiveHandItemMessageEvent, "GiveHandItemEvent");
            _packetNames.Add(ClientPacketHeader.GetWardrobeMessageEvent, "GetWardrobeEvent");
            _packetNames.Add(ClientPacketHeader.SaveWardrobeOutfitMessageEvent, "SaveWardrobeOutfitEvent");
            _packetNames.Add(ClientPacketHeader.ActionMessageEvent, "ActionEvent");
            _packetNames.Add(ClientPacketHeader.ApplySignMessageEvent, "ApplySignEvent");
            _packetNames.Add(ClientPacketHeader.DanceMessageEvent, "DanceEvent");
            _packetNames.Add(ClientPacketHeader.SitMessageEvent, "SitEvent");
            _packetNames.Add(ClientPacketHeader.ChangeMottoMessageEvent, "ChangeMottoEvent");
            _packetNames.Add(ClientPacketHeader.LookToMessageEvent, "LookToEvent");
            _packetNames.Add(ClientPacketHeader.DropHandItemMessageEvent, "DropHandItemEvent");
            _packetNames.Add(ClientPacketHeader.GiveRoomScoreMessageEvent, "GiveRoomScoreEvent");
            _packetNames.Add(ClientPacketHeader.IgnoreUserMessageEvent, "IgnoreUserEvent");
            _packetNames.Add(ClientPacketHeader.UnIgnoreUserMessageEvent, "UnIgnoreUserEvent");
            _packetNames.Add(ClientPacketHeader.OpenFlatConnectionMessageEvent, "OpenFlatConnectionEvent");
            _packetNames.Add(ClientPacketHeader.GoToFlatMessageEvent, "GoToFlatEvent");
            _packetNames.Add(ClientPacketHeader.ChatMessageEvent, "ChatEvent");
            _packetNames.Add(ClientPacketHeader.ShoutMessageEvent, "ShoutEvent");
            _packetNames.Add(ClientPacketHeader.WhisperMessageEvent, "WhisperEvent");
            _packetNames.Add(ClientPacketHeader.StartTypingMessageEvent, "StartTypingEvent");
            _packetNames.Add(ClientPacketHeader.CancelTypingMessageEvent, "CancelTypingEvent");
            _packetNames.Add(ClientPacketHeader.GetRoomEntryDataMessageEvent, "GetRoomEntryDataEvent");
            _packetNames.Add(ClientPacketHeader.GetFurnitureAliasesMessageEvent, "GetFurnitureAliasesEvent");
            _packetNames.Add(ClientPacketHeader.MoveAvatarMessageEvent, "MoveAvatarEvent");
            _packetNames.Add(ClientPacketHeader.MoveObjectMessageEvent, "MoveObjectEvent");
            _packetNames.Add(ClientPacketHeader.PickupObjectMessageEvent, "PickupObjectEvent");
            _packetNames.Add(ClientPacketHeader.MoveWallItemMessageEvent, "MoveWallItemEvent");
            _packetNames.Add(ClientPacketHeader.ApplyDecorationMessageEvent, "ApplyDecorationEvent");
            _packetNames.Add(ClientPacketHeader.PlaceObjectMessageEvent, "PlaceObjectEvent");
            _packetNames.Add(ClientPacketHeader.UseFurnitureMessageEvent, "UseFurnitureEvent");
            _packetNames.Add(ClientPacketHeader.UseWallItemMessageEvent, "UseWallItemEvent");
            _packetNames.Add(ClientPacketHeader.InitTradeMessageEvent, "InitTradeEvent");
            _packetNames.Add(ClientPacketHeader.TradingOfferItemMessageEvent, "TradingOfferItemEvent");
            _packetNames.Add(ClientPacketHeader.TradingRemoveItemMessageEvent, "TradingRemoveItemEvent");
            _packetNames.Add(ClientPacketHeader.TradingAcceptMessageEvent, "TradingAcceptEvent");
            _packetNames.Add(ClientPacketHeader.TradingCancelMessageEvent, "TradingCancelEvent");
            _packetNames.Add(ClientPacketHeader.TradingConfirmMessageEvent, "TradingConfirmEvent");
            _packetNames.Add(ClientPacketHeader.TradingModifyMessageEvent, "TradingModifyEvent");
            _packetNames.Add(ClientPacketHeader.TradingCancelConfirmMessageEvent, "TradingCancelConfirmEvent");
            _packetNames.Add(ClientPacketHeader.RequestFurniInventoryMessageEvent, "RequestFurniInventoryEvent");
            _packetNames.Add(ClientPacketHeader.GetBadgesMessageEvent, "GetBadgesEvent");
            _packetNames.Add(ClientPacketHeader.GetAchievementsMessageEvent, "GetAchievementsEvent");
            _packetNames.Add(ClientPacketHeader.SetActivatedBadgesMessageEvent, "SetActivatedBadgesEvent");
            _packetNames.Add(ClientPacketHeader.GetBotInventoryMessageEvent, "GetBotInventoryEvent");
            _packetNames.Add(ClientPacketHeader.GetPetInventoryMessageEvent, "GetPetInventoryEvent");
            _packetNames.Add(ClientPacketHeader.AvatarEffectActivatedMessageEvent, "AvatarEffectActivatedEvent");
            _packetNames.Add(ClientPacketHeader.AvatarEffectSelectedMessageEvent, "AvatarEffectSelectedEvent");
            _packetNames.Add(ClientPacketHeader.GetTalentTrackMessageEvent, "GetTalentTrackEvent");
            _packetNames.Add(ClientPacketHeader.GetCreditsInfoMessageEvent, "GetCreditsInfoEvent");
            _packetNames.Add(ClientPacketHeader.GetHabboClubWindowMessageEvent, "GetHabboClubWindowEvent");
            _packetNames.Add(ClientPacketHeader.ScrGetUserInfoMessageEvent, "ScrGetUserInfoEvent");
            _packetNames.Add(ClientPacketHeader.SetChatPreferenceMessageEvent, "SetChatPreferenceEvent");
            _packetNames.Add(ClientPacketHeader.SetUserFocusPreferenceEvent, "SetUserFocusPreferenceEvent");
            _packetNames.Add(ClientPacketHeader.SetMessengerInviteStatusMessageEvent, "SetMessengerInviteStatusEvent");
            _packetNames.Add(ClientPacketHeader.RespectUserMessageEvent, "RespectUserEvent");
            _packetNames.Add(ClientPacketHeader.UpdateFigureDataMessageEvent, "UpdateFigureDataEvent");
            _packetNames.Add(ClientPacketHeader.OpenPlayerProfileMessageEvent, "OpenPlayerProfileEvent");
            _packetNames.Add(ClientPacketHeader.GetSelectedBadgesMessageEvent, "GetSelectedBadgesEvent");
            _packetNames.Add(ClientPacketHeader.GetRelationshipsMessageEvent, "GetRelationshipsEvent");
            _packetNames.Add(ClientPacketHeader.SetRelationshipMessageEvent, "SetRelationshipEvent");
            _packetNames.Add(ClientPacketHeader.CheckValidNameMessageEvent, "CheckValidNameEvent");
            _packetNames.Add(ClientPacketHeader.ChangeNameMessageEvent, "ChangeNameEvent");
            _packetNames.Add(ClientPacketHeader.SetUsernameMessageEvent, "SetUsernameEvent");
            _packetNames.Add(ClientPacketHeader.GetHabboGroupBadgesMessageEvent, "GetHabboGroupBadgesEvent");
            _packetNames.Add(ClientPacketHeader.GetUserTagsMessageEvent, "GetUserTagsEvent");
            _packetNames.Add(ClientPacketHeader.SetSoundSettingsMessageEvent, "SetSoundSettingsEvent");
            _packetNames.Add(ClientPacketHeader.GetSongInfoMessageEvent, "GetSongInfoEvent");
            _packetNames.Add(ClientPacketHeader.EventTrackerMessageEvent, "EventTrackerEvent");
            _packetNames.Add(ClientPacketHeader.ClientVariablesMessageEvent, "ClientVariablesEvent");
            _packetNames.Add(ClientPacketHeader.DisconnectionMessageEvent, "DisconnectEvent");
            _packetNames.Add(ClientPacketHeader.LatencyTestMessageEvent, "LatencyTestEvent");
            _packetNames.Add(ClientPacketHeader.MemoryPerformanceMessageEvent, "MemoryPerformanceEvent");
            _packetNames.Add(ClientPacketHeader.SetFriendBarStateMessageEvent, "SetFriendBarStateEvent");
            _packetNames.Add(ClientPacketHeader.MessengerInitMessageEvent, "MessengerInitEvent");
            _packetNames.Add(ClientPacketHeader.GetBuddyRequestsMessageEvent, "GetBuddyRequestsEvent");
            _packetNames.Add(ClientPacketHeader.FollowFriendMessageEvent, "FollowFriendEvent");
            _packetNames.Add(ClientPacketHeader.FindNewFriendsMessageEvent, "FindNewFriendsEvent");
            _packetNames.Add(ClientPacketHeader.FriendListUpdateMessageEvent, "FriendListUpdateEvent");
            _packetNames.Add(ClientPacketHeader.RemoveBuddyMessageEvent, "RemoveBuddyEvent");
            _packetNames.Add(ClientPacketHeader.RequestBuddyMessageEvent, "RequestBuddyEvent");
            _packetNames.Add(ClientPacketHeader.SendMsgMessageEvent, "SendMsgEvent");
            _packetNames.Add(ClientPacketHeader.SendRoomInviteMessageEvent, "SendRoomInviteEvent");
            _packetNames.Add(ClientPacketHeader.HabboSearchMessageEvent, "HabboSearchEvent");
            _packetNames.Add(ClientPacketHeader.AcceptBuddyMessageEvent, "AcceptBuddyEvent");
            _packetNames.Add(ClientPacketHeader.DeclineBuddyMessageEvent, "DeclineBuddyEvent");
            _packetNames.Add(ClientPacketHeader.JoinGroupMessageEvent, "JoinGroupEvent");
            _packetNames.Add(ClientPacketHeader.RemoveGroupFavouriteMessageEvent, "RemoveGroupFavouriteEvent");
            _packetNames.Add(ClientPacketHeader.SetGroupFavouriteMessageEvent, "SetGroupFavouriteEvent");
            _packetNames.Add(ClientPacketHeader.GetGroupInfoMessageEvent, "GetGroupInfoEvent");
            _packetNames.Add(ClientPacketHeader.GetGroupMembersMessageEvent, "GetGroupMembersEvent");
            _packetNames.Add(ClientPacketHeader.GetGroupCreationWindowMessageEvent, "GetGroupCreationWindowEvent");
            _packetNames.Add(ClientPacketHeader.GetBadgeEditorPartsMessageEvent, "GetBadgeEditorPartsEvent");
            _packetNames.Add(ClientPacketHeader.PurchaseGroupMessageEvent, "PurchaseGroupEvent");
            _packetNames.Add(ClientPacketHeader.UpdateGroupIdentityMessageEvent, "UpdateGroupIdentityEvent");
            _packetNames.Add(ClientPacketHeader.UpdateGroupBadgeMessageEvent, "UpdateGroupBadgeEvent");
            _packetNames.Add(ClientPacketHeader.UpdateGroupColoursMessageEvent, "UpdateGroupColoursEvent");
            _packetNames.Add(ClientPacketHeader.UpdateGroupSettingsMessageEvent, "UpdateGroupSettingsEvent");
            _packetNames.Add(ClientPacketHeader.ManageGroupMessageEvent, "ManageGroupEvent");
            _packetNames.Add(ClientPacketHeader.GiveAdminRightsMessageEvent, "GiveAdminRightsEvent");
            _packetNames.Add(ClientPacketHeader.TakeAdminRightsMessageEvent, "TakeAdminRightsEvent");
            _packetNames.Add(ClientPacketHeader.RemoveGroupMemberMessageEvent, "RemoveGroupMemberEvent");
            _packetNames.Add(ClientPacketHeader.AcceptGroupMembershipMessageEvent, "AcceptGroupMembershipEvent");
            _packetNames.Add(ClientPacketHeader.DeclineGroupMembershipMessageEvent, "DeclineGroupMembershipEvent");
            _packetNames.Add(ClientPacketHeader.DeleteGroupMessageEvent, "DeleteGroupEvent");
            _packetNames.Add(ClientPacketHeader.GetForumsListDataMessageEvent, "GetForumsListDataEvent");
            _packetNames.Add(ClientPacketHeader.GetForumStatsMessageEvent, "GetForumStatsEvent");
            _packetNames.Add(ClientPacketHeader.GetThreadsListDataMessageEvent, "GetThreadsListDataEvent");
            _packetNames.Add(ClientPacketHeader.GetThreadDataMessageEvent, "GetThreadDataEvent");
            _packetNames.Add(ClientPacketHeader.GetRoomSettingsMessageEvent, "GetRoomSettingsEvent");
            _packetNames.Add(ClientPacketHeader.SaveRoomSettingsMessageEvent, "SaveRoomSettingsEvent");
            _packetNames.Add(ClientPacketHeader.DeleteRoomMessageEvent, "DeleteRoomEvent");
            _packetNames.Add(ClientPacketHeader.ToggleMuteToolMessageEvent, "ToggleMuteToolEvent");
            _packetNames.Add(ClientPacketHeader.GetRoomFilterListMessageEvent, "GetRoomFilterListEvent");
            _packetNames.Add(ClientPacketHeader.ModifyRoomFilterListMessageEvent, "ModifyRoomFilterListEvent");
            _packetNames.Add(ClientPacketHeader.GetRoomRightsMessageEvent, "GetRoomRightsEvent");
            _packetNames.Add(ClientPacketHeader.GetRoomBannedUsersMessageEvent, "GetRoomBannedUsersEvent");
            _packetNames.Add(ClientPacketHeader.UnbanUserFromRoomMessageEvent, "UnbanUserFromRoomEvent");
            _packetNames.Add(ClientPacketHeader.SaveEnforcedCategorySettingsMessageEvent, "SaveEnforcedCategorySettingsEvent");
            _packetNames.Add(ClientPacketHeader.RespectPetMessageEvent, "RespectPetEvent");
            _packetNames.Add(ClientPacketHeader.GetPetInformationMessageEvent, "GetPetInformationEvent");
            _packetNames.Add(ClientPacketHeader.PickUpPetMessageEvent, "PickUpPetEvent");
            _packetNames.Add(ClientPacketHeader.PlacePetMessageEvent, "PlacePetEvent");
            _packetNames.Add(ClientPacketHeader.RideHorseMessageEvent, "RideHorseEvent");
            _packetNames.Add(ClientPacketHeader.ApplyHorseEffectMessageEvent, "ApplyHorseEffectEvent");
            _packetNames.Add(ClientPacketHeader.RemoveSaddleFromHorseMessageEvent, "RemoveSaddleFromHorseEvent");
            _packetNames.Add(ClientPacketHeader.ModifyWhoCanRideHorseMessageEvent, "ModifyWhoCanRideHorseEvent");
            _packetNames.Add(ClientPacketHeader.GetPetTrainingPanelMessageEvent, "GetPetTrainingPanelEvent");
            _packetNames.Add(ClientPacketHeader.PlaceBotMessageEvent, "PlaceBotEvent");
            _packetNames.Add(ClientPacketHeader.PickUpBotMessageEvent, "PickUpBotEvent");
            _packetNames.Add(ClientPacketHeader.OpenBotActionMessageEvent, "OpenBotActionEvent");
            _packetNames.Add(ClientPacketHeader.SaveBotActionMessageEvent, "SaveBotActionEvent");
            _packetNames.Add(ClientPacketHeader.UpdateMagicTileMessageEvent, "UpdateMagicTileEvent");
            _packetNames.Add(ClientPacketHeader.GetYouTubeTelevisionMessageEvent, "GetYouTubeTelevisionEvent");
            _packetNames.Add(ClientPacketHeader.GetRentableSpaceMessageEvent, "GetRentableSpaceEvent");
            _packetNames.Add(ClientPacketHeader.ToggleYouTubeVideoMessageEvent, "ToggleYouTubeVideoEvent");
            _packetNames.Add(ClientPacketHeader.YouTubeVideoInformationMessageEvent, "YouTubeVideoInformationEvent");
            _packetNames.Add(ClientPacketHeader.YouTubeGetNextVideo, "YouTubeGetNextVideo");
            _packetNames.Add(ClientPacketHeader.SaveWiredTriggerConfigMessageEvent, "SaveWiredConfigEvent");
            _packetNames.Add(ClientPacketHeader.SaveWiredEffectConfigMessageEvent, "SaveWiredConfigEvent");
            _packetNames.Add(ClientPacketHeader.SaveWiredConditionConfigMessageEvent, "SaveWiredConfigEvent");
            _packetNames.Add(ClientPacketHeader.SaveBrandingItemMessageEvent, "SaveBrandingItemEvent");
            _packetNames.Add(ClientPacketHeader.SetTonerMessageEvent, "SetTonerEvent");
            _packetNames.Add(ClientPacketHeader.DiceOffMessageEvent, "DiceOffEvent");
            _packetNames.Add(ClientPacketHeader.ThrowDiceMessageEvent, "ThrowDiceEvent");
            _packetNames.Add(ClientPacketHeader.SetMannequinNameMessageEvent, "SetMannequinNameEvent");
            _packetNames.Add(ClientPacketHeader.SetMannequinFigureMessageEvent, "SetMannequinFigureEvent");
            _packetNames.Add(ClientPacketHeader.CreditFurniRedeemMessageEvent, "CreditFurniRedeemEvent");
            _packetNames.Add(ClientPacketHeader.GetStickyNoteMessageEvent, "GetStickyNoteEvent");
            _packetNames.Add(ClientPacketHeader.AddStickyNoteMessageEvent, "AddStickyNoteEvent");
            _packetNames.Add(ClientPacketHeader.UpdateStickyNoteMessageEvent, "UpdateStickyNoteEvent");
            _packetNames.Add(ClientPacketHeader.DeleteStickyNoteMessageEvent, "DeleteStickyNoteEvent");
            _packetNames.Add(ClientPacketHeader.GetMoodlightConfigMessageEvent, "GetMoodlightConfigEvent");
            _packetNames.Add(ClientPacketHeader.MoodlightUpdateMessageEvent, "MoodlightUpdateEvent");
            _packetNames.Add(ClientPacketHeader.ToggleMoodlightMessageEvent, "ToggleMoodlightEvent");
            _packetNames.Add(ClientPacketHeader.UseOneWayGateMessageEvent, "UseFurnitureEvent");
            _packetNames.Add(ClientPacketHeader.UseHabboWheelMessageEvent, "UseFurnitureEvent");
            _packetNames.Add(ClientPacketHeader.OpenGiftMessageEvent, "OpenGiftEvent");
            _packetNames.Add(ClientPacketHeader.GetGroupFurniSettingsMessageEvent, "GetGroupFurniSettingsEvent");
            _packetNames.Add(ClientPacketHeader.UseSellableClothingMessageEvent, "UseSellableClothingEvent");
            _packetNames.Add(ClientPacketHeader.ConfirmLoveLockMessageEvent, "ConfirmLoveLockEvent");
            _packetNames.Add(ClientPacketHeader.SaveFloorPlanModelMessageEvent, "SaveFloorPlanModelEvent");
            _packetNames.Add(ClientPacketHeader.InitializeFloorPlanSessionMessageEvent, "InitializeFloorPlanSessionEvent");
            _packetNames.Add(ClientPacketHeader.FloorPlanEditorRoomPropertiesMessageEvent, "FloorPlanEditorRoomPropertiesEvent");
            _packetNames.Add(ClientPacketHeader.OpenHelpToolMessageEvent, "OpenHelpToolEvent");
            _packetNames.Add(ClientPacketHeader.GetModeratorRoomInfoMessageEvent, "GetModeratorRoomInfoEvent");
            _packetNames.Add(ClientPacketHeader.GetModeratorUserInfoMessageEvent, "GetModeratorUserInfoEvent");
            _packetNames.Add(ClientPacketHeader.GetModeratorUserRoomVisitsMessageEvent, "GetModeratorUserRoomVisitsEvent");
            _packetNames.Add(ClientPacketHeader.ModerateRoomMessageEvent, "ModerateRoomEvent");
            _packetNames.Add(ClientPacketHeader.ModeratorActionMessageEvent, "ModeratorActionEvent");
            _packetNames.Add(ClientPacketHeader.SubmitNewTicketMessageEvent, "SubmitNewTicketEvent");
            _packetNames.Add(ClientPacketHeader.GetModeratorRoomChatlogMessageEvent, "GetModeratorRoomChatlogEvent");
            _packetNames.Add(ClientPacketHeader.GetModeratorUserChatlogMessageEvent, "GetModeratorUserChatlogEvent");
            _packetNames.Add(ClientPacketHeader.GetModeratorTicketChatlogsMessageEvent, "GetModeratorTicketChatlogsEvent");
            _packetNames.Add(ClientPacketHeader.PickTicketMessageEvent, "PickTicketEvent");
            _packetNames.Add(ClientPacketHeader.ReleaseTicketMessageEvent, "ReleaseTicketEvent");
            _packetNames.Add(ClientPacketHeader.CloseTicketMesageEvent, "CloseTicketEvent");
            _packetNames.Add(ClientPacketHeader.ModerationMuteMessageEvent, "ModerationMuteEvent");
            _packetNames.Add(ClientPacketHeader.ModerationKickMessageEvent, "ModerationKickEvent");
            _packetNames.Add(ClientPacketHeader.ModerationBanMessageEvent, "ModerationBanEvent");
            _packetNames.Add(ClientPacketHeader.ModerationMsgMessageEvent, "ModerationMsgEvent");
            _packetNames.Add(ClientPacketHeader.ModerationCautionMessageEvent, "ModerationCautionEvent");
            _packetNames.Add(ClientPacketHeader.ModerationTradeLockMessageEvent, "ModerationTradeLockEvent");
            _packetNames.Add(ClientPacketHeader.GetGameListingMessageEvent, "GetGameListingEvent");
            _packetNames.Add(ClientPacketHeader.InitializeGameCenterMessageEvent, "InitializeGameCenterEvent");
            _packetNames.Add(ClientPacketHeader.GetPlayableGamesMessageEvent, "GetPlayableGamesEvent");
            _packetNames.Add(ClientPacketHeader.JoinPlayerQueueMessageEvent, "JoinPlayerQueueEvent");
            _packetNames.Add(ClientPacketHeader.Game2GetWeeklyLeaderboardMessageEvent, "Game2GetWeeklyLeaderboardEvent");
        }
    }
}