﻿using System;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using Plus.Utilities;
using Plus.HabboHotel.Items;
using Plus.Communication.Packets.Outgoing.Inventory.Furni;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;

namespace Plus.Communication.Packets.Incoming.Rooms.Nux
{
    class GetNuxPresentEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            int Data1 = Packet.ReadInteger();
            int Data2 = Packet.ReadInteger();
            int Data3 = Packet.ReadInteger();
            int Data4 = Packet.ReadInteger();
            var RewardName = "";

            switch (Data4)
            {
                case 0:
                    int RewardDiamonds = RandomNumber.GenerateRandom(1, 5);
                    Session.GetHabbo().Diamonds += RewardDiamonds;
                    Session.Send(new HabboActivityPointNotificationComposer(Session.GetHabbo().Diamonds, RewardDiamonds, 5));
                    break;
                case 1:
                    int RewardGotw = RandomNumber.GenerateRandom(1, 5);
                    Session.GetHabbo().GOTWPoints += RewardGotw;
                    Session.Send(new HabboActivityPointNotificationComposer(Session.GetHabbo().GOTWPoints, RewardGotw, 103));
                    break;
                case 2:
                    int RewardItem = RandomNumber.GenerateRandom(1, 10);
                    var RewardItemId = 0;

                    switch (RewardItem)
                    {
                        case 1:
                            RewardItemId = 212; // VIP - club_sofa
                            RewardName = "Sofa VIP";
                            break;
                        case 2:
                            RewardItemId = 9510; // Elefante Azul Colorable - rare_colourable_elephant_statue*1
                            RewardName = "Eléphant Azul Colorable";
                            break;
                        case 3:
                            RewardItemId = 1587; // Lámpara Calippo - ads_calip_lava
                            RewardName = "Lampe à lave Calippo";
                            break;
                        case 4:
                            RewardItemId = 212; // VIP - club_sofa
                            RewardName = "Sofa VIP";
                            break;
                        case 5:
                            RewardItemId = 385; // Toldo Amarillo - marquee*4
                            RewardName = "Auvent jaune";
                            break;
                        case 6:
                            RewardItemId = 212; // VIP - club_sofa
                            RewardName = "Sofa VIP";
                            break;
                        case 7:
                            RewardItemId = 212; // VIP - club_sofa
                            RewardName = "Sofa VIP";
                            break;
                        case 8:
                            RewardItemId = 9506;
                            RewardName = "Parasol Azul";
                            break;
                        case 9:
                            RewardItemId = 212;
                            RewardName = "Sofa VIP";
                            break;
                        case 10:
                            RewardItemId = 212;
                            RewardName = "Sofa VIP";
                            break;
                    }
                    ItemData Item = null;
                    if (!HotelGameManager.GetItemManager().GetItem(RewardItemId, out Item))
                        return;

                    Item GiveItem = ItemFactory.CreateSingleItemNullable(Item, Session.GetHabbo(), "", "");
                    if (GiveItem != null)
                    {
                        Session.GetHabbo().GetInventoryComponent().TryAddItem(GiveItem);

                        Session.Send(new FurniListNotificationComposer(GiveItem.Id, 1));
                        Session.Send(new FurniListUpdateComposer());
                        Session.Send(NotificationManager.Bubble("voucher", LanguageLocale.Value("newuser.gifts", "%user%", Session.GetHabbo().Username, "%mobis%", RewardName), ""));
                    }

                    Session.GetHabbo().GetInventoryComponent().UpdateItems(true);
                    break;
            }
            HotelGameManager.GetAchievementManager().ProgressAchievement(Session, "ACH_AnimationRanking", 1);
        }
    }
}