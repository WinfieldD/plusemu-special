﻿using Plus;
using Plus.Communication.Packets;
using Plus.Communication.Packets.Incoming;
using Plus.Communication.Packets.Outgoing;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Communication.Packets.Outgoing.Rooms.Nux;
using Plus.Database.Interfaces;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Rooms.Chat.Commands;
using Plus.HabboHotel.Settings;
using Plus.HabboHotel.Users;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.Users
{
    class NuxAlertNotificationEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            var GetUser = Session.GetHabbo();
            var GetClicked = GetUser.CountClick++;
            var GetNewUser = GetUser.NewUser;

            if (GetUser == null)
                return;

            NuxStepMessageKeyManager.GetNuxListRun(Session, GetClicked);

            if (GetNewUser == false)
            {
                Session.SendStaffMessage(NotificationManager.Bubble("", LanguageLocale.Value("nux.alert.newuser.step.finished"), ""));
                return;
            }
        }
    }
}