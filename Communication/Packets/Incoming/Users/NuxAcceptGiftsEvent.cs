﻿using System;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using Plus.Utilities;
using Plus.HabboHotel.Items;
using Plus.Communication.Packets.Outgoing.Inventory.Furni;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Communication.Packets.Outgoing.Rooms.Nux;
using Plus.HabboHotel.Settings;
using System.Linq;

namespace Plus.Communication.Packets.Incoming.Rooms.Nux
{
    class NuxAcceptGiftsEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
           // NuxListItem i = SettingsHotelManager.GetListItemNux().ToList();
            Session.Send(new NuxItemListComposer());
        }
    }
}