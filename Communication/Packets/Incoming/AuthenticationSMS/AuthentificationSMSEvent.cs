﻿using Plus.Communication.Packets.Outgoing;
using Plus.Communication.Packets.Outgoing.AuthentificationSMS;
using Plus.Communication.Packets.Outgoing.Catalog;
using Plus.Communication.Packets.Outgoing.Moderation;
using Plus.Communication.Packets.Outgoing.Notifications;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Core;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Moderation;
using Plus.HabboHotel.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.AuthentificationSMS
{
    class AuthentificationSMSEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            string Key = Packet.ReadString();
            var KeyAccess = ConfigurationData.data["key.access"];

            if (Key == KeyAccess)
            {
                Session.Send(NotificationManager.Custom(";message.keylogin\t%user%," + Session.GetHabbo().Username));
                Session.GetHabbo().PinStaff = false;



                if (Session.GetHabbo().GetPermissions().HasRight("mod_tickets"))
                {
                    Session.Send(new ModeratorInitComposer(
                                     ModerationManager.UserMessagePresets,
                                     ModerationManager.RoomMessagePresets,
                                     ModerationManager.GetTickets));
                }
            }
            else //LanguageLocale.Value("message.error.keylogin", "%user%", Session.GetHabbo().Username)
            {
                // Session.GetHabbo().Username.Replace("%user%", Session.GetHabbo().Username);
                Session.Send(NotificationManager.Custom(";message.error.keylogin\t%user%," + Session.GetHabbo().Username));
                Session.Send(new VerifyMobilePhoneWindowComposer(1, 1));
            }

            switch(Key == null)
            {
                case true: // try?
                    Session.Send(NotificationManager.Custom(";test.message"));
                    Session.Send(new VerifyMobilePhoneWindowComposer(1, 1));
                    break;
            }
            Session.Send(new SMSErrorComposer(2, 2));
            Session.Send(new VerifyMobilePhoneWindowComposer(-1, -1));
        }
    }
}