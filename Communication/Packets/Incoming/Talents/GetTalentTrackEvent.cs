﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Talents;
using Plus.Communication.Packets.Outgoing.Talents;

namespace Plus.Communication.Packets.Incoming.Talents
{
    class GetTalentTrackEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            string Type = Packet.ReadString();

            ICollection<TalentTrackLevel> Levels = HotelGameManager.GetTalentTrackManager().GetLevels();

            Session.Send(new TalentTrackComposer(Levels, Type));
        }
    }
}
