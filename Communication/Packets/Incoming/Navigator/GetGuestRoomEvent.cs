﻿using Plus.Communication.Packets.Outgoing.Navigator;
using Plus.HabboHotel.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.Navigator
{
    class GetGuestRoomEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            int roomID = Packet.ReadInteger();

            RoomData roomData = HotelGameManager.GetRoomManager().GenerateRoomData(roomID);
            if (roomData == null)
                return;

            Boolean isLoading = Packet.ReadInteger() == 1;
            Boolean checkEntry = Packet.ReadInteger() == 1;

            Session.Send(new GetGuestRoomResultComposer(Session, roomData, isLoading, checkEntry));
        }
    }
}
