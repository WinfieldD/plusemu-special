﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Navigator;
using Plus.Communication.Packets.Outgoing.Navigator;
using Plus.Communication.Packets.Outgoing.Navigator.New;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Users.Navigator.SavedSearches;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;

namespace Plus.Communication.Packets.Incoming.Navigator
{
    class InitializeNewNavigatorEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            switch (Session.GetHabbo().Rank >= 8)
            {
                case true:
                    ICollection<TopLevelItem> TopLevelItems = HotelGameManager.GetNavigator().GetTopLevelItems();
                    ICollection<SearchResultList> SearchResultLists = HotelGameManager.GetNavigator().GetSearchResultLists();
                    ICollection<SavedSearch> ListSaved = SearchesComponent.GetSearchLists();

                    Session.Send(new NavigatorMetaDataParserComposer(TopLevelItems));
                    Session.Send(new NavigatorLiftedRoomsComposer());
                    Session.Send(new NavigatorCollapsedCategoriesComposer());
                    Session.Send(new NavigatorPreferencesComposer());
                    Session.Send(new NavigatorSavedSearchComposer(ListSaved));
                    break;

                case false:
                    Session.Send(NotificationManager.Bubble("", ";mode.rp.enabled", ""));
                    break;
            }
        }
    }
}
