﻿using Plus.Communication.Packets.Outgoing;
using Plus.Communication.Packets.Outgoing.Navigator.New;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Navigator;
using Plus.HabboHotel.Users.Navigator.SavedSearches;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.Navigator.New
{
    class DeleteNavigatorSavedSearchEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            Int32 searchId = Packet.ReadInteger();
            var ListSaved = new SavedSearch(true, 0, "", "");

            using (IQueryAdapter db = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                db.SetQuery("DELETE FROM `user_saved_searches` WHERE `user_id` = @id LIMIT 1;");
                db.AddParameter("id", Session.GetHabbo().Id);
                db.RunQuery();
            }

            ICollection<SavedSearch> ListSearch = SearchesComponent.GetSearchLists();
            if (SearchesComponent.TryRemove(searchId, out ListSaved))
                Session.Send(new NavigatorSavedSearchComposer(ListSearch));
        }
    }
}
