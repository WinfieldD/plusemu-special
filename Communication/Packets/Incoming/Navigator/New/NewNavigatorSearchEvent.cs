﻿using System.Collections.Generic;

using Plus.HabboHotel.Navigator;
using Plus.Communication.Packets.Outgoing.Navigator;
using System;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Core;
using Plus.HabboHotel.Rooms;
using Plus.Communication.Packets.Outgoing.AuthentificationSMS;

namespace Plus.Communication.Packets.Incoming.Navigator
{
    class NewNavigatorSearchEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (Convert.ToBoolean(ConfigurationData.data["enable.beta.access"]) == true && Session.GetHabbo().GetPermissions().HasRight("mod_tool"))
            {
                if (Session.GetHabbo().PinStaff)
                {
                    Session.Send(new VerifyMobilePhoneWindowComposer(1, 1));
                    Session.Send(NotificationManager.Custom(";message.error.key\t%user%," + Session.GetHabbo().Username));
                    return;
                }
            }

            string Category = Packet.ReadString();
            string Search = Packet.ReadString();

            ICollection<SearchResultList> Categories = new List<SearchResultList>();

            if (!string.IsNullOrEmpty(Search))
            {
                SearchResultList QueryResult = null;
                if (HotelGameManager.GetNavigator().TryGetSearchResultList(0, out QueryResult))
                {
                    Categories.Add(QueryResult);
                }
            }
            else
            {
                Categories = HotelGameManager.GetNavigator().GetCategorysForSearch(Category);
                if (Categories.Count == 0)
                {
                    //Are we going in deep?!
                    Categories = HotelGameManager.GetNavigator().GetResultByIdentifier(Category);
                    if (Categories.Count > 0)
                    {
                        Session.Send(new NavigatorSearchResultSetComposer(Category, Search, Categories, Session, 2, 100));
                        return;
                    }
                }
            }

            Session.Send(new NavigatorSearchResultSetComposer(Category, Search, Categories, Session));
        }
    }
}