﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Rooms;
using Plus.Communication.Packets.Outgoing.Rooms.Session;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.HabboHotel.Users;

namespace Plus.Communication.Packets.Incoming.Navigator
{
    class FindRandomFriendingRoomEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            string StepKey = Packet.ReadString();

            switch (StepKey == NuxStepMessageKeyManager.GetNuxStepKey(NuxListMessage.PREDEFINED))
            {
                case true:
                    Session.Send(new SpecialNotificationComposer(NuxStepMessageKeyManager.GetNuxStepKey(NuxListMessage.HIDE)));
                    Session.Send(new RoomForwardComposer(int.Parse(NuxStepMessageKeyManager.GetNuxStepKey(NuxListMessage.PREDEFINEDLOBBY))));
                    break;
            }

            Room Instance = HotelGameManager.GetRoomManager().TryGetRandomLoadedRoom();

            if (Instance != null)
                Session.Send(new RoomForwardComposer(Instance.Id));
        }
    }
}
