﻿using Plus.Communication.Packets.Outgoing;
using Plus.Communication.Packets.Outgoing.Navigator;
using Plus.Communication.Packets.Outgoing.Navigator.New;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Navigator;
using Plus.HabboHotel.Users.Navigator.SavedSearches;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.Navigator.New
{
    class NavigatorSavedSearchEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            String Filter = Packet.ReadString();
            String SearchCode = Packet.ReadString();

            var ListSaved = new SavedSearch(false, SearchesComponent.GetSearchLists().Count, Filter, SearchCode);
            using (IQueryAdapter db = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                db.SetQuery("INSERT INTO `user_saved_searches` (`id`,`user_id`,`filter`,`search_code`) VALUES (@id, @habboid, @filter, @code)");
                db.AddParameter("id", null);
                db.AddParameter("habboid", Session.GetHabbo().Id);
                db.AddParameter("filter", Filter);
                db.AddParameter("code", SearchCode);
                db.RunQuery();
            }

            ICollection<SavedSearch> ListSearch = SearchesComponent.GetSearchLists();
            if (ListSaved.Remove == false)
            {
                if (SearchesComponent.TryAdd(ListSaved.Id, ListSaved))
                    Session.Send(new NavigatorSavedSearchComposer(ListSearch));
            }
        }
    }
}
