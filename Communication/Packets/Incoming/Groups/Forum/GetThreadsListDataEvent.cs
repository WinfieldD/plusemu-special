﻿using Plus.Communication.Packets.Outgoing;
using Plus.Communication.Packets.Outgoing.Groups;
using Plus.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.Groups
{
    class GetThreadsListDataEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            var ForumId = Packet.ReadInteger(); //Forum ID
            var Int2 = Packet.ReadInteger(); //Start Index of Thread Count
            var Int3 = Packet.ReadInteger(); //Length of Thread Count

            var Forum = HotelGameManager.GetGroupForumManager().GetForum(ForumId);
            if (Forum == null)
            {
                return;
            }

            Session.Send(new ThreadsListDataComposer(Forum, Session, Int2, Int3));
        }
    }
}

