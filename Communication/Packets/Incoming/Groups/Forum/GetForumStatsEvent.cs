﻿using Plus.Communication.Packets.Outgoing;
using Plus.Communication.Packets.Outgoing.Groups;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Groups.Forums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.Groups
{
    class GetForumStatsEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            var GroupForumId = Packet.ReadInteger();

            GroupForum Forum;
            if (!HotelGameManager.GetGroupForumManager().TryGetForum(GroupForumId, out Forum))
            {
                Session.Send(NotificationManager.Bubble("", ";forum.error.stats", ""));
                return;
            }

            Session.Send(new ForumDataComposer(Forum, Session));

        }
    }
}
