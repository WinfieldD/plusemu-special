﻿using Plus.Communication.Packets.Outgoing.Groups;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.Groups
{
    class DeleteGroupThreadEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            var int1 = Packet.ReadInteger();
            var int2 = Packet.ReadInteger();
            var int3 = Packet.ReadInteger();

            var forum = HotelGameManager.GetGroupForumManager().GetForum(int1);

            if (forum == null)
            {
                Session.SendNotification(";forums.thread.delete.error.forumnotfound");
                return;
            }

            if (forum.Settings.GetReasonForNot(Session, forum.Settings.WhoCanModerate) != "")
            {
                Session.SendNotification(";forums.thread.delete.error.rights");
                return;
            }

            var thread = forum.GetThread(int2);
            if (thread == null)
            {
                Session.SendNotification(";forums.thread.delete.error.threadnotfound");
                return;
            }

            thread.DeletedLevel = int3 / 10;

            thread.DeleterUserId = thread.DeletedLevel != 0 ? Session.GetHabbo().Id : 0;

            thread.Save();

            Session.Send(new ThreadsListDataComposer(forum, Session));

            if (thread.DeletedLevel != 0)
                Session.Send(NotificationManager.GetType("forums.thread.hidden"));
            else
                Session.Send(NotificationManager.GetType("forums.thread.restored"));
        }
    }
}
