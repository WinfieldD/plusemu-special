﻿using Plus.Communication.Packets.Outgoing.Groups;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.Groups
{
    public class UpdateForumThreadStatusEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            var ForumID = Packet.ReadInteger();
            var ThreadID = Packet.ReadInteger();
            var Pinned = Packet.ReadBoolean();
            var Locked = Packet.ReadBoolean();


            var forum = HotelGameManager.GetGroupForumManager().GetForum(ForumID);
            var thread = forum.GetThread(ThreadID);

            if (forum.Settings.GetReasonForNot(Session, forum.Settings.WhoCanModerate) != "")
            {
                Session.SendNotification(";forums.thread.update.error.rights");
                return;
            }

            bool isPining = thread.Pinned != Pinned, isLocking = thread.Locked != Locked;

            thread.Pinned = Pinned;
            thread.Locked = Locked;

            thread.Save();

            Session.Send(new ThreadUpdatedComposer(Session, thread));

            if (isPining)
                if (Pinned)
                    Session.Send(NotificationManager.GetType("forums.thread.pinned"));
                else
                    Session.Send(NotificationManager.GetType("forums.thread.unpinned"));

            if (isLocking)
                if (Locked)
                    Session.Send(NotificationManager.GetType("forums.thread.locked"));
                else
                    Session.Send(NotificationManager.GetType("forums.thread.unlocked"));

        }
    }
}
