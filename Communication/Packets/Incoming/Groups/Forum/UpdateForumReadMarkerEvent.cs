﻿using Plus.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.Groups
{
    class UpdateForumReadMarkerEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            var length = Packet.ReadInteger();
            for (var i = 0; i < length; i++)
            {
                var forumid = Packet.ReadInteger();
                var postid = Packet.ReadInteger();
                var readall = Packet.ReadBoolean();

                var forum = HotelGameManager.GetGroupForumManager().GetForum(forumid);
                if (forum == null)
                    continue;

                var post = forum.GetPost(postid);
                if (post == null)
                    continue;

                var thread = post.ParentThread;
                var index = thread.Posts.IndexOf(post);
                thread.AddView(Session.GetHabbo().Id, index + 1);
                HotelGameManager.GetAchievementManager().ProgressAchievement(Session, "ACH_SelfModForumCanReadSeen", 1);
            }
        }
    }
}
