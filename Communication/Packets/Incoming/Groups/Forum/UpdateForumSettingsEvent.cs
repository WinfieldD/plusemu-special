﻿using Plus.Communication.Packets.Outgoing.Groups;
using Plus.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.Groups
{
    class UpdateForumSettingsEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            var ForumId = Packet.ReadInteger();
            var WhoCanRead = Packet.ReadInteger();
            var WhoCanReply = Packet.ReadInteger();
            var WhoCanPost = Packet.ReadInteger();
            var WhoCanMod = Packet.ReadInteger();


            var forum = HotelGameManager.GetGroupForumManager().GetForum(ForumId);

            if (forum == null)
            {
                Session.SendNotification(";forums.not.found");
                return;
            }

            if (forum.Settings.GetReasonForNot(Session, forum.Settings.WhoCanModerate) != "")
            {
                Session.SendNotification(";forums.settings.update.error.rights");
                return;
            }

            forum.Settings.WhoCanRead = WhoCanRead;
            forum.Settings.WhoCanModerate = WhoCanMod;
            forum.Settings.WhoCanPost = WhoCanReply;
            forum.Settings.WhoCanInitDiscussions = WhoCanPost;

            forum.Settings.Save();

            Session.Send(new ForumDataComposer(forum, Session));

            HotelGameManager.GetAchievementManager().ProgressAchievement(Session, "ACH_SelfModForumCanModerateSeen", 1);
            HotelGameManager.GetAchievementManager().ProgressAchievement(Session, "ACH_SelfModForumCanPostSeen", 1);
            Session.Send(new ThreadsListDataComposer(forum, Session));

        }
    }
}