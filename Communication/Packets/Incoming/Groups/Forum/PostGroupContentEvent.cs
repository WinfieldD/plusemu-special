﻿using Plus.Communication.Packets.Outgoing.Groups;
using Plus.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.Groups
{
    class PostGroupContentEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            var ForumId = Packet.ReadInteger();
            var ThreadId = Packet.ReadInteger();
            var Caption = Packet.ReadString();
            var Message = Packet.ReadString();

            var Forum = HotelGameManager.GetGroupForumManager().GetForum(ForumId);
            if (Forum == null)
            {
                Session.SendNotification(";forum.not.exist");
                return;
            }

            var IsNewThread = ThreadId == 0;
            if (IsNewThread)
            {
                var Thread = Forum.CreateThread(Session.GetHabbo().Id, Caption, Session);
                var Post = Thread.CreatePost(Session.GetHabbo().Id, Message, Session);

                Session.Send(new ThreadCreatedComposer(Session, Thread));
            }
            else
            {
                var Thread = Forum.GetThread(ThreadId);
                if (Thread == null)
                {
                    Session.SendNotification(";forum.not.exist.threads");
                    return;
                }

                var Post = Thread.CreatePost(Session.GetHabbo().Id, Message, Session);
                Session.Send(new ThreadReplyComposer(Session, Post));
            }
        }
    }
}