﻿using Plus.Communication.Packets.Outgoing.Groups;
using Plus.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.Groups
{
    class GetThreadDataEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            var ForumId = Packet.ReadInteger(); //Maybe Forum ID
            var ThreadId = Packet.ReadInteger(); //Maybe Thread ID
            var StartIndex = Packet.ReadInteger(); //Start index
            var length = Packet.ReadInteger(); //List Length

            var Forum = HotelGameManager.GetGroupForumManager().GetForum(ForumId);

            if (Forum == null)
            {
                Session.SendNotification("Forum introuvable!");
                return;
            }

            var Thread = Forum.GetThread(ThreadId);
            if (Thread == null)
            {
                Session.SendNotification("Forum discussion introuvable!");
                return;
            }

            
            Session.Send(new ThreadDataComposer(Thread, StartIndex, length));
            
        }
    }
}
