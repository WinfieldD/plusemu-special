﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Communication.Packets.Outgoing.Groups;

namespace Plus.Communication.Packets.Incoming.Groups
{
    class GetBadgeEditorPartsEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            Session.Send(new BadgeEditorPartsComposer(
                HotelGameManager.GetGroupManager().Bases,
                HotelGameManager.GetGroupManager().Symbols,
                HotelGameManager.GetGroupManager().BaseColours,
                HotelGameManager.GetGroupManager().SymbolColours,
                HotelGameManager.GetGroupManager().BackGroundColours));
       
        }
    }
}
