﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Groups;
using Plus.Communication.Packets.Outgoing.Groups;
using Plus.Communication.Packets.Outgoing.Catalog;
using Plus.Communication.Packets.Outgoing.Rooms.Session;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using Plus.Communication.Packets.Outgoing.Moderation;
using Plus.HabboHotel.Rooms.Chat;

namespace Plus.Communication.Packets.Incoming.Groups
{
    class PurchaseGroupEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (Session.GetHabbo().Credits < StaticSettings.GroupPurchaseAmount)
            {
                Session.Send(new BroadcastMessageAlertComposer("A group costs " + StaticSettings.GroupPurchaseAmount + " credits! You only have " + Session.GetHabbo().Credits + "!"));
                return;
            }
            else
            {
                Session.GetHabbo().Credits -= StaticSettings.GroupPurchaseAmount;
                Session.Send(new CreditBalanceComposer(Session.GetHabbo().Credits));
            }

            String Name = ChatManager.GetFilter().CheckMessage(Packet.ReadString());
            String Description = ChatManager.GetFilter().CheckMessage(Packet.ReadString());
            int RoomId = Packet.ReadInteger();
            int Colour1 = Packet.ReadInteger();
            int Colour2 = Packet.ReadInteger();
            int groupID3 = Packet.ReadInteger();
            int groupID4 = Packet.ReadInteger();
            int groupID5 = Packet.ReadInteger();
            int groupID6 = Packet.ReadInteger();
            int groupID7 = Packet.ReadInteger();
            int groupID8 = Packet.ReadInteger();
            int groupID9 = Packet.ReadInteger();
            int groupID10 = Packet.ReadInteger();
            int groupID11 = Packet.ReadInteger();
            int groupID12 = Packet.ReadInteger();
            int groupID13 = Packet.ReadInteger();
            int groupID14 = Packet.ReadInteger();
            int groupID15 = Packet.ReadInteger();
            int groupID16 = Packet.ReadInteger();
            int groupID17 = Packet.ReadInteger();
            int groupID18 = Packet.ReadInteger();

            RoomData Room = HotelGameManager.GetRoomManager().GenerateRoomData(RoomId);
            if (Room == null || Room.OwnerId != Session.GetHabbo().Id || Room.Group != null)
                return;

            string Base = "b" + ((groupID4 < 10) ? "0" + groupID4.ToString() : groupID4.ToString()) + ((groupID5 < 10) ? "0" + groupID5.ToString() : groupID5.ToString()) + groupID6;
            string Symbol1 = "s" + ((groupID7 < 10) ? "0" + groupID7.ToString() : groupID7.ToString()) + ((groupID8 < 10) ? "0" + groupID8.ToString() : groupID8.ToString()) + groupID9;
            string Symbol2 = "s" + ((groupID10 < 10) ? "0" + groupID10.ToString() : groupID10.ToString()) + ((groupID11 < 10) ? "0" + groupID11.ToString() : groupID11.ToString()) + groupID12;
            string Symbol3 = "s" + ((groupID13 < 10) ? "0" + groupID13.ToString() : groupID13.ToString()) + ((groupID14 < 10) ? "0" + groupID14.ToString() : groupID14.ToString()) + groupID15;
            string Symbol4 = "s" + ((groupID16 < 10) ? "0" + groupID16.ToString() : groupID16.ToString()) + ((groupID17 < 10) ? "0" + groupID17.ToString() : groupID17.ToString()) + groupID18;

            Symbol1 = HotelGameManager.GetGroupManager().CheckActiveSymbol(Symbol1);
            Symbol2 = HotelGameManager.GetGroupManager().CheckActiveSymbol(Symbol2);
            Symbol3 = HotelGameManager.GetGroupManager().CheckActiveSymbol(Symbol3);
            Symbol4 = HotelGameManager.GetGroupManager().CheckActiveSymbol(Symbol4);

            string Badge = Base + Symbol1 + Symbol2 + Symbol3 + Symbol4;

            Group Group = null;
            if (!HotelGameManager.GetGroupManager().TryCreateGroup(Session.GetHabbo(), Name, Description, RoomId, Badge, Colour1, Colour2, out Group))
            {
                Session.SendNotification("An error occured whilst trying to create this group.\n\nTry again. If you get this message more than once, report it at the link below.\r\rhttp://boonboards.com");
                return;
            }

            Session.Send(new PurchaseOKComposer());

            Room.Group = Group;

            if (Session.GetHabbo().CurrentRoomId != Room.Id)
                Session.Send(new RoomForwardComposer(Room.Id));

            Session.Send(new NewGroupInfoComposer(RoomId, Group.Id));
        }
    }
}