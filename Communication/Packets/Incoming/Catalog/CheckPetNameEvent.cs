﻿using Plus.Communication.Packets.Outgoing.Catalog;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Incoming;
using Plus.HabboHotel.Rooms.Chat;

namespace Plus.Communication.Packets.Incoming.Catalog
{
    public class CheckPetNameEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            string PetName = Packet.ReadString();

            if (PetName.Length < 2)
            {
                Session.Send(new CheckPetNameComposer(2, "2"));
                return;
            }
            else if (PetName.Length > 15)
            {
                Session.Send(new CheckPetNameComposer(1, "15"));
                return;
            }
            else if (!PlusEnvironment.IsValidAlphaNumeric(PetName))
            {
                Session.Send(new CheckPetNameComposer(3, ""));
                return;
            }
            else if (ChatManager.GetFilter().IsFiltered(PetName))
            {
                Session.Send(new CheckPetNameComposer(4, ""));
                return;
            }

            Session.Send(new CheckPetNameComposer(0, ""));
        }
    }
}