﻿using Plus.Communication.Packets.Outgoing.Catalog;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Rooms.AI;
using Plus.Communication.Packets.Incoming;

namespace Plus.Communication.Packets.Incoming.Catalog
{
    public class GetSellablePetBreedsEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            string Type = Packet.ReadString();
            string PacketType = "";
            int PetId = HotelGameManager.GetCatalog().GetPetRaceManager().GetPetId(Type, out PacketType);

            Session.Send(new SellablePetBreedsComposer(PacketType, PetId, HotelGameManager.GetCatalog().GetPetRaceManager().GetRacesForRaceId(PetId)));
        }
    }
}