﻿using Plus.Core;
using Plus.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.Handshake
{
    class SSOHandlerManager
    {
        public static void Handle(GameClient Session, ClientPacket Packet, String HandleRunSSO)
        {
            switch(Convert.ToBoolean(ConfigurationData.data["enable.rc4"]))
            {
                case true:
                    if (Session == null || Session.RC4Client == null || Session.GetHabbo() != null)
                        return;

                    if (String.IsNullOrEmpty(HandleRunSSO) || HandleRunSSO.Length < 15)
                        return;

                    Session.TryAuthenticate(HandleRunSSO);
                    break;

                case false:
                    if (Session == null || Session.GetHabbo() != null)
                        return;

                    if (String.IsNullOrEmpty(HandleRunSSO) || HandleRunSSO.Length < 15)
                        return;

                    Session.TryAuthenticate(HandleRunSSO);
                    break;
            }
        }
    }
}
