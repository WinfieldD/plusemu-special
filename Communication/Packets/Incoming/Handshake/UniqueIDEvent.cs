﻿using System;
using Plus.Database.Interfaces;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Handshake;

namespace Plus.Communication.Packets.Incoming.Handshake
{
    public class UniqueIDEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            string Junk = Packet.ReadString();
            string MachineId = Packet.ReadString();

            Session.MachineId = MachineId;

            Session.Send(new SetUniqueIdComposer(MachineId));
        }
    }
}