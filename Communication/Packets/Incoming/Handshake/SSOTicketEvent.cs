﻿using System;

using Plus.Communication.Packets.Incoming;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.HabboHotel.Rooms;
using Plus.Core;

namespace Plus.Communication.Packets.Incoming.Handshake
{
    public class SSOTicketEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            String HandleRunSSO = Packet.ReadString();

            switch (Convert.ToBoolean(ConfigurationData.data["enable.rc4"]))
            {
                case true:
                    if (Session == null || Session.RC4Client == null || Session.GetHabbo() != null)
                        return;

                    if (String.IsNullOrEmpty(HandleRunSSO) || HandleRunSSO.Length < 15)
                        return;

                    Session.TryAuthenticate(HandleRunSSO);
                    break;

                case false:
                    if (Session == null || Session.GetHabbo() != null)
                        return;

                    if (String.IsNullOrEmpty(HandleRunSSO) || HandleRunSSO.Length < 15)
                        return;

                    Session.TryAuthenticate(HandleRunSSO);
                    break;
            }

            //  SSOHandlerManager.Handle(Session, Packet, HandleRunSSO);
        }
    }
}