﻿using System;

using Plus.Communication.Packets.Incoming;
using Plus.HabboHotel.Groups;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Communication.Packets.Outgoing.Groups;
using System.Linq;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using System.Collections.Generic;
using Plus.HabboHotel.Rooms;

namespace Plus.Communication.Packets.Incoming.Handshake
{
    public class InfoRetrieveEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            var OnCountForum = HotelGameManager.GetGroupForumManager().GetForumsByUserId(Session.GetHabbo().Id);
            
            Session.Send(new UserObjectComposer(Session.GetHabbo()));
            Session.Send(new UserPerksComposer(Session.GetHabbo()));
            Session.Send(new UnreadForumThreadPostsComposer(OnCountForum.Where(c => c.UnreadMessages(Session.GetHabbo().Id) > 0).Count()));
        }
    }
}