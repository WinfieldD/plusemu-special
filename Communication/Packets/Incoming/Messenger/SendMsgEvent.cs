﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.Messenger;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Rooms.Chat;

namespace Plus.Communication.Packets.Incoming.Messenger
{
    class SendMsgEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null || Session.GetHabbo().GetMessenger() == null)
                return;

            int userId = Packet.ReadInteger();

            if (userId == 0 || userId == Session.GetHabbo().Id)
                return;

            var GetChat = userId == 0x7fffffff;
            var GetChatVIP = userId == 0x7fffff;
            var GetChatZOZO = userId == 0x7ff;
            var GetChatPublics = userId == 0x7f;
            var CheckedMessage = Packet.ReadString();

            string message = ChatManager.GetFilter().CheckMessage(CheckedMessage);
            if (string.IsNullOrWhiteSpace(message))
                return;

            if (Session.GetHabbo().TimeMuted > 0)
            {
                Session.SendNotification(";error.mute.Send");
                return;
            }

            if (GetChat == true)
            {
                if(Session.GetHabbo().GetPermissions().HasRight("mod_tickets"))
                GameClientManager.Alert(new NewConsoleMessageComposer(0x7fffffff, Session.GetHabbo().Username + ": " + message), Session.GetHabbo().Id);
                return;
            }

            if (GetChatVIP == true)
            {
                if(Session.GetHabbo().Rank >= 2)
                GameClientManager.VAlert(new NewConsoleMessageComposer(0x7fffff, Session.GetHabbo().Username + ": " + message), Session.GetHabbo().Id);
                return;
            }

            if(GetChatZOZO == true)
            {
                if (Session.GetHabbo().GetPermissions().HasRight("mod_zozo"))
                    GameClientManager.ZAlert(new NewConsoleMessageComposer(0x7ff, Session.GetHabbo().Username + ": " + message), Session.GetHabbo().Id);
                return;
            }

            if(GetChatPublics == true)
            {
                GameClientManager.PAlert(new NewConsoleMessageComposer(0x7f, Session.GetHabbo().Username + ": " + message), Session.GetHabbo().Id);
                return;
            }

            Session.GetHabbo().GetMessenger().SendInstantMessage(userId, message);

        }
    }
}