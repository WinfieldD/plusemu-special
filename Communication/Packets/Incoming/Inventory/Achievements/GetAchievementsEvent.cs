﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Communication.Packets.Outgoing.Inventory.Achievements;

namespace Plus.Communication.Packets.Incoming.Inventory.Achievements
{
    class GetAchievementsEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            Session.Send(new AchievementsComposer(Session, HotelGameManager.GetAchievementManager()._achievements.Values.ToList()));
        }
    }
}
