﻿using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.Inventory.Purse
{
    class GetHabboClubCenterInfoEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            Session.Send(new HabboClubCenterInfoComposer(Session));
        }
    }
}