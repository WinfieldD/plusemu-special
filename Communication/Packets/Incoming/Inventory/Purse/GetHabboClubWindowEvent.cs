﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using Plus.Communication.Packets.Outgoing;
using Plus.HabboHotel.Catalog;
using Plus.Communication.Packets.Outgoing.Purse;

namespace Plus.Communication.Packets.Incoming.Inventory.Purse
{
    class GetHabboClubWindowEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            CatalogPage page = HotelGameManager.GetCatalog().TryGetPageByTemplate("vip_buy");

            if (page == null)
                return;

            int nope = Packet.ReadInteger();

            foreach (CatalogItem catalogItem in page.Items.Values)
            {
                Session.Send(new GetClubComposer(Session.GetHabbo(), catalogItem, page, nope));
            }
        }
    }
}
