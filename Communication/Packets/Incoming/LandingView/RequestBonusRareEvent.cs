﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Communication.Packets.Outgoing.LandingView;
using Plus.HabboHotel.Items;
using Plus.Core;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Communication.Packets.Outgoing.Inventory.Furni;
using Plus.Database.Interfaces;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.LandingView
{
    class RequestBonusRareEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            Session.Send(new BonusRareComposer(Session,
                StaticSettings.BonusRareProduct,
                StaticSettings.BonusRareBaseId,
                StaticSettings.BonusRareTotalScore));

            switch (Session.GetHabbo().BonusRareQuest)
            {
                case true:
                    if (Session.GetHabbo().BonusPoints >= StaticSettings.BonusRareTotalScore)
                    {
                        Task bonus = new Task(() => Session.GetHabbo().BonusPoints -= StaticSettings.BonusRareTotalScore);
                        bonus.Start();

                        Task Points = new Task(() => Session.Send(new HabboActivityPointNotificationComposer(Session.GetHabbo().BonusPoints, StaticSettings.BonusRareTotalScore, 101)));
                        Points.Start();

                        Task sendNotif = new Task(() => Session.Send(NotificationManager.Bubble("", StaticSettings.BonusRareRequestClear.Replace("%user%", Session.GetHabbo().Username), "")));
                        sendNotif.Start();

                        ItemData Item = null;
                        if (!HotelGameManager.GetItemManager().GetItem((StaticSettings.BonusRareBaseId), out Item))
                            return;

                        Item GiveItem = ItemFactory.CreateSingleItemNullable(Item, Session.GetHabbo(), "", "");
                        if (GiveItem != null)
                        {
                            Task FurniList = new Task(() => Session.GetHabbo().GetInventoryComponent().TryAddItem(GiveItem));
                            FurniList.Start();

                            Task FurniLists = new Task(() => Session.Send(new FurniListNotificationComposer(GiveItem.Id, 1)));
                            FurniLists.Start();

                            Task FurniListUpdate = new Task(() => Session.Send(new FurniListUpdateComposer()));
                            FurniListUpdate.Start();
                        }

                        Task InventoryUpdateList = new Task(() => Session.GetHabbo().GetInventoryComponent().UpdateItems(false));
                        InventoryUpdateList.Start();
                    }

                    using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                    {
                        dbClient.SetQuery("UPDATE `users` SET `quest_bonusrare` = @quest WHERE `id` = @id LIMIT 1");
                        dbClient.AddParameter("quest", Session.GetHabbo().BonusRareQuest == true);
                        dbClient.AddParameter("id", Session.GetHabbo().Id);
                        dbClient.RunQuery();
                    }
                    break;

                case false:

                    using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                    {
                        dbClient.SetQuery("UPDATE `users` SET `user_points` = @points WHERE `id` = @id LIMIT 1");
                        dbClient.AddParameter("points", Session.GetHabbo().BonusPoints == 0);
                        dbClient.AddParameter("id", Session.GetHabbo().Id);
                        dbClient.RunQuery();
                    }
                    break;
            }
        }
    }
}