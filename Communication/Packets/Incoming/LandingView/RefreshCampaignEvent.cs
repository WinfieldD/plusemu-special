﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Communication.Packets.Outgoing.LandingView;

namespace Plus.Communication.Packets.Incoming.LandingView
{
    class RefreshCampaignEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            try
            {
               /* String parseCampaings = Packet.ReadString();
                if (parseCampaings.Contains("gamesmaker"))
                    return;*/
                string parseCampaings = Packet.ReadString();
                if (parseCampaings.Contains("gamesmaker"))
                    return;
                string campaignName = "";
                string[] strArray = parseCampaings.Split(';');
                int index = 0;
                while (index < strArray.Length)
                {
                    if (!string.IsNullOrEmpty(strArray[index]) && !strArray[index].EndsWith(","))
                        campaignName = strArray[index].Split(',')[1];
                    checked { ++index; }
                }

                String campaingName = "";

                if (campaingName == "2013-05-08 13:00,besthalloffame;2013-05-11 13:00")
                {
                    Session.Send(new HotelViewLooksComposer());
                }
                String[] parser = parseCampaings.Split(';');

                for (int i = 0; i < parser.Length; i++)
                {
                    if (String.IsNullOrEmpty(parser[i]) || parser[i].EndsWith(","))
                        continue;

                    String[] data = parser[i].Split(',');
                    campaingName = data[1];
                }
                Session.Send(new CampaignComposer(parseCampaings, campaingName));
            }
            catch {  }
        }
    }
}