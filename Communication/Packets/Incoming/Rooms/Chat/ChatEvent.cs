﻿using System;

using Plus.Core;
using Plus.Communication.Packets.Incoming;
using Plus.Utilities;
using Plus.HabboHotel.Global;
using Plus.HabboHotel.Quests;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Rooms.Chat.Logs;
using Plus.Communication.Packets.Outgoing.Messenger;
using Plus.Communication.Packets.Outgoing.Rooms.Chat;
using Plus.Communication.Packets.Outgoing.Moderation;
using Plus.HabboHotel.Rooms.Chat.Styles;
using Plus.HabboHotel.Users;
using Plus.HabboHotel.Rooms.Chat;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Communication.Packets.Outgoing.AuthentificationSMS;
using Plus.HabboHotel.Moderation;

namespace Plus.Communication.Packets.Incoming.Rooms.Chat
{
    public class ChatEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null || !Session.GetHabbo().InRoom)
                return;

            if (Convert.ToBoolean(ConfigurationData.data["enable.beta.access"]) == true && Session.GetHabbo().GetPermissions().HasRight("mod_tool"))
            {
                if (Session.GetHabbo().PinStaff)
                {
                    Session.Send(new VerifyMobilePhoneWindowComposer(1, 1));
                    Session.Send(NotificationManager.Custom(";message.error.key\t%user%," + Session.GetHabbo().Username));
                    return;
                }
            }

            Room Room = Session.GetHabbo().CurrentRoom;
            if (Room == null)
                return;

            RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (User == null)
                return;

            string Message = StringCharFilter.Escape(Packet.ReadString());
            if (Message.Length > 100)
                Message = Message.Substring(0, 100);

            int Colour = Packet.ReadInteger();

            ChatStyle Style = null;
            if (!ChatManager.GetChatStyles().TryGetStyle(Colour, out Style) || (Style.RequiredRight.Length > 0 && !Session.GetHabbo().GetPermissions().HasRight(Style.RequiredRight)))
                Colour = 0;

            User.UnIdle();

            if (PlusEnvironment.GetUnixTimestamp() < Session.GetHabbo().FloodTime && Session.GetHabbo().FloodTime != 0)
                return;

            if (Session.GetHabbo().TimeMuted > 0)
            {
                Session.Send(new MutedComposer(Session.GetHabbo().TimeMuted));
                return;
            }

            if (!Session.GetHabbo().GetPermissions().HasRight("room_ignore_mute") && Room.CheckMute(Session))
            {
                Session.SendWhisper(";error.muted.enable");
                return;
            }

            User.LastBubble = Session.GetHabbo().CustomBubbleId == 0 ? Colour : Session.GetHabbo().CustomBubbleId;

            if (!Session.GetHabbo().GetPermissions().HasRight("mod_tool"))
            {
                int MuteTime;
                if (User.IncrementAndCheckFlood(out MuteTime))
                {
                    Session.Send(new FloodControlComposer(MuteTime));
                    return;
                }
            }

            if (Message.StartsWith(":", StringComparison.CurrentCulture) && ChatManager.GetCommands().Parse(Session, Message))
                return;

            ChatlogManager.StoreChatlog(new ChatlogEntry(Session.GetHabbo().Id, Room.Id, Message, UnixTimestamp.GetNow(), Session.GetHabbo(), Room));

            if (ChatManager.GetFilter().CheckBannedWords(Message))
            {
                Session.GetHabbo().BannedPhraseCount++;
                if (Session.GetHabbo().BannedPhraseCount >= StaticSettings.BannedPhrasesAmount)
                {
                    ModerationManager.BanUser("System", HabboHotel.Moderation.ModerationBanType.USERNAME, Session.GetHabbo().Username, "Spamming banned phrases (" + Message + ")", (PlusEnvironment.GetUnixTimestamp() + 78892200));
                    Session.Disconnect();
                    return;
                }

                Session.Send(new ChatComposer(User.VirtualId, Message, 0, Colour));
                return;
            }

            if (!Session.GetHabbo().GetPermissions().HasRight("word_filter_override"))
                Message = ChatManager.GetFilter().CheckMessage(Message);


            HotelGameManager.GetQuestManager().ProgressUserQuest(Session, QuestType.SOCIAL_CHAT);
            // on ?
            UserManager.Init();
            string sPattern = "@";
            if (System.Text.RegularExpressions.Regex.IsMatch(Message, sPattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase))
            {
                Message = UserManager.CheckNotification(Message, Session);
            }
            User.OnChat(User.LastBubble, Message, false);

        }
    }
}