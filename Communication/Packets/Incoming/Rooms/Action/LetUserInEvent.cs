﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Plus.Communication.Packets.Incoming;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Navigator;
using Plus.Communication.Packets.Outgoing.Rooms.Session;

namespace Plus.Communication.Packets.Incoming.Rooms.Action
{
    class LetUserInEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            Room Room;

            if (!HotelGameManager.GetRoomManager().TryGetRoom(Session.GetHabbo().CurrentRoomId, out Room))
                return;

            if (!Room.CheckRights(Session))
                return;

            string Name = Packet.ReadString();
            bool Accepted = Packet.ReadBoolean();

            GameClient Client = GameClientManager.GetClientByUsername(Name);
            if (Client == null)
                return;

            if (Accepted)
            {
                Client.GetHabbo().RoomAuthOk = true;
                Client.Send(new FlatAccessibleComposer(""));
                Room.Send(new FlatAccessibleComposer(Client.GetHabbo().Username), true);
            }
            else
            {
                Client.Send(new FlatAccessDeniedComposer(""));
                Room.Send(new FlatAccessDeniedComposer(Client.GetHabbo().Username), true);
            }
        }
    }
}
