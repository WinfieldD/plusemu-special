﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Rooms;
using System.Drawing;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Communication.Packets.Outgoing.Rooms.Nux;
using Plus.HabboHotel.Users;
using Plus.Communication.Packets.Outgoing.Handshake;

namespace Plus.Communication.Packets.Incoming.Rooms.Engine
{
    class MoveAvatarEvent : IPacketEvent
    {
        private bool CanChangeName(Habbo Habbo)
        {
            if (Habbo.Rank == 1 && Habbo.VIPRank == 0 && Habbo.LastNameChange == 0)
                return true;
            else if (Habbo.Rank == 1 && Habbo.VIPRank == 1 && (Habbo.LastNameChange == 0 || (PlusEnvironment.GetUnixTimestamp() + 604800) > Habbo.LastNameChange))
                return true;
            else if (Habbo.Rank == 1 && Habbo.VIPRank == 2 && (Habbo.LastNameChange == 0 || (PlusEnvironment.GetUnixTimestamp() + 86400) > Habbo.LastNameChange))
                return true;
            else if (Habbo.Rank == 1 && Habbo.VIPRank == 3)
                return true;
            else if (Habbo.GetPermissions().HasRight("mod_tool"))
                return true;

            return false;
        }

        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null)
                return;

            if (!Session.GetHabbo().InRoom)
                return;

            Room Room = Session.GetHabbo().CurrentRoom;
            if (Room == null)
                return;

            RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (User == null || !User.CanWalk)
                return;

            int MoveX = Packet.ReadInteger();
            int MoveY = Packet.ReadInteger();

            if (MoveX == User.X && MoveY == User.Y)
            {
                if (User.IsWalking)
                    User.PathCounter++;
                else
                    return;

                if (User.PathCounter == 4)
                    return;

                User.SamePath = true;
            }
            else
                User.SamePath = false;

            //    return;

            if (User.RidingHorse)
            {
                RoomUser Horse = Room.GetRoomUserManager().GetRoomUserByVirtualId(User.HorseID);
                if (Horse != null)
                    Horse.MoveTo(MoveX, MoveY);
            }
            User.DistancePath = 0;
            int a = Math.Abs((MoveX - User.X));
            int b = Math.Abs((MoveY - User.Y));
            int c = ((a * a) + (b * b));
            int distance = Convert.ToInt32(Math.Sqrt(c));
            if (User.X != MoveX && User.Y != MoveY)
            {
                distance = distance - 1;
                User.DiagMove = true;
            }
            else
            {
                User.DiagMove = false;
            }
            if (!User.IsWalking)
            {
                User.DistancePath = distance;
            }
            User.boolcount = 0;
            User.MoveTo(MoveX, MoveY);

            if (Session.GetHabbo().NewUser)
            {
                Session.Send(new NuxUserStatusComposer(2));
                if (Session.GetHabbo().CountClick == 0)
                {
                    Session.GetHabbo().CountClick++;
                    Session.Send(new SpecialNotificationComposer(NuxStepMessageKeyManager.GetNuxStepKey(NuxListMessage.VIP)));
                }

                if (!this.CanChangeName(Session.GetHabbo()))
                {
                    Session.Send(NotificationManager.Bubble("", LanguageLocale.Value("no.change.name.accept"), ""));
                    return;
                }

                Session.GetHabbo().ChangingName = true;
                Session.Send(new UserObjectComposer(Session.GetHabbo()));
            }
        }
    }
}