﻿using System;
using System.Linq;
using System.Text;

using Plus.HabboHotel.Items.Televisions;
using Plus.Communication.Packets.Outgoing.Rooms.Furni.YouTubeTelevisions;

namespace Plus.Communication.Packets.Incoming.Rooms.Furni
{
    class ToggleYouTubeVideoEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            int ItemId = Packet.ReadInteger();//Item Id
            string VideoId = Packet.ReadString(); //Video ID

            Session.Send(new GetYouTubeVideoComposer(ItemId, VideoId));
        }
    }
}