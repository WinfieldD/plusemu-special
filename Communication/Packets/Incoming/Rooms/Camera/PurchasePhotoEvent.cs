﻿using System;

using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Rooms.Camera;
using Plus.HabboHotel.Camera;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using Plus.HabboHotel.Items;
using Plus.Communication.Packets.Outgoing.Inventory.Furni;
using Plus.Database.Interfaces;
using Plus.Utilities;

namespace Plus.Communication.Packets.Incoming.Rooms.Camera
{
    public class PurchasePhotoEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            if (!Session.GetHabbo().InRoom || Session.GetHabbo().Credits < int.Parse(SettingsCamera.Value("camera.photo.purchase.price.coins")) || Session.GetHabbo().Duckets < int.Parse(SettingsCamera.Value("camera.photo.purchase.price.duckets")))
                return;

            Room Room = Session.GetHabbo().CurrentRoom;

            if (Room == null)
                return;

            RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);

            if (User == null || User.LastPhotoPreview == null)
                return;

            CameraPhotoPreview preview = User.LastPhotoPreview;

            if (int.Parse(SettingsCamera.Value("camera.photo.purchase.price.coins")) > 0)
            {
                Session.GetHabbo().Credits -= int.Parse(SettingsCamera.Value("camera.photo.purchase.price.coins"));
                Session.Send(new CreditBalanceComposer(Session.GetHabbo().Credits));
            }

            if (int.Parse(SettingsCamera.Value("camera.photo.purchase.price.duckets")) > 0)
            {
                Session.GetHabbo().Duckets -= int.Parse(SettingsCamera.Value("camera.photo.purchase.price.duckets"));
                Session.Send(new HabboActivityPointNotificationComposer(Session.GetHabbo().Duckets, Session.GetHabbo().Duckets));
            }

            using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("UPDATE `camera_photos` SET `file_state` = 'purchased' WHERE `id` = @id LIMIT 1");
                dbClient.AddParameter("id", preview.Id);
                dbClient.RunQuery();
            }

            Item photoPoster = ItemFactory.CreateSingleItemNullable(CameraPhotoManager.PhotoPoster, Session.GetHabbo(),
            "{\"w\":\"" + StringCharFilter.EscapeJSONString(CameraPhotoManager.GetPath(CameraPhotoType.PURCHASED, preview.Id, preview.CreatorId)) + "\", \"n\":\"" + StringCharFilter.EscapeJSONString(Session.GetHabbo().Username) + "\", \"s\":\"" + Session.GetHabbo().Id + "\", \"u\":\"" + preview.Id + "\", \"t\":\"" + preview.CreatedAt + "\"}", "");

            var getInteractType = InteractionType.CAMERA_PICTURE;
            if (photoPoster.Data.InteractionType != getInteractType)
                return;

            if (photoPoster != null)
            {
                
                Session.GetHabbo().GetInventoryComponent().TryAddItem(photoPoster);

                HotelGameManager.GetAchievementManager().ProgressAchievement(Session, "ACH_CameraPhotoCount", 1);
                Session.Send(new FurniListAddComposer(photoPoster));
                Session.Send(new FurniListUpdateComposer());
            }

            Session.Send(new CameraPhotoPurchaseOkComposer());
        }
    }
}