﻿
using System;

using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Rooms.Camera;
using Plus.HabboHotel.Camera;

namespace Plus.Communication.Packets.Incoming.Rooms.Camera
{
    public class CustomRenderRoomEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            if (!Session.GetHabbo().InRoom)
                return;

            Room Room = Session.GetHabbo().CurrentRoom;

            if (Room == null)
                return;

            RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);

            if (User == null)
                return;

            int photoId;

            if (!int.TryParse(Packet.ReadString(), out photoId) || photoId < 0)
                return;

            CameraPhotoPreview preview = CameraPhotoManager.GetPreview(photoId);

            if (preview == null || preview.CreatorId != Session.GetHabbo().Id)
                return;

            User.LastPhotoPreview = preview;
            Session.Send(new CameraPhotoPreviewComposer(CameraPhotoManager.GetPath(CameraPhotoType.PREVIEW, preview.Id, preview.CreatorId)));
        }
    }
}