﻿
using System;

using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Rooms.Camera;

namespace Plus.Communication.Packets.Incoming.Rooms.Camera
{
    public class GetCameraPriceEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            Session.Send(new CameraPriceComposer(int.Parse(SettingsCamera.Value("camera.photo.purchase.price.coins")), int.Parse(SettingsCamera.Value("camera.photo.purchase.price.duckets")), int.Parse(SettingsCamera.Value("camera.photo.publish.price.duckets"))));
        }
    }
}