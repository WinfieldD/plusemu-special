﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Communication.Packets.Outgoing.GameCenter;

namespace Plus.Communication.Packets.Incoming.GameCenter
{
    class GetPlayableGamesEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            int GameId = Packet.ReadInteger();

            Session.Send(new GameAccountStatusComposer(GameId));
            Session.Send(new PlayableGamesComposer(GameId));
            Session.Send(new GameAchievementListComposer(Session, HotelGameManager.GetAchievementManager().GetGameAchievements(GameId), GameId));
        }
    }
}