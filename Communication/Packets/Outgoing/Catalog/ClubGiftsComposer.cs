﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.Catalog
{
    class ClubGiftsComposer : ServerPacket
    {
        public ClubGiftsComposer()
            : base(ServerPacketHeader.ClubGiftsMessageComposer)
        {
            base.WriteInteger(0);
            base.WriteInteger(10);
            base.WriteInteger(1);
            {
                base.WriteInteger(14689);
                base.WriteString("hc_arab_chair");
                base.WriteBoolean(false);
                base.WriteInteger(5);
                base.WriteInteger(0);
                base.WriteInteger(0);
                base.WriteBoolean(true);
                base.WriteInteger(1);
                {
                    base.WriteString("s");
                    base.WriteInteger(6341);
                    base.WriteString("");
                    base.WriteInteger(1);
                    base.WriteBoolean(false);
                }
                base.WriteInteger(0);
                base.WriteBoolean(false);
                base.WriteBoolean(false);
                base.WriteString("");
            }

            base.WriteInteger(1);
            {
                base.WriteInteger(14689);
                base.WriteBoolean(true);
                base.WriteInteger(-1);
                base.WriteBoolean(true);
            }
        }
    }
}
