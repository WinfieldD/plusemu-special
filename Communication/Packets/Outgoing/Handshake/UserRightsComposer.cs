﻿using Plus.HabboHotel.Subscriptions;
using Plus.HabboHotel.Users;

namespace Plus.Communication.Packets.Outgoing.Handshake
{
    public class UserRightsComposer : ServerPacket
    {
        public UserRightsComposer(Habbo habbo)
            : base(ServerPacketHeader.UserRightsMessageComposer)
        {
            if (HabboClubManager.HasSubscription("habbo_vip"))
                base.WriteInteger(2);
            else if (HabboClubManager.HasSubscription("habbo_club"))
                base.WriteInteger(1);
            else
                base.WriteInteger(0);

            base.WriteInteger(habbo.Rank);

            if (habbo.Rank >= 3)
                base.WriteBoolean(true);
            else
                base.WriteBoolean(false);
        }
    }
}