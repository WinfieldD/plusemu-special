﻿using Plus.HabboHotel.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.Handshake
{
    class UpdateFigureUserComposer : ServerPacket
    {
        public UpdateFigureUserComposer(Habbo habbo)
            : base(ServerPacketHeader.UpdateFigureUserMessageComposer)
        {
            base.WriteString(habbo.Look);
            base.WriteString(habbo.Gender);
        }
    }
}
