﻿using System;
using System.Collections.Generic;

using Plus.HabboHotel.Users;
using Plus.HabboHotel.Settings;
using System.Linq;

namespace Plus.Communication.Packets.Outgoing.Handshake
{
    public class UserPerksComposer : ServerPacket
    {
        public UserPerksComposer(Habbo Habbo)
            : base(ServerPacketHeader.UserPerksMessageComposer)
        {
            base.WriteInteger(SettingsHotelManager.GetUserPerksComponent().ToList().Count);
            foreach (UserPerks Perks in SettingsHotelManager.GetUserPerksComponent().ToList())
            {
                base.WriteString(Perks.Content);
                base.WriteString(Perks.Required);
                base.WriteBoolean(Perks.Enable);
            }
        }
    }
}