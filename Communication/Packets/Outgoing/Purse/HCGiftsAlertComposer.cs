﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.Purse
{
    class HCGiftsAlertComposer : ServerPacket
    {
        public HCGiftsAlertComposer(int type) 
          : base(ServerPacketHeader.HCGiftsAlertMessageComposer)
        {
            base.WriteInteger(type);
        }
    }
}
