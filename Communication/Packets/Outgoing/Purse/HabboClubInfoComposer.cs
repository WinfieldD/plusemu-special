﻿using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Subscriptions;
using Plus.HabboHotel.Users;
using Plus.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.Inventory.Purse
{
    class HabboClubCenterInfoComposer : ServerPacket
    {
        public HabboClubCenterInfoComposer(GameClient Session)
            : base(ServerPacketHeader.HabboClubCenterInfoMessageComposer)
        {
            // Optimisation code, and structure. #Amine
            DateTime Origin = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(HabboClubManager.GetSubscription("habbo_vip").ActivateTime);
            DateTime future = DateTime.Now;
            Double Expire = HabboClubManager.GetSubscription("habbo_vip").ExpireTime;
            Double TimeLeft = Expire - PlusEnvironment.GetUnixTimestamp();
            int TotalDaysLeft = (int)Math.Ceiling(TimeLeft / 86400);
            future = DateTime.Now.AddDays(TotalDaysLeft);
            var expired = HabboClubManager.HasSubscription("habbo_vip");

            if (expired == true)
            {
                base.WriteInteger(TotalDaysLeft); // Combien de temps?
                base.WriteString(Origin.Day + "/" + Origin.Month + "/" + Origin.Year + " à " + Origin.Hour + ":" + Origin.Minute + ":" + Origin.Second); // à rejoins le club
            }
            else if (expired == false)
            {
                base.WriteInteger(0); // Combien de temps?
                base.WriteString(";hc.not.enabled");
            }
            base.WriteInteger(0); // ??
            base.WriteInteger(0); // ??
            base.WriteInteger(0); // ??
            base.WriteInteger(0); // ??

            if (expired == true && Origin.Minute == 1)
            {
                int Amount = 60;
                Session.GetHabbo().Credits = Session.GetHabbo().Credits += Amount;
                Session.Send(new CreditBalanceComposer(Session.GetHabbo().Credits));
                Session.Send(new HabboActivityPointNotificationComposer(Session.GetHabbo().Credits, 1, Session.GetHabbo().Credits));
                Session.Send(NotificationManager.Bubble("", ";received.credits.isclear", ""));
            }
            base.WriteInteger(60); // ??
            base.WriteInteger(30); // ??
            base.WriteInteger(30);
            base.WriteInteger(15); // ??
        }
    }
}