﻿using Plus.HabboHotel.Catalog;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Items;
using Plus.HabboHotel.Subscriptions;
using Plus.HabboHotel.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.Purse
{
    class GetClubComposer : ServerPacket
    {
        public GetClubComposer(Habbo habbo, CatalogItem Item, CatalogPage page, int nope)
            : base(ServerPacketHeader.GetClubMessageComposer)
        {
            base.WriteInteger(page.Items.Values.Count);
            base.WriteInteger(Item.Id);
            base.WriteString(Item.Name);
            base.WriteBoolean(false);
            base.WriteInteger(Item.CostCredits);
            base.WriteInteger(Item.CostDiamonds > 0 ? Item.CostDiamonds : Item.CostDiamonds);
            base.WriteInteger(Item.CostDiamonds > 0 ? 105 : 0);
            base.WriteBoolean(true);
            int Days = 0;
            int Months = 0;

            if (Item.GetBaseItem(Item.ItemId).InteractionType == InteractionType.HABBO_CLUB_1_MONTH)
            {

                switch (Item.GetBaseItem(Item.ItemId).InteractionType)
                {
                    case InteractionType.HABBO_CLUB_1_MONTH:
                        Months = 1;
                        break;
                }

                Days = 31 * Months;
            }

            DateTime future = DateTime.Now;

            if (Item.PageID == 699 && HabboClubManager.HasSubscription("habbo_vip"))
            {
                Double Expire = HabboClubManager.GetSubscription("habbo_vip").ExpireTime;
                Double TimeLeft = Expire - PlusEnvironment.GetUnixTimestamp();
                int TotalDaysLeft = (int)Math.Ceiling(TimeLeft / 86400);
                future = DateTime.Now.AddDays(TotalDaysLeft);
            }
            else if (HabboClubManager.HasSubscription("club_vip"))
            {
                Double Expire = HabboClubManager.GetSubscription("club_vip").ExpireTime;
                Double TimeLeft = Expire - PlusEnvironment.GetUnixTimestamp();
                int TotalDaysLeft = (int)Math.Ceiling(TimeLeft / 86400);
                future = DateTime.Now.AddDays(TotalDaysLeft);
            }

            future = future.AddDays(Days);

            base.WriteInteger(Months);
            base.WriteInteger(Days);
            base.WriteBoolean(true);
            base.WriteInteger(Days);
            base.WriteInteger(future.Year);
            base.WriteInteger(future.Month);
            base.WriteInteger(future.Day);
            base.WriteInteger(nope);
        }
    }
}
