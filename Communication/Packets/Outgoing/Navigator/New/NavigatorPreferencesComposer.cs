﻿using Plus.HabboHotel.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.Navigator
{
    class NavigatorPreferencesComposer : ServerPacket
    {
        public NavigatorPreferencesComposer()
            : base(ServerPacketHeader.NavigatorPreferencesMessageComposer)
        {
            foreach (NavigatorSettings Preference in SettingsHotelManager.GetSettingsNavigator())
            {
                if (Preference.Id == SettingsHotelManager.GetSettingsNavigator().Count)
                base.WriteInteger(Preference.Top);
                base.WriteInteger(Preference.Bottom);
                base.WriteInteger(Preference.Width);
                base.WriteInteger(Preference.Height);
                base.WriteBoolean(Preference.Unk);
            }
            base.WriteInteger(0);
        }
    }
}

