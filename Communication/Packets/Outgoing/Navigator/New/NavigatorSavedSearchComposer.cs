﻿using Plus.Database.Interfaces;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Navigator;
using Plus.HabboHotel.Users.Navigator.SavedSearches;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.Navigator.New
{
    class NavigatorSavedSearchComposer : ServerPacket
    {
        public NavigatorSavedSearchComposer(ICollection<SavedSearch> Saved)
            : base(ServerPacketHeader.NavigatorSavedSearchMessageComposer)
        {    /*
                 _SafeStr_8930["popular"] = _SafeStr_6659;
                 _SafeStr_8930["official"] = _SafeStr_6856;
                 _SafeStr_8930["me"] = _SafeStr_6857;
                 _SafeStr_8930["events"] = _SafeStr_6858;
                 _SafeStr_8930["search"] = _SafeStr_6709;
                 _SafeStr_8930["categories"] = _SafeStr_8929;
             */
            base.WriteInteger(Saved.Count);
            foreach (SavedSearch Save in Saved)
            {
                base.WriteInteger(Save.Id);
                base.WriteString(Save.Filter);
                base.WriteString(Save.Search);
                base.WriteString("");
            }
        }
    }
}
