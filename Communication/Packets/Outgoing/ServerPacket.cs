﻿using System;
using System.Collections.Generic;
using System.Text;
using Plus.Communication.Interfaces;
using Plus.HabboHotel.GameClients;

namespace Plus.Communication.Packets.Outgoing
{
    public class ServerPacket : IServerPacket
    {
        private readonly Encoding Encoding = Encoding.Default;

        private List<byte> Body = new List<byte>();
        private static Dictionary<int, IServerPacket> _outgoingPackets;
        private static Dictionary<int, string> _packetNames;

        public ServerPacket(int id)
        {
            _outgoingPackets = new Dictionary<int, IServerPacket>();
            _packetNames = new Dictionary<int, string>();

            Id = id;
            WriteShort(id);

            if (System.Diagnostics.Debugger.IsAttached)
            {
                if (_packetNames.ContainsKey(id))
                    Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write("OUTGOING ");
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.Write("MANIPULÉ ");
                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.Write(Environment.NewLine + ToString());
                if (id.ToString().Length > 0)
                    Console.WriteLine();
                if (id > 0)
                    Console.WriteLine();
            }
        }

        public int Id { get; private set; }

        public byte[] GetBytes()
        {
            var Final = new List<byte>();
            Final.AddRange(BitConverter.GetBytes(Body.Count)); // packet len
            Final.Reverse();
            Final.AddRange(Body); // Add Packet
            return Final.ToArray();
        }

        public void WriteByte(byte b)
        {
            Body.Add(b);
        }

        public void WriteByte(int b)
        {
            Body.Add((byte)b);
        }

        public void WriteBytes(byte[] b, bool IsInt) // d
        {
            if (IsInt)
            {
                for (int i = (b.Length - 1); i > -1; i--)
                {
                    Body.Add(b[i]);
                }
            }
            else
            {
                Body.AddRange(b);
            }
        }

        public void WriteDouble(double d) // d
        {
            string Raw = Math.Round(d, 1).ToString();

            if (Raw.Length == 1)
            {
                Raw += ".0";
            }

            WriteString(Raw.Replace(',', '.'));
        }

        public void WriteString(string s) // d
        {
            if (s.StartsWith(";"))
            {
                string[] strArray1 = s.Split('\t');
                string key = strArray1[0].Substring(1);
                string[] strArray2 = new string[0];
                if (strArray1.Length > 1)
                    strArray2 = strArray1[1].Split(',');
                s = LanguageLocale.Value(key, strArray2);
            }

            WriteShort(s.Length);
            WriteBytes(Encoding.GetBytes(s), false);
        }

        public void WriteShort(int s) // d
        {
            var i = (Int16) s;
            WriteBytes(BitConverter.GetBytes(i), true);
        }

        public void WriteInteger(int i) // d
        {
            WriteBytes(BitConverter.GetBytes(i), true);
        }

        public void WriteBoolean(bool b) // d
        {
            WriteBytes(new[] {(byte) (b ? 1 : 0)}, false);
        }

        public void WritePacketMessage(IServerPacket Message, GameClient s)
        {
            byte[] bytes = Message.GetBytes();

            if (Message == null)
                return;

            if (s.GetConnection() == null)
                return;

            s.GetConnection().SendData(bytes);
        }
    }
}