﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.LandingView
{
    class HotelViewExpiringCatalogPageCommposer : ServerPacket
    {
        public HotelViewExpiringCatalogPageCommposer(string unknown, int nope, string image)
            : base(ServerPacketHeader.HotelViewExpiringCatalogPageMessageCommposer)
        {
            base.WriteString(unknown);
            base.WriteInteger(nope);
            base.WriteString(image);
        }
    }
}
