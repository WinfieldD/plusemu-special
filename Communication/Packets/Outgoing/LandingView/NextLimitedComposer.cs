﻿using Plus.Database.Interfaces;
using Plus.HabboHotel.GameClients;
using System;
using System.Data;

namespace Plus.Communication.Packets.Outgoing.LandingView
{
    class NextLimitedComposer : ServerPacket
    {
        public NextLimitedComposer(GameClient Session)
            : base(ServerPacketHeader.NextLimitedMessageComposer)
        {
            base.WriteInteger(500); // Time Seconds
            base.WriteInteger(0); // Page ID
            base.WriteInteger(0); // Sprite ID
            base.WriteString("");
        }
    }
}
