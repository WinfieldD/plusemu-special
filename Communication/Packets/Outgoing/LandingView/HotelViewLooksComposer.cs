﻿using Plus.Core;
using Plus.Database.Interfaces;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.LandingView;
using Plus.HabboHotel.Users;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Plus.Communication.Packets.Outgoing.LandingView
{
    class HotelViewLooksComposer : ServerPacket
    {
        public HotelViewLooksComposer()
            : base(ServerPacketHeader.HotelViewLooksMessageComposer)
        {
            base.WriteString(ServerConfiguration.ValueStr("hotelviewlooks.name"));

            base.WriteInteger(HotelViewLooksManager.GetHotelLookUser().Count);
            foreach (HotelViewLooks LooksView in HotelViewLooksManager.GetHotelLookUser().ToList())
            {
                base.WriteInteger(LooksView.Id);
                base.WriteString(LooksView.Username);
                base.WriteString(LooksView.Look);
                base.WriteInteger(2);
                base.WriteInteger(LooksView.Place);
            }
        }
    }
}