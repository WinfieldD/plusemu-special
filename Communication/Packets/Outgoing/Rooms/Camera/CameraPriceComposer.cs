﻿using System;

namespace Plus.Communication.Packets.Outgoing.Rooms.Camera
{
    public class CameraPriceComposer : ServerPacket
    {
        public CameraPriceComposer(int purchasePriceCoins, int purchasePriceDuckets, int publishPriceDuckets)
            : base(ServerPacketHeader.CameraPriceMessageComposer)
        {
            base.WriteInteger(purchasePriceCoins);
            base.WriteInteger(purchasePriceDuckets);
            base.WriteInteger(publishPriceDuckets);
        }
    }
}