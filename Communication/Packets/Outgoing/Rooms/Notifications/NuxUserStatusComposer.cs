﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.Rooms.Notifications
{
    class NuxUserStatusComposer : ServerPacket
    {
        public NuxUserStatusComposer(int Type)
            : base(ServerPacketHeader.NuxUserStatusMessageComposer)
        {
            base.WriteInteger(Type);
        }
    }
}
