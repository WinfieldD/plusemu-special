﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.Rooms.Notifications
{
    class SpecialNotificationComposer : ServerPacket
    {
        public SpecialNotificationComposer(string externalLink)
            : base(ServerPacketHeader.SpecialNotificationMessageComposer)
        {
            base.WriteString(externalLink);
        }
    }

    class NuxNotificationComposer : ServerPacket
    {
        public NuxNotificationComposer(List<string> nux)
            : base(ServerPacketHeader.SpecialNotificationMessageComposer)
        {
            base.WriteString(nux.ToString());
        }
    }
}
