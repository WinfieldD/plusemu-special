﻿using Plus.Communication.Packets.Outgoing.Rooms.Chat;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.Rooms.Notifications
{
    class NotificationManager
    {
        public static ServerPacket Bubble(String Image, String Message, String LinkUrl)
        {
            if (Message.StartsWith(";"))
            {
                string[] strArray1 = Message.Split('\t');
                string key = strArray1[0].Substring(1);
                string[] strArray2 = new string[0];
                if (strArray1.Length > 1)
                    strArray2 = strArray1[1].Split(',');
                Message = LanguageLocale.Value(key, strArray2);
            }

            var bubbleNotification = new ServerPacket(ServerPacketHeader.RoomNotificationMessageComposer);
            bubbleNotification.WriteString(Image = "advice");

            bubbleNotification.WriteInteger(string.IsNullOrEmpty(LinkUrl) ? 2 : 3);
            bubbleNotification.WriteString("display");
            bubbleNotification.WriteString("BUBBLE");
            bubbleNotification.WriteString("message");
            bubbleNotification.WriteString(Message);

            if (string.IsNullOrEmpty(LinkUrl))
                return bubbleNotification;

            bubbleNotification.WriteString("linkUrl");
            bubbleNotification.WriteString(LinkUrl);
            return bubbleNotification;
        }

        public static ServerPacket GetType(String Type)
        {
            if (Type.StartsWith(";"))
            {
                string[] strArray1 = Type.Split('\t');
                string key = strArray1[0].Substring(1);
                string[] strArray2 = new string[0];
                if (strArray1.Length > 1)
                    strArray2 = strArray1[1].Split(',');
                Type = LanguageLocale.Value(key, strArray2);
            }
            var getType = new ServerPacket(ServerPacketHeader.RoomNotificationMessageComposer);
            getType.WriteString(Type);
            getType.WriteInteger(0);//Count
            return getType;
        }

        public static ServerPacket Custom(String Message)
        {
            if (Message.StartsWith(";"))
            {
                string[] strArray1 = Message.Split('\t');
                string key = strArray1[0].Substring(1);
                string[] strArray2 = new string[0];
                if (strArray1.Length > 1)
                    strArray2 = strArray1[1].Split(',');
                Message = LanguageLocale.Value(key, strArray2);
            }
            var Super = new ServerPacket(ServerPacketHeader.ModeratorSupportTicketResponseMessageComposer);
            Super.WriteInteger(1);
            Super.WriteString(Message);
            return Super;
        }
        public static ServerPacket Basic(String Message, String URL)
        {
            if (Message.StartsWith(";"))
            {
                string[] strArray1 = Message.Split('\t');
                string key = strArray1[0].Substring(1);
                string[] strArray2 = new string[0];
                if (strArray1.Length > 1)
                    strArray2 = strArray1[1].Split(',');
                Message = LanguageLocale.Value(key, strArray2);
            }
            if (Message.StartsWith("="))
            {
                string[] strArray1 = Message.Split('\t');
                string key = strArray1[0].Substring(1);
                string[] strArray2 = new string[0];
                if (strArray1.Length > 1)
                    strArray2 = strArray1[1].Split(',');
                Message = LocaleCommands.Get(key, strArray2);
            }

            var Basic = new ServerPacket(ServerPacketHeader.BroadcastMessageAlertMessageComposer);
            Basic.WriteString(Message);
            Basic.WriteString(URL);
            return Basic;
        }
        public static ServerPacket Whisper(GameClient c, String Message, Int32 Colour = 0)
        {
            if (Message.StartsWith("="))
            {
                string[] strArray1 = Message.Split('\t');
                string key = strArray1[0].Substring(1);
                string[] strArray2 = new string[0];
                if (strArray1.Length > 1)
                    strArray2 = strArray1[1].Split(',');
                Message = LocaleCommands.Get(key, strArray2);
            }

            if (c == null || c.GetHabbo() == null || c.GetHabbo().CurrentRoom == null)
                return null;

            RoomUser User = c.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(c.GetHabbo().Username);
            if (User == null)
                return null;

            var GetWhisper = new ServerPacket(ServerPacketHeader.WhisperMessageComposer);
            GetWhisper.WriteInteger(User.VirtualId);
            GetWhisper.WriteString(Message);
            GetWhisper.WriteInteger(0);
            GetWhisper.WriteInteger((Colour == 0 ? User.LastBubble : Colour));

            GetWhisper.WriteInteger(0);
            GetWhisper.WriteInteger(-1);
            return GetWhisper;
        }
        public static ServerPacket GetKey(Int32 Id, String Type, String Key, String Value)
        {
            var getKey = new ServerPacket(ServerPacketHeader.RoomNotificationMessageComposer);
            getKey.WriteString(Type);
            getKey.WriteInteger(Id); // 1 default
            {
                getKey.WriteString(Key);
                getKey.WriteString(Value);
            }
            return getKey;
        }

        public static ServerPacket GetKeyByType(String Type, Dictionary<String, String> Keys)
        {
            var getKeyByType = new ServerPacket(ServerPacketHeader.RoomNotificationMessageComposer);
            getKeyByType.WriteString(Type);
            getKeyByType.WriteInteger(Keys.Count);
            foreach (var i in Keys)
            {
                getKeyByType.WriteString(i.Key);
                getKeyByType.WriteString(i.Value);
            }
            return getKeyByType;
        }
        public static ServerPacket NotificationCustom(String Title, String Message, String Image, String HotelName = "", String HotelURL = "")
        {
            if (Message.StartsWith(";"))
            {
                string[] strArray1 = Message.Split('\t');
                string key = strArray1[0].Substring(1);
                string[] strArray2 = new string[0];
                if (strArray1.Length > 1)
                    strArray2 = strArray1[1].Split(',');
                Message = LanguageLocale.Value(key, strArray2);
            }
            var Notification = new ServerPacket(ServerPacketHeader.RoomNotificationMessageComposer);
            Notification.WriteString(Image);
            Notification.WriteInteger(string.IsNullOrEmpty(HotelName) ? 2 : 4);
            Notification.WriteString("title");
            Notification.WriteString(Title);
            Notification.WriteString("message");
            Notification.WriteString(Message);

            if (!string.IsNullOrEmpty(HotelName))
            {
                Notification.WriteString("linkUrl");
                Notification.WriteString(HotelURL);
                Notification.WriteString("linkTitle");
                Notification.WriteString(HotelName);
            }
            return Notification;
        }
    }
}
