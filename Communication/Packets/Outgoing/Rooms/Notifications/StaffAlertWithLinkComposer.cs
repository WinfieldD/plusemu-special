﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.Rooms.Notifications
{
    class AlertWithLinkComposer : ServerPacket
    {
        public AlertWithLinkComposer(string Message, string URL)
            : base(ServerPacketHeader.AlertWithLinkMessageComposer)
        {
            base.WriteString(Message);
            base.WriteString(URL);
        }
    }
}
