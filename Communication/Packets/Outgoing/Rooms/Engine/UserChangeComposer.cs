﻿using System;
using System.Linq;
using System.Text;

using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.GameClients;

namespace Plus.Communication.Packets.Outgoing.Rooms.Engine
{
    class UserChangeComposer : ServerPacket
    {
        public UserChangeComposer(RoomUser User, bool Self)
            : base(ServerPacketHeader.UserChangeMessageComposer)
        {
            base.WriteInteger((Self) ? -1 : User.VirtualId);
            base.WriteString(User.GetClient().GetHabbo().Look);
            base.WriteString(User.GetClient().GetHabbo().Gender);
            base.WriteString(User.GetClient().GetHabbo().Motto);
            base.WriteInteger(User.GetClient().GetHabbo().GetStats().AchievementPoints);
        }
    }

    class BotChangeComposer : ServerPacket
    {
        public BotChangeComposer(int type, string unks, string unk2, string unk3, int unk)
            : base(ServerPacketHeader.UserChangeMessageComposer)
        {
            base.WriteInteger(type);
            base.WriteString(unks);
            base.WriteString(unk2);
            base.WriteString(unk3);
            base.WriteInteger(unk);
        }
    }
}