﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.Help
{
    class SanctionStatusComposer : ServerPacket
    {
        public SanctionStatusComposer(String mode, String Reason)
            : base(ServerPacketHeader.SanctionStatusMessageComposer)
        {
            /*
            var _local_4:String = ("help.sanction." + k);
            switch (_arg_2)
            {
                case "ALERT":
                    _local_4 = (_local_4 + ".alert");
                    break;
                case "MUTE":
                    _local_4 = (_local_4 + ".mute");
                    break;
                case "BAN_PERMANENT":
                    _local_4 = (_local_4 + ".permban");
                    break;
                default:
                    _local_4 = (_local_4 + ".ban");
                    if (_arg_3 > 24)
                    {
                        _local_4 = (_local_4 + ".days");
                        return (this._SafeStr_6943.localization.registerParameter(_local_4, "days", (_arg_3 / 24).toString()));
                    };
            };
            return (this._SafeStr_6943.localization.registerParameter(_local_4, "hours", _arg_3.toString()));
            */
            

            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(PlusEnvironment.GetUnixTimestamp());
            base.WriteBoolean(true);
            base.WriteBoolean(false);
            base.WriteString(mode);
            base.WriteInteger(0);
            base.WriteInteger(30);
            base.WriteString(Reason);
            base.WriteString(origin.Day + "/" + origin.Month + "/" + origin.Year);
            base.WriteInteger(720);
            base.WriteString(mode);
            base.WriteInteger(0);
            base.WriteInteger(30);
            base.WriteString("");
            base.WriteBoolean(false);
        }
    }
}