﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.AuthentificationSMS
{
    class SMSErrorComposer : ServerPacket
    {
        public SMSErrorComposer(int type, int type2)
            :base(ServerPacketHeader.SMSErrorMessageComposer)
        {
            base.WriteInteger(type);
            base.WriteInteger(type2);
        }
    }
}
