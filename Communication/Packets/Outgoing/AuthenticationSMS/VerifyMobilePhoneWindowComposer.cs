﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.AuthentificationSMS
{
    class VerifyMobilePhoneWindowComposer : ServerPacket
    {
        public VerifyMobilePhoneWindowComposer(int type, int type2)
            : base(ServerPacketHeader.VerifyMobilePhoneWindowMessageComposer)
        {
            base.WriteInteger(type);
            base.WriteInteger(type2);
        }
    }
}
