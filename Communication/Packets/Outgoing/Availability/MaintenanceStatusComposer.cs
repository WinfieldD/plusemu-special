﻿using System;

namespace Plus.Communication.Packets.Outgoing.Availability
{
    class MaintenanceStatusComposer : ServerPacket
    {
        public MaintenanceStatusComposer(double Minutes, double Duration)
            : base(ServerPacketHeader.MaintenanceStatusMessageComposer)
        {
            base.WriteInteger(Convert.ToInt32(Minutes));
            base.WriteInteger(Convert.ToInt32(Duration));
            base.WriteBoolean(true);
        }
    }
}