﻿using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Subscriptions;
using Plus.HabboHotel.Users;
using Plus.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.Users
{
    class ScrSendUserInfoComposer : ServerPacket
    {
        public ScrSendUserInfoComposer(GameClient habbo)
            : base(ServerPacketHeader.ScrSendUserInfoMessageComposer)
        {
            base.WriteString("club_habbo");

            var habboGetExpired = HabboClubManager.HasSubscription("habbo_vip");
            switch(habboGetExpired)
            {
                case true:
                
                    Double Expire = HabboClubManager.GetSubscription("habbo_vip").ExpireTime;
                    Double TimeLeft = Expire - PlusEnvironment.GetUnixTimestamp();
                    int TotalDaysLeft = (int)Math.Ceiling(TimeLeft / 86400);
                    int MonthsLeft = TotalDaysLeft / 31;

                    if (MonthsLeft >= 1)
                    {
                        MonthsLeft--;
                    }
                    if (HabboClubManager.GetSubscription("habbo_vip") != null)
                    {
                        TimeSpan TimeSpan = UnixTimestamp.FromUnixTimestamp(HabboClubManager.GetSubscription("habbo_vip").ExpireTime) - DateTime.Now;
                        if (TimeSpan.TotalSeconds > 0)
                        {
                            TotalDaysLeft = (int)Math.Floor((double)(TimeSpan.Days / 31.0));
                            TotalDaysLeft = TimeSpan.Days - (31 * MonthsLeft);
                        }
                    }
                    base.WriteInteger(TotalDaysLeft - (MonthsLeft * 31));
                    base.WriteInteger(2); // ??
                    base.WriteInteger(MonthsLeft);
                    base.WriteInteger(1); // type
                    base.WriteBoolean(true);
                    base.WriteBoolean(true);
                    base.WriteInteger(0);
                    base.WriteInteger(TotalDaysLeft); // days i have on hc
                    base.WriteInteger(TotalDaysLeft); // days i have on vip
                    break;

                case false:
                    Double Expires = HabboClubManager.GetSubscription("habbo_vip").ExpireTime;
                    Double TimeLefts = Expires - PlusEnvironment.GetUnixTimestamp();
                    int TotalDaysLefts = (int)Math.Ceiling(TimeLefts / 86400);
                    int MonthsLefts = TotalDaysLefts / 31;
                    if (HabboClubManager.GetSubscription("habbo_vip") != null)
                    {
                        TimeSpan TimeSpan = UnixTimestamp.FromUnixTimestamp(HabboClubManager.GetSubscription("habbo_vip").ExpireTime) - DateTime.Now;
                        if (TimeSpan.TotalSeconds > 0)
                        {
                            TotalDaysLeft = (int)Math.Floor((double)(TimeSpan.Days / 31.0));
                            TotalDaysLeft = TimeSpan.Days - (31 * MonthsLefts);
                        }
                    }

                    if (MonthsLefts >= 1)
                    {
                        MonthsLefts--;
                    }
                    base.WriteInteger(3);
                    base.WriteInteger(0);
                    base.WriteInteger(0);
                    base.WriteInteger(3);
                    base.WriteBoolean(true);
                    base.WriteBoolean(true);
                    base.WriteInteger(0);
                    base.WriteInteger(0);
                    base.WriteInteger(TotalDaysLefts);// time when is expire.
                    base.WriteInteger(TotalDaysLefts);
                    break;
            }
        }
    }
}
