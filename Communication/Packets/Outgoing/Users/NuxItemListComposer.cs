﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Settings;

namespace Plus.Communication.Packets.Outgoing.Rooms.Nux
{
    class NuxItemListComposer : ServerPacket
    {
        public NuxItemListComposer()
            : base(ServerPacketHeader.NuxItemListMessageComposer)
        {
            base.WriteInteger(1);
            base.WriteInteger(1);

            base.WriteInteger(SettingsHotelManager.GetListItemNux().Count); // count? 
            base.WriteInteger(SettingsHotelManager.GetListItemNux().Count); // count too?

            foreach (NuxListItem ItemGifts in SettingsHotelManager.GetListItemNux().ToList())
            {
                base.WriteString(ItemGifts.Image);
                base.WriteInteger(1);
                base.WriteString(ItemGifts.Title);
                base.WriteString("");
            }
        }
    }
}