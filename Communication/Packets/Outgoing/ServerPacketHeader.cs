﻿namespace Plus.Communication.Packets.Outgoing
{
    public static class ServerPacketHeader
    {
        public const int AvatarEffectActivatedMessageComposer = 1064; //2903
        public const int MassEventComposer = 669; //3773
        public const int HelperToolMessageComposer = 2; //2463
        public const int NameChangeUpdateMessageComposer = 2013; //3031
        public const int HeightMapMessageComposer = 1973; //1824
        public const int CallForHelpPendingCallsMessageComposer = 1953; //2605
        public const int ChatMessageComposer = 175; //2310
        public const int UpdateFigureUserMessageComposer = 2804; //2678
        public const int GroupMembersMessageComposer = 12; //2398
        public const int OpenBotActionMessageComposer = 2773; //605
        public const int UserObjectMessageComposer = 195; //1229
        public const int ActionMessageComposer = 3666; //3935
        public const int ManageGroupMessageComposer = 2639; //1619
        public const int FloodControlMessageComposer = 831; //3922
        public const int FlatControllerAddedMessageComposer = 2553; //1745
        public const int VoucherRedeemOkMessageComposer = 2424; //2582
        public const int TradingClosedMessageComposer = 1645; //1022
        public const int MarketplaceConfigurationMessageComposer = 2659; //2800
        public const int FlatCreatedMessageComposer = 3146; //1708
        public const int ServerErrorMessageComposer = 1265; //2882
        public const int ScrSendUserInfoMessageComposer = 2125; //1101
        public const int RecyclerRewardsMessageComposer = 3533; //1026
        public const int ReloadRecyclerComposer = 2786; //2786
        public const int RecyclerStateComposer = 3570; //249
        public const int CheckPetNameMessageComposer = 3910; //2112
        public const int GuideSessionAttachedMessageComposer = 1450; //2503; //3229
        public const int QuestAbortedMessageComposer = 1084; //351
        public const int RespectPetNotificationMessageComposer = 3931; //1729
        public const int PromotableRoomsMessageComposer = 2717; //2294
        public const int CameraPurchaseOkComposer = 3933; //1658
        public const int CloseConnectionMessageComposer = 1591; //3607
        public const int HabboClubOffersMessageComposer = 907; //863
        public const int CfhTopicsInitMessageComposer = 1291; //2566
        public const int WiredEffectConfigMessageComposer = 368; //3338
        public const int FriendListUpdateMessageComposer = 2177; //103
        public const int ObjectAddMessageComposer = 1453; //2075
        public const int GuideSessionDetachedMessageComposer = 267; //2748
        public const int MarketplaceCanMakeOfferResultMessageComposer = 3459; //1680
        public const int NavigatorCollapsedCategoriesMessageComposer = 1369; //745
        public const int RoomRightsListMessageComposer = 3150; //1226
        public const int SetGroupIdMessageComposer = 3729; //1850
        public const int TradingUpdateMessageComposer = 2461; //429
        public const int QuizDataMessageComposer = 1887; //1426
        public const int CarryObjectMessageComposer = 1543; //2835
        public const int PollContentsMessageComposer = 223; //1253
        public const int NewGroupInfoMessageComposer = 2863; //2344
        public const int RoomForwardMessageComposer = 1117; //1044
        public const int GroupFurniSettingsMessageComposer = 533; //1408
        public const int CreditBalanceMessageComposer = 295;//
        public const int GnomeBoxMessageComposer = 1125;//
        public const int CatalogUpdatedMessageComposer = 326; //847
        public const int JoinQueueMessageComposer = 2856; //1535
        public const int UserTypingMessageComposer = 99; //2942
        public const int ObjectRemoveMessageComposer = 1590; //3219
        public const int RoomEntryInfoMessageComposer = 1729; //2366
        public const int CatalogOfferMessageComposer = 711; //2130
        public const int CatalogIndexMessageComposer = 1854; //1743
        public const int UserGameAchievementsMessageComposer = 2291; //2092
        public const int GroupFurniConfigMessageComposer = 2531; //3625
        public const int HabboUserBadgesMessageComposer = 3432; //2646
        public const int FlatAccessibleMessageComposer = 3714; //3565
        public const int VoucherRedeemErrorMessageComposer = 1820; //1453
        public const int GetRoomFilterListMessageComposer = 2002; //3777
        public const int YouAreNotControllerMessageComposer = 1574; //2958
        public const int ModeratorInitMessageComposer = 3125; //2273
        public const int ModeratorUserClassMessageComposer = 1948; //2522
        public const int FloorPlanSendDoorMessageComposer = 3289; //2120
        public const int SleepMessageComposer = 2103; //3908
        public const int FlatControllerRemovedMessageComposer = 2208; //1694
        public const int UniqueMachineIDMessageComposer = 2126; //671
        public const int ItemAddMessageComposer = 1077; //3118
        public const int GroupForumDataMessageComposer = 3825; //3925
        public const int FigureSetIdsMessageComposer = 549; //3205
        public const int InstantMessageErrorMessageComposer = 2399; //3522
        public const int UpdateFreezeLivesMessageComposer = 3518; //3781
        public const int NavigatorSettingsMessageComposer = 387; //3580
        public const int ItemUpdateMessageComposer = 1335; //2053
        public const int AchievementsMessageComposer = 3597; //222
        public const int PetBreedingMessageComposer = 484; //1831
        public const int LatencyResponseMessageComposer = 4; //3822
        public const int RoomReadyMessageComposer = 708; //259
        public const int HabboActivityPointNotificationMessageComposer = 1718; //3077
        public const int CheckGnomeNameMessageComposer = 1548; //3398
        public const int BuddyListMessageComposer = 3860; //2279
        public const int UserTagsMessageComposer = 1650; //3638
        public const int MarketPlaceOwnOffersMessageComposer = 186; //493
        public const int GroupDeletedMessageComposer = 304; //1381
        public const int YoutubeDisplayPlaylistsMessageComposer = 1656; //2799
        public const int AvatarEffectExpiredMessageComposer = 208; //1208
        public const int TradingCompleteMessageComposer = 3242; //1752
        public const int PetInformationMessageComposer = 2520; //3313
        public const int ModeratorRoomChatlogMessageComposer = 3949; //813
        public const int ClubGiftsMessageComposer = 2619; //2619
        public const int MarketplaceMakeOfferResultMessageComposer = 3276; //537
        public const int MOTDNotificationMessageComposer = 441; //93
        public const int TalentTrackMessageComposer = 3849; //3197
        public const int MessengerErrorMessageComposer = 461; //1145
        public const int TraxSongInfoMessageComposer = 1448; //715
        public const int GroupInfoMessageComposer = 2170; //2433
        public const int SlideObjectBundleMessageComposer = 2024; //3669
        public const int FurniListRemoveMessageComposer = 1261; //166
        public const int FriendNotificationMessageComposer = 3208; //3486
        public const int FurniListNotificationMessageComposer = 2266; //1933
        public const int RoomInfoUpdatedMessageComposer = 1418; //530
        public const int FurniListAddMessageComposer = 1663; //1916
        public const int AvatarEffectMessageComposer = 3282; //3148
        public const int OpenConnectionMessageComposer = 1714; //1784
        public const int FurniListMessageComposer = 3471; //2740
        public const int FigureUpdateMessageComposer = 2804; //2678
        public const int UserFlatCatsMessageComposer = 797; //727
        public const int GuideSessionPartnerIsTypingMessageComposer = 2960; //3621
        public const int ObjectUpdateMessageComposer = 1329; //1164
        public const int HabboSearchResultMessageComposer = 2536; //365
        public const int RespectNotificationMessageComposer = 425; //2168
        public const int PetHorseFigureInformationMessageComposer = 2988; //994
        public const int MessengerInitMessageComposer = 2287; //3302
        public const int PollOfferMessageComposer = 241; //1174

        public const int ThumbnailSuccessMessageComposer = 1983; //3287
        public const int MarketplaceItemStatsMessageComposer = 1902; //53
        public const int ConcurrentUsersGoalProgressMessageComposer = 3594; //1839
        public const int ModeratorUserInfoMessageComposer = 2493; //2429
        public const int YouAreControllerMessageComposer = 3721; //128
        public const int RoomRatingMessageComposer = 1352; //2155
        public const int RefreshFavouriteGroupMessageComposer = 2437; //2531
        public const int AvailabilityStatusMessageComposer = 3445; //1478

        public const int PlayableGamesMessageComposer = 568; //2040
        public const int AchievementUnlockedMessageComposer = 3166; //6974
        public const int PostQuizAnswersMessageComposer = 864; //3566
        public const int GroupMembershipRequestedMessageComposer = 1841; //3178
        public const int FlatAccessDeniedMessageComposer = 1864; //1248
        public const int NavigatorFlatCatsMessageComposer = 3568; //67
        public const int AvatarEffectAddedMessageComposer = 3027; //300
        public const int UsersMessageComposer = 1071; //3905
        public const int SecretKeyMessageComposer = 3313; //2629
        public const int RentableSpacesErrorMessageComposer = 738; //3662
        public const int TradingStartMessageComposer = 1890; //3495
        public const int RoomSettingsDataMessageComposer = 1788; //3489
        public const int NewBuddyRequestMessageComposer = 1769; //3372
        public const int DoorbellMessageComposer = 106; //2655
        public const int OpenGiftMessageComposer = 2362; //639
        public const int CantConnectMessageComposer = 685; //2003
        public const int GuideSessionRequesterRoomMessageComposer = 139; //141
        public const int FloorHeightMapMessageComposer = 1202; //3579
        public const int SubmitBullyReportMessageComposer = 3691; //1791
        public const int PresentDeliverErrorMessageComposer = 2468; //364
        public const int SellablePetBreedsMessageComposer = 3099; //1910
        public const int MarketplaceCancelOfferResultMessageComposer = 3251; //2506
        public const int LoadGameMessageComposer = 1573; //3130
        public const int AchievementScoreMessageComposer = 3233; //1798
        public const int TalentLevelUpMessageComposer = 3253; //152
        public const int BuildersClubMembershipMessageComposer = 1969; //1017
        public const int PetTrainingPanelMessageComposer = 1090; //3990
        public const int QuestCompletedMessageComposer = 3083; //1292
        public const int QuestionParserMessageComposer = 1142; //3677
        public const int UserRightsMessageComposer = 3263; //1827
        public const int PongMessageComposer = 3892; //2664
        public const int UserChangeMessageComposer = 3932; //2147
        public const int ModeratorUserChatlogMessageComposer = 3228; //1238
        public const int CommunityGoalHallOfFameMessageComposer = 1663; //1916
        public const int GiftWrappingConfigurationMessageComposer = 33; //2048
        public const int FloorPlanFloorMapMessageComposer = 2752; //938
        public const int UserNameChangeMessageComposer = 286; //3045
        public const int TalentTrackLevelMessageComposer = 1620; //3772
        public const int TradingConfirmedMessageComposer = 2820; //3048
        public const int GroupCreationWindowMessageComposer = 3874; //3844
        public const int GetGuestRoomResultMessageComposer = 766; //3008
        public const int RoomNotificationMessageComposer = 2722; //2260
        public const int RoomAlertComposer = 3300; //2500
        public const int InitCryptoMessageComposer = 3339; //3860
        public const int SoundSettingsMessageComposer = 1517; //2327
        public const int WiredTriggerConfigMessageComposer = 3590; //3094
        public const int CampaignCalendarGiftMessageComposer = 1956; //496
        public const int ItemsMessageComposer = 2496; //744
        public const int PurchaseOKMessageComposer = 3215; //1391
        public const int BadgeEditorPartsMessageComposer = 1809; //2883
        public const int NewConsoleMessageMessageComposer = 1784; //865
        public const int Game2WeeklyLeaderboardMessageComposer = 3778; //3469
        public const int HideWiredConfigMessageComposer = 2394; //755
        public const int IgnoredUsersMessageComposer = 1364; //944
        public const int FollowFriendFailedMessageComposer = 1866; //2307
        public const int CatalogPageMessageComposer = 1860; //287
        public const int AddExperiencePointsMessageComposer = 1192; //3934
        public const int AvatarEffectsMessageComposer = 331; //2116
        public const int QuestListMessageComposer = 1846; //602
        public const int UnbanUserFromRoomMessageComposer = 3330; //1563
        public const int WiredConditionConfigMessageComposer = 2671; //2856
        public const int GuideSessionInvitedToGuideRoomMessageComposer = 1154; //1414
        public const int StickyNoteMessageComposer = 2821; //2498
        public const int SanctionStatusMessageComposer = 1853; //34
        public const int ObjectsMessageComposer = 1136; //2001
        public const int NewUserExperienceGiftOfferMessageComposer = 1028; //3747
        public const int RoomVisualizationSettingsMessageComposer = 3971; //82
        public const int PromoArticlesMessageComposer = 433; //3633
        public const int MaintenanceStatusMessageComposer = 3658; //2141
        public const int BuddyRequestsMessageComposer = 3620; //433
        public const int CameraStorageUrlMessageComposer = 2605; //3018
        public const int Game3WeeklyLeaderboardMessageComposer = 2778; //3469
        public const int AuthenticationOKMessageComposer = 3002; //3184
        public const int QuestStartedMessageComposer = 865; //3697
        public const int BotInventoryMessageComposer = 2146; //3259
        public const int GameListMessageComposer = 3386; //3544
        public const int SendBullyReportMessageComposer = 2364; //1564
        public const int VideoOffersRewardsMessageComposer = 3841; //2341
        public const int PerkAllowancesMessageComposer = 2814; //1124
        public const int RoomEventMessageComposer = 2802; //932
        public const int MuteAllInRoomMessageComposer = 2631; //1998
        public const int Game1WeeklyLeaderboardMessageComposer = 2778; //3469
        public const int UpdateFavouriteRoomMessageComposer = 3108; //277
        public const int ModeratorSupportTicketResponseMessageComposer = 3300;//2500
        public const int YouTubeDisplayVideoMessageComposer = 1497; //267
        public const int RoomPropertyMessageComposer = 1524; //2577
        public const int QuestionAnswersMessageComposer = 2760; //2714
        public const int ModeratorSupportTicketMessageComposer = 2321; //56
        public const int SetCameraPriceMessageComposer = 590; //2707
        public const int RoomInviteMessageComposer = 3446; //1919
        public const int FurniListUpdateMessageComposer = 1954; // 3530
        public const int BadgesMessageComposer = 1228; //211
        public const int SendGameInvitationMessageComposer = 2533; //3891
        public const int NavigatorSearchResultSetMessageComposer = 2808; //3053
        public const int IgnoreStatusMessageComposer = 1404; //3260
        public const int RentableSpaceMessageComposer = 3948; //2693
        public const int GuideSessionEndedMessageComposer = 1416; //318
        public const int UpdateMagicTileMessageComposer = 2855; //1410
        public const int ShoutMessageComposer = 2956; //1660
        public const int MoodlightConfigMessageComposer = 1239; //1529
        public const int FurnitureAliasesMessageComposer = 3872; //3663
        public const int LoveLockDialogueCloseMessageComposer = 2538; //2257
        public const int GuideSessionErrorMessageComposer = 1910; //2842
        public const int TradingErrorMessageComposer = 2044; //164
        public const int ProfileInformationMessageComposer = 3997; //159
        public const int UnknownCalendarMessageComposer = 1956; //496
        public const int ModeratorRoomInfoMessageComposer = 2016; //1882
        public const int CampaignMessageComposer = 1032; //640
        public const int LoveLockDialogueMessageComposer = 822; //788
        public const int GuideSessionStartedMessageComposer = 20; //3802
        public const int PurchaseErrorMessageComposer = 72; //286
        public const int PopularRoomTagsResultMessageComposer = 3038; //870
        public const int CampaignCalendarDataMessageComposer = 649; //2520
        public const int GiftWrappingErrorMessageComposer = 3396; //2569
        public const int WhisperMessageComposer = 869; //10
        public const int CatalogItemDiscountMessageComposer = 540; //512
        public const int HabboGroupBadgesMessageComposer = 3868; //2519
        public const int CanCreateRoomMessageComposer = 3375; //1532
        public const int HabboClubCenterInfoMessageComposer = 3961; //1995
        public const int TradingFinishMessageComposer = 264; //1259
        public const int GuestRoomSearchResultMessageComposer = 1395; //3655
        public const int DanceMessageComposer = 3952; //3190
        public const int GenericErrorMessageComposer = 291; //2805
        public const int NavigatorPreferencesMessageComposer = 600; //1663
        public const int GuideSessionMsgMessageComposer = 2676; //2742
        public const int MutedMessageComposer = 161; //1092
        public const int BroadcastMessageAlertMessageComposer = 2869; //3243
        public const int YouAreOwnerMessageComposer = 1984; //857
        public const int FindFriendsProcessResultMessageComposer = 2865; //1528
        public const int GroupMemberUpdatedMessageComposer = 192; //589
        public const int ModeratorTicketChatlogMessageComposer = 3086;//1554
        public const int BadgeDefinitionsMessageComposer = 3442; //1109
        public const int UserRemoveMessageComposer = 3951; //3192
        public const int RoomSettingsSavedMessageComposer = 2462; //609
        public const int ModeratorUserRoomVisitsMessageComposer = 2187; //821
        public const int RoomErrorNotifMessageComposer = 2899; //3894
        public const int UpdateUsernameMessageComposer = 526; //107
        public const int NavigatorLiftedRoomsMessageComposer = 3383; //1193
        public const int UnknownGroupMessageComposer = 3742; //2760
        public const int NavigatorMetaDataParserMessageComposer = 3969; //2796
        public const int NavigatorSavedSearchMessageComposer = 2993;
        public const int UpdateFavouriteGroupMessageComposer = 3080; //86
        public const int GetRelationshipsMessageComposer = 1337; //26
        public const int ItemRemoveMessageComposer = 1946; //627
        public const int BCBorrowedItemsMessageComposer = 976; //2188
        public const int GameAccountStatusMessageComposer = 2621; //934
        public const int EnforceCategoryUpdateMessageComposer = 1180; //150
        public const int AchievementProgressedMessageComposer = 686; //334
        public const int ActivityPointsMessageComposer = 3303; //2939
        public const int PetInventoryMessageComposer = 1048; //2521
        public const int GetRoomBannedUsersMessageComposer = 2588; //975
        public const int UserUpdateMessageComposer = 2353; //2412
        public const int FavouritesMessageComposer = 1402; //66
        public const int WardrobeMessageComposer = 3027; //300
        public const int QuizResultsMessageComposer = 864; //3566
        public const int MarketPlaceOffersMessageComposer = 2720; //2223
        public const int LoveLockDialogueSetLockedMessageComposer = 2538; //2257
        public const int TradingAcceptMessageComposer = 2820; //3048
        public const int GroupForumNewThreadMessageComposer = 3966;//2588
        public const int GameAchievementListMessageComposer = 2291; //2092
        public const int GetYouTubePlaylistMessageComposer = 1656; //2799
        public const int SetUniqueIdMessageComposer = 2126; //671
        public const int GroupForumReadThreadMessageComposer = 2810; //2774
        public const int RoomMuteSettingsMessageComposer = 2631; //1998
        public const int GroupForumListingsMessageComposer = 2963; //1567
        public const int GetYouTubeVideoMessageComposer = 1497; //267
        public const int GetClubMessageComposer = 907; //863
        public const int OpenHelpToolMessageComposer = 737; //2605
        public const int GroupForumThreadUpdateMessageComposer = 70; //1209
        public const int SuperNotificationMessageComposer = 2722; //2260
        public const int GroupForumNewResponseMessageComposer = 2337; //2613
        public const int GroupForumThreadRootMessageComposer = 1736; //1088
        public const int UserPerksMessageComposer = 2814; //1124
        public const int TargetOfferMessageComposer = 3829; //2314

        public const int SpecialNotificationMessageComposer = 669; //3773
        public const int NuxUserStatusMessageComposer = 1035; //3324
        public const int LTDCountdownMessageComposer = 3377; //2136
        public const int NextLimitedMessageComposer = 391; //
        public const int NuxItemListMessageComposer = 3851; //165
        public const int NuxSuggestFreeGiftsMessageComposer = 225; //1879

        public const int FurniMaticNoRoomErrorMessageComposer = 2786; //2786
        public const int FurniMaticReceiveItemMessageComposer = 3570; //249
        public const int FurniMaticRewardsMessageComposer = 3533; //1026

        public const int QuickPollMessageComposer = 1142; //3677
        public const int QuickPollResultMessageComposer = 2760; //2714
        public const int QuickPollResultsMessageComposer = 3121; //2721
        public const int SendPollInvinteMessageComposer = 241; //1174
        public const int PollQuestionsMessageComposer = 223; //1253
        public const int PollErrorAlertMessageComposer = 11; //99
        public const int MatchingPollMessageComposer = 1142; //3677
        public const int MatchingPollResultsMessageComposer = 3121; //2721
        public const int MatchingPollFinishMessageComposer = 2760; //2714

        public const int CallForHelperWindowMessageComposer = 2147; //3229
        public const int HandleHelperToolMessageComposer = 2; //2463
        public const int CallForHelperErrorMessageComposer = 1910; //2842
        public const int CloseHelperSessionMessageComposer = 267; //2748
        public const int InitHelperSessionChatMessageComposer = 20; //3802
        public const int EndHelperSessionMessageComposer = 1416; //318
        public const int HelperSessionSendChatMessageComposer = 2676; //2742
        public const int HelperSessionVisiteRoomMessageComposer = 139; //141
        public const int HelperSessionInvinteRoomMessageComposer = 1154; //1414
        public const int HelperSessionChatIsTypingMessageComposer = 2960; //3621

        public const int HotelViewLooksMessageComposer = 1640; //2705
        public const int UpdateHallOfFameCodeDataMessageComposer = 1032; //640

        public const int CameraPhotoPreviewMessageComposer = 2605; //3018
        public const int CamereFinishPurchaseMessageComposer = 3933; //1658
        public const int CameraFinishPublishMessageComposer = 2293; //2292
        public const int CameraFinishParticipateCompetitionMessageComposer = 1891; //3336
        public const int CameraPriceMessageComposer = 590; //2707
        public const int SendRoomThumbnailAlertMessageComposer = 1983; //3287

        public const int CameraPhotoPreviewComposer = 2605; //9845
        public const int CameraPhotoPurchaseOkComposer = 3933; //5483
        public const int CameraPriceComposer = 590; //6952

        public const int ForumDataMessageComposer = 3825; //3925
        public const int ForumsListDataMessageComposer = 2963; //1567
        public const int ThreadsListDataMessageComposer = 1736; //1088
        public const int ThreadDataMessageComposer = 2810; //2774
        public const int ThreadUpdatedMessageComposer = 70; //1209
        public const int ThreadCreatedMessageComposer = 3966; //2588
        public const int ThreadReplyMessageComposer = 2337; //2613
        public const int PostUpdatedMessageComposer = 395; //2009
        public const int UnreadForumThreadPostsMessageComposer = 3698; //406
        public const int UpdateForumReadMarkerMessageComposer = 1077; //1612

        public const int SetJukeboxSongMusicDataMessageComposer = 1448; //715
        public const int SetJukeboxPlayListMessageComposer = 3720; //565
        public const int SetJukeboxNowPlayingMessageComposer = 2100; //2272
        public const int LoadJukeboxUserMusicItemsMessageComposer = 2062; //3402

        public const int AlertNotificationHCMessageComposer = 814; //3833
        public const int WiredSmartAlertMessageComposer = 140; //1452
        public const int HCGiftsAlertMessageComposer = 3381; //3966;

        public const int BonusRareMessageComposer = 391; //1132

        public const int CommunityGoalMessageComposer = 3715; //2606

        public const int CraftableProductsMessageComposer = 322; //1936
        public const int CraftingRecipeMessageComposer = 3032; //985
        public const int CraftingResultMessageComposer = 482; //2354
        public const int CraftingFoundMessageComposer = 2558; //1891

        public const int SMSErrorMessageComposer = 1028; //3747
        public const int VerifyMobilePhoneWindowMessageComposer = 3307; //2685
        public const int AlertWithLinkMessageComposer = 3257;
        public const int HotelViewExpiringCatalogPageMessageCommposer = 1916;
        public const int YouAreSpectatorModeMessageComposer = 2031;
    }
}