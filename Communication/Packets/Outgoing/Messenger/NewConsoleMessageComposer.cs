﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.HabboHotel.Users;
using Plus.HabboHotel.Groups;

namespace Plus.Communication.Packets.Outgoing.Messenger
{
    class NewConsoleMessageComposer : ServerPacket
    {
        public NewConsoleMessageComposer(int Sender, string Message, int Time = 0, int ToId = 0)
            : base(ServerPacketHeader.NewConsoleMessageMessageComposer)
        {
            if (ToId < 0)
            {
                base.WriteInteger(ToId);
            }
            else
            {
                base.WriteInteger(Sender);
            }
            base.WriteString(Message);
            base.WriteInteger(Time);
            if (ToId < 0)
            {
                string name = "LE ";
                string look = "";

                if (Sender != -1)
                {
                    Habbo habbo = PlusEnvironment.GetHabboById(Sender);

                    if (habbo != null)
                    {
                        name = habbo.Username;
                        look = habbo.Look;
                    }
                    else
                    {
                        name = "DECONNECTER";
                    }
                }

                base.WriteString(name + "/" + look + "/" + Sender);
            }
        }
    }
}
