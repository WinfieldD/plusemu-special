﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Users;
using Plus.HabboHotel.Users.Messenger;
using Plus.HabboHotel.Users.Relationships;

namespace Plus.Communication.Packets.Outgoing.Messenger
{
    class MessengerInitComposer : ServerPacket
    {
        public MessengerInitComposer(Habbo player)
            : base(ServerPacketHeader.MessengerInitMessageComposer)
        {
            base.WriteInteger(StaticSettings.MessengerFriendLimit);
            base.WriteInteger(300);
            base.WriteInteger(800);

            if (player.GetPermissions().HasRight("mod_tickets") && player.GetPermissions().HasRight("mod_zozo"))
            {
                base.WriteInteger(1);
                base.WriteInteger(1);
                base.WriteString("Spécial Chat");
            }
            else
            {
                base.WriteInteger(0);
            }

            base.WriteInteger(1);
            base.WriteInteger(1);
            base.WriteString("Public Chat");
        }
    }
}
