﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.HabboHotel.Users;
using Plus.HabboHotel.Users.Messenger;
using Plus.HabboHotel.Users.Relationships;
using Plus.HabboHotel.Groups;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.HabboHotel.GameClients;

namespace Plus.Communication.Packets.Outgoing.Messenger
{
    class BuddyListComposer : ServerPacket
    {
        public BuddyListComposer(ICollection<MessengerBuddy> friends, Habbo player, int pages, int page)
            : base(ServerPacketHeader.BuddyListMessageComposer)
        {
            var friendCount = friends.Count;

            if (player.GetPermissions().HasRight("mod_zozo")) friendCount++;
            if (player.GetPermissions().HasRight("mod_tickets")) friendCount++;
            if (player.GetPermissions().HasRight("mod_zozo")) friendCount++;
            friendCount++;

            base.WriteInteger(pages);
            base.WriteInteger(page);
            base.WriteInteger(friendCount);

            if (player.GetPermissions().HasRight("mod_tickets"))
            {
                base.WriteInteger(0x7fffffff);
                base.WriteString(LanguageLocale.Value("staff.chat.name"));
                base.WriteInteger(1);//Gender.
                base.WriteBoolean(true);
                base.WriteBoolean(false);
                base.WriteString(LanguageLocale.Value("staff.chat.look"));
                base.WriteInteger(1);
                base.WriteString(string.Empty);
                base.WriteString(string.Empty);
                base.WriteString(string.Empty);
                base.WriteBoolean(true);
                base.WriteBoolean(false);
                base.WriteBoolean(false);
                base.WriteShort(0);
            }

            if (player.GetPermissions().HasRight("mod_zozo"))
            {
                base.WriteInteger(0x7ff);
                base.WriteString(LanguageLocale.Value("zozo.chat.name"));
                base.WriteInteger(1);//Gender.
                base.WriteBoolean(true);
                base.WriteBoolean(false);
                base.WriteString(LanguageLocale.Value("zozo.chat.look"));
                base.WriteInteger(1);
                base.WriteString(string.Empty);
                base.WriteString(string.Empty);
                base.WriteString(string.Empty);
                base.WriteBoolean(true);
                base.WriteBoolean(false);
                base.WriteBoolean(false);
                base.WriteShort(0);
            }
            if (player.GetPermissions().HasRight("mod_zozo"))
            {
                base.WriteInteger(0x7fffff);
                base.WriteString(LanguageLocale.Value("vip.chat.name"));
                base.WriteInteger(1);//Gender.
                base.WriteBoolean(true);
                base.WriteBoolean(false);
                base.WriteString(LanguageLocale.Value("vip.chat.look"));
                base.WriteInteger(1);
                base.WriteString(string.Empty);
                base.WriteString(string.Empty);
                base.WriteString(string.Empty);
                base.WriteBoolean(true);
                base.WriteBoolean(false);
                base.WriteBoolean(false);
                base.WriteShort(0);
            }

            base.WriteInteger(0x7f);
            base.WriteString(LanguageLocale.Value("public.chat.name"));
            base.WriteInteger(1);//Gender.
            base.WriteBoolean(true);
            base.WriteBoolean(false);
            base.WriteString(LanguageLocale.Value("public.chat.look"));
            base.WriteInteger(5);
            base.WriteString(string.Empty);
            base.WriteString(string.Empty);
            base.WriteString(string.Empty);
            base.WriteBoolean(true);
            base.WriteBoolean(false);
            base.WriteBoolean(false);
            base.WriteShort(0);
        

            foreach (MessengerBuddy Friend in friends.ToList())
            {
                Relationship Relationship = player.Relationships.FirstOrDefault(x => x.Value.UserId == Convert.ToInt32(Friend.UserId)).Value;

                base.WriteInteger(Friend.Id);
                base.WriteString(Friend.mUsername);
                base.WriteInteger(1);//Gender.
                base.WriteBoolean(Friend.IsOnline);
                base.WriteBoolean(Friend.IsOnline && Friend.InRoom);
                base.WriteString(Friend.IsOnline ? Friend.mLook : string.Empty);
                base.WriteInteger(0);
                base.WriteString(Friend.IsOnline ? Friend.mMotto : string.Empty);
                base.WriteString((Friend.mUsername == "O" ? "Cool" : string.Empty));
                base.WriteString(string.Empty);
                base.WriteBoolean(true);
                base.WriteBoolean(false);
                base.WriteBoolean(false);
                base.WriteShort(Relationship == null ? 0 : Relationship.Type);
            }

        }
    }
}
