﻿using System;
using log4net;
using Plus.HabboHotel;

using Plus.Communication.Packets.Outgoing.Moderation;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Communication.Packets.Outgoing.Messenger;

namespace Plus.Core
{
    public class ConsoleCommandHandler
    {

        public static void InvokeCommand(string inputData)
        {
            if (string.IsNullOrEmpty(inputData))
                return;

            try
            {
                #region Command parsing
                string[] parameters = inputData.Split(' ');

                switch (parameters[0].ToLower())
                {
                    #region stop
                    case "stop":
                    case "shutdown":
                        {
                            Logging.DisablePrimaryWriting(true);
                            Logging.WriteLine("The server is saving users furniture, rooms, etc. WAIT FOR THE SERVER TO CLOSE, DO NOT EXIT THE PROCESS IN TASK MANAGER!!", ConsoleColor.Yellow);
                            PlusEnvironment.PerformShutDown();
                            break;
                        }
                    #endregion
                    #region StartPacket
                    case "packet":
                        {
                            //test? for generating packets.
                            HotelGameManager.Init();
                        }
                        break;
                    #endregion
                    #region alert
                    case "alert":
                        {
                            string Notice = inputData.Substring(6);
                            GameClientManager.Send(NotificationManager.Custom(";console.noticefromadmin\r%message%, " + Notice));
                            foreach (GameClient c in GameClientManager.GetClients)
                            {
                                GameClientManager.Alert(new NewConsoleMessageComposer(0x7fffffff, "message.alert.shutdown\r%user%," + c.GetHabbo().Username, 0), 0x7fffffff);
                            }
                            break;
                        }
                    #endregion

                    default:
                        {
                            TextEmulator.Error(parameters[0].ToLower() + " is an unknown or unsupported command. Type help for more information");
                            break;
                        }
                }
                #endregion
            }
            catch (Exception e)
            {
                TextEmulator.Error("Error in command [" + inputData + "]: " + e);
            }
        }
    }
}