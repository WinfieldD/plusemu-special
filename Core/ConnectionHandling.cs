﻿using System;
using ConnectionManager;
using Plus.Net;
using Plus.HabboHotel;
using Plus.HabboHotel.GameClients;

namespace Plus.Core
{
    public class ConnectionHandling
    {
        private static SocketManager manager;

        public static void Init(int port, int maxConnections, int connectionsPerIP, bool enabeNagles)
        {
            manager = new SocketManager();
            manager.init(port, maxConnections, connectionsPerIP, new InitialPacketParser(), !enabeNagles);
            manager.connectionEvent += manager_connectionEvent;
            manager.initializeConnectionRequests();
        }

        private static void manager_connectionEvent(ConnectionInformation connection)
        {
            connection.connectionChanged += connectionChanged;
            GameClientManager.CreateAndStartClient(Convert.ToInt32(connection.getConnectionID()), connection);
        }

        private static void connectionChanged(ConnectionInformation information, ConnectionState state)
        {
            if (state == ConnectionState.CLOSED)
            {
                CloseConnection(information);
            }
        }

        private static void CloseConnection(ConnectionInformation Connection)
        {
            try
            {
                Connection.Dispose();
                GameClientManager.DisposeConnection(Convert.ToInt32(Connection.getConnectionID()));
            }
            catch (Exception e)
            {
                Logging.LogException(e.ToString());
            }
        }

        public static void Destroy()
        {
            manager.destroy();
        }
    }
}